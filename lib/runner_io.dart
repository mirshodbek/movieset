import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/constants/logger.dart';
import 'package:movie_set/init_app/app.dart';
import 'package:movie_set/provider/db_provider/database.dart';
import 'package:movie_set/provider/provider_shared_prefs.dart';

/// Запуск приложения как io
void run() async {
  runZonedGuarded<void>(
    () async {
      WidgetsFlutterBinding.ensureInitialized();
      // Запустить приложение

      // SystemChrome.setSystemUIOverlayStyle(AppSystem.systemOverlayStyleLight);
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitDown,
        DeviceOrientation.portraitUp,
      ]);
      String platformLocale = Constants.empty;

      try {
        platformLocale = Platform.localeName;
      } catch (error) {
        platformLocale = Constants.empty;
        appLogger.e("👀 Can't get Platform.localeName | $error}");
      }
      final dbProvider = await DbProvider().init();
      final repoSharedPrefs = ProviderSharedPrefs();
      await repoSharedPrefs.init();


      App.run(
        repoSharedPrefs: repoSharedPrefs,
        dbProvider: dbProvider,
        platform: platformLocale,
      );
    },
    (final error, final StackTrace? stackTrace) {
      // final allError = error is! NetworkImageLoadException ? error.toString() : Constants.empty;
      // appLogger.e(
      //   'io_top_level_error: $allError ${stackTrace.toString()}',
      //   // stackTrace ?? StackTrace.empty,
      // );
    },
  );
}
