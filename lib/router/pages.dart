import 'package:flutter/material.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/bookmark/bookmark_screen.dart';
import 'package:movie_set/features/details/person_details/person_details_screen.dart';
import 'package:movie_set/features/home/home_screen.dart';
import 'package:movie_set/features/init/init_screen.dart';
import 'package:movie_set/features/init/not_found_screen.dart';
import 'package:movie_set/features/search/search_screen.dart';
import 'package:movie_set/features/settings/settings_screen.dart';
import 'package:movie_set/router/app_page.dart';
import 'package:movie_set/router/route_path.dart';

import '../features/details/media_details/media_details_screen.dart';
import '../features/filter/filter_screen.dart';
import '../features/filter/result/filter_result_screen.dart';
import '../features/panels/panel_media/panel_media_screen.dart';
import '../features/panels/panel_people/panel_people_screen.dart';
import '../models/base_person.dart';

/// Инициализация приложение
class InitPage extends AppPage<void> {
  InitPage() : super(location: RoutePath.init);

  @override
  Widget build(BuildContext context) => const InitScreen();
}

// Home Panel
class HomePage extends AppPage<void> {
  HomePage() : super(location: RoutePath.home);

  @override
  Widget build(BuildContext context) => const HomeScreen();
}

class PanelMediaPage extends AppPage<void> {
  PanelMediaPage(this.data) : super(location: RoutePath.panelMedia, arguments: data);
  final Map<String, dynamic> data;

  @override
  Widget build(BuildContext context) => PanelMediaScreen(
        items: data[Constants.dataOne],
        primaryTitle: data[Constants.dataTwo],
        type: data[Constants.dataThree],
        totalPages: data[Constants.dataFour],
        id: data[Constants.dataFive],
      );
}

class PanelPeoplePage extends AppPage<void> {
  PanelPeoplePage(this.data)
      : super(
          location: RoutePath.panelPeople,
          arguments: data,
        );

  final Map<String, dynamic> data;

  @override
  Widget build(BuildContext context) => PanelPeopleScreen(
        people: data[Constants.dataOne],
        totalPages: data[Constants.dataTwo],
      );
}

class MediaDetailsPage extends AppPage<void> {
  MediaDetailsPage(this.data)
      : super(
          location: '${RoutePath.mediaDetails}-${data[Constants.dataOne].id}',
          arguments: data,
        );

  final Map<String, dynamic> data;

  @override
  Widget build(BuildContext context) => MediaDetailsScreen(
        media: data[Constants.dataOne],
        type: data[Constants.dataTwo],
      );
}

class PersonDetailsPage extends AppPage<void> {
  PersonDetailsPage(this.person)
      : super(
          location: '${RoutePath.personDetails}-${person.id}',
          arguments: person,
        );

  final BasePerson person;

  @override
  Widget build(BuildContext context) => PersonDetailsScreen(person: person);
}

// Search Panel
class SearchPage extends AppPage<void> {
  SearchPage() : super(location: RoutePath.search);

  @override
  Widget build(BuildContext context) => const SearchScreen();
}

class FilterPage extends AppPage<void> {
  FilterPage() : super(location: RoutePath.filter);

  @override
  Widget build(BuildContext context) => const FilterScreen();
}

class FilterResultPage extends AppPage<void> {
  FilterResultPage(this.data)
      : super(
          location: RoutePath.filterResult,
          arguments: data,
        );
  final Map<String, dynamic> data;

  @override
  Widget build(BuildContext context) => FilterResultScreen(
        filter: data[Constants.dataOne],
      );
}

// Bookmark Panel
class BookmarkPage extends AppPage<void> {
  BookmarkPage() : super(location: RoutePath.bookmark);

  @override
  Widget build(BuildContext context) => const BookmarkScreen();
}

// Settings Panel
class SettingsPage extends AppPage<void> {
  SettingsPage({this.newLocation})
      : super(
          location: '${RoutePath.settings}${newLocation ?? Constants.empty}',
        );
  final String? newLocation;

  @override
  Widget build(BuildContext context) => const SettingsScreen();
}

/// Ошибка экрана
class NotFoundPage extends AppPage<void> {
  NotFoundPage() : super(location: RoutePath.init);

  @override
  Widget build(BuildContext context) => const NotFoundScreen();
}
