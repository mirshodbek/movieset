import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:movie_set/constants/logger.dart';
import 'package:movie_set/router/configuration.dart';
import 'package:movie_set/router/route_information_util.dart';
import 'package:movie_set/router/route_path.dart';

class AppRouteInformationParser = RouteInformationParser<IRouteConfiguration>
    with _RestoreRouteInformationMixin, _ParseRouteInformationMixin;

mixin _RestoreRouteInformationMixin
    on RouteInformationParser<IRouteConfiguration> {
  @override
  RouteInformation? restoreRouteInformation(IRouteConfiguration configuration) {
    try {
      final location = RouteInformationUtil.normalize(configuration.location);
      log('RouteInformationParser.restoreRouteInformation($location)');
      final route = RouteInformation(uri: Uri.parse(location));
      return route;
    } on Object catch (error) {
      appLogger.e('Ошибка навигации restoreRouteInformation: $error');
      return RouteInformation(uri: Uri.parse(RoutePath.init));
    }
  }
}

mixin _ParseRouteInformationMixin
    on RouteInformationParser<IRouteConfiguration> {
  @override
  Future<IRouteConfiguration> parseRouteInformation(
      RouteInformation routeInformation) {
    try {
      if (routeInformation is IRouteConfiguration) {
        return SynchronousFuture<IRouteConfiguration>(routeInformation);
      }
      final location =
          RouteInformationUtil.normalize(routeInformation.uri.path);
      log('RouteInformationParser.parseRouteInformation($location)');
      var state = routeInformation.state;
      if (state is! Map<String, Map<String, Object?>?>?) {
        state = null;
      }
      final configuration = DynamicRouteConfiguration(location,
          state: <String, Object?>{location: state});
      return SynchronousFuture<IRouteConfiguration>(configuration);
    } on Object catch (error) {
      appLogger.e('Ошибка навигации parseRouteInformation: $error');
      return SynchronousFuture<IRouteConfiguration>(
          const NotFoundRouteConfiguration());
    }
  }
}
