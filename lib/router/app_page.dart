import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/route_path.dart';

import '../models/base_person.dart';

/// Базовый класс роута для приложения, с ним работает корневой роутер
@immutable
abstract class AppPage<T extends Object?> extends Page<T> {
  AppPage({
    required this.location,
    Object? arguments,
    String? restorationId,
    this.maintainState = true,
    this.fullscreenDialog = false,
    LocalKey? key,
  })  : assert(
          location.toLowerCase().trim() == location,
          'Предполагается, что адрес страницы всегда в нижнем регистре',
        ),
        super(
          name: location,
          arguments: arguments,
          restorationId: restorationId ?? location,
          key: key ?? ValueKey<String>(location),
        );

  /// Создать роут из сегмента пути
  static AppPage fromPath({
    required final String location,
    final Map<String, Object?>? arguments,
  }) {
    // Предполагаем, что каждый сегмент состоит из имени,
    // описывающий тип роута, а затем, через "-", идут
    // дополнительные, позиционные, параметры, например id
    final segments = location.toLowerCase().split('-');
    final name = segments.firstOrNull?.trim();
    assert(
      name != null && name.isNotEmpty,
      // && name.codeUnits.every((e) => e > 96 && e < 123),
      'Имя должно состоять только из символов латинского алфавита в нижнем регистре: a..z',
    );
    // Тут объявляем все роуты приложения
    switch (name) {
      case RoutePath.empty:
      case RoutePath.hash:
      case RoutePath.init:
        return InitPage();
      case RoutePath.home:
        return HomePage();
      case RoutePath.panelMedia:
        return PanelMediaPage(arguments![location] as Map<String, dynamic>);
      case RoutePath.panelPeople:
        return PanelPeoplePage(arguments![location] as Map<String, dynamic>);
      case RoutePath.mediaDetails:
        return MediaDetailsPage(arguments![location] as Map<String, dynamic>);
      case RoutePath.personDetails:
        return PersonDetailsPage(arguments![location] as BasePerson);
      case RoutePath.search:
        return SearchPage();
      case RoutePath.filter:
        return FilterPage();
      case RoutePath.filterResult:
        return FilterResultPage(arguments![location] as Map<String, dynamic>);
      case RoutePath.bookmark:
        return BookmarkPage();
      case RoutePath.settings:
        return SettingsPage();
      default:
        return NotFoundPage();
    }
  }

  /// Сегмент пути с которым создался роут
  /// Например для экрана настроек это "settings"
  final String location;

  /// {@macro flutter.widgets.ModalRoute.maintainState}
  final bool maintainState;

  /// {@macro flutter.widgets.PageRoute.fullscreenDialog}
  final bool fullscreenDialog;

  @override
  Route<T> createRoute(BuildContext context) => MaterialPageRoute<T>(
        builder: build,
        settings: this,
        maintainState: maintainState,
        fullscreenDialog: fullscreenDialog,
      );

  Widget build(BuildContext context);
}
