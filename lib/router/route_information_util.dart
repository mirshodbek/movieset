import 'package:path/path.dart' as p;
import 'package:movie_set/router/route_path.dart';

abstract class RouteInformationUtil {
  RouteInformationUtil._();

  /// Выбросить из локации повторяющиеся сегменты
  /// Роут всегда должен начинаться с /
  static String normalize(String? sourceLocation) {
    if (sourceLocation == null) return RoutePath.init;
    final segments = <String>[];
    sourceLocation
        .toLowerCase()
        .split('/')
        .map<String>((e) => e.trim())
        .where((e) => e.isNotEmpty && e != RoutePath.init)
        .forEach(
          (e) => segments
            ..remove(e)
            ..add(e),
        );
    return p.normalize(
      p.joinAll(
        <String>[
          RoutePath.init,
          ...segments,
        ],
      ),
    );
  }
}
