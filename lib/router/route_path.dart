class RoutePath {
  const RoutePath();

  static const String empty = '';
  static const String hash = '/';
  static const String init = 'movie&set';
  static const String home = 'home';
  static const String panelMedia = 'panel&media';
  static const String panelPeople = 'panel&people';
  static const String mediaDetails = 'media&details';
  static const String personDetails = 'person&details';
  static const String search = 'search';
  static const String filter = 'filter';
  static const String filterResult = 'result';
  static const String searchBottomSheetGenres = '-search&bottom&sheet&genres';
  static const String searchBottomSheetYears = '-search&bottom&sheet&years';
  static const String bookmark = 'bookmark';
  static const String settings = 'settings';
  static const String settingsBottomSheetLanguages = '-settings&bottom&sheet&languages';
  static const String notFoundScreen = 'not&found&screen';

  /// These are routes of path which is initial routes
  static const String homeScreen = '$init$hash$home';
  static const String searchScreen = '$init$hash$search';
  static const String bookmarkScreen = '$init$hash$bookmark';
  static const String settingsScreen = '$init$hash$settings';
}
