import 'dart:developer';

import 'package:flutter/widgets.dart';

class AppBackButtonDispatcher extends RootBackButtonDispatcher {
  AppBackButtonDispatcher();

  @override
  Future<bool> didPopRoute() => backButton();

  Future<bool> backButton() {
    // Это срабатывает только на мобильнике.
    // Нажатие кнопки "назад" в браузере воспринимается как
    // переход на другую страницу
    log('RootBackButtonDispatcher.didPopRoute()');
    return super.didPopRoute();
  }
}
