// ignore_for_file: prefer_mixin, avoid_types_on_closure_parameters

import 'dart:developer';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:movie_set/constants/logger.dart';
import 'package:movie_set/features/init/not_found_screen.dart';
import 'package:movie_set/router/configuration.dart';
import 'package:movie_set/router/navigator_observer.dart';
import 'package:movie_set/router/pages_builder.dart';
import 'package:movie_set/router/route_path.dart';
import 'package:movie_set/router/router.dart';
import 'package:platform_info/platform_info.dart';

class AppRouterDelegate extends RouterDelegate<IRouteConfiguration> with ChangeNotifier {
  AppRouterDelegate()
      : pageObserver = PageObserver(),
        modalObserver = ModalObserver();

  final PageObserver pageObserver;
  final ModalObserver modalObserver;

  @override
  IRouteConfiguration get currentConfiguration {
    final configuration = _currentConfiguration;
    if (configuration == null) {
      throw UnsupportedError('Изначальная конфигурация не установлена');
    }
    return configuration;
  }

  IRouteConfiguration? _currentConfiguration;

  @override
  Widget build(BuildContext context) {
    final configuration = currentConfiguration;
    return AppRouter(
      routerDelegate: this,
      child: PagesBuilder(
        configuration: configuration,
        builder: (context, pages, child) {
          return Navigator(
            transitionDelegate: const DefaultTransitionDelegate<Object?>(),
            onUnknownRoute: _onUnknownRoute,
            reportsRouteUpdateToEngine: true,
            observers: <NavigatorObserver>[
              pageObserver,
              modalObserver,
            ],
            pages: pages,
            onPopPage: (Route<Object?> route, Object? result) {
              log('Navigator.onPopPage(${route.settings.name}, ${result?.toString() ?? '<null>'})');
              if (!route.didPop(result)) {
                return false;
              }
              setNewRoutePath(configuration.previous ?? const NotFoundRouteConfiguration());
              return true;
            },
          );
        },
      ),
    );
  }

  @override
  Future<bool> popRoute() {
    log('RouterDelegate.popRoute()');
    try {
      switch (currentConfiguration.location) {
        case RoutePath.homeScreen:
        case RoutePath.searchScreen:
        case RoutePath.bookmarkScreen:
        case RoutePath.settingsScreen:
          return SynchronousFuture<bool>(false);
      }
      final navigator = pageObserver.navigator;
      if (navigator == null) return SynchronousFuture<bool>(false);
      return navigator.maybePop().then<bool>(
        (value) {
          if (!value) {
            if (platform.isIO || platform.isAndroid) {
              return false;
              /*
              return SystemNavigator.pop().then<bool>(
                (value) => true,
                onError: (Object error, StackTrace stackTrace) => false,
              );
              */
            }
            // В вебе, вместо перехода на главный экран, можно закрывать текущую активную вкладку
            return setNewRoutePath(
              const HomeRouteConfiguration(),
            ).then<bool>(
              (value) => true,
              onError: (Object error, StackTrace stackTrace) => false,
            );
          }
          return true;
        },
        onError: (Object error, StackTrace stackTrace) => false,
      );
    } on Object catch (err) {
      appLogger.w('RouterDelegate.popRoute: $err');
      return SynchronousFuture(false);
    }
  }

  @override
  Future<void> setNewRoutePath(IRouteConfiguration configuration) {
    log('RouterDelegate.setNewRoutePath(${configuration.location})');
    if (_currentConfiguration == configuration) {
      // Конфигурация не изменилась
      return SynchronousFuture<void>(null);
    }
    _currentConfiguration = configuration;
    notifyListeners();
    return SynchronousFuture<void>(null);
  }

  @override
  Future<void> setRestoredRoutePath(IRouteConfiguration configuration) {
    log('RouterDelegate.setRestoredRoutePath(${configuration.location})');
    return super.setRestoredRoutePath(configuration);
  }

  @override
  Future<void> setInitialRoutePath(IRouteConfiguration configuration) {
    log('RouterDelegate.setInitialRoutePath(${configuration.location})');
    return super.setInitialRoutePath(configuration);
  }

  Route<void> _onUnknownRoute(RouteSettings settings) => MaterialPageRoute<void>(
        settings: settings,
        builder: (context) => const NotFoundScreen(),
      );
}
