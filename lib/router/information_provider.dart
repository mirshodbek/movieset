import 'dart:developer';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:movie_set/router/route_information_util.dart';
// ignore_for_file: prefer_mixin

/// The route information provider that propagates the platform route information changes.
///
/// This provider also reports the new route information from the [Router] widget
/// back to engine using message channel method, the
/// [SystemNavigator.routeInformationUpdated].
///
/// Each time [SystemNavigator.routeInformationUpdated] is called, the
/// [SystemNavigator.selectMultiEntryHistory] method is also called. This
/// overrides the initialization behavior of
/// [Navigator.reportsRouteUpdateToEngine].
class AppRouteInformationProvider extends RouteInformationProvider
    with WidgetsBindingObserver, ChangeNotifier {
  /// Create a platform route information provider.
  ///
  /// Use the [initialRouteInformation] to set the default route information for this
  /// provider.
  AppRouteInformationProvider({
    RouteInformation? initialRouteInformation,
  }) : _value = initialRouteInformation ?? _getDefaultRoute();

  static RouteInformation _getDefaultRoute() {
    final route = RouteInformation(
      uri: Uri.parse(RouteInformationUtil.normalize(PlatformDispatcher.instance.defaultRouteName)),
    );
    log('AppRouteInformationProvider._getDefaultRoute() => ${route.uri}');
    return route;
  }

  @override
  void routerReportsNewRouteInformation(
    RouteInformation routeInformation, {
    RouteInformationReportingType? type,
  }) {
    log(
      'RouteInformationProvider.routerReportsNewRouteInformation(${routeInformation.uri}',
    );
    final replace = type == RouteInformationReportingType.neglect ||
        (type == RouteInformationReportingType.none &&
            _valueInEngine.uri == routeInformation.uri);
    //SystemNavigator.selectMultiEntryHistory();
    SystemNavigator.selectSingleEntryHistory();
    SystemNavigator.routeInformationUpdated(
      uri: routeInformation.uri,
      state: routeInformation.state,
      replace: replace,
    );
    _value = routeInformation;
    _valueInEngine = routeInformation;
  }

  @override
  RouteInformation get value => _value;
  RouteInformation _value;

  RouteInformation _valueInEngine =
      RouteInformation(uri: Uri.parse(WidgetsBinding.instance.window.defaultRouteName));

  void _platformReportsNewRouteInformation(RouteInformation routeInformation) {
    // Если роут не изменился - игнорируем
    if (_value.uri == routeInformation.uri && _value.state == routeInformation.state) {
      return;
    }
    log(
      'RouteInformationProvider._platformReportsNewRouteInformation(${_value.uri} => ${routeInformation.uri})',
    );
    _value = routeInformation;
    _valueInEngine = routeInformation;
    notifyListeners();
  }

  @override
  void addListener(VoidCallback listener) {
    if (!hasListeners) WidgetsBinding.instance.addObserver(this);
    super.addListener(listener);
  }

  @override
  void removeListener(VoidCallback listener) {
    super.removeListener(listener);
    if (!hasListeners) WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void dispose() {
    // In practice, this will rarely be called. We assume that the listeners
    // will be added and removed in a coherent fashion such that when the object
    // is no longer being used, there's no listener, and so it will get garbage
    // collected.
    if (hasListeners) WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Future<bool> didPushRouteInformation(RouteInformation routeInformation) async {
    /// Платформа присылает сообщения которые надо бы обрезать
    log('RouteInformationProvider.didPushRouteInformation(${routeInformation.uri})');

    assert(hasListeners, 'RouteInformationProvider должен обладать подписчиками');

    /// и присылает не тот роут, на который надо перейти, а тот что обрезать

    // Срабатывает например при нажатии кнопок [вперед] и [назад] в браузере
    final newRouteInformation = RouteInformation(
      uri: Uri.parse(RouteInformationUtil.normalize(routeInformation.uri.path)),
      state: routeInformation.state,
    );
    if (newRouteInformation.uri == value.uri) {
      return SynchronousFuture<bool>(true);
    }
    _platformReportsNewRouteInformation(routeInformation);
    return true;
  }

  @override
  Future<bool> didPushRoute(String route) async {
    log('RouteInformationProvider.didPushRoute($route)');
    assert(hasListeners, 'RouteInformationProvider должен обладать подписчиками');
    _platformReportsNewRouteInformation(RouteInformation(uri: Uri.parse(route)));
    return true;
  }

  @override
  Future<bool> didPopRoute() {
    log('RouteInformationProvider.didPopRoute()');
    return super.didPopRoute();
  }
}
