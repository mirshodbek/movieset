import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/features/root/cubits/app_nav_bar_cubit.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar_item.dart';
import 'package:movie_set/features/settings/cubits/settings_cubit.dart';
import 'package:movie_set/init_app/app_material_context.dart';

import '../../features/bookmark/bloc/bookmark_bloc.dart';
import '../../features/root/blocs/exception_bloc/exception_bloc.dart';
import '../../features/root/blocs/internet_connection_bloc/internet_connection_bloc.dart';
import '../../features/settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';
import '../../features/settings/blocs/settings_theme_bloc/settings_theme_bloc.dart';
import '../../models/bookmark/bookmark.dart';

extension ExtensionBuildContext on BuildContext {
  // Size
  double get viewPaddingTop => MediaQuery.of(this).viewPadding.top;

  double get height => MediaQuery.of(this).size.height;

  double get width => MediaQuery.of(this).size.width;

  double get logoFontSize => height * .25;

  // width, it is horizontal size of screen device
  // three, it is count of card in one row
  // sixteen. it is padding of between cards
  double widthGridViewListTile(int countInRow) => (width / countInRow) - 16;

  // Theme
  ThemeData get appTheme => Theme.of(this);

  TextTheme get primaryTextTheme => Theme.of(this).primaryTextTheme;

  TextTheme get textTheme => Theme.of(this).textTheme;

  ColorScheme get colorScheme => Theme.of(this).colorScheme;

  Brightness get brightness => Theme.of(this).brightness;

  // navigation cubit
  AppNavBarItem get appNavBarItem => watch<AppNavBarCubit>().state.item;

  // exception bloc
  void readException(Exception exception) =>
      read<ExceptionBloc>().add(ExceptionEvent.read(exception: exception));

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason>? snackBar(String message) =>
      AppMaterialContext.of(this).scaffoldMessengerKey.currentState?.showSnackBar(
            SnackBar(
              content: Text(message),
              duration: const Duration(seconds: 3),
            ),
          );

  // localization bloc
  SettingsLocalizationBloc get localizationBloc => read<SettingsLocalizationBloc>();

  // app localization
  AppLocale get appLocale => watch<SettingsLocalizationBloc>().state.locale;

  // theme bloc
  ThemeMode get themeMode => watch<SettingsThemeBloc>().state.theme;

  // light theme
  bool get light => themeMode == ThemeMode.light;

  // dark theme
  bool get dark => themeMode == ThemeMode.dark;

  // bookmark bloc
  BookmarkBloc get bookmarkBloc => read<BookmarkBloc>();

  // bookmark list
  List<Bookmark>? get bookmarks => watch<BookmarkBloc>().state.maybeBookmarks();

  // internet connection bloc
  InternetConnectionBloc get internetBloc => read<InternetConnectionBloc>();

  InternetConnectionState get internetBlocState => watch<InternetConnectionBloc>().state;

  SettingsCubit get settingsCubit => read<SettingsCubit>();

  SettingsState get settingsState => watch<SettingsCubit>().state;
}
