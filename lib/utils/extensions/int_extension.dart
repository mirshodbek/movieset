import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

extension ToInt on int? {
  String get genderToString {
    switch (this) {
      case 1:
        return S.current.actress;
      case 2:
        return S.current.actor;
      default:
        return S.current.artist;
    }
  }

  bool get moreData => (this ?? 0) > 1;

  String get runTimeToHourString {
    if (this != null) {
      final runtime = (this! / 60).toStringAsFixed(2);
      final hour = int.parse(runtime[0]) > 0 ? S.current.hour(runtime[0]) : Constants.empty;
      final numberMinute = int.parse(runtime.substring(2, 4));
      final minute = numberMinute > 0 ? S.current.minute(numberMinute) : Constants.empty;
      return '$hour $minute • ';
    }
    return Constants.empty;
  }

  String get seasons {
    if (this != null) {
      return '$this ${S.current.season} • ';
    }
    return Constants.empty;
  }

  String get series {
    if (this != null) {
      return '$this ${S.current.series} • ';
    }
    return Constants.empty;
  }

  String get toLocaleName {
    switch (this) {
      case 1:
        return Constants.english;
      case 0:
      default:
        return Constants.russian;
    }
  }

  String get toCountryLanguage {
    switch (this) {
      case 0:
        return 'az';
      case 1:
        return 'be';
      case 2:
        return 'cs';
      case 3:
        return 'da';
      case 4:
        return 'et';
      case 5:
        return 'fr';
      case 6:
        return 'de';
      case 7:
        return 'hi';
      case 8:
        return 'id';
      case 9:
        return 'it';
      case 10:
        return 'ja';
      case 11:
        return 'kk';
      case 12:
        return 'ky';
      case 13:
        return 'mn';
      case 14:
        return 'no';
      case 15:
        return 'pl';
      case 16:
        return 'ro';
      case 17:
        return 'ru';
      case 18:
        return 'sk';
      case 19:
        return 'ko';
      case 20:
        return 'sv';
      case 21:
        return 'tr';
      case 22:
        return 'uk';
      case 23:
        return 'en';
      case 24:
        return 'uz';
      case 25:
        return 'vi';
      default:
        return 'en';
    }
  }
}
