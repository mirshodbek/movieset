import '../../constants/constants.dart';
import 'package:collection/collection.dart';

extension ToList on List? {
  String get convertListToString {
    String outputs = '';
    for (var item in this ?? []) {
      if (this?.last == item) {
        outputs += '$item';
      } else {
        outputs += '$item, ';
      }
    }
    return outputs;
  }

  void get sortListCredits => this?.sort(
      (a, b) => compareNatural(b.profilePath ?? Constants.empty, a.profilePath ?? Constants.empty));

  List get setInList {
    if (this?.isNotEmpty ?? false) {
      return [for (var item in this!) item.name];
    }
    return [];
  }
}
