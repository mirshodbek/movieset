import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';

extension EventHandler on Bloc {
  ///Allows for cubit to handle an event awaiting for any of the [awaitForStates] to occur and returning the state
  Future handleEvent(
    event, {
    required List awaitForStates,
    bool Function(dynamic state)? validator,
  }) {
    final completer = Completer();
    late StreamSubscription subscription;
    subscription = stream.listen(
      (state) {
        if (awaitForStates.contains(state.runtimeType)) {
          var isValidated = validator?.call(state) ?? true;
          if (awaitForStates.isEmpty || isValidated) {
            subscription.cancel();
            completer.complete(state);
          }
        }
      },
    );
    add(event);
    return completer.future;
  }
}
