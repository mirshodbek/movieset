import 'package:collection/collection.dart';
import 'package:intl/intl.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/utils/extensions/int_extension.dart';

import '../../features/settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';

extension ToString on String? {
  AppLocale? get toAppLocale {
    if (this == null) return null;
    return AppLocale.values.firstWhereOrNull(
      (locale) => locale.name == this,
    );
  }

  String get yearOfDate {
    if (this != null && (this?.isNotEmpty ?? false)) {
      return '(${DateTime.parse(this!).year})';
    }
    return Constants.empty;
  }

  String get formatDate {
    if (this != null && (this?.isNotEmpty ?? false)) {
      return DateFormat('d MMMM yyyy').format(DateTime.parse(this!));
    }
    return Constants.empty;
  }

  String get age {
    if (this != null && (this?.isNotEmpty ?? false)) {
      return '${DateTime.now().year - DateTime.parse(this!).year} ${S.current.ageOld}';
    }
    return Constants.empty;
  }

  String jobType(int? gender) {
    switch (this) {
      case Constants.director:
        return S.current.director;
      case Constants.actor:
      default:
        return gender.genderToString;
    }
  }

  String get crew {
    switch (this) {
      case Constants.director:
        return S.current.director;
      default:
        return S.current.chronicle;
    }
  }

  String get convertTypeToLocal {
    switch (this) {
      case Constants.typeMovies:
        return S.current.movies;
      case Constants.typeTvs:
        return S.current.serials;
      case Constants.typeTvAnimations:
        return S.current.anime;
      case Constants.typeMovieAnimations:
        return S.current.animations;
      default:
        return Constants.empty;
    }
  }

  String get convertLocalToType {
    if (this == S.current.movies) {
      return Constants.typeMovies;
    } else if (this == S.current.serials) {
      return Constants.typeTvs;
    } else if (this == S.current.anime) {
      return Constants.typeTvAnimations;
    } else if (this == S.current.animations) {
      return Constants.typeMovieAnimations;
    }
    return Constants.empty;
  }
}
// https://api.themoviedb.org/3/discover/movie?api_key=###&primary_release_date.gte=1990-01-01&primary_release_date.lte=1990-12-31
