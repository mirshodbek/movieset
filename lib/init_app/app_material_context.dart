import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:movie_set/app_design/app_theme.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/init_app/app_bloc_listeners.dart';
import 'package:movie_set/init_app/init_providers.dart';
import 'package:movie_set/provider/provider_shared_prefs.dart';
import 'package:movie_set/provider/db_provider/database.dart';
import 'package:movie_set/router/back_button_dispatcher.dart';
import 'package:movie_set/router/configuration.dart';
import 'package:movie_set/router/information_parser.dart';
import 'package:movie_set/router/information_provider.dart';
import 'package:movie_set/router/router_delegate.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

class AppMaterialContext extends StatefulWidget {
  const AppMaterialContext({
    final Key? key,
    required this.repoSharedPrefs,
    required this.dbProvider,
    required this.platform,
  }) : super(key: key);

  final ProviderSharedPrefs repoSharedPrefs;
  final String platform;
  final DbProvider dbProvider;

  @override
  State<AppMaterialContext> createState() => AppMaterialContextState();

  static AppMaterialContextState of(BuildContext context) =>
      context.findAncestorStateOfType<AppMaterialContextState>()!;
}

class AppMaterialContextState extends State<AppMaterialContext> {
  final scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  final RouteInformationParser<IRouteConfiguration> _routeInformationParser =
      const AppRouteInformationParser();
  final RouteInformationProvider _routeInformationProvider = AppRouteInformationProvider();
  final RouterDelegate<IRouteConfiguration> _routerDelegate = AppRouterDelegate();
  final BackButtonDispatcher _backButtonDispatcher = AppBackButtonDispatcher();

  @override
  Widget build(BuildContext context) {
    return InitProviders(
      platform: widget.platform,
      dbProvider: widget.dbProvider,
      repoSharedPrefs: widget.repoSharedPrefs,
      child: (context) {
        return AppBlocListeners(
          child: MaterialApp.router(
            supportedLocales: context.localizationBloc.supportedLocales,
            scaffoldMessengerKey: scaffoldMessengerKey,
            debugShowCheckedModeBanner: false,
            theme: AppMainTheme().lightTheme,
            darkTheme: AppMainTheme().darkTheme,
            themeMode: context.themeMode,
            locale: Locale(context.appLocale.name),
            localizationsDelegates: const [
              S.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            onGenerateTitle: (final context) => Constants.onGenerateTitle,
            restorationScopeId: Constants.restorationScopeId,
            routerDelegate: _routerDelegate,
            routeInformationParser: _routeInformationParser,
            routeInformationProvider: _routeInformationProvider,
            backButtonDispatcher: _backButtonDispatcher,
          ),
        );
      },
    );
  }
}
