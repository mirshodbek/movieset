import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/provider/api.dart';
import 'package:movie_set/provider/api/people/provider.dart';
import 'package:movie_set/provider/db_provider/database.dart';
import 'package:movie_set/repo/api/media/repo.dart';
import 'package:movie_set/repo/api/people/repo.dart';
import 'package:movie_set/repo/internet_connection.dart';
import 'package:movie_set/provider/provider_shared_prefs.dart';
import 'package:movie_set/repo/storage/repo_storage.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../features/bookmark/bloc/bookmark_bloc.dart';
import '../features/filter/bloc/filter_bloc.dart';
import '../features/home/blocs/movie_animations_bloc/movie_animations_bloc.dart';
import '../features/home/blocs/movies_bloc/movies_bloc.dart';
import '../features/home/blocs/people_bloc/people_bloc.dart';
import '../features/home/blocs/tv_animations_bloc/tv_animations_bloc.dart';
import '../features/home/blocs/tvs_bloc/tvs_bloc.dart';
import '../features/root/blocs/exception_bloc/exception_bloc.dart';
import '../features/root/blocs/internet_connection_bloc/internet_connection_bloc.dart';
import '../features/root/cubits/app_nav_bar_cubit.dart';
import '../features/search/blocs/search_data_bloc/search_data_bloc.dart';
import '../features/search/blocs/search_text_bloc/search_text_bloc.dart';
import '../features/settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';
import '../features/settings/blocs/settings_theme_bloc/settings_theme_bloc.dart';
import '../features/settings/cubits/settings_cubit.dart';
import '../provider/api/media/provider.dart';

class InitProviders extends StatelessWidget {
  const InitProviders({
    Key? key,
    required this.child,
    required this.dbProvider,
    required this.platform,
    required this.repoSharedPrefs,
  }) : super(key: key);

  final WidgetBuilder child;
  final DbProvider dbProvider;
  final ProviderSharedPrefs repoSharedPrefs;
  final String platform;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<InternetConnectionRepo>(
          create: (context) => InternetConnectionRepo(),
        ),
      ],
      child: Builder(
        builder: (context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider<SettingsLocalizationBloc>(
                create: (context) => SettingsLocalizationBloc(
                  RepoStorage(sharedPrefs: repoSharedPrefs),
                )..add(SettingsLocalizationEvent.init(platform: platform)),
              ),
              BlocProvider<SettingsThemeBloc>(
                create: (context) => SettingsThemeBloc(
                  RepoStorage(sharedPrefs: repoSharedPrefs),
                )..add(const SettingsThemeEvent.init()),
              ),
              BlocProvider<InternetConnectionBloc>(
                create: (context) => InternetConnectionBloc(
                  repo: RepositoryProvider.of<InternetConnectionRepo>(context),
                ),
              ),
            ],
            child: Builder(
              builder: (context) {
                return MultiRepositoryProvider(
                  providers: [
                    RepositoryProvider<MovieSetApi>(
                      create: (context) => MovieSetApi(
                        blocLocale: context.localizationBloc,
                      ),
                    ),
                    RepositoryProvider<ProviderMedia>(
                      create: (context) => ProviderMedia(
                        RepositoryProvider.of<MovieSetApi>(context),
                      ),
                    ),
                    RepositoryProvider<ProviderPeople>(
                      create: (context) => ProviderPeople(
                        RepositoryProvider.of<MovieSetApi>(context),
                      ),
                    ),
                  ],
                  child: Builder(
                    builder: (context) {
                      return MultiRepositoryProvider(
                        providers: [
                          RepositoryProvider<RepoStorage>(
                            create: (context) => RepoStorage(sharedPrefs: repoSharedPrefs),
                          ),
                          RepositoryProvider<RepoMedia>(
                            create: (context) => RepoMedia(
                              provider: RepositoryProvider.of<ProviderMedia>(context),
                              dbProvider: dbProvider,
                            ),
                          ),
                          RepositoryProvider<RepoPeople>(
                            create: (context) => RepoPeople(
                              provider: RepositoryProvider.of<ProviderPeople>(context),
                              dbProvider: dbProvider,
                            ),
                          ),
                          RepositoryProvider<SearchTextBloc>(
                            create: (context) => SearchTextBloc(
                              RepositoryProvider.of<ProviderMedia>(context),
                            ),
                          ),
                        ],
                        child: Builder(
                          builder: (context) {
                            return MultiBlocProvider(
                              providers: [
                                BlocProvider<ExceptionBloc>(create: (context) => ExceptionBloc()),
                                BlocProvider<AppNavBarCubit>(create: (context) => AppNavBarCubit()),
                                BlocProvider<MoviesBloc>(
                                  create: (context) => MoviesBloc(
                                    repo: RepositoryProvider.of<RepoMedia>(context),
                                    localizationBloc: context.localizationBloc,
                                    internetBloc: context.internetBloc,
                                  ),
                                ),
                                BlocProvider<MovieAnimationsBloc>(
                                  create: (context) => MovieAnimationsBloc(
                                    repo: RepositoryProvider.of<RepoMedia>(context),
                                    localizationBloc: context.localizationBloc,
                                    internetBloc: context.internetBloc,
                                  ),
                                ),
                                BlocProvider<TvsBloc>(
                                  create: (context) => TvsBloc(
                                    repo: RepositoryProvider.of<RepoMedia>(context),
                                    localizationBloc: context.localizationBloc,
                                    internetBloc: context.internetBloc,
                                  ),
                                ),
                                BlocProvider<TvAnimationsBloc>(
                                  create: (context) => TvAnimationsBloc(
                                    repo: RepositoryProvider.of<RepoMedia>(context),
                                    localizationBloc: context.localizationBloc,
                                    internetBloc: context.internetBloc,
                                  ),
                                ),
                                BlocProvider<PeopleBloc>(
                                  create: (context) => PeopleBloc(
                                    repoPeople: RepositoryProvider.of<RepoPeople>(context),
                                    internetBloc: context.internetBloc,
                                  ),
                                ),
                                BlocProvider<SearchDataBloc>(
                                  create: (context) => SearchDataBloc(
                                    provider: RepositoryProvider.of<ProviderMedia>(context),
                                    localizationBloc: context.localizationBloc,
                                    internetBloc: context.internetBloc,
                                  )..add(const SearchDataEvent.init()),
                                ),
                                BlocProvider<FilterBloc>(
                                  create: (context) => FilterBloc(
                                    dbProvider,
                                  )..add(const FilterEvent.init()),
                                ),
                                BlocProvider<BookmarkBloc>(
                                  create: (context) => BookmarkBloc(
                                    repo: RepositoryProvider.of<RepoMedia>(context),
                                    dbProvider: dbProvider,
                                    localizationBloc: context.localizationBloc,
                                  ),
                                ),
                                BlocProvider<SettingsCubit>(
                                  create: (context) => SettingsCubit(
                                    repo: RepositoryProvider.of<RepoStorage>(context),
                                  ),
                                ),
                              ],
                              child: Builder(
                                builder: (context) {
                                  return child(context);
                                },
                              ),
                            );
                          },
                        ),
                      );
                    },
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
