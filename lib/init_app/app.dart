import 'package:flutter/material.dart';
import 'package:movie_set/init_app/app_material_context.dart';
import 'package:movie_set/provider/provider_shared_prefs.dart';
import 'package:movie_set/provider/db_provider/database.dart';

class App extends StatelessWidget {
  const App({
    Key? key,
    required this.repoSharedPrefs,
    required this.dbProvider,
    required this.platform,
  }) : super(key: key);

  final ProviderSharedPrefs repoSharedPrefs;
  final DbProvider dbProvider;
  final String platform;

  static void run({
    required ProviderSharedPrefs repoSharedPrefs,
    required DbProvider dbProvider,
    required String platform,
  }) =>
      runApp(
        App(
          platform: platform,
          dbProvider: dbProvider,
          repoSharedPrefs: repoSharedPrefs,
        ),
      );

  @override
  Widget build(BuildContext context) => AppMaterialContext(
        platform: platform,
        dbProvider: dbProvider,
        repoSharedPrefs: repoSharedPrefs,
      );
}
