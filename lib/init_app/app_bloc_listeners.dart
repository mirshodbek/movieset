import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../features/root/blocs/exception_bloc/exception_bloc.dart';


class AppBlocListeners extends StatelessWidget {
  const AppBlocListeners({
    Key? key,
    required this.child,
  }) : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return BlocListener<ExceptionBloc, ExceptionState>(
      listener: (_, state) {
        state.when(
          init: () {},
          invalidApiKey: (_, message) {
            context.snackBar(message);
          },
          resourceRequestedNotFound: (_, message) {
            context.snackBar(message);
          },
          noInternetException: (_, message) {
            context.snackBar(message);
          },
          unknownError: (_, message) {
            context.snackBar(message);
          },
        );
      },
      child: child,
    );
  }
}
