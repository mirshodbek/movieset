import 'package:movie_set/runner_stub.dart'
// ignore: uri_does_not_exist
if (dart.library.io) 'package:movie_set/runner_io.dart'
// ignore: uri_does_not_exist
if (dart.library.html) 'package:movie_set/runner_web.dart' as runner;

void main() => runner.run();
