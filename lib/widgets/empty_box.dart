import 'package:flutter/material.dart';

import '../app_design/app_assets.dart';

class EmptyBox extends StatelessWidget {
  const EmptyBox({
    Key? key,
    required this.height,
    required this.width,
  }) : super(key: key);
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Image.asset(
        AppAssets.image.empty,
        width: width,
        height: height,
      ),
    );
  }
}
