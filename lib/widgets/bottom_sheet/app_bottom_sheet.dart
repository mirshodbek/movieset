import 'package:flutter/material.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/widgets/bottom_sheet/bottom_sheet_item.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../router/router.dart';
import '../app_elevated_button.dart';

class AppBottomSheet extends StatefulWidget {
  const AppBottomSheet({
    Key? key,
    this.type,
    this.initial,
    this.loading,
    this.minChildSize,
    this.initialChildSize,
    this.onTap,
    this.clearButton = true,
    required this.items,
    required this.titleButton,
    required this.primaryTitle,
  }) : super(key: key);
  final String? type;
  final Widget? loading;
  final bool clearButton;
  final String titleButton;
  final String primaryTitle;
  final double? minChildSize;
  final double? initialChildSize;
  final List<BottomSheetItem>? initial;
  final List<BottomSheetItem> items;
  final Function(List<BottomSheetItem>?)? onTap;

  @override
  State<AppBottomSheet> createState() => _AppBottomSheetState();
}

class _AppBottomSheetState extends State<AppBottomSheet> {
  late final List<BottomSheetItem> data = List.from(widget.initial ?? []);

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      expand: false,
      initialChildSize: widget.initialChildSize ?? 1,
      minChildSize: widget.minChildSize ?? .5,
      builder: (context, controller) {
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.primaryTitle,
                    style: context.primaryTextTheme.labelMedium,
                  ),
                  if (widget.clearButton)
                    TextButton(
                      onPressed: () => setState(() => data.clear()),
                      child: Text(S.of(context).clear),
                    ),
                ],
              ),
            ),
            const SizedBox(height: 16),
            Expanded(
              child: widget.loading ??
                  SingleChildScrollView(
                    child: Column(
                      children: List.generate(
                        widget.items.length,
                        (index) {
                          final item = widget.items[index];
                          return CheckboxListTile(
                            tileColor: Colors.transparent,
                            title: Padding(
                              padding: const EdgeInsets.all(8),
                              child: Text(
                                item.name ?? Constants.empty,
                                style: context.textTheme.titleSmall,
                              ),
                            ),
                            value: data.any((it) => it.id == item.id),
                            onChanged: (value) {
                              if (value == true) {
                                if (widget.type == Constants.typeOneCheck) data.clear();
                                data.add(item);
                              } else {
                                data.removeWhere((it) => it.id == item.id);
                              }
                              setState(() {});
                            },
                          );
                        },
                      ),
                    ),
                  ),
            ),
            AppElevatedButton(
              title: widget.titleButton,
              onPressed: () {
                widget.onTap?.call(data);
                AppRouter.maybePop(context);
              },
            ),
          ],
        );
      },
    );
  }
}
