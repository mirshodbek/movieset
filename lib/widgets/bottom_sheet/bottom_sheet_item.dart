import 'package:equatable/equatable.dart';

class BottomSheetItem extends Equatable {
  final String? name;
  final int id;

  const BottomSheetItem({
    required this.id,
    this.name,
  });

  @override
  List<Object?> get props => [name, id];
}
