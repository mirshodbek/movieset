import 'package:flutter/material.dart';
import 'package:movie_set/widgets/animations/spin_kit_pouring_hour_glass_refined.dart';
import 'package:pull_to_refresh_notification/pull_to_refresh_notification.dart';

class PullToRefresh extends StatefulWidget {
  const PullToRefresh({
    Key? key,
    this.loadNextPage,
    required this.widget,
    required this.totalPages,
    required this.loadFirstPage,
  }) : super(key: key);
  final Function(int)? loadNextPage;
  final VoidCallback loadFirstPage;
  final List<Widget> widget;
  final int? totalPages;

  @override
  State<PullToRefresh> createState() => _PullToRefreshState();
}

class _PullToRefreshState extends State<PullToRefresh> {
  final ScrollController _scrollController = ScrollController();
  late final int totalPages = (widget.totalPages ?? 0);
  int page = 2;

  bool get loading =>
      (totalPages == 2 && (totalPages + 1 > page)) ||
      (totalPages != 2 && totalPages > page);

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent / 2 <
          _scrollController.offset) {
        if (loading) {
          setState(() {});
          widget.loadNextPage?.call(page++);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return PullToRefreshNotification(
      onRefresh: () async {
        widget.loadFirstPage.call();
        page = 2;
        setState(() {});
        return true;
      },
      maxDragOffset: 50,
      refreshOffset: 50,
      pullBackDuration: const Duration(milliseconds: 1000),
      child: CustomScrollView(
        physics: const BouncingScrollPhysics(),
        controller: _scrollController,
        slivers: [
          PullToRefreshContainer(
            (info) {
              var offset = info?.dragOffset ?? 0.0;

              return SliverAppBar(
                automaticallyImplyLeading: false,
                toolbarHeight: offset,
                title: const SpinKitPouringHourGlassRefined(),
              );
            },
          ),
          ...widget.widget,
          if (widget.loadNextPage != null && loading) ...[
            const SliverToBoxAdapter(
              child: SpinKitPouringHourGlassRefined(),
            ),
            const SliverToBoxAdapter(
              child: SizedBox(height: 16),
            ),
          ]
        ],
      ),
    );
  }
}
