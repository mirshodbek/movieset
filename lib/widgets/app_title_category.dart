import 'package:flutter/material.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

class AppTitleCategory extends StatelessWidget {
  const AppTitleCategory({
    Key? key,
    required this.title,
    this.onPressed,
  }) : super(key: key);
  final VoidCallback? onPressed;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16, bottom: 0, left: 16, right: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.displayLarge,
          ),
          if (onPressed != null)
            TextButton(
              onPressed: onPressed,
              child: Text(S.of(context).all),
            ),
        ],
      ),
    );
  }
}
