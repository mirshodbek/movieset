import 'package:flutter/material.dart';

class MainAppBar extends StatelessWidget implements PreferredSizeWidget {
  const MainAppBar({
    Key? key,
    this.actions,
    this.backButton = true,
    required this.primaryTitle,
  }) : super(key: key);
  final String primaryTitle;
  final bool backButton;
  final List<Widget>? actions;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(primaryTitle),
      actions: actions,
      automaticallyImplyLeading: backButton,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
