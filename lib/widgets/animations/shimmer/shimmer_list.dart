import 'package:flutter/material.dart';
import 'package:movie_set/widgets/app_title_category.dart';
import 'package:movie_set/widgets/animations/shimmer/shimmer_list_tile.dart';

class ShimmerList extends StatelessWidget {
  const ShimmerList({
    Key? key,
    this.shimmerTile,
    this.width = 0,
    this.height = 0,
    required this.title,
  }) : super(key: key);
  final double width;
  final double height;
  final String title;
  final Widget? shimmerTile;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AppTitleCategory(title: title),
        const SizedBox(height: 16),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: List.generate(
              10,
              (index) {
                return shimmerTile ??
                    ShimmerListTile(
                      width: width,
                      height: height,
                    );
              },
            ),
          ),
        ),
      ],
    );
  }
}
