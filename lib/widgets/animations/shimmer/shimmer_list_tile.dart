import 'package:flutter/material.dart';
import 'package:movie_set/widgets/animations/shimmer/shimmer_loading.dart';

class ShimmerListTile extends StatelessWidget {
  const ShimmerListTile({
    Key? key,
    this.smallShimmer = false,
    required this.width,
    required this.height,
  }) : super(key: key);
  final double width;
  final double height;
  final bool smallShimmer;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Shimmer(
            child: ShimmerLoading(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                ),
                height: smallShimmer ? height : height - 50,
                width: width,
              ),
            ),
          ),
          const SizedBox(height: 8),
          Shimmer(
            child: ShimmerLoading(
              child: Container(
                color: Colors.white,
                width: width,
                height: 10,
              ),
            ),
          ),
          const SizedBox(height: 4),
          Shimmer(
            child: ShimmerLoading(
              child: Container(
                color: Colors.white,
                width: width / 2,
                height: 10,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
