import 'package:flutter/material.dart';
import 'package:movie_set/widgets/app_list_tile/app_list_tile_item.dart';
import 'package:movie_set/widgets/app_list_tile/parts/assigned_adult.dart';
import 'package:movie_set/widgets/app_list_tile/parts/image_list_tile.dart';
import 'package:movie_set/widgets/app_list_tile/parts/title_list_tile.dart';

class AppListTile extends StatelessWidget {
  const AppListTile({
    Key? key,
    required this.onTap,
    required this.item,
    required this.width,
    required this.height,
  }) : super(key: key);
  final VoidCallback onTap;
  final AppListTileItem item;
  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(8),
      child: SizedBox(
        width: width,
        height: height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Stack(
              children: [
                ImageListTile(
                  imagePath: item.imagePath ?? '',
                  width: width,
                  height: height - 61,
                ),
                if (item.adult ?? false) const AssignedAdult(),
              ],
            ),
            const SizedBox(height: 4),
            TitleListTile(item: item),
          ],
        ),
      ),
    );
  }
}
