import 'package:equatable/equatable.dart';

class AppListTileItem extends Equatable {
  final double? voteAverage;
  final String? releaseDate;
  final String? imagePath;
  final String? subTitle;
  final String? title;
  final bool? adult;
  final bool smallTextStyleTitle;

  const AppListTileItem({
    this.smallTextStyleTitle = false,
    this.voteAverage,
    this.releaseDate,
    this.imagePath,
    this.subTitle,
    this.title,
    this.adult,
  });

  @override
  List<Object?> get props => [
        smallTextStyleTitle,
        voteAverage,
        releaseDate,
        imagePath,
        subTitle,
        title,
        adult,
      ];
}
