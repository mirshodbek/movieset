import 'package:flutter/material.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../app_design/app_gradients.dart';
import '../../../constants/constants.dart';

class VoteAverage extends StatelessWidget {
  const VoteAverage({
    Key? key,
    required this.voteAverage,
  }) : super(key: key);
  final String? voteAverage;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      blendMode: BlendMode.srcIn,
      shaderCallback: (bounds) => AppGradients.textGradient.createShader(
        Rect.fromLTWH(0, 0, bounds.width, bounds.height),
      ),
      child: Text(
        voteAverage ?? Constants.empty,
        style: context.textTheme.bodyLarge,
      ),
    );
  }
}
