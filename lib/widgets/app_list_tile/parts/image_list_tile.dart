import 'package:flutter/material.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/widgets/animations/shimmer/shimmer_loading.dart';

import 'error_image.dart';

class ImageListTile extends StatelessWidget {
  const ImageListTile({
    Key? key,
    this.borderRadius,
    required this.width,
    required this.height,
    required this.imagePath,
  }) : super(key: key);
  final String imagePath;
  final double width;
  final double height;
  final double? borderRadius;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(borderRadius ?? 8),
      child: imagePath.isEmpty
          ? ErrorImage(
              width: width,
              height: height,
            )
          : Image.network(
              Constants.w500SizeUrlImage + imagePath,
              width: width,
              height: height,
              fit: BoxFit.cover,
              errorBuilder: (context, error, stackTrace) {
                return ErrorImage(
                  width: width,
                  height: height,
                );
              },
              loadingBuilder: (context, child, loadingProgress) {
                if (loadingProgress == null) return child;
                return Shimmer(
                  child: ShimmerLoading(
                    child: Container(
                      color: Colors.white,
                      height: height,
                      width: width,
                    ),
                  ),
                );
              },
            ),
    );
  }
}
