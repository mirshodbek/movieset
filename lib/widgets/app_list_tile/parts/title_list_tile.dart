import 'package:flutter/material.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/widgets/app_list_tile/app_list_tile_item.dart';
import 'package:movie_set/widgets/app_list_tile/parts/vote_average.dart';

class TitleListTile extends StatelessWidget {
  const TitleListTile({
    Key? key,
    required this.item,
  }) : super(key: key);
  final AppListTileItem item;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 57,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Flexible(
                child: Text.rich(
                  TextSpan(
                    text: item.title ?? Constants.empty,
                    children: [
                      TextSpan(
                        text: ' ${item.releaseDate ?? Constants.empty}',
                        style: context.textTheme.bodySmall?.copyWith(
                          color: context.colorScheme.onPrimary,
                        ),
                      ),
                    ],
                  ),
                  maxLines: 2,
                  style: item.smallTextStyleTitle
                      ? context.textTheme.labelMedium
                      : context.textTheme.titleMedium,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 4),
                child: VoteAverage(
                  voteAverage: item.voteAverage?.toStringAsFixed(1),
                ),
              ),
            ],
          ),
          Text(
            item.subTitle ?? Constants.empty,
            style: context.textTheme.labelSmall,
          ),
        ],
      ),
    );
  }
}
