import 'package:flutter/material.dart';
import 'package:movie_set/app_design/app_colors.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

class AssignedAdult extends StatelessWidget {
  const AssignedAdult({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 0,
      top: 0,
      child: Container(
        width: 24,
        height: 16,
        decoration: BoxDecoration(
          color: AppColors.othersBlack.withOpacity(.4),
          borderRadius: const BorderRadius.only(
            topRight: Radius.circular(8),
            bottomLeft: Radius.circular(8),
          ),
        ),
        child: Text(
          Constants.plusSixteen,
          style: context.textTheme.labelLarge,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
