import 'package:flutter/cupertino.dart';
import '../../../app_design/app_colors.dart';

class ErrorImage extends StatelessWidget {
  const ErrorImage({
    Key? key,
    required this.width,
    required this.height,
  }) : super(key: key);
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      alignment: Alignment.center,
      color: AppColors.grayScaleBorderLightGrey,
      child: Icon(
        CupertinoIcons.photo,
        size: (width + height) / 3,
        color: AppColors.grayScaleGrey.withOpacity(.5),
      ),
    );
  }
}
