// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `{value}-е`
  String year(Object value) {
    return Intl.message(
      '$value-е',
      name: 'year',
      desc: '',
      args: [value],
    );
  }

  /// `{value} ч`
  String hour(Object value) {
    return Intl.message(
      '$value ч',
      name: 'hour',
      desc: '',
      args: [value],
    );
  }

  /// `{value} мин`
  String minute(Object value) {
    return Intl.message(
      '$value мин',
      name: 'minute',
      desc: '',
      args: [value],
    );
  }

  /// `Более {value}`
  String over(Object value) {
    return Intl.message(
      'Более $value',
      name: 'over',
      desc: '',
      args: [value],
    );
  }

  /// `В тренде`
  String get trending {
    return Intl.message(
      'В тренде',
      name: 'trending',
      desc: '',
      args: [],
    );
  }

  /// `Все`
  String get all {
    return Intl.message(
      'Все',
      name: 'all',
      desc: '',
      args: [],
    );
  }

  /// `Лучшие сериалы`
  String get bestSeries {
    return Intl.message(
      'Лучшие сериалы',
      name: 'bestSeries',
      desc: '',
      args: [],
    );
  }

  /// `Популярные Аниме`
  String get popularAnime {
    return Intl.message(
      'Популярные Аниме',
      name: 'popularAnime',
      desc: '',
      args: [],
    );
  }

  /// `Мультфильмы-новинки`
  String get newAnimations {
    return Intl.message(
      'Мультфильмы-новинки',
      name: 'newAnimations',
      desc: '',
      args: [],
    );
  }

  /// `Любимые актеры`
  String get favoriteActors {
    return Intl.message(
      'Любимые актеры',
      name: 'favoriteActors',
      desc: '',
      args: [],
    );
  }

  /// `Актриса`
  String get actress {
    return Intl.message(
      'Актриса',
      name: 'actress',
      desc: '',
      args: [],
    );
  }

  /// `Актер`
  String get actor {
    return Intl.message(
      'Актер',
      name: 'actor',
      desc: '',
      args: [],
    );
  }

  /// `Неверный ключ API: вам должен быть предоставлен действительный ключ.`
  String get invalidApiKey {
    return Intl.message(
      'Неверный ключ API: вам должен быть предоставлен действительный ключ.',
      name: 'invalidApiKey',
      desc: '',
      args: [],
    );
  }

  /// `Запрошенный вами ресурс не найден.`
  String get resourceRequestedNotFound {
    return Intl.message(
      'Запрошенный вами ресурс не найден.',
      name: 'resourceRequestedNotFound',
      desc: '',
      args: [],
    );
  }

  /// `Вы, кажется, не в сети. Данные могут быть не актуальными.`
  String get noInternet {
    return Intl.message(
      'Вы, кажется, не в сети. Данные могут быть не актуальными.',
      name: 'noInternet',
      desc: '',
      args: [],
    );
  }

  /// `Что-то сломалось :(`
  String get unknownError {
    return Intl.message(
      'Что-то сломалось :(',
      name: 'unknownError',
      desc: '',
      args: [],
    );
  }

  /// `Персоны`
  String get people {
    return Intl.message(
      'Персоны',
      name: 'people',
      desc: '',
      args: [],
    );
  }

  /// `Подробнее`
  String get more {
    return Intl.message(
      'Подробнее',
      name: 'more',
      desc: '',
      args: [],
    );
  }

  /// `Продюссер`
  String get producer {
    return Intl.message(
      'Продюссер',
      name: 'producer',
      desc: '',
      args: [],
    );
  }

  /// `лет`
  String get ageOld {
    return Intl.message(
      'лет',
      name: 'ageOld',
      desc: '',
      args: [],
    );
  }

  /// `Похожие`
  String get similar {
    return Intl.message(
      'Похожие',
      name: 'similar',
      desc: '',
      args: [],
    );
  }

  /// `Актерский состав`
  String get castMembers {
    return Intl.message(
      'Актерский состав',
      name: 'castMembers',
      desc: '',
      args: [],
    );
  }

  /// `Съемочная группа`
  String get crewMovie {
    return Intl.message(
      'Съемочная группа',
      name: 'crewMovie',
      desc: '',
      args: [],
    );
  }

  /// `Режиссёр`
  String get director {
    return Intl.message(
      'Режиссёр',
      name: 'director',
      desc: '',
      args: [],
    );
  }

  /// `сезон`
  String get season {
    return Intl.message(
      'сезон',
      name: 'season',
      desc: '',
      args: [],
    );
  }

  /// `серий`
  String get series {
    return Intl.message(
      'серий',
      name: 'series',
      desc: '',
      args: [],
    );
  }

  /// `Читать больше`
  String get readMore {
    return Intl.message(
      'Читать больше',
      name: 'readMore',
      desc: '',
      args: [],
    );
  }

  /// `Настройки`
  String get settings {
    return Intl.message(
      'Настройки',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `Загузка только по Wi-Fi`
  String get downloadOnlyViaWifi {
    return Intl.message(
      'Загузка только по Wi-Fi',
      name: 'downloadOnlyViaWifi',
      desc: '',
      args: [],
    );
  }

  /// `Изменить язык`
  String get changeLanguage {
    return Intl.message(
      'Изменить язык',
      name: 'changeLanguage',
      desc: '',
      args: [],
    );
  }

  /// `Тема оформления`
  String get designTheme {
    return Intl.message(
      'Тема оформления',
      name: 'designTheme',
      desc: '',
      args: [],
    );
  }

  /// `Системная`
  String get system {
    return Intl.message(
      'Системная',
      name: 'system',
      desc: '',
      args: [],
    );
  }

  /// `Тёмная`
  String get dark {
    return Intl.message(
      'Тёмная',
      name: 'dark',
      desc: '',
      args: [],
    );
  }

  /// `Светлая`
  String get light {
    return Intl.message(
      'Светлая',
      name: 'light',
      desc: '',
      args: [],
    );
  }

  /// `Воспроизведение`
  String get playback {
    return Intl.message(
      'Воспроизведение',
      name: 'playback',
      desc: '',
      args: [],
    );
  }

  /// `Автовоспроизведение эпизодов`
  String get autoplayEpisodes {
    return Intl.message(
      'Автовоспроизведение эпизодов',
      name: 'autoplayEpisodes',
      desc: '',
      args: [],
    );
  }

  /// `Когда вы досмотрите одну серию, следующая автомотически включается через 5 секунд`
  String get autoplayInfo {
    return Intl.message(
      'Когда вы досмотрите одну серию, следующая автомотически включается через 5 секунд',
      name: 'autoplayInfo',
      desc: '',
      args: [],
    );
  }

  /// `Только горизонтальная`
  String get horizontalOnly {
    return Intl.message(
      'Только горизонтальная',
      name: 'horizontalOnly',
      desc: '',
      args: [],
    );
  }

  /// `Ориентация плеера`
  String get orientationPlayer {
    return Intl.message(
      'Ориентация плеера',
      name: 'orientationPlayer',
      desc: '',
      args: [],
    );
  }

  /// `Версия приложения`
  String get versionApp {
    return Intl.message(
      'Версия приложения',
      name: 'versionApp',
      desc: '',
      args: [],
    );
  }

  /// `Применить`
  String get apply {
    return Intl.message(
      'Применить',
      name: 'apply',
      desc: '',
      args: [],
    );
  }

  /// `Картинка в картинке`
  String get pictureInPicture {
    return Intl.message(
      'Картинка в картинке',
      name: 'pictureInPicture',
      desc: '',
      args: [],
    );
  }

  /// `Если свернуть приложение, видео продолжется в отдельном окне`
  String get pictureInPictureInfo {
    return Intl.message(
      'Если свернуть приложение, видео продолжется в отдельном окне',
      name: 'pictureInPictureInfo',
      desc: '',
      args: [],
    );
  }

  /// `Смотреть позже`
  String get watchLater {
    return Intl.message(
      'Смотреть позже',
      name: 'watchLater',
      desc: '',
      args: [],
    );
  }

  /// `Удалить`
  String get delete {
    return Intl.message(
      'Удалить',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `Поиск`
  String get search {
    return Intl.message(
      'Поиск',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `Сбросить`
  String get clear {
    return Intl.message(
      'Сбросить',
      name: 'clear',
      desc: '',
      args: [],
    );
  }

  /// `Что хотите посмотреть?`
  String get whatDoYouWantToSee {
    return Intl.message(
      'Что хотите посмотреть?',
      name: 'whatDoYouWantToSee',
      desc: '',
      args: [],
    );
  }

  /// `Показать результат`
  String get showResult {
    return Intl.message(
      'Показать результат',
      name: 'showResult',
      desc: '',
      args: [],
    );
  }

  /// `Жанры`
  String get genres {
    return Intl.message(
      'Жанры',
      name: 'genres',
      desc: '',
      args: [],
    );
  }

  /// `Годы`
  String get years {
    return Intl.message(
      'Годы',
      name: 'years',
      desc: '',
      args: [],
    );
  }

  /// `Страны`
  String get countries {
    return Intl.message(
      'Страны',
      name: 'countries',
      desc: '',
      args: [],
    );
  }

  /// `Рейтинг`
  String get rating {
    return Intl.message(
      'Рейтинг',
      name: 'rating',
      desc: '',
      args: [],
    );
  }

  /// `Ничего не нашлось`
  String get nothingFound {
    return Intl.message(
      'Ничего не нашлось',
      name: 'nothingFound',
      desc: '',
      args: [],
    );
  }

  /// `Попробуте ввести название фильма, жанра или имя персоны`
  String get tryAgainSearch {
    return Intl.message(
      'Попробуте ввести название фильма, жанра или имя персоны',
      name: 'tryAgainSearch',
      desc: '',
      args: [],
    );
  }

  /// `Может быть, вы ищите то, что пока нет в каталоге`
  String get wrongSearch {
    return Intl.message(
      'Может быть, вы ищите то, что пока нет в каталоге',
      name: 'wrongSearch',
      desc: '',
      args: [],
    );
  }

  /// `Возможно, вас заинтересует`
  String get maybeInteresting {
    return Intl.message(
      'Возможно, вас заинтересует',
      name: 'maybeInteresting',
      desc: '',
      args: [],
    );
  }

  /// `Категория`
  String get category {
    return Intl.message(
      'Категория',
      name: 'category',
      desc: '',
      args: [],
    );
  }

  /// `Фильтр`
  String get filter {
    return Intl.message(
      'Фильтр',
      name: 'filter',
      desc: '',
      args: [],
    );
  }

  /// `Фильмы`
  String get movies {
    return Intl.message(
      'Фильмы',
      name: 'movies',
      desc: '',
      args: [],
    );
  }

  /// `Сериалы`
  String get serials {
    return Intl.message(
      'Сериалы',
      name: 'serials',
      desc: '',
      args: [],
    );
  }

  /// `Аниме`
  String get anime {
    return Intl.message(
      'Аниме',
      name: 'anime',
      desc: '',
      args: [],
    );
  }

  /// `Мультфильмы`
  String get animations {
    return Intl.message(
      'Мультфильмы',
      name: 'animations',
      desc: '',
      args: [],
    );
  }

  /// `Поиск лучших фильмы`
  String get searchMovies {
    return Intl.message(
      'Поиск лучших фильмы',
      name: 'searchMovies',
      desc: '',
      args: [],
    );
  }

  /// `Россия`
  String get russia {
    return Intl.message(
      'Россия',
      name: 'russia',
      desc: '',
      args: [],
    );
  }

  /// `Казахстан`
  String get kazakhstan {
    return Intl.message(
      'Казахстан',
      name: 'kazakhstan',
      desc: '',
      args: [],
    );
  }

  /// `США`
  String get usa {
    return Intl.message(
      'США',
      name: 'usa',
      desc: '',
      args: [],
    );
  }

  /// `Китай`
  String get china {
    return Intl.message(
      'Китай',
      name: 'china',
      desc: '',
      args: [],
    );
  }

  /// `Южная Корея`
  String get southKorea {
    return Intl.message(
      'Южная Корея',
      name: 'southKorea',
      desc: '',
      args: [],
    );
  }

  /// `Япония`
  String get japan {
    return Intl.message(
      'Япония',
      name: 'japan',
      desc: '',
      args: [],
    );
  }

  /// `Франция`
  String get france {
    return Intl.message(
      'Франция',
      name: 'france',
      desc: '',
      args: [],
    );
  }

  /// `Германия`
  String get germany {
    return Intl.message(
      'Германия',
      name: 'germany',
      desc: '',
      args: [],
    );
  }

  /// `Италия`
  String get italy {
    return Intl.message(
      'Италия',
      name: 'italy',
      desc: '',
      args: [],
    );
  }

  /// `Украина`
  String get ukraine {
    return Intl.message(
      'Украина',
      name: 'ukraine',
      desc: '',
      args: [],
    );
  }

  /// `Турция`
  String get turkey {
    return Intl.message(
      'Турция',
      name: 'turkey',
      desc: '',
      args: [],
    );
  }

  /// `Узбекистан`
  String get uzbekistan {
    return Intl.message(
      'Узбекистан',
      name: 'uzbekistan',
      desc: '',
      args: [],
    );
  }

  /// `Азербайджан`
  String get azerbaijan {
    return Intl.message(
      'Азербайджан',
      name: 'azerbaijan',
      desc: '',
      args: [],
    );
  }

  /// `Чехия`
  String get czech {
    return Intl.message(
      'Чехия',
      name: 'czech',
      desc: '',
      args: [],
    );
  }

  /// `Словакия`
  String get slovakia {
    return Intl.message(
      'Словакия',
      name: 'slovakia',
      desc: '',
      args: [],
    );
  }

  /// `Дания`
  String get dania {
    return Intl.message(
      'Дания',
      name: 'dania',
      desc: '',
      args: [],
    );
  }

  /// `Эстония`
  String get estonia {
    return Intl.message(
      'Эстония',
      name: 'estonia',
      desc: '',
      args: [],
    );
  }

  /// `Индонезия`
  String get indonesia {
    return Intl.message(
      'Индонезия',
      name: 'indonesia',
      desc: '',
      args: [],
    );
  }

  /// `Киргизистан`
  String get kyrgyzstan {
    return Intl.message(
      'Киргизистан',
      name: 'kyrgyzstan',
      desc: '',
      args: [],
    );
  }

  /// `Монголия`
  String get mongolia {
    return Intl.message(
      'Монголия',
      name: 'mongolia',
      desc: '',
      args: [],
    );
  }

  /// `Норвегия`
  String get norway {
    return Intl.message(
      'Норвегия',
      name: 'norway',
      desc: '',
      args: [],
    );
  }

  /// `Польша`
  String get poland {
    return Intl.message(
      'Польша',
      name: 'poland',
      desc: '',
      args: [],
    );
  }

  /// `Румыния`
  String get romania {
    return Intl.message(
      'Румыния',
      name: 'romania',
      desc: '',
      args: [],
    );
  }

  /// `Швеция`
  String get sweden {
    return Intl.message(
      'Швеция',
      name: 'sweden',
      desc: '',
      args: [],
    );
  }

  /// `Таджикистан`
  String get tajikistan {
    return Intl.message(
      'Таджикистан',
      name: 'tajikistan',
      desc: '',
      args: [],
    );
  }

  /// `Вьетнам`
  String get vietnam {
    return Intl.message(
      'Вьетнам',
      name: 'vietnam',
      desc: '',
      args: [],
    );
  }

  /// `Индия`
  String get india {
    return Intl.message(
      'Индия',
      name: 'india',
      desc: '',
      args: [],
    );
  }

  /// `Белоруссия`
  String get belarus {
    return Intl.message(
      'Белоруссия',
      name: 'belarus',
      desc: '',
      args: [],
    );
  }

  /// `Хроники`
  String get chronicle {
    return Intl.message(
      'Хроники',
      name: 'chronicle',
      desc: '',
      args: [],
    );
  }

  /// `Артист`
  String get artist {
    return Intl.message(
      'Артист',
      name: 'artist',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'ru', countryCode: 'RU'),
      Locale.fromSubtags(languageCode: 'en', countryCode: 'EN'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
