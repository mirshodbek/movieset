// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_EN locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_EN';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "actor": MessageLookupByLibrary.simpleMessage("Actor"),
        "actress": MessageLookupByLibrary.simpleMessage("Actress"),
        "all": MessageLookupByLibrary.simpleMessage("All"),
        "bestSeries":
            MessageLookupByLibrary.simpleMessage("The best TV series"),
        "castMembers": MessageLookupByLibrary.simpleMessage("Cast Members"),
        "crewMovie": MessageLookupByLibrary.simpleMessage("The Film Crew"),
        "director": MessageLookupByLibrary.simpleMessage("Director"),
        "favoriteActors":
            MessageLookupByLibrary.simpleMessage("Favorite Actors"),
        "invalidApiKey": MessageLookupByLibrary.simpleMessage(
            "Invalid API key: You must be granted a valid key."),
        "more": MessageLookupByLibrary.simpleMessage("More"),
        "newAnimations": MessageLookupByLibrary.simpleMessage("New Animations"),
        "noInternet": MessageLookupByLibrary.simpleMessage(
            "You seem to be offline. Data might not be up to date."),
        "people": MessageLookupByLibrary.simpleMessage("People"),
        "popularAnime": MessageLookupByLibrary.simpleMessage("Popular Anime"),
        "producer": MessageLookupByLibrary.simpleMessage("Producer"),
        "readMore": MessageLookupByLibrary.simpleMessage("Read More"),
        "resourceRequestedNotFound": MessageLookupByLibrary.simpleMessage(
            "The resource you requested could not be found."),
        "season": MessageLookupByLibrary.simpleMessage("season"),
        "series": MessageLookupByLibrary.simpleMessage("series"),
        "similar": MessageLookupByLibrary.simpleMessage("Similar"),
        "trending": MessageLookupByLibrary.simpleMessage("Trending"),
        "unknownError":
            MessageLookupByLibrary.simpleMessage("Something was broken :("),
        "years": MessageLookupByLibrary.simpleMessage("years")
      };
}
