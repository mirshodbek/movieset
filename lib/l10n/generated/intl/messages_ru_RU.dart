// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru_RU locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru_RU';

  static String m0(value) =>
      "{value, 1 {1st} 2 {2nd} 3 {3rd} other{${value}th}}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "actor": MessageLookupByLibrary.simpleMessage("Актер"),
        "actress": MessageLookupByLibrary.simpleMessage("Актриса"),
        "all": MessageLookupByLibrary.simpleMessage("Все"),
        "arrangement_index": m0,
        "bestSeries": MessageLookupByLibrary.simpleMessage("Лучшие сериалы"),
        "castMembers": MessageLookupByLibrary.simpleMessage("Актерский состав"),
        "crewMovie": MessageLookupByLibrary.simpleMessage("Съемочная группа"),
        "director": MessageLookupByLibrary.simpleMessage("Режиссёр"),
        "favoriteActors":
            MessageLookupByLibrary.simpleMessage("Любимые актеры"),
        "invalidApiKey": MessageLookupByLibrary.simpleMessage(
            "Неверный ключ API: вам должен быть предоставлен действительный ключ."),
        "more": MessageLookupByLibrary.simpleMessage("Подробнее"),
        "newAnimations":
            MessageLookupByLibrary.simpleMessage("Мультфильмы-новинки"),
        "noInternet": MessageLookupByLibrary.simpleMessage(
            "Вы, кажется, не в сети. Данные могут быть не актуальными."),
        "people": MessageLookupByLibrary.simpleMessage("Персоны"),
        "popularAnime":
            MessageLookupByLibrary.simpleMessage("Популярные Аниме"),
        "producer": MessageLookupByLibrary.simpleMessage("Продюссер"),
        "readMore": MessageLookupByLibrary.simpleMessage("Читать больше"),
        "resourceRequestedNotFound": MessageLookupByLibrary.simpleMessage(
            "Запрошенный вами ресурс не найден."),
        "season": MessageLookupByLibrary.simpleMessage("сезон"),
        "series": MessageLookupByLibrary.simpleMessage("серий"),
        "similar": MessageLookupByLibrary.simpleMessage("Похожие"),
        "trending": MessageLookupByLibrary.simpleMessage("В тренде"),
        "unknownError":
            MessageLookupByLibrary.simpleMessage("Что-то сломалось :("),
        "years": MessageLookupByLibrary.simpleMessage("лет")
      };
}
