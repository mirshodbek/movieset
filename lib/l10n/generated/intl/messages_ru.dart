// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  static String m0(value) => "${value} ч";

  static String m1(value) => "${value} мин";

  static String m2(value) => "Более ${value}";

  static String m3(value) => "${value}-е";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "actor": MessageLookupByLibrary.simpleMessage("Актер"),
        "actress": MessageLookupByLibrary.simpleMessage("Актриса"),
        "ageOld": MessageLookupByLibrary.simpleMessage("лет"),
        "all": MessageLookupByLibrary.simpleMessage("Все"),
        "animations": MessageLookupByLibrary.simpleMessage("Мультфильмы"),
        "anime": MessageLookupByLibrary.simpleMessage("Аниме"),
        "apply": MessageLookupByLibrary.simpleMessage("Применить"),
        "artist": MessageLookupByLibrary.simpleMessage("Артист"),
        "autoplayEpisodes": MessageLookupByLibrary.simpleMessage(
            "Автовоспроизведение эпизодов"),
        "autoplayInfo": MessageLookupByLibrary.simpleMessage(
            "Когда вы досмотрите одну серию, следующая автомотически включается через 5 секунд"),
        "azerbaijan": MessageLookupByLibrary.simpleMessage("Азербайджан"),
        "belarus": MessageLookupByLibrary.simpleMessage("Белоруссия"),
        "bestSeries": MessageLookupByLibrary.simpleMessage("Лучшие сериалы"),
        "castMembers": MessageLookupByLibrary.simpleMessage("Актерский состав"),
        "category": MessageLookupByLibrary.simpleMessage("Категория"),
        "changeLanguage": MessageLookupByLibrary.simpleMessage("Изменить язык"),
        "china": MessageLookupByLibrary.simpleMessage("Китай"),
        "chronicle": MessageLookupByLibrary.simpleMessage("Хроники"),
        "clear": MessageLookupByLibrary.simpleMessage("Сбросить"),
        "countries": MessageLookupByLibrary.simpleMessage("Страны"),
        "crewMovie": MessageLookupByLibrary.simpleMessage("Съемочная группа"),
        "czech": MessageLookupByLibrary.simpleMessage("Чехия"),
        "dania": MessageLookupByLibrary.simpleMessage("Дания"),
        "dark": MessageLookupByLibrary.simpleMessage("Тёмная"),
        "delete": MessageLookupByLibrary.simpleMessage("Удалить"),
        "designTheme": MessageLookupByLibrary.simpleMessage("Тема оформления"),
        "director": MessageLookupByLibrary.simpleMessage("Режиссёр"),
        "downloadOnlyViaWifi":
            MessageLookupByLibrary.simpleMessage("Загузка только по Wi-Fi"),
        "estonia": MessageLookupByLibrary.simpleMessage("Эстония"),
        "favoriteActors":
            MessageLookupByLibrary.simpleMessage("Любимые актеры"),
        "filter": MessageLookupByLibrary.simpleMessage("Фильтр"),
        "france": MessageLookupByLibrary.simpleMessage("Франция"),
        "genres": MessageLookupByLibrary.simpleMessage("Жанры"),
        "germany": MessageLookupByLibrary.simpleMessage("Германия"),
        "horizontalOnly":
            MessageLookupByLibrary.simpleMessage("Только горизонтальная"),
        "hour": m0,
        "india": MessageLookupByLibrary.simpleMessage("Индия"),
        "indonesia": MessageLookupByLibrary.simpleMessage("Индонезия"),
        "invalidApiKey": MessageLookupByLibrary.simpleMessage(
            "Неверный ключ API: вам должен быть предоставлен действительный ключ."),
        "italy": MessageLookupByLibrary.simpleMessage("Италия"),
        "japan": MessageLookupByLibrary.simpleMessage("Япония"),
        "kazakhstan": MessageLookupByLibrary.simpleMessage("Казахстан"),
        "kyrgyzstan": MessageLookupByLibrary.simpleMessage("Киргизистан"),
        "light": MessageLookupByLibrary.simpleMessage("Светлая"),
        "maybeInteresting":
            MessageLookupByLibrary.simpleMessage("Возможно, вас заинтересует"),
        "minute": m1,
        "mongolia": MessageLookupByLibrary.simpleMessage("Монголия"),
        "more": MessageLookupByLibrary.simpleMessage("Подробнее"),
        "movies": MessageLookupByLibrary.simpleMessage("Фильмы"),
        "newAnimations":
            MessageLookupByLibrary.simpleMessage("Мультфильмы-новинки"),
        "noInternet": MessageLookupByLibrary.simpleMessage(
            "Вы, кажется, не в сети. Данные могут быть не актуальными."),
        "norway": MessageLookupByLibrary.simpleMessage("Норвегия"),
        "nothingFound":
            MessageLookupByLibrary.simpleMessage("Ничего не нашлось"),
        "orientationPlayer":
            MessageLookupByLibrary.simpleMessage("Ориентация плеера"),
        "over": m2,
        "people": MessageLookupByLibrary.simpleMessage("Персоны"),
        "pictureInPicture":
            MessageLookupByLibrary.simpleMessage("Картинка в картинке"),
        "pictureInPictureInfo": MessageLookupByLibrary.simpleMessage(
            "Если свернуть приложение, видео продолжется в отдельном окне"),
        "playback": MessageLookupByLibrary.simpleMessage("Воспроизведение"),
        "poland": MessageLookupByLibrary.simpleMessage("Польша"),
        "popularAnime":
            MessageLookupByLibrary.simpleMessage("Популярные Аниме"),
        "producer": MessageLookupByLibrary.simpleMessage("Продюссер"),
        "rating": MessageLookupByLibrary.simpleMessage("Рейтинг"),
        "readMore": MessageLookupByLibrary.simpleMessage("Читать больше"),
        "resourceRequestedNotFound": MessageLookupByLibrary.simpleMessage(
            "Запрошенный вами ресурс не найден."),
        "romania": MessageLookupByLibrary.simpleMessage("Румыния"),
        "russia": MessageLookupByLibrary.simpleMessage("Россия"),
        "search": MessageLookupByLibrary.simpleMessage("Поиск"),
        "searchMovies":
            MessageLookupByLibrary.simpleMessage("Поиск лучших фильмы"),
        "season": MessageLookupByLibrary.simpleMessage("сезон"),
        "serials": MessageLookupByLibrary.simpleMessage("Сериалы"),
        "series": MessageLookupByLibrary.simpleMessage("серий"),
        "settings": MessageLookupByLibrary.simpleMessage("Настройки"),
        "showResult":
            MessageLookupByLibrary.simpleMessage("Показать результат"),
        "similar": MessageLookupByLibrary.simpleMessage("Похожие"),
        "slovakia": MessageLookupByLibrary.simpleMessage("Словакия"),
        "southKorea": MessageLookupByLibrary.simpleMessage("Южная Корея"),
        "sweden": MessageLookupByLibrary.simpleMessage("Швеция"),
        "system": MessageLookupByLibrary.simpleMessage("Системная"),
        "tajikistan": MessageLookupByLibrary.simpleMessage("Таджикистан"),
        "trending": MessageLookupByLibrary.simpleMessage("В тренде"),
        "tryAgainSearch": MessageLookupByLibrary.simpleMessage(
            "Попробуте ввести название фильма, жанра или имя персоны"),
        "turkey": MessageLookupByLibrary.simpleMessage("Турция"),
        "ukraine": MessageLookupByLibrary.simpleMessage("Украина"),
        "unknownError":
            MessageLookupByLibrary.simpleMessage("Что-то сломалось :("),
        "usa": MessageLookupByLibrary.simpleMessage("США"),
        "uzbekistan": MessageLookupByLibrary.simpleMessage("Узбекистан"),
        "versionApp": MessageLookupByLibrary.simpleMessage("Версия приложения"),
        "vietnam": MessageLookupByLibrary.simpleMessage("Вьетнам"),
        "watchLater": MessageLookupByLibrary.simpleMessage("Смотреть позже"),
        "whatDoYouWantToSee":
            MessageLookupByLibrary.simpleMessage("Что хотите посмотреть?"),
        "wrongSearch": MessageLookupByLibrary.simpleMessage(
            "Может быть, вы ищите то, что пока нет в каталоге"),
        "year": m3,
        "years": MessageLookupByLibrary.simpleMessage("Годы")
      };
}
