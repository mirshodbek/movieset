// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(value) => "${value} h";

  static String m1(value) => "${value} min";

  static String m2(value) => "Over ${value}";

  static String m3(value) => "${value}s";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "actor": MessageLookupByLibrary.simpleMessage("Actor"),
        "actress": MessageLookupByLibrary.simpleMessage("Actress"),
        "ageOld": MessageLookupByLibrary.simpleMessage("years"),
        "all": MessageLookupByLibrary.simpleMessage("All"),
        "animations": MessageLookupByLibrary.simpleMessage("Animations"),
        "anime": MessageLookupByLibrary.simpleMessage("Anime"),
        "apply": MessageLookupByLibrary.simpleMessage("Apply"),
        "artist": MessageLookupByLibrary.simpleMessage("Artist"),
        "autoplayEpisodes":
            MessageLookupByLibrary.simpleMessage("Autoplay episodes"),
        "autoplayInfo": MessageLookupByLibrary.simpleMessage(
            "When you finish watching one episode, the next one automatically turns on after 5 seconds"),
        "azerbaijan": MessageLookupByLibrary.simpleMessage("Azerbaijan"),
        "belarus": MessageLookupByLibrary.simpleMessage("Belarus"),
        "bestSeries":
            MessageLookupByLibrary.simpleMessage("The best TV series"),
        "castMembers": MessageLookupByLibrary.simpleMessage("Cast Members"),
        "category": MessageLookupByLibrary.simpleMessage("Category"),
        "changeLanguage":
            MessageLookupByLibrary.simpleMessage("Change language"),
        "china": MessageLookupByLibrary.simpleMessage("China"),
        "chronicle": MessageLookupByLibrary.simpleMessage("Chronicle"),
        "clear": MessageLookupByLibrary.simpleMessage("Clear"),
        "countries": MessageLookupByLibrary.simpleMessage("Countries"),
        "crewMovie": MessageLookupByLibrary.simpleMessage("The Film Crew"),
        "czech": MessageLookupByLibrary.simpleMessage("Czech"),
        "dania": MessageLookupByLibrary.simpleMessage("Dania"),
        "dark": MessageLookupByLibrary.simpleMessage("Dark"),
        "delete": MessageLookupByLibrary.simpleMessage("Delete"),
        "designTheme": MessageLookupByLibrary.simpleMessage("Design Theme"),
        "director": MessageLookupByLibrary.simpleMessage("Director"),
        "downloadOnlyViaWifi":
            MessageLookupByLibrary.simpleMessage("Download only via Wi-Fi"),
        "estonia": MessageLookupByLibrary.simpleMessage("Estonia"),
        "favoriteActors":
            MessageLookupByLibrary.simpleMessage("Favorite Actors"),
        "filter": MessageLookupByLibrary.simpleMessage("Filter"),
        "france": MessageLookupByLibrary.simpleMessage("France"),
        "genres": MessageLookupByLibrary.simpleMessage("Genres"),
        "germany": MessageLookupByLibrary.simpleMessage("Germany"),
        "horizontalOnly":
            MessageLookupByLibrary.simpleMessage("Horizontal only"),
        "hour": m0,
        "india": MessageLookupByLibrary.simpleMessage("India"),
        "indonesia": MessageLookupByLibrary.simpleMessage("Indonesia"),
        "invalidApiKey": MessageLookupByLibrary.simpleMessage(
            "Invalid API key: You must be granted a valid key."),
        "italy": MessageLookupByLibrary.simpleMessage("Italy"),
        "japan": MessageLookupByLibrary.simpleMessage("Japan"),
        "kazakhstan": MessageLookupByLibrary.simpleMessage("Kazakhstan"),
        "kyrgyzstan": MessageLookupByLibrary.simpleMessage("Kyrgyzstan"),
        "light": MessageLookupByLibrary.simpleMessage("Light"),
        "maybeInteresting":
            MessageLookupByLibrary.simpleMessage("You might be interested"),
        "minute": m1,
        "mongolia": MessageLookupByLibrary.simpleMessage("Mongolia"),
        "more": MessageLookupByLibrary.simpleMessage("More"),
        "movies": MessageLookupByLibrary.simpleMessage("Movies"),
        "newAnimations": MessageLookupByLibrary.simpleMessage("New Animations"),
        "noInternet": MessageLookupByLibrary.simpleMessage(
            "You seem to be offline. Data might not be up to date."),
        "norway": MessageLookupByLibrary.simpleMessage("Norway"),
        "nothingFound": MessageLookupByLibrary.simpleMessage("Nothing found"),
        "orientationPlayer":
            MessageLookupByLibrary.simpleMessage("Orientation of the player"),
        "over": m2,
        "people": MessageLookupByLibrary.simpleMessage("People"),
        "pictureInPicture":
            MessageLookupByLibrary.simpleMessage("Picture in picture"),
        "pictureInPictureInfo": MessageLookupByLibrary.simpleMessage(
            "If you minimize the app, the video will continue in a separate window"),
        "playback": MessageLookupByLibrary.simpleMessage("Playback"),
        "poland": MessageLookupByLibrary.simpleMessage("Poland"),
        "popularAnime": MessageLookupByLibrary.simpleMessage("Popular Anime"),
        "producer": MessageLookupByLibrary.simpleMessage("Producer"),
        "rating": MessageLookupByLibrary.simpleMessage("Rating"),
        "readMore": MessageLookupByLibrary.simpleMessage("Read More"),
        "resourceRequestedNotFound": MessageLookupByLibrary.simpleMessage(
            "The resource you requested could not be found."),
        "romania": MessageLookupByLibrary.simpleMessage("Romania"),
        "russia": MessageLookupByLibrary.simpleMessage("Russia"),
        "search": MessageLookupByLibrary.simpleMessage("Search"),
        "searchMovies":
            MessageLookupByLibrary.simpleMessage("Search for the best movies"),
        "season": MessageLookupByLibrary.simpleMessage("season"),
        "serials": MessageLookupByLibrary.simpleMessage("Serials"),
        "series": MessageLookupByLibrary.simpleMessage("series"),
        "settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "showResult": MessageLookupByLibrary.simpleMessage("Show result"),
        "similar": MessageLookupByLibrary.simpleMessage("Similar"),
        "slovakia": MessageLookupByLibrary.simpleMessage("Slovakia"),
        "southKorea": MessageLookupByLibrary.simpleMessage("South Korea"),
        "sweden": MessageLookupByLibrary.simpleMessage("Sweden"),
        "system": MessageLookupByLibrary.simpleMessage("System"),
        "tajikistan": MessageLookupByLibrary.simpleMessage("Tajikistan"),
        "trending": MessageLookupByLibrary.simpleMessage("Trending"),
        "tryAgainSearch": MessageLookupByLibrary.simpleMessage(
            "Try to enter the name of the movie, genre or person\'s name"),
        "turkey": MessageLookupByLibrary.simpleMessage("Turkey"),
        "ukraine": MessageLookupByLibrary.simpleMessage("Ukraine"),
        "unknownError":
            MessageLookupByLibrary.simpleMessage("Something was broken :("),
        "usa": MessageLookupByLibrary.simpleMessage("Usa"),
        "uzbekistan": MessageLookupByLibrary.simpleMessage("Uzbekistan"),
        "versionApp": MessageLookupByLibrary.simpleMessage("Version app"),
        "vietnam": MessageLookupByLibrary.simpleMessage("Vietnam"),
        "watchLater": MessageLookupByLibrary.simpleMessage("Watch later"),
        "whatDoYouWantToSee":
            MessageLookupByLibrary.simpleMessage("What do you want to see?"),
        "wrongSearch": MessageLookupByLibrary.simpleMessage(
            "Maybe you are looking for something that is not yet in the catalog"),
        "year": m3,
        "years": MessageLookupByLibrary.simpleMessage("Years")
      };
}
