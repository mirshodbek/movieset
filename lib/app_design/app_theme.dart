import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:movie_set/app_design/app_colors.dart';
import 'package:movie_set/app_design/app_text_style.dart';

class AppMainTheme {
  final darkTheme = ThemeData(
    scaffoldBackgroundColor: AppColors.primaryMain,
    brightness: Brightness.dark,
    colorScheme: const ColorScheme.dark(
      primary: AppColors.primarySuperExtraLight,
      onPrimary: AppColors.grayScaleBorderIconsGrey,
      tertiary: AppColors.grayScaleBorderLightGrey,
      onTertiary: AppColors.primaryDark,
      tertiaryContainer: AppColors.secondaryMain,
    ),
    appBarTheme: AppBarTheme(
      systemOverlayStyle: AppSystem.systemOverlayStyleDark,
      backgroundColor: AppColors.primaryMain,
      titleTextStyle: AppTextStyle.s24w400.copyWith(
        color: AppColors.grayScaleBackgroundGrey,
      ),
      iconTheme: const IconThemeData(
        color: AppColors.grayScaleBackgroundGrey,
      ),
      elevation: 0.0,
    ),
    primaryTextTheme: TextTheme(
      displayLarge: AppTextStyle.w900.copyWith(
        color: AppColors.grayScaleWhite.withOpacity(.04),
      ),
      displayMedium: AppTextStyle.s48w900.copyWith(
        color: AppColors.grayScaleWhite,
      ),
      displaySmall: AppTextStyle.s32w900.copyWith(
        color: AppColors.grayScaleBackgroundGrey,
      ),
      titleLarge: AppTextStyle.s32w900,
      titleMedium: AppTextStyle.s32w700,
      titleSmall: AppTextStyle.s32w500,
      bodyLarge: AppTextStyle.s24w700,
      bodyMedium: AppTextStyle.s24w500.copyWith(
        color: AppColors.grayScaleBackgroundGrey,
      ),
      bodySmall: AppTextStyle.s24w400,
      labelLarge: AppTextStyle.s18w700,
      labelMedium: AppTextStyle.s18w500.copyWith(
        color: AppColors.grayScaleBackgroundGrey,
      ),
      labelSmall: AppTextStyle.s18w400.copyWith(
        color: AppColors.primarySuperExtraLight,
      ),
    ),
    textTheme: TextTheme(
      displayLarge: AppTextStyle.s16w700.copyWith(
        color: AppColors.grayScaleWhite,
      ),
      displayMedium: AppTextStyle.s16w500.copyWith(
        color: AppColors.grayScaleBackgroundGrey,
      ),
      displaySmall: AppTextStyle.s16w400.copyWith(
        color: AppColors.grayScaleWhite,
      ),
      titleLarge: AppTextStyle.s14w700.copyWith(
        color: AppColors.secondaryMain,
      ),
      titleMedium: AppTextStyle.s14w500.copyWith(
        color: AppColors.grayScaleBackgroundGrey,
      ),
      titleSmall: AppTextStyle.s14w400.copyWith(
        color: AppColors.grayScaleWhite,
      ),
      bodyLarge: AppTextStyle.s12w700,
      bodyMedium: AppTextStyle.s12w500.copyWith(
          // color: AppColors.primaryMain,
          ),
      bodySmall: AppTextStyle.s12w400.copyWith(
        color: AppColors.grayScaleBackgroundGrey,
      ),
      labelLarge: AppTextStyle.s10w700.copyWith(
          // color: AppColors.grayScaleWhite.withOpacity(.6),
          ),
      labelMedium: AppTextStyle.s10w500.copyWith(
        color: AppColors.grayScaleBackgroundGrey,
      ),
      labelSmall: AppTextStyle.s10w400.copyWith(
        color: AppColors.secondaryDark,
      ),
    ),
    tabBarTheme: TabBarTheme(
      unselectedLabelColor: AppColors.primaryExtraLight,
      labelColor: AppColors.grayScaleWhite,
      labelStyle: AppTextStyle.s16w500,
      indicatorColor: AppColors.secondaryMain,
      indicatorSize: TabBarIndicatorSize.tab,
      unselectedLabelStyle: AppTextStyle.s16w500,
      indicator: UnderlineTabIndicator(
        borderRadius: BorderRadius.circular(8),
        borderSide: const BorderSide(
          color: AppColors.secondaryMain,
          width: 5,
        ),
      ),
    ),
    listTileTheme: ListTileThemeData(
      tileColor: AppColors.primaryDark,
      textColor: AppColors.primarySuperExtraLight,
      iconColor: AppColors.grayScaleLightGrey,
      contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
    ),
    bottomSheetTheme: const BottomSheetThemeData(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25.0),
        ),
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      fillColor: AppColors.primaryDark,
      suffixIconColor: AppColors.primaryExtraLight,
      prefixIconColor: AppColors.secondaryMain,
      hintStyle: AppTextStyle.s14w400.copyWith(
        color: AppColors.primaryExtraLight,
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: const BorderSide(
          color: AppColors.primaryDark,
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: const BorderSide(
          color: AppColors.primaryDark,
        ),
      ),
    ),
    checkboxTheme: CheckboxThemeData(
      checkColor: const MaterialStatePropertyAll<Color>(AppColors.primaryMain),
      fillColor: const MaterialStatePropertyAll<Color>(AppColors.secondaryMain),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6),
      ),
      side: const BorderSide(color: AppColors.primaryExtraLight),
    ),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        textStyle: MaterialStateProperty.all<TextStyle>(
          AppTextStyle.s16w700.copyWith(
            foreground: Paint()..color = AppColors.secondaryMain,
          ),
        ),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: const MaterialStatePropertyAll<Color>(AppColors.secondaryMain),
        shape: MaterialStatePropertyAll<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
        ),
        textStyle: MaterialStatePropertyAll<TextStyle>(
          AppTextStyle.s16w500.copyWith(
            foreground: Paint()..color = AppColors.primaryMain,
          ),
        ),
      ),
    ),
    snackBarTheme: const SnackBarThemeData(
      backgroundColor: AppColors.grayScaleLightGrey,
      behavior: SnackBarBehavior.floating,
      contentTextStyle: AppTextStyle.s10w400,
    ),
    bottomAppBarTheme: const BottomAppBarTheme(
      color: AppColors.primaryMain,
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: AppColors.primaryMain.withOpacity(.8),
      type: BottomNavigationBarType.fixed,
      selectedIconTheme: const IconThemeData(
        color: AppColors.grayScaleWhite,
        size: 32,
      ),
      unselectedIconTheme: const IconThemeData(
        color: AppColors.grayScaleBorderLightGrey,
        size: 32,
      ),
      showSelectedLabels: false,
      showUnselectedLabels: false,
      landscapeLayout: BottomNavigationBarLandscapeLayout.spread,
    ),
  );

  final lightTheme = ThemeData(
    scaffoldBackgroundColor: AppColors.primarySuperExtraLight,
    brightness: Brightness.light,
    colorScheme: const ColorScheme.light(
      primary: AppColors.primaryExtraLight,
      onPrimary: AppColors.primaryLight,
      tertiary: AppColors.grayScaleGrey,
      onTertiary: AppColors.grayScaleBackgroundGrey,
      tertiaryContainer: AppColors.grayScaleWhite,
    ),
    appBarTheme: AppBarTheme(
      systemOverlayStyle: AppSystem.systemOverlayStyleLight,
      backgroundColor: AppColors.primarySuperExtraLight,
      titleTextStyle: AppTextStyle.s24w500.copyWith(
        color: AppColors.primaryMain,
      ),
      iconTheme: const IconThemeData(
        color: AppColors.primaryMain,
      ),
      elevation: 0.0,
    ),
    primaryTextTheme: TextTheme(
      displayLarge: AppTextStyle.w900.copyWith(
        color: AppColors.primaryMain.withOpacity(.04),
      ),
      displayMedium: AppTextStyle.s48w900.copyWith(
        color: AppColors.primaryMain,
      ),
      displaySmall: AppTextStyle.s32w900.copyWith(
        color: AppColors.primaryMain,
      ),
      titleLarge: AppTextStyle.s32w900,
      titleMedium: AppTextStyle.s32w700,
      titleSmall: AppTextStyle.s32w500,
      bodyLarge: AppTextStyle.s24w700,
      bodyMedium: AppTextStyle.s24w500.copyWith(
        color: AppColors.primaryMain,
      ),
      bodySmall: AppTextStyle.s24w400,
      labelLarge: AppTextStyle.s18w700,
      labelMedium: AppTextStyle.s18w500.copyWith(
        color: AppColors.primaryMain,
      ),
      labelSmall: AppTextStyle.s18w400.copyWith(
        color: AppColors.primaryMain,
      ),
    ),
    textTheme: TextTheme(
      displayLarge: AppTextStyle.s16w700.copyWith(
        color: AppColors.primaryLight,
      ),
      displayMedium: AppTextStyle.s16w500.copyWith(
        color: AppColors.primaryMain,
      ),
      displaySmall: AppTextStyle.s16w400.copyWith(
        color: AppColors.primaryDark,
      ),
      titleLarge: AppTextStyle.s14w700.copyWith(
        color: AppColors.primaryLight,
      ),
      titleMedium: AppTextStyle.s14w500.copyWith(
        color: AppColors.primaryLight,
      ),
      titleSmall: AppTextStyle.s14w400.copyWith(
        color: AppColors.primaryMain,
      ),
      bodyLarge: AppTextStyle.s12w700,
      bodyMedium: AppTextStyle.s12w500.copyWith(
        color: AppColors.primaryMain,
      ),
      bodySmall: AppTextStyle.s12w400.copyWith(
        color: AppColors.primaryMain,
      ),
      labelLarge: AppTextStyle.s10w700.copyWith(
        color: AppColors.grayScaleWhite.withOpacity(.6),
      ),
      labelMedium: AppTextStyle.s10w500.copyWith(
        color: AppColors.primaryLight,
      ),
      labelSmall: AppTextStyle.s10w400.copyWith(
        color: AppColors.primaryLight,
      ),
    ),
    tabBarTheme: TabBarTheme(
      unselectedLabelColor: AppColors.primaryExtraLight,
      labelColor: AppColors.primaryDark,
      labelStyle: AppTextStyle.s16w500,
      unselectedLabelStyle: AppTextStyle.s16w500,
      indicatorSize: TabBarIndicatorSize.tab,
      indicator: UnderlineTabIndicator(
        borderRadius: BorderRadius.circular(8),
        borderSide: const BorderSide(
          color: AppColors.secondaryMain,
          width: 5,
        ),
      ),
    ),
    listTileTheme: ListTileThemeData(
      tileColor: AppColors.grayScaleBackgroundGrey,
      textColor: AppColors.primaryMain,
      iconColor: AppColors.grayScaleLightGrey,
      contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
    ),
    bottomSheetTheme: const BottomSheetThemeData(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25.0),
        ),
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      fillColor: AppColors.grayScaleBackgroundGrey,
      suffixIconColor: AppColors.primaryExtraLight,
      prefixIconColor: AppColors.primaryLight,
      hintStyle: AppTextStyle.s14w400.copyWith(
        color: AppColors.primaryExtraLight,
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: const BorderSide(
          color: AppColors.grayScaleBackgroundGrey,
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: const BorderSide(
          color: AppColors.grayScaleBackgroundGrey,
        ),
      ),
    ),
    checkboxTheme: CheckboxThemeData(
      checkColor: const MaterialStatePropertyAll<Color>(AppColors.primaryMain),
      fillColor: const MaterialStatePropertyAll<Color>(AppColors.secondaryMain),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6),
      ),
      side: const BorderSide(color: AppColors.primaryMain),
    ),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        textStyle: MaterialStateProperty.all<TextStyle>(
          AppTextStyle.s16w700.copyWith(
            foreground: Paint()..color = AppColors.primaryExtraLight,
          ),
        ),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        shape: MaterialStatePropertyAll<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
        ),
        backgroundColor: const MaterialStatePropertyAll<Color>(AppColors.secondaryMain),
        textStyle: MaterialStatePropertyAll<TextStyle>(
          AppTextStyle.s16w500.copyWith(
            foreground: Paint()..color = AppColors.primaryMain,
          ),
        ),
      ),
    ),
    snackBarTheme: const SnackBarThemeData(
      backgroundColor: AppColors.grayScaleLightGrey,
      behavior: SnackBarBehavior.floating,
      contentTextStyle: AppTextStyle.s10w400,
    ),
    bottomAppBarTheme: const BottomAppBarTheme(
      color: AppColors.primaryMain,
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: AppColors.primarySuperExtraLight.withOpacity(.8),
      type: BottomNavigationBarType.fixed,
      selectedIconTheme: const IconThemeData(
        color: AppColors.primaryMain,
        size: 32,
        fill: 1,
      ),
      unselectedIconTheme: const IconThemeData(
        color: AppColors.primaryLight,
        size: 32,
      ),
      showSelectedLabels: false,
      showUnselectedLabels: false,
      landscapeLayout: BottomNavigationBarLandscapeLayout.spread,
    ),
  );
}

class AppSystem {
  static const systemOverlayStyleLight = SystemUiOverlayStyle(
    statusBarColor: AppColors.primaryExtraLight,
    statusBarBrightness: Brightness.light,
    statusBarIconBrightness: Brightness.light,
    systemNavigationBarIconBrightness: Brightness.light,
    systemNavigationBarDividerColor: null,
  );
  static const systemOverlayStyleDark = SystemUiOverlayStyle(
    statusBarColor: AppColors.primaryMain,
    statusBarBrightness: Brightness.dark,
    statusBarIconBrightness: Brightness.light,
    systemNavigationBarIconBrightness: Brightness.dark,
    systemNavigationBarDividerColor: null,
  );
}
