import 'package:flutter/material.dart';
import 'package:movie_set/constants/constants.dart';

class AppTextStyle {
  // 200 - w900
  static const w900 = TextStyle(
    fontWeight: FontWeight.w900,
    fontFamily: Constants.fontFamily,
  );

  // 44 - w900
  static const s48w900 = TextStyle(
    fontSize: 44,
    fontWeight: FontWeight.w900,
    fontFamily: Constants.fontFamily,
  );

  // 32 - w900
  static const s32w900 = TextStyle(
    fontSize: 32,
    fontWeight: FontWeight.w900,
    fontFamily: Constants.fontFamily,
  );

  // 32 - w700
  static const s32w700 = TextStyle(
    fontSize: 32,
    fontWeight: FontWeight.w700,
    fontFamily: Constants.fontFamily,
  );

  // 32 - w500
  static const s32w500 = TextStyle(
    fontSize: 32,
    fontWeight: FontWeight.w500,
    fontFamily: Constants.fontFamily,
  );

  // 24 - w700
  static const s24w700 = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.w700,
    fontFamily: Constants.fontFamily,
  );

  // 24 - w500
  static const s24w500 = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.w500,
    fontFamily: Constants.fontFamily,
  );

  // 24 - w400
  static const s24w400 = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.w400,
    fontFamily: Constants.fontFamily,
  );

  // 18 - w700
  static const s18w700 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w700,
    fontFamily: Constants.fontFamily,
  );

  // 18 - w500
  static const s18w500 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w500,
    fontFamily: Constants.fontFamily,
  );

  // 18 - w400
  static const s18w400 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w400,
    fontFamily: Constants.fontFamily,
  );

  // 16 - w700
  static const s16w700 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w700,
    fontFamily: Constants.fontFamily,
  );

  // 16 - w500
  static const s16w500 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    fontFamily: Constants.fontFamily,
  );

  // 16 - w400
  static const s16w400 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontFamily: Constants.fontFamily,
  );

  // 14 - w700
  static const s14w700 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w700,
    fontFamily: Constants.fontFamily,
  );

  // 14 - w500
  static const s14w500 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    fontFamily: Constants.fontFamily,
  );

  // 14 - w400
  static const s14w400 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontFamily: Constants.fontFamily,
  );

  // 12 - w700
  static const s12w700 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w700,
    fontFamily: Constants.fontFamily,
  );

  // 12 - w500
  static const s12w500 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w500,
    fontFamily: Constants.fontFamily,
  );

  // 12 - w400
  static const s12w400 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
    fontFamily: Constants.fontFamily,
  );

  // 10 - w700
  static const s10w700 = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w700,
    fontFamily: Constants.fontFamily,
  );

  // 10 - w500
  static const s10w500 = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w500,
    fontFamily: Constants.fontFamily,
  );

  // 10 - w400
  static const s10w400 = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w400,
    fontFamily: Constants.fontFamily,
  );
}
