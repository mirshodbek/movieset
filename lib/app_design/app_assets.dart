abstract class AppAssets {
  static const svg = _Svg();
  static const image = _Image();
}

class _Svg {
  const _Svg();
}

class _Image {
  const _Image();

  final String empty = 'assets/images/empty-box.png';
}
