import 'package:flutter/material.dart';
import 'package:movie_set/app_design/app_colors.dart';

class AppGradients {
  static final splashDarkGradient = LinearGradient(
    transform: const GradientRotation(0.6),
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: <Color>[
      AppColors.gradientFour,
      AppColors.gradientFour.withGreen(35),
      AppColors.gradientFour.withGreen(35),
      AppColors.gradientFour.withBlue(25),
      AppColors.gradientFour.withBlue(25),
      AppColors.gradientFour,
    ],
    stops: const [
      0.1,
      0.5,
      0.5,
      0.6,
      0.7,
      1.0,
    ],
  );
  static final splashLightGradient = LinearGradient(
    transform: const GradientRotation(0.6),
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: <Color>[
      AppColors.primarySuperExtraLight,
      AppColors.primarySuperExtraLight.withGreen(500),
      AppColors.primarySuperExtraLight.withGreen(500),
      AppColors.primarySuperExtraLight.withBlue(480),
      AppColors.primarySuperExtraLight.withBlue(480),
      AppColors.primarySuperExtraLight,
    ],
    stops: const [
      0.1,
      0.4,
      0.4,
      0.6,
      0.6,
      1.0,
    ],
  );

  static const textGradient = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      AppColors.gradientOneZero,
      AppColors.gradientOneHundred,
    ],
  );

  static LinearGradient shimmerGradient(AnimationController shimmerController) => LinearGradient(
        colors: const [
          AppColors.othersGrey300,
          AppColors.othersGrey100,
          AppColors.othersGrey300,
        ],
        stops: const [
          0.1,
          0.3,
          0.4,
        ],
        begin: const Alignment(-1.0, -0.3),
        end: const Alignment(1.0, 0.3),
        tileMode: TileMode.clamp,
        transform: _SlidingGradientTransform(slidePercent: shimmerController.value),
      );
}

class _SlidingGradientTransform extends GradientTransform {
  const _SlidingGradientTransform({
    required this.slidePercent,
  });

  final double slidePercent;

  @override
  Matrix4? transform(Rect bounds, {TextDirection? textDirection}) {
    return Matrix4.translationValues(bounds.width * slidePercent, 0.0, 0.0);
  }
}
