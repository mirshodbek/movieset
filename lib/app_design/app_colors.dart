import 'dart:ui';

class AppColors {
  // Primary Colors
  static const primaryExtraDark = Color(0xFF07090d);
  static const primaryDark = Color(0xFF101828);
  static const primaryMain = Color(0xFF1d2637);
  static const primaryLight = Color(0xFF475467);
  static const primaryExtraLight = Color(0xFF98a2b3);
  static const primarySuperExtraLight = Color(0xFFe4e7ec);

  // Secondary Colors
  static const secondarySuperExtraDark = Color(0xFF434c09);
  static const secondaryExtraDark = Color(0xFF859812);
  static const secondaryDark = Color(0xFFc7e41c);
  static const secondaryMain = Color(0xFFdaed68);
  static const secondaryLight = Color(0xFFe9f4a4);
  static const secondaryExtraLight = Color(0xFFf0f8c3);
  static const secondarySuperExtraLight = Color(0xFFf8fbe1);

  // Grayscale Colors
  static const grayScaleBlack = Color(0xFF07090d);
  static const grayScaleDarkGrey = Color(0xFF31363d);
  static const grayScaleGrey = Color(0xFF727f88);
  static const grayScaleLightGrey = Color(0xFF9a9ea7);
  static const grayScaleBorderIconsGrey = Color(0xFFb6bbc6);
  static const grayScaleBorderLightGrey = Color(0xFFd8dcde);
  static const grayScaleBackgroundGrey = Color(0xFFf2f2f2);
  static const grayScaleBackgroundLightGrey = Color(0xFFf8f8f8);
  static const grayScaleWhite = Color(0xFFffffff);

  // Status Colors
  static const statusSuccess = Color(0xFF0f9d58);
  static const statusWarning = Color(0xFFfbbb05);
  static const statusWarningLight = Color(0xFFfef4d7);
  static const statusError = Color(0xFFee1b20);
  static const statusErrorLight = Color(0xFFfbe8e6);
  static const statusSystemBlue = Color(0xFF0a84ff);

  // Gradient Colors
  static const gradientOneZero = Color(0xFF97cf53);
  static const gradientOneHundred = Color(0xFF45b649);
  static const gradientTwoZero = Color(0xFFf09819);
  static const gradientTwoHundred = Color(0xFFedde5d);
  static const gradientThreeZero = Color(0xFFce005b);
  static const gradientThreeHundred = Color(0xFFff2a00);
  static const gradientFour = Color(0xFF101828);

  // Others
  static const othersBlack = Color(0xFF000000);
  static const othersGrey300 = Color(0xFFe0e0e0);
  static const othersGrey100 = Color(0xFFf5f5f5);
}
