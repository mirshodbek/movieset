class Constants {
  // String

  // api key
  static const apiKey = 'api_key';

  // app design
  static const language = 'language';
  static const fontFamily = 'Montserrat';
  static const onGenerateTitle = 'Movie Set';
  static const restorationScopeId = 'router';
  static const logoMoviePartOne = 'MOVIE';
  static const logoMoviePartTwo = 'SET';

  // theme type
  static const dark = 'dark';
  static const light = 'light';
  static const platformTheme = 'platform_theme';

  // navigation label
  static const home = 'home';
  static const search = 'search';
  static const bookmark = 'bookmark';
  static const settings = 'settings';

  // base urls
  static const originalSizeUrlImage = 'https://image.tmdb.org/t/p/original';
  static const w500SizeUrlImage = 'https://image.tmdb.org/t/p/w500';
  static const w200SizeUrlImage = 'https://image.tmdb.org/t/p/w200';
  static const youtubeUrl = 'https://youtu.be/';
  static const vimeoUrl = ' https://vimeo.com/';

  // types media
  static const movie = 'movie';
  static const typeMovies = 'type_movies';
  static const typeMovieAnimations = 'type_movie_animations';
  static const typeMovieRecommendations = 'type_movie_recommendations';
  static const tv = 'tv';
  static const typeTvs = 'type_seasons';
  static const typeTvAnimations = 'type_season_animations';
  static const typeTvRecommendations = 'type_tv_recommendations';
  static const typeTrending = 'type_trending';
  static const typeBookmark = 'type_bookmark';

  static const typeListMovies = [movie, typeMovies, typeMovieAnimations];
  static const typeListTvs = [tv, typeTvs, typeTvAnimations];

  // type jobs person
  static const actor = 'Acting';
  static const director = 'Directing';

  // type person details
  static const cast = 'cast';
  static const crew = 'crew';

  // pass data to screen
  static const dataOne = 'data_one';
  static const dataTwo = 'data_two';
  static const dataThree = 'data_three';
  static const dataFour = 'data_four';
  static const dataFive = 'data_five';

  // type language
  static const russian = 'Русский';
  static const english = 'English';

  // type bottom sheet
  static const typeOneCheck = 'type_one_check';

  // others
  static const empty = '';
  static const plusSixteen = '+16';
  static const en = 'en';
  static const youtube = 'YouTube';

  // minus numeric
  static const minusOne = -1;
  static const minusTwo = -2;
  static const minusThree = -3;
  static const minusFour = -4;
}

class Environment {
  // static const key = String.fromEnvironment(Constants.apiKey);
  static const key = '0a7df9bed7990910a33ed5f4ca26ecd9';
}
