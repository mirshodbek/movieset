import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../../constants/constants.dart';
import '../../../router/pages.dart';
import '../../../router/router.dart';
import '../../../widgets/animations/shimmer/shimmer_list_tile.dart';
import '../../../widgets/app_list_tile/app_list_tile.dart';
import '../../../widgets/app_list_tile/app_list_tile_item.dart';
import '../../../widgets/app_title_category.dart';
import '../blocs/search_data_bloc/search_data_bloc.dart';

class SearchMain extends StatelessWidget {
  const SearchMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchDataBloc, SearchDataState>(
      builder: (context, state) {
        return state.map(
          init: (_) {
            return Column(
              children: [
                AppTitleCategory(title: S.of(context).maybeInteresting),
                SingleChildScrollView(
                  child: Wrap(
                    children: List.generate(
                      9,
                      (index) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4),
                          child: ShimmerListTile(
                            width: context.widthGridViewListTile(3),
                            height: 180,
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            );
          },
          data: (map) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FittedBox(
                  child: AppTitleCategory(
                    title: S.of(context).maybeInteresting,
                    onPressed: () {
                      AppRouter.navigate(
                        context,
                        (configuration) => configuration.add(
                          PanelMediaPage({
                            Constants.dataOne: map.trending,
                            Constants.dataTwo: S.of(context).maybeInteresting,
                            Constants.dataThree: Constants.typeTrending,
                            Constants.dataFour: map.trending.firstOrNull?.totalPages,
                          }),
                        ),
                      );
                    },
                  ),
                ),
                SingleChildScrollView(
                  child: Wrap(
                    children: List.generate(
                      map.trending.length,
                      (index) {
                        final item = map.trending[index];
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: AppListTile(
                            onTap: () {
                              AppRouter.navigate(
                                context,
                                (configuration) => configuration.add(
                                  MediaDetailsPage({
                                    Constants.dataOne: item,
                                    Constants.dataTwo: item.type,
                                  }),
                                ),
                              );
                            },
                            item: AppListTileItem(
                              imagePath: item.posterPath ?? item.backdropPath,
                              voteAverage: item.voteAverage,
                              adult: item.adult,
                              title: item.title,
                              smallTextStyleTitle: true,
                            ),
                            width: context.widthGridViewListTile(3),
                            height: 180,
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }
}
