import 'package:flutter/material.dart';

import '../blocs/search_text_bloc/search_text_bloc.dart';
import 'search_empty_result.dart';
import 'search_loading_data.dart';
import 'search_main.dart';
import 'search_results.dart';

class SearchStateWidgets extends StatelessWidget {
  const SearchStateWidgets({
    Key? key,
    required this.state,
  }) : super(key: key);
  final SearchTextState state;

  @override
  Widget build(BuildContext context) {
    if (state is SearchPopulated) {
      return SearchResults(
        medias: (state as SearchPopulated).result,
      );
    } else if (state is SearchEmpty) {
      return const SearchEmptyResult();
    } else if (state is SearchLoading) {
      return const SearchLoadingData();
    }

    return const SearchMain();
  }
}
