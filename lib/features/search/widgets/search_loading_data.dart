import 'package:flutter/material.dart';

import '../../../widgets/animations/spin_kit_pouring_hour_glass_refined.dart';

class SearchLoadingData extends StatelessWidget {
  const SearchLoadingData({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(vertical: 32),
      child: SpinKitPouringHourGlassRefined(duration: Duration(milliseconds: 500)),
    );
  }
}
