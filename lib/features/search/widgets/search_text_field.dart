import 'package:flutter/material.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

class SearchTextField extends StatefulWidget {
  const SearchTextField({
    Key? key,
    this.onChanged,
  }) : super(key: key);
  final Function(String)? onChanged;

  @override
  State<SearchTextField> createState() => _SearchTextFieldState();
}

class _SearchTextFieldState extends State<SearchTextField> {
  late final TextEditingController _controller;
  late FocusNode _focusNode;
  bool focused = false;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
    _focusNode = FocusNode()
      ..addListener(() {
        if (!_focusNode.hasFocus && !focused) {
          _focusNode.canRequestFocus = false;
          focused = true;
        } else {
          _focusNode.requestFocus();
          focused = false;
        }
      });
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      onTap: () => _focusNode.canRequestFocus = true,
      focusNode: _focusNode,
      controller: _controller,
      onChanged: widget.onChanged,
      style: context.appTheme.inputDecorationTheme.hintStyle,
      decoration: InputDecoration(
        hintText: S.of(context).whatDoYouWantToSee,
        prefixIcon: const Icon(Icons.search),
        suffixIcon: _controller.value.text.isNotEmpty
            ? ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: Material(
                  type: MaterialType.transparency,
                  child: IconButton(
                    onPressed: () {
                      _controller.clear();
                      widget.onChanged?.call(Constants.empty);
                    },
                    icon: const Icon(Icons.clear),
                  ),
                ),
              )
            : null,
      ),
    );
  }
}
