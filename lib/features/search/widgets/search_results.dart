import 'package:flutter/material.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/utils/extensions/string_extension.dart';
import 'package:movie_set/widgets/app_list_tile/parts/image_list_tile.dart';

import '../../../app_design/app_colors.dart';
import '../../../app_design/app_text_style.dart';
import '../../../constants/constants.dart';
import '../../../models/media/media.dart';
import '../../../router/pages.dart';
import '../../../router/router.dart';
import '../../../widgets/app_list_tile/parts/vote_average.dart';

class SearchResults extends StatelessWidget {
  const SearchResults({
    Key? key,
    required this.medias,
  }) : super(key: key);
  final List<Media> medias;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
        medias.length,
        (index) {
          final media = medias[index];
          final year = media.releaseDate?.yearOfDate ?? Constants.empty;
          final adult = media.adult ?? false ? ', ${Constants.plusSixteen}' : Constants.empty;
          return InkWell(
            onTap: () {
              AppRouter.navigate(
                context,
                (configuration) => configuration.add(
                  MediaDetailsPage({
                    Constants.dataOne: media,
                    Constants.dataTwo: media.type,
                  }),
                ),
              );
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 80,
                    child: ImageListTile(
                      width: 80,
                      height: 120,
                      imagePath: media.posterPath ?? media.backdropPath ?? Constants.empty,
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          media.title ?? Constants.empty,
                          style: context.textTheme.titleLarge?.copyWith(
                            color: context.dark ? AppColors.grayScaleBackgroundGrey : null,
                          ),
                        ),
                        const SizedBox(height: 8),
                        Text(
                          '$year$adult',
                          style: AppTextStyle.s12w400.copyWith(
                            color: AppColors.primaryExtraLight,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 8),
                  VoteAverage(
                    voteAverage: media.voteAverage?.toStringAsFixed(1),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
