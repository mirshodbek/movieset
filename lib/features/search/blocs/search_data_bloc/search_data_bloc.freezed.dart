// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'search_data_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SearchDataEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SearchDataEventCopyWith<$Res> {
  factory $SearchDataEventCopyWith(
          SearchDataEvent value, $Res Function(SearchDataEvent) then) =
      _$SearchDataEventCopyWithImpl<$Res, SearchDataEvent>;
}

/// @nodoc
class _$SearchDataEventCopyWithImpl<$Res, $Val extends SearchDataEvent>
    implements $SearchDataEventCopyWith<$Res> {
  _$SearchDataEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitImplCopyWith<$Res> {
  factory _$$InitImplCopyWith(
          _$InitImpl value, $Res Function(_$InitImpl) then) =
      __$$InitImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitImplCopyWithImpl<$Res>
    extends _$SearchDataEventCopyWithImpl<$Res, _$InitImpl>
    implements _$$InitImplCopyWith<$Res> {
  __$$InitImplCopyWithImpl(_$InitImpl _value, $Res Function(_$InitImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitImpl implements _Init {
  const _$InitImpl();

  @override
  String toString() {
    return 'SearchDataEvent.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements SearchDataEvent {
  const factory _Init() = _$InitImpl;
}

/// @nodoc
mixin _$SearchDataState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> trending, List<Genre> genres) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> trending, List<Genre> genres)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> trending, List<Genre> genres)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitSearchDataState value) init,
    required TResult Function(DataSearchDataState value) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitSearchDataState value)? init,
    TResult? Function(DataSearchDataState value)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitSearchDataState value)? init,
    TResult Function(DataSearchDataState value)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SearchDataStateCopyWith<$Res> {
  factory $SearchDataStateCopyWith(
          SearchDataState value, $Res Function(SearchDataState) then) =
      _$SearchDataStateCopyWithImpl<$Res, SearchDataState>;
}

/// @nodoc
class _$SearchDataStateCopyWithImpl<$Res, $Val extends SearchDataState>
    implements $SearchDataStateCopyWith<$Res> {
  _$SearchDataStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitSearchDataStateImplCopyWith<$Res> {
  factory _$$InitSearchDataStateImplCopyWith(_$InitSearchDataStateImpl value,
          $Res Function(_$InitSearchDataStateImpl) then) =
      __$$InitSearchDataStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitSearchDataStateImplCopyWithImpl<$Res>
    extends _$SearchDataStateCopyWithImpl<$Res, _$InitSearchDataStateImpl>
    implements _$$InitSearchDataStateImplCopyWith<$Res> {
  __$$InitSearchDataStateImplCopyWithImpl(_$InitSearchDataStateImpl _value,
      $Res Function(_$InitSearchDataStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitSearchDataStateImpl extends InitSearchDataState {
  const _$InitSearchDataStateImpl() : super._();

  @override
  String toString() {
    return 'SearchDataState.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitSearchDataStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> trending, List<Genre> genres) data,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> trending, List<Genre> genres)? data,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> trending, List<Genre> genres)? data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitSearchDataState value) init,
    required TResult Function(DataSearchDataState value) data,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitSearchDataState value)? init,
    TResult? Function(DataSearchDataState value)? data,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitSearchDataState value)? init,
    TResult Function(DataSearchDataState value)? data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitSearchDataState extends SearchDataState {
  const factory InitSearchDataState() = _$InitSearchDataStateImpl;
  const InitSearchDataState._() : super._();
}

/// @nodoc
abstract class _$$DataSearchDataStateImplCopyWith<$Res> {
  factory _$$DataSearchDataStateImplCopyWith(_$DataSearchDataStateImpl value,
          $Res Function(_$DataSearchDataStateImpl) then) =
      __$$DataSearchDataStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Media> trending, List<Genre> genres});
}

/// @nodoc
class __$$DataSearchDataStateImplCopyWithImpl<$Res>
    extends _$SearchDataStateCopyWithImpl<$Res, _$DataSearchDataStateImpl>
    implements _$$DataSearchDataStateImplCopyWith<$Res> {
  __$$DataSearchDataStateImplCopyWithImpl(_$DataSearchDataStateImpl _value,
      $Res Function(_$DataSearchDataStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? trending = null,
    Object? genres = null,
  }) {
    return _then(_$DataSearchDataStateImpl(
      trending: null == trending
          ? _value._trending
          : trending // ignore: cast_nullable_to_non_nullable
              as List<Media>,
      genres: null == genres
          ? _value._genres
          : genres // ignore: cast_nullable_to_non_nullable
              as List<Genre>,
    ));
  }
}

/// @nodoc

class _$DataSearchDataStateImpl extends DataSearchDataState {
  const _$DataSearchDataStateImpl(
      {required final List<Media> trending, required final List<Genre> genres})
      : _trending = trending,
        _genres = genres,
        super._();

  final List<Media> _trending;
  @override
  List<Media> get trending {
    if (_trending is EqualUnmodifiableListView) return _trending;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_trending);
  }

  final List<Genre> _genres;
  @override
  List<Genre> get genres {
    if (_genres is EqualUnmodifiableListView) return _genres;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_genres);
  }

  @override
  String toString() {
    return 'SearchDataState.data(trending: $trending, genres: $genres)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataSearchDataStateImpl &&
            const DeepCollectionEquality().equals(other._trending, _trending) &&
            const DeepCollectionEquality().equals(other._genres, _genres));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_trending),
      const DeepCollectionEquality().hash(_genres));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataSearchDataStateImplCopyWith<_$DataSearchDataStateImpl> get copyWith =>
      __$$DataSearchDataStateImplCopyWithImpl<_$DataSearchDataStateImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> trending, List<Genre> genres) data,
  }) {
    return data(trending, genres);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> trending, List<Genre> genres)? data,
  }) {
    return data?.call(trending, genres);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> trending, List<Genre> genres)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(trending, genres);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitSearchDataState value) init,
    required TResult Function(DataSearchDataState value) data,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitSearchDataState value)? init,
    TResult? Function(DataSearchDataState value)? data,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitSearchDataState value)? init,
    TResult Function(DataSearchDataState value)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class DataSearchDataState extends SearchDataState {
  const factory DataSearchDataState(
      {required final List<Media> trending,
      required final List<Genre> genres}) = _$DataSearchDataStateImpl;
  const DataSearchDataState._() : super._();

  List<Media> get trending;
  List<Genre> get genres;
  @JsonKey(ignore: true)
  _$$DataSearchDataStateImplCopyWith<_$DataSearchDataStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
