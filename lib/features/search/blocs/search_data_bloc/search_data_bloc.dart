import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/provider/api/media/base.dart';

import '../../../../models/details/base_details/genre/genre.dart';
import '../../../../models/media/media.dart';
import '../../../../repo/internet_connection.dart';
import '../../../root/blocs/internet_connection_bloc/internet_connection_bloc.dart';
import '../../../settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';

part 'search_data_bloc.freezed.dart';

class SearchDataBloc extends Bloc<SearchDataEvent, SearchDataState> {
  SearchDataBloc({
    required this.internetBloc,
    required this.provider,
    required this.localizationBloc,
  }) : super(const SearchDataState.init()) {
    _updateDataWithLocale();
    _listenInternetConnection();
    on<_Init>((event, emit) async {
      final trending = await provider.getTrendingMedia();
      final genres = await provider.getGenres();
      emit(SearchDataState.data(trending: trending, genres: genres));
    });
  }

  final BaseProviderMedia provider;
  final InternetConnectionBloc internetBloc;
  final SettingsLocalizationBloc localizationBloc;
  late final StreamSubscription _subLocale;
  late final StreamSubscription _subInternet;

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) {
      if (event.connection != ConnectionStatus.offline) {
        add(const SearchDataEvent.init());
      }
    });
  }

  Future<void> _updateDataWithLocale() async =>
      _subLocale = localizationBloc.stream.listen((event) => add(const SearchDataEvent.init()));

  @override
  Future<void> close() {
    _subLocale.cancel();
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class SearchDataEvent with _$SearchDataEvent {
  const factory SearchDataEvent.init() = _Init;
}

@freezed
class SearchDataState with _$SearchDataState {
  const SearchDataState._();

  const factory SearchDataState.init() = InitSearchDataState;

  const factory SearchDataState.data({
    required List<Media> trending,
    required List<Genre> genres,
  }) = DataSearchDataState;

  T? maybeGenres<T extends List<Genre>>() => maybeWhen(
        data: (_, genres) {
          if (genres is T) {
            return genres;
          }
          return null;
        },
        orElse: () => null,
      );
}
