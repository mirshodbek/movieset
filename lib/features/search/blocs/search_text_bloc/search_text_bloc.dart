import 'package:movie_set/models/media/media.dart';
import 'package:rxdart/rxdart.dart';

import '../../../../provider/api/media/base.dart';

class SearchTextBloc {
  final Sink<String> onTextChanged;
  final Stream<SearchTextState> state;

  factory SearchTextBloc(BaseProviderMedia provider) {
    final onTextChanged = PublishSubject<String>();

    final state = onTextChanged
        .distinct()
        .debounceTime(const Duration(milliseconds: 250))
        .switchMap<SearchTextState>((String term) => _search(term, provider))
        .startWith(SearchNoTerm());

    return SearchTextBloc._(onTextChanged, state);
  }

  SearchTextBloc._(this.onTextChanged, this.state);

  void dispose() {
    onTextChanged.close();
  }

  static Stream<SearchTextState> _search(String term, BaseProviderMedia provider) => term.isEmpty
      ? Stream.value(SearchNoTerm())
      : Rx.fromCallable(() => provider.getSearchMedia(query: term))
          .map((result) => result.isEmpty ? SearchEmpty() : SearchPopulated(result))
          .startWith(SearchLoading())
          .onErrorReturn(SearchError());
}

class SearchTextState {}

class SearchLoading extends SearchTextState {}

class SearchError extends SearchTextState {}

class SearchNoTerm extends SearchTextState {}

class SearchPopulated extends SearchTextState {
  final List<Media> result;

  SearchPopulated(this.result);
}

class SearchEmpty extends SearchTextState {}
