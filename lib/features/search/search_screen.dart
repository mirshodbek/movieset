import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../router/router.dart';
import '../../widgets/main_app_bar.dart';
import 'blocs/search_text_bloc/search_text_bloc.dart';
import 'widgets/search_state_widgets.dart';
import 'widgets/search_text_field.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  late final SearchTextBloc searchTextBloc;

  @override
  void initState() {
    super.initState();
    searchTextBloc = RepositoryProvider.of<SearchTextBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppBar(
        primaryTitle: S.of(context).search,
        actions: [
          IconButton(
            onPressed: () {
              AppRouter.navigate(
                context,
                (configuration) => configuration.add(
                  FilterPage(),
                ),
              );
            },
            icon: const Icon(Icons.tune),
          ),
        ],
        backButton: false,
      ),
      body: StreamBuilder<SearchTextState>(
        stream: searchTextBloc.state,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final state = snapshot.requireData;
            return CustomScrollView(
              slivers: [
                SliverAppBar(
                  automaticallyImplyLeading: false,
                  pinned: true,
                  toolbarHeight: kToolbarHeight+20,
                  title: SearchTextField(
                    onChanged: searchTextBloc.onTextChanged.add,
                  ),
                ),
                SliverToBoxAdapter(
                  child: SearchStateWidgets(state: state),
                ),
              ],
            );
          }
          return const CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: SearchTextField(),
              ),
            ],
          );
        },
      ),
      bottomNavigationBar: AppNavBar(currentItem: context.appNavBarItem),
    );
  }
}
