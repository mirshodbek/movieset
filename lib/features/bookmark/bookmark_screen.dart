import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/bookmark/widgets/bookmark_list.dart';
import 'package:movie_set/features/bookmark/widgets/bookmark_shimmer_list.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/widgets/main_app_bar.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../widgets/empty_box.dart';
import 'bloc/bookmark_bloc.dart';

class BookmarkScreen extends StatelessWidget {
  const BookmarkScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppBar(
        primaryTitle: S.of(context).watchLater,
        actions: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              '${context.bookmarks?.length ?? Constants.empty}',
              style: context.textTheme.titleLarge,
              textAlign: TextAlign.center,
            ),
          ),
        ],
        backButton: false,
      ),
      body: AnimatedSwitcher(
        duration: const Duration(milliseconds: 300),
        child: BlocBuilder<BookmarkBloc, BookmarkState>(
          builder: (context, state) {
            return state.map(
              init: (_) => const BookmarkShimmerList(),
              data: (map) {
                if (map.bookmarks.isEmpty) {
                  return const Padding(
                    padding: EdgeInsets.all(16.0),
                    child: EmptyBox(
                      height: 200,
                      width: 200,
                    ),
                  );
                }
                return BookmarkList(bookmarks: map.bookmarks);
              },
            );
          },
        ),
      ),
      bottomNavigationBar: AppNavBar(currentItem: context.appNavBarItem),
    );
  }
}
