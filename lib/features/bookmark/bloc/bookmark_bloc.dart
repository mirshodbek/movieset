import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/provider/db_provider/database.dart';

import '../../../models/bookmark/bookmark.dart';
import '../../../repo/api/media/base.dart';
import '../../settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';

part 'bookmark_bloc.freezed.dart';

class BookmarkBloc extends Bloc<BookmarkEvent, BookmarkState> {
  BookmarkBloc({
    required this.repo,
    required this.dbProvider,
    required this.localizationBloc,
  }) : super(const BookmarkState.init()) {
    _init();
    _updateDataWithLocale();
    on<_Init>((event, emit) {
      emit(BookmarkState.data(bookmarks: event.bookmarks));
    });
    on<_Bookmark>((event, emit) async {
      if (event.add) {
        await dbProvider.insertBookmark(event.bookmark);
      } else {
        await dbProvider.deleteBookmark(event.bookmark.id);
      }
      final result = await dbProvider.getBookmarks();
      emit(BookmarkState.data(bookmarks: result));
    });
  }

  final BaseRepoMedia repo;
  final DbProvider dbProvider;
  final SettingsLocalizationBloc localizationBloc;
  late final StreamSubscription subscription;
  late final StreamSubscription subLocale;

  Future<void> _init() async {
    subscription = repo.getBookmarks().listen((it) {
      add(BookmarkEvent.init(bookmarks: it));
    });
  }

  Future<void> _updateDataWithLocale() async =>
      subLocale = localizationBloc.stream.listen((event) async => await _init());

  @override
  Future<void> close() {
    subscription.cancel();
    subLocale.cancel();
    return super.close();
  }
}

@freezed
class BookmarkEvent with _$BookmarkEvent {
  const factory BookmarkEvent.init({
    required List<Bookmark> bookmarks,
  }) = _Init;

  const factory BookmarkEvent.bookmark({
    required bool add,
    required Bookmark bookmark,
  }) = _Bookmark;
}

@freezed
class BookmarkState with _$BookmarkState {
  const BookmarkState._();

  const factory BookmarkState.init() = InitBookmarkState;

  const factory BookmarkState.data({
    required List<Bookmark> bookmarks,
  }) = DataBookmarkState;

  T? maybeBookmarks<T extends List<Bookmark>>() => maybeWhen(
        data: (model) {
          if (model is T) {
            return model;
          }
          return null;
        },
        orElse: () => null,
      );
}
