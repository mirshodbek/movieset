import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../../app_design/app_colors.dart';
import '../../../app_design/app_text_style.dart';

class BookmarkTrash extends StatelessWidget {
  const BookmarkTrash({
    Key? key,
    required this.onTap,
  }) : super(key: key);
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 16),
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: AppColors.statusError,
          ),
          child: Column(
            children: [
              const Icon(
                CupertinoIcons.trash,
                color: AppColors.primarySuperExtraLight,
              ),
              Text(
                S.of(context).delete,
                style: AppTextStyle.s14w500.copyWith(
                  color: AppColors.primarySuperExtraLight,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
