import 'package:flutter/material.dart';
import 'package:movie_set/app_design/app_colors.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../widgets/animations/shimmer/shimmer_loading.dart';

class BookmarkShimmerList extends StatelessWidget {
  const BookmarkShimmerList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: List.generate(
          4,
          (index) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Shimmer(
                    child: ShimmerLoading(
                      child: Container(
                        decoration: BoxDecoration(
                          color: AppColors.grayScaleWhite,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        width: 80,
                        height: 120,
                      ),
                    ),
                  ),
                  const SizedBox(width: 8),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Shimmer(
                        child: ShimmerLoading(
                          child: Container(
                            color: AppColors.grayScaleWhite,
                            height: 10,
                            width: context.width / 3,
                          ),
                        ),
                      ),
                      const SizedBox(height: 8),
                      Shimmer(
                        child: ShimmerLoading(
                          child: Container(
                            color: AppColors.grayScaleWhite,
                            height: 5,
                            width: context.width / 2,
                          ),
                        ),
                      ),
                      const SizedBox(height: 4),
                      Shimmer(
                        child: ShimmerLoading(
                          child: Container(
                            color: AppColors.grayScaleWhite,
                            height: 5,
                            width: context.width / 2.5,
                          ),
                        ),
                      ),
                      const SizedBox(height: 4),
                      Shimmer(
                        child: ShimmerLoading(
                          child: Container(
                            color: AppColors.grayScaleWhite,
                            height: 5,
                            width: context.width / 3,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
