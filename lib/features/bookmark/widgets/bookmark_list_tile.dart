import 'package:flutter/material.dart';
import 'package:movie_set/models/bookmark/bookmark.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/utils/extensions/int_extension.dart';
import 'package:movie_set/utils/extensions/list_extension.dart';
import 'package:movie_set/utils/extensions/string_extension.dart';

import '../../../app_design/app_colors.dart';
import '../../../constants/constants.dart';
import '../../../models/media/media.dart';
import '../../../router/pages.dart';
import '../../../router/router.dart';
import '../../../widgets/app_list_tile/parts/image_list_tile.dart';

class BookmarkListTile extends StatelessWidget {
  const BookmarkListTile({
    Key? key,
    required this.bookmark,
  }) : super(key: key);
  final Bookmark bookmark;

  String subTitle(Bookmark bookmark) {
    final genres = bookmark.genreNames?.convertListToString ?? Constants.empty;
    final season = bookmark.numberOfSeasons?.seasons ?? Constants.empty;
    final runtime = bookmark.runtime.runTimeToHourString;
    final series = bookmark.numberOfEpisodes.series;
    final year = bookmark.releaseDate?.yearOfDate ?? Constants.empty;
    final productCountries = bookmark.originCountry.convertListToString;
    final adult = bookmark.adult ?? false
        ? ' • ${Constants.plusSixteen}'
        : Constants.empty;
    return '$year • $productCountries • $genres • $season$series$runtime$adult';
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        AppRouter.navigate(
          context,
          (configuration) => configuration.add(
            MediaDetailsPage({
              Constants.dataOne: Media(
                id: bookmark.id,
                title: bookmark.title,
                backdropPath: bookmark.backdropPath,
                posterPath: bookmark.posterPath,
                originCountry: bookmark.originCountry,
                releaseDate: bookmark.releaseDate,
                adult: bookmark.adult,
              ),
              Constants.dataTwo: bookmark.type,
            }),
          ),
        );
      },
      child: Container(
        width: context.width,
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: 80,
              child: ImageListTile(
                width: 80,
                height: 120,
                imagePath: bookmark.posterPath ??
                    bookmark.backdropPath ??
                    Constants.empty,
              ),
            ),
            const SizedBox(width: 8),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    bookmark.title ?? Constants.empty,
                    style: context.primaryTextTheme.labelSmall,
                  ),
                  const SizedBox(height: 16),
                  Text(
                    subTitle(bookmark),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: context.textTheme.bodySmall?.copyWith(
                      color: context.light
                          ? AppColors.primaryLight
                          : AppColors.grayScaleBorderLightGrey,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
