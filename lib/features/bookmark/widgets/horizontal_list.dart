import 'package:flutter/material.dart';

class HorizontalList extends StatefulWidget {
  const HorizontalList({
    Key? key,
    required this.children,
  }) : super(key: key);
  final List<Widget> children;

  @override
  State<HorizontalList> createState() => _HorizontalListState();
}

class _HorizontalListState extends State<HorizontalList> {
  late final ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()
      ..addListener(
        () {
          if (_scrollController.position.maxScrollExtent == _scrollController.offset) {
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
              _setInitialOffsetLater();
            });
          } else {
            _scrollController.position.isScrollingNotifier.addListener(
              () {
                if (!_scrollController.position.isScrollingNotifier.value &&
                    _scrollController.position.maxScrollExtent != _scrollController.offset) {
                  _setInitialOffset();
                }
              },
            );
          }
        },
      );
  }

  void _setInitialOffset() {
    _scrollController.animateTo(
      0,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeInOut,
    );
  }

  Future<void> _setInitialOffsetLater() async =>
      await Future.delayed(const Duration(seconds: 2)).whenComplete(() => _setInitialOffset());

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      controller: _scrollController,
      child: Row(
        children: widget.children,
      ),
    );
  }
}
