import 'package:flutter/material.dart';

class BookmarkSlideTransition extends StatelessWidget {
  const BookmarkSlideTransition({
    required this.animation,
    required this.child,
    this.initWidget = false,
    Key? key,
  }) : super(key: key);
  final Animation<double> animation;
  final Widget child;
  final bool initWidget;

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: CurvedAnimation(
        curve: Curves.easeOut,
        parent: animation,
      ).drive(
        Tween<Offset>(
          begin: const Offset(-1, 0),
          end: Offset.zero,
        ),
      ),
      child: child,
    );
  }
}
