import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_set/features/bookmark/bloc/bookmark_bloc.dart';
import 'package:movie_set/features/bookmark/widgets/bookmark_trash.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../app_design/app_colors.dart';
import '../../../models/bookmark/bookmark.dart';
import 'bookmark_list_tile.dart';
import 'bookmark_slide_transition.dart';
import 'horizontal_list.dart';

class BookmarkList extends StatefulWidget {
  const BookmarkList({
    Key? key,
    required this.bookmarks,
  }) : super(key: key);
  final List<Bookmark>? bookmarks;

  @override
  State<BookmarkList> createState() => _BookmarkListState();
}

class _BookmarkListState extends State<BookmarkList> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();
  final List<Widget> _listBookmarks = <Widget>[];
  late final _bookmarks = List.from(widget.bookmarks!);

  @override
  void initState() {
    super.initState();
    _loadBookmarks();
  }

  void _loadBookmarks() {
    for (var item in _bookmarks) {
      _listBookmarks.add(BookmarkListTile(bookmark: item));
      _listKey.currentState?.insertItem(_listBookmarks.length - 1);
    }
  }

  Future<void> _unloadItem(Bookmark item) async {
    setState(() {
      final indexWhere = _bookmarks.indexWhere((e) => e.id == item.id);
      final deletedBookmark = _listBookmarks.removeAt(indexWhere);
      _listKey.currentState?.removeItem(
        indexWhere,
        (context, animation) {
          return BookmarkSlideTransition(
            animation: animation,
            child: deletedBookmark,
          );
        },
      );
      _bookmarks.removeWhere((it) => it.id == item.id);
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedList(
      key: _listKey,
      initialItemCount: _listBookmarks.length,
      itemBuilder: (context, index, animation) {
        final bookmark = _bookmarks[index];
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 2),
          child: ColoredBox(
            color: context.light
                ? AppColors.primarySuperExtraLight
                : AppColors.primaryDark,
            child: HorizontalList(
              children: [
                BookmarkListTile(bookmark: bookmark),
                BookmarkTrash(
                  onTap: () async {
                    await _unloadItem(bookmark).then(
                      (value) => context.bookmarkBloc.add(
                        BookmarkEvent.bookmark(
                          add: false,
                          bookmark: bookmark,
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
