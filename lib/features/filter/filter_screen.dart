import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../../widgets/app_title_category.dart';
import '../../../widgets/main_app_bar.dart';
import '../root/widgets/app_nav_bar.dart';
import 'bloc/filter_bloc.dart';
import 'widgets/filter_button.dart';
import 'widgets/filter_countries.dart';
import 'widgets/filter_genres.dart';
import 'widgets/filter_ratings.dart';
import 'widgets/filter_years.dart';
import 'widgets/type_media_list.dart';

class FilterScreen extends StatelessWidget {
  const FilterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppBar(
        primaryTitle: S.of(context).filter,
        actions: [
          TextButton(
            onPressed: () => context.read<FilterBloc>().add(const FilterEvent.clear()),
            child: Text(
              S.of(context).clear,
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          const TypeMediaList(),
          AppTitleCategory(title: S.of(context).filter),
          const SizedBox(height: 8),
          const FilterGenres(),
          FilterYears(),
          FilterCountries(),
          FilterRatings(),
          const Spacer(),
          const FilterButton(),
        ],
      ),
      bottomNavigationBar: const AppNavBar(root: false),
    );
  }
}
