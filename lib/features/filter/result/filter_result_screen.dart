import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/models/filter/filter.dart';
import 'package:movie_set/provider/api/media/provider.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/utils/extensions/string_extension.dart';
import 'package:movie_set/widgets/empty_box.dart';

import '../../../constants/constants.dart';
import '../../../router/pages.dart';
import '../../../router/router.dart';
import '../../../widgets/animations/shimmer/shimmer_list_tile.dart';
import '../../../widgets/app_list_tile/app_list_tile.dart';
import '../../../widgets/app_list_tile/app_list_tile_item.dart';
import '../../../widgets/main_app_bar.dart';
import '../../../widgets/pull_to_refresh/pull_to_refresh.dart';
import '../../root/widgets/app_nav_bar.dart';
import 'bloc/filter_result_bloc.dart';

class FilterResultScreen extends StatelessWidget {
  const FilterResultScreen({
    Key? key,
    required this.filter,
  }) : super(key: key);
  final Filter? filter;

  int? totalPages(int? first, int? second) {
    return (first ?? 0) >= (second ?? 0) ? first : second;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FilterResultBloc>(
      create: (context) => FilterResultBloc(
        provider: RepositoryProvider.of<ProviderMedia>(context),
        handleException: context.readException,
        filter: filter,
      )..add(const FilterResultEvent.init()),
      child: Builder(
        builder: (context) {
          return Scaffold(
            appBar: MainAppBar(
              primaryTitle: filter?.typeMedia?.convertTypeToLocal ?? S.of(context).filter,
            ),
            body: BlocBuilder<FilterResultBloc, FilterResultState>(
              builder: (context, state) {
                return state.map(
                  init: (_) {
                    return SingleChildScrollView(
                      child: Wrap(
                        children: List.generate(
                          9,
                          (index) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 4),
                              child: ShimmerListTile(
                                width: context.widthGridViewListTile(3),
                                height: 180,
                              ),
                            );
                          },
                        ),
                      ),
                    );
                  },
                  data: (map) {
                    final media = map.media;
                    if (media.isEmpty) {
                      return Container(
                        alignment: Alignment.center,
                        child: const EmptyBox(height: 200, width: 200),
                      );
                    }
                    return PullToRefresh(
                      widget: [
                        SliverToBoxAdapter(
                          child: Wrap(
                            children: List.generate(
                              media.length,
                              (index) {
                                final item = media[index];
                                return Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8),
                                  child: AppListTile(
                                    height: 180,
                                    width: context.widthGridViewListTile(3),
                                    onTap: () {
                                      AppRouter.navigate(
                                        context,
                                        (configuration) => configuration.add(
                                          MediaDetailsPage({
                                            Constants.dataOne: item,
                                            Constants.dataTwo: item.type,
                                          }),
                                        ),
                                      );
                                    },
                                    item: AppListTileItem(
                                      imagePath: item.posterPath ?? item.backdropPath,
                                      voteAverage: item.voteAverage,
                                      smallTextStyleTitle: true,
                                      adult: item.adult,
                                      title: item.title,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                      totalPages: totalPages(
                        media.firstOrNull?.totalPages,
                        media.lastOrNull?.totalPages,
                      ),
                      loadNextPage: (page) {
                        context.read<FilterResultBloc>().add(FilterResultEvent.load(page++));
                      },
                      loadFirstPage: () {
                        context.read<FilterResultBloc>().add(const FilterResultEvent.init());
                      },
                    );
                  },
                );
              },
            ),
            bottomNavigationBar: const AppNavBar(root: false),
          );
        },
      ),
    );
  }
}
