import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/models/filter/filter.dart';
import 'package:movie_set/provider/api/media/base.dart';

import '../../../../models/media/media.dart';

part 'filter_result_bloc.freezed.dart';

class FilterResultBloc extends Bloc<FilterResultEvent, FilterResultState> {
  FilterResultBloc({
    required this.filter,
    required this.provider,
    required this.handleException,
  }) : super(const FilterResultState.init()) {
    on<_Init>((event, emit) async {
      try {
        final result = await provider.getDiscoverMedia(filter: filter);
        emit(FilterResultState.data(media: result));
      } on Exception catch (exception) {
        handleException(exception);
      }
    });

    on<_Load>((event, emit) async {
      final result = await provider.getDiscoverMedia(filter: filter, page: event.page);
      emit(FilterResultState.data(media: [...?state.maybeMedia(), ...result]));
    });
  }

  final Filter? filter;
  final BaseProviderMedia provider;
  final Function(Exception) handleException;
}

@freezed
class FilterResultEvent with _$FilterResultEvent {
  const factory FilterResultEvent.init() = _Init;

  const factory FilterResultEvent.load(int page) = _Load;
}

@freezed
class FilterResultState with _$FilterResultState {
  const FilterResultState._();

  const factory FilterResultState.init() = InitFilterResultState;

  const factory FilterResultState.data({required List<Media> media}) = DataFilterResultState;

  T? maybeMedia<T extends List<Media>>() => maybeWhen(
        data: (media) {
          if (media is T) {
            return media;
          }

          return null;
        },
        orElse: () => null,
      );
}
