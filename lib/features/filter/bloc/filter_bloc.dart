import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/models/filter/filter.dart';
import 'package:movie_set/provider/db_provider/database.dart';

import '../../../constants/constants.dart';

part 'filter_bloc.freezed.dart';

part 'parts/update.dart';

class FilterBloc extends Bloc<FilterEvent, FilterState> {
  FilterBloc(this.dbProvider) : super(const FilterState.data()) {
    on<_Init>((event, emit) async {
      final result = await dbProvider.getFilter();
      emit(state.copyWith(filter: result));
    });
    on<_Clear>((event, emit) async {
      await dbProvider.deleteFilter();
      emit(state.copyWith(filter: null));
    });
    on<_Update>(_update);
  }

  final DbProvider dbProvider;
}

@freezed
class FilterEvent with _$FilterEvent {
  const factory FilterEvent.init() = _Init;

  const factory FilterEvent.clear() = _Clear;

  const factory FilterEvent.update({
    String? type,
    List<String?>? genreIds,
    int? years,
    int? country,
    int? ratings,
  }) = _Update;
}

@freezed
class FilterState with _$FilterState {
  const factory FilterState.data({Filter? filter}) = DataFilterState;
}
