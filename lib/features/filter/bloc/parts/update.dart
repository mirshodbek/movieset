part of '../filter_bloc.dart';

extension UpdateFilter on FilterBloc {
  Future<void> _update(_Update event, Emitter<FilterState> emit) async {
    final result = await dbProvider.insertFilter(
      Filter(
        typeMedia: event.type ?? state.filter?.typeMedia,
        genreIds: event.genreIds?.contains(Constants.minusThree.toString()) ?? false
            ? null
            : event.genreIds ?? state.filter?.genreIds,
        years: event.years == Constants.minusTwo ? null : event.years ?? state.filter?.years,
        country:
            event.country == Constants.minusTwo ? null : event.country ?? state.filter?.country,
        ratings:
            event.ratings == Constants.minusTwo ? null : event.ratings ?? state.filter?.ratings,
      ),
    );
    emit(state.copyWith(filter: result));
  }
}
