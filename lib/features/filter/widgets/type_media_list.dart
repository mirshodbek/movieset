import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/utils/extensions/string_extension.dart';
import 'package:movie_set/widgets/app_title_category.dart';

import '../../../../app_design/app_colors.dart';
import '../bloc/filter_bloc.dart';

class TypeMediaList extends StatelessWidget {
  const TypeMediaList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilterBloc, FilterState>(
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppTitleCategory(title: S.of(context).category),
            const SizedBox(height: 8),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Wrap(
                children: [
                  S.of(context).movies,
                  S.of(context).serials,
                  S.of(context).anime,
                  S.of(context).animations,
                ].map(
                  (it) {
                    final selected = state.filter?.typeMedia?.convertTypeToLocal == it;
                    return Padding(
                      padding: const EdgeInsets.only(right: 8, bottom: 8),
                      child: InkWell(
                        onTap: () {
                          context.read<FilterBloc>().add(
                                FilterEvent.update(
                                  type: it.convertLocalToType,
                                ),
                              );
                        },
                        borderRadius: BorderRadius.circular(8),
                        child: Container(
                          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: selected
                                ? context.dark
                                    ? AppColors.primaryMain
                                    : AppColors.secondaryMain
                                : context.colorScheme.onTertiary,
                            border: Border.all(
                              color:
                                  selected ? AppColors.secondaryMain : context.colorScheme.onTertiary,
                            ),
                          ),
                          child: Text(
                            it,
                            style: context.textTheme.titleSmall?.copyWith(
                              color: context.light
                                  ? AppColors.primaryMain
                                  : AppColors.primarySuperExtraLight,
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ).toList(),
              ),
            ),
          ],
        );
      },
    );
  }
}
