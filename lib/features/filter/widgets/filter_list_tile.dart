import 'package:flutter/material.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../../constants/constants.dart';

class FilterListTile extends StatelessWidget {
  const FilterListTile({
    Key? key,
    this.count,
    required this.title,
    required this.onTap,
  }) : super(key: key);
  final String title;
  final String? count;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: ListTile(
        onTap: onTap,
        title: Row(
          children: [
            Expanded(
              child: Text(
                title,
                style: context.textTheme.titleSmall,
              ),
            ),
            Text(
              count ?? Constants.empty,
              style: context.textTheme.titleSmall,
            ),
          ],
        ),
        trailing: const Icon(Icons.keyboard_arrow_right),
      ),
    );
  }
}
