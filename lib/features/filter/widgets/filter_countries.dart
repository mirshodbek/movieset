import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../../constants/constants.dart';
import '../../../router/router.dart';
import '../../../widgets/bottom_sheet/app_bottom_sheet.dart';
import '../../../widgets/bottom_sheet/bottom_sheet_item.dart';
import '../bloc/filter_bloc.dart';
import 'filter_list_tile.dart';

class FilterCountries extends StatelessWidget {
  FilterCountries({Key? key}) : super(key: key);
  final List<BottomSheetItem> countries = <BottomSheetItem>[
    BottomSheetItem(
      id: 0,
      name: S.current.azerbaijan,
    ),
    BottomSheetItem(
      id: 1,
      name: S.current.belarus,
    ),
    BottomSheetItem(
      id: 2,
      name: S.current.czech,
    ),
    BottomSheetItem(
      id: 3,
      name: S.current.dania,
    ),
    BottomSheetItem(
      id: 4,
      name: S.current.estonia,
    ),
    BottomSheetItem(
      id: 5,
      name: S.current.france,
    ),
    BottomSheetItem(
      id: 6,
      name: S.current.germany,
    ),
    BottomSheetItem(
      id: 7,
      name: S.current.india,
    ),
    BottomSheetItem(
      id: 8,
      name: S.current.indonesia,
    ),
    BottomSheetItem(
      id: 9,
      name: S.current.italy,
    ),
    BottomSheetItem(
      id: 10,
      name: S.current.japan,
    ),
    BottomSheetItem(
      id: 11,
      name: S.current.kazakhstan,
    ),
    BottomSheetItem(
      id: 12,
      name: S.current.kyrgyzstan,
    ),
    BottomSheetItem(
      id: 13,
      name: S.current.mongolia,
    ),
    BottomSheetItem(
      id: 14,
      name: S.current.norway,
    ),
    BottomSheetItem(
      id: 15,
      name: S.current.poland,
    ),
    BottomSheetItem(
      id: 16,
      name: S.current.romania,
    ),
    BottomSheetItem(
      id: 17,
      name: S.current.russia,
    ),
    BottomSheetItem(
      id: 18,
      name: S.current.slovakia,
    ),
    BottomSheetItem(
      id: 19,
      name: S.current.southKorea,
    ),
    BottomSheetItem(
      id: 20,
      name: S.current.sweden,
    ),
    BottomSheetItem(
      id: 21,
      name: S.current.turkey,
    ),
    BottomSheetItem(
      id: 22,
      name: S.current.ukraine,
    ),
    BottomSheetItem(
      id: 23,
      name: S.current.usa,
    ),
    BottomSheetItem(
      id: 24,
      name: S.current.uzbekistan,
    ),
    BottomSheetItem(
      id: 25,
      name: S.current.vietnam,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilterBloc, FilterState>(
      builder: (context, state) {
        return FilterListTile(
          title: S.of(context).countries,
          count: countries.firstWhereOrNull((it) => it.id == state.filter?.country)?.name ??
              Constants.empty,
          onTap: () {
            AppRouter.showBottomSheet(
              context: context,
              isScrollControlled: true,
              builder: (_) {
                return AppBottomSheet(
                  initialChildSize: .9,
                  items: countries,
                  initial: [BottomSheetItem(id: state.filter?.country ?? Constants.minusOne)],
                  type: Constants.typeOneCheck,
                  titleButton: S.of(context).apply,
                  primaryTitle: S.of(context).countries,
                  onTap: (items) => context.read<FilterBloc>().add(
                        FilterEvent.update(
                          country:
                              (items?.isNotEmpty ?? false) ? items?.first.id : Constants.minusTwo,
                        ),
                      ),
                );
              },
            );
          },
        );
      },
    );
  }
}
