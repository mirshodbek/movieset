import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../../constants/constants.dart';
import '../../../router/router.dart';
import '../../../widgets/bottom_sheet/app_bottom_sheet.dart';
import '../../../widgets/bottom_sheet/bottom_sheet_item.dart';
import '../bloc/filter_bloc.dart';
import 'filter_list_tile.dart';

class FilterRatings extends StatelessWidget {
  FilterRatings({Key? key}) : super(key: key);
  final List<BottomSheetItem> ratings = <BottomSheetItem>[
    BottomSheetItem(
      id: 9,
      name: S.current.over(9),
    ),
    BottomSheetItem(
      id: 8,
      name: S.current.over(8),
    ),
    BottomSheetItem(
      id: 7,
      name: S.current.over(7),
    ),
    BottomSheetItem(
      id: 6,
      name: S.current.over(6),
    ),
    BottomSheetItem(
      id: 5,
      name: S.current.over(5),
    ),
    BottomSheetItem(
      id: 4,
      name: S.current.over(4),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilterBloc, FilterState>(
      builder: (context, state) {
        return FilterListTile(
          title: S.of(context).rating,
          count: ratings.firstWhereOrNull((it) => it.id == state.filter?.ratings)?.name ??
              Constants.empty,
          onTap: () {
            AppRouter.showBottomSheet(
              context: context,
              isScrollControlled: true,
              builder: (_) {
                return AppBottomSheet(
                  initialChildSize: .75,
                  items: ratings,
                  initial: [BottomSheetItem(id: state.filter?.ratings ?? Constants.minusOne)],
                  type: Constants.typeOneCheck,
                  titleButton: S.of(context).apply,
                  primaryTitle: S.of(context).rating,
                  onTap: (items) => context.read<FilterBloc>().add(
                        FilterEvent.update(
                          ratings:
                              (items?.isNotEmpty ?? false) ? items?.first.id : Constants.minusTwo,
                        ),
                      ),
                );
              },
            );
          },
        );
      },
    );
  }
}
