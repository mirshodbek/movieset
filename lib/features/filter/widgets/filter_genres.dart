import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/router/router.dart';
import 'package:movie_set/widgets/bottom_sheet/bottom_sheet_item.dart';

import '../../../../widgets/animations/spin_kit_pouring_hour_glass_refined.dart';
import '../../../../widgets/bottom_sheet/app_bottom_sheet.dart';
import '../../search/blocs/search_data_bloc/search_data_bloc.dart';
import '../bloc/filter_bloc.dart';
import 'filter_list_tile.dart';

class FilterGenres extends StatelessWidget {
  const FilterGenres({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilterBloc, FilterState>(
      builder: (context, bloc) {
        return FilterListTile(
          title: S.of(context).genres,
          count: bloc.filter?.genreIds?.length.toString(),
          onTap: () async {
            await AppRouter.showBottomSheet(
              context: context,
              isScrollControlled: true,
              builder: (_) {
                return BlocProvider.value(
                  value: context.read<SearchDataBloc>(),
                  child: BlocBuilder<SearchDataBloc, SearchDataState>(
                    builder: (context, state) {
                      return state.map(
                        init: (_) {
                          return AppBottomSheet(
                            initialChildSize: .9,
                            items: const [],
                            loading: const SpinKitPouringHourGlassRefined(),
                            titleButton: S.of(context).apply,
                            primaryTitle: S.of(context).genres,
                          );
                        },
                        data: (map) {
                          return AppBottomSheet(
                            initialChildSize: .9,
                            onTap: (items) {
                              context.read<FilterBloc>().add(
                                    FilterEvent.update(
                                      genreIds: ((items?.isEmpty ?? true)
                                              ? [const BottomSheetItem(id: Constants.minusThree)]
                                              : items)
                                          ?.map((e) => e.id.toString())
                                          .toList(),
                                    ),
                                  );
                            },
                            initial: bloc.filter?.genreIds?.map(
                              (it) {
                                return BottomSheetItem(
                                  id: int.tryParse(it ?? '${Constants.minusTwo}') ??
                                      Constants.minusTwo,
                                );
                              },
                            ).toList(),
                            items: map.genres.map(
                              (it) {
                                return BottomSheetItem(
                                  id: it.id ?? Constants.minusOne,
                                  name: it.name,
                                );
                              },
                            ).toList(),
                            titleButton: S.of(context).apply,
                            primaryTitle: S.of(context).genres,
                          );
                        },
                      );
                    },
                  ),
                );
              },
            );
          },
        );
      },
    );
  }
}
