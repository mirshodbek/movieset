import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/router.dart';
import 'package:movie_set/widgets/app_elevated_button.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../../constants/constants.dart';
import '../bloc/filter_bloc.dart';

class FilterButton extends StatelessWidget {
  const FilterButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilterBloc, FilterState>(
      builder: (context, state) {
        return AppElevatedButton(
          title: S.of(context).showResult,
          onPressed: () {
            AppRouter.navigate(
              context,
              (configuration) => configuration.add(
                FilterResultPage({
                  Constants.dataOne: state.filter,
                }),
              ),
            );
          },
        );
      },
    );
  }
}
