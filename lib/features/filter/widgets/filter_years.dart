import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../../../router/router.dart';
import '../../../../widgets/bottom_sheet/app_bottom_sheet.dart';
import '../../../constants/constants.dart';
import '../../../widgets/bottom_sheet/bottom_sheet_item.dart';
import '../bloc/filter_bloc.dart';
import 'filter_list_tile.dart';

class FilterYears extends StatelessWidget {
  FilterYears({Key? key}) : super(key: key);

  final List<BottomSheetItem> years = <BottomSheetItem>[
    BottomSheetItem(
      id: 2020,
      name: S.current.year(2020),
    ),
    BottomSheetItem(
      id: 2010,
      name: S.current.year(2010),
    ),
    BottomSheetItem(
      id: 2000,
      name: S.current.year(2000),
    ),
    BottomSheetItem(
      id: 1990,
      name: S.current.year(1990),
    ),
    BottomSheetItem(
      id: 1980,
      name: S.current.year(1980),
    ),
    BottomSheetItem(
      id: 1970,
      name: S.current.year(1970),
    ),
    BottomSheetItem(
      id: 1960,
      name: S.current.year(1960),
    ),
    BottomSheetItem(
      id: 1950,
      name: S.current.year(1950),
    ),
    BottomSheetItem(
      id: 1940,
      name: S.current.year(1940),
    ),
    BottomSheetItem(
      id: 1930,
      name: S.current.year(1930),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilterBloc, FilterState>(
      builder: (context, state) {
        return FilterListTile(
          title: S.of(context).years,
          count: state.filter?.years?.toString(),
          onTap: () async {
            await AppRouter.showBottomSheet(
              context: context,
              isScrollControlled: true,
              builder: (_) {
                return AppBottomSheet(
                  initialChildSize: .9,
                  items: years,
                  initial: [BottomSheetItem(id: state.filter?.years ?? Constants.minusOne)],
                  type: Constants.typeOneCheck,
                  titleButton: S.of(context).apply,
                  primaryTitle: S.of(context).years,
                  onTap: (items) => context.read<FilterBloc>().add(
                        FilterEvent.update(
                          years:
                              (items?.isNotEmpty ?? false) ? items?.first.id : Constants.minusTwo,
                        ),
                      ),
                );
              },
            );
          },
        );
      },
    );
  }
}
