import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar_item.dart';

class AppNavBarCubit extends Cubit<AppNavBarState> {
  AppNavBarCubit() : super(const AppNavBarState(item: AppNavBarItem.home));

  void onChanged(AppNavBarItem item) => emit(
        state.copyWith(
          key: UniqueKey(),
          item: item,
        ),
      );
}

class AppNavBarState extends Equatable {
  final Key? key;
  final AppNavBarItem item;

  const AppNavBarState({
    this.key,
    required this.item,
  });

  @override
  List<Object?> get props => [key, item];

  AppNavBarState copyWith({
    Key? key,
    AppNavBarItem? item,
  }) {
    return AppNavBarState(
      key: key ?? this.key,
      item: item ?? this.item,
    );
  }
}
