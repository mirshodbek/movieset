// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'exception_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ExceptionEvent {
  Exception get exception => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Exception exception) read,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Exception exception)? read,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Exception exception)? read,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Read value) read,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Read value)? read,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Read value)? read,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ExceptionEventCopyWith<ExceptionEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExceptionEventCopyWith<$Res> {
  factory $ExceptionEventCopyWith(
          ExceptionEvent value, $Res Function(ExceptionEvent) then) =
      _$ExceptionEventCopyWithImpl<$Res, ExceptionEvent>;
  @useResult
  $Res call({Exception exception});
}

/// @nodoc
class _$ExceptionEventCopyWithImpl<$Res, $Val extends ExceptionEvent>
    implements $ExceptionEventCopyWith<$Res> {
  _$ExceptionEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? exception = null,
  }) {
    return _then(_value.copyWith(
      exception: null == exception
          ? _value.exception
          : exception // ignore: cast_nullable_to_non_nullable
              as Exception,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ReadImplCopyWith<$Res>
    implements $ExceptionEventCopyWith<$Res> {
  factory _$$ReadImplCopyWith(
          _$ReadImpl value, $Res Function(_$ReadImpl) then) =
      __$$ReadImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Exception exception});
}

/// @nodoc
class __$$ReadImplCopyWithImpl<$Res>
    extends _$ExceptionEventCopyWithImpl<$Res, _$ReadImpl>
    implements _$$ReadImplCopyWith<$Res> {
  __$$ReadImplCopyWithImpl(_$ReadImpl _value, $Res Function(_$ReadImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? exception = null,
  }) {
    return _then(_$ReadImpl(
      exception: null == exception
          ? _value.exception
          : exception // ignore: cast_nullable_to_non_nullable
              as Exception,
    ));
  }
}

/// @nodoc

class _$ReadImpl with DiagnosticableTreeMixin implements _Read {
  const _$ReadImpl({required this.exception});

  @override
  final Exception exception;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ExceptionEvent.read(exception: $exception)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ExceptionEvent.read'))
      ..add(DiagnosticsProperty('exception', exception));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadImpl &&
            (identical(other.exception, exception) ||
                other.exception == exception));
  }

  @override
  int get hashCode => Object.hash(runtimeType, exception);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ReadImplCopyWith<_$ReadImpl> get copyWith =>
      __$$ReadImplCopyWithImpl<_$ReadImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Exception exception) read,
  }) {
    return read(exception);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Exception exception)? read,
  }) {
    return read?.call(exception);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Exception exception)? read,
    required TResult orElse(),
  }) {
    if (read != null) {
      return read(exception);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Read value) read,
  }) {
    return read(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Read value)? read,
  }) {
    return read?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Read value)? read,
    required TResult orElse(),
  }) {
    if (read != null) {
      return read(this);
    }
    return orElse();
  }
}

abstract class _Read implements ExceptionEvent {
  const factory _Read({required final Exception exception}) = _$ReadImpl;

  @override
  Exception get exception;
  @override
  @JsonKey(ignore: true)
  _$$ReadImplCopyWith<_$ReadImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ExceptionState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(Key key, String message) invalidApiKey,
    required TResult Function(Key key, String message)
        resourceRequestedNotFound,
    required TResult Function(Key key, String message) noInternetException,
    required TResult Function(Key key, String message) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(Key key, String message)? invalidApiKey,
    TResult? Function(Key key, String message)? resourceRequestedNotFound,
    TResult? Function(Key key, String message)? noInternetException,
    TResult? Function(Key key, String message)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(Key key, String message)? invalidApiKey,
    TResult Function(Key key, String message)? resourceRequestedNotFound,
    TResult Function(Key key, String message)? noInternetException,
    TResult Function(Key key, String message)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitExceptionState value) init,
    required TResult Function(InvalidApiKeyExceptionState value) invalidApiKey,
    required TResult Function(ResourceRequestedNotFoundExceptionState value)
        resourceRequestedNotFound,
    required TResult Function(noInternetExceptionExceptionState value)
        noInternetException,
    required TResult Function(UnknownErrorExceptionState value) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitExceptionState value)? init,
    TResult? Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult? Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult? Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult? Function(UnknownErrorExceptionState value)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitExceptionState value)? init,
    TResult Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult Function(UnknownErrorExceptionState value)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExceptionStateCopyWith<$Res> {
  factory $ExceptionStateCopyWith(
          ExceptionState value, $Res Function(ExceptionState) then) =
      _$ExceptionStateCopyWithImpl<$Res, ExceptionState>;
}

/// @nodoc
class _$ExceptionStateCopyWithImpl<$Res, $Val extends ExceptionState>
    implements $ExceptionStateCopyWith<$Res> {
  _$ExceptionStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitExceptionStateImplCopyWith<$Res> {
  factory _$$InitExceptionStateImplCopyWith(_$InitExceptionStateImpl value,
          $Res Function(_$InitExceptionStateImpl) then) =
      __$$InitExceptionStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitExceptionStateImplCopyWithImpl<$Res>
    extends _$ExceptionStateCopyWithImpl<$Res, _$InitExceptionStateImpl>
    implements _$$InitExceptionStateImplCopyWith<$Res> {
  __$$InitExceptionStateImplCopyWithImpl(_$InitExceptionStateImpl _value,
      $Res Function(_$InitExceptionStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitExceptionStateImpl extends InitExceptionState
    with DiagnosticableTreeMixin {
  const _$InitExceptionStateImpl() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ExceptionState.init()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'ExceptionState.init'));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitExceptionStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(Key key, String message) invalidApiKey,
    required TResult Function(Key key, String message)
        resourceRequestedNotFound,
    required TResult Function(Key key, String message) noInternetException,
    required TResult Function(Key key, String message) unknownError,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(Key key, String message)? invalidApiKey,
    TResult? Function(Key key, String message)? resourceRequestedNotFound,
    TResult? Function(Key key, String message)? noInternetException,
    TResult? Function(Key key, String message)? unknownError,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(Key key, String message)? invalidApiKey,
    TResult Function(Key key, String message)? resourceRequestedNotFound,
    TResult Function(Key key, String message)? noInternetException,
    TResult Function(Key key, String message)? unknownError,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitExceptionState value) init,
    required TResult Function(InvalidApiKeyExceptionState value) invalidApiKey,
    required TResult Function(ResourceRequestedNotFoundExceptionState value)
        resourceRequestedNotFound,
    required TResult Function(noInternetExceptionExceptionState value)
        noInternetException,
    required TResult Function(UnknownErrorExceptionState value) unknownError,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitExceptionState value)? init,
    TResult? Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult? Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult? Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult? Function(UnknownErrorExceptionState value)? unknownError,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitExceptionState value)? init,
    TResult Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult Function(UnknownErrorExceptionState value)? unknownError,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitExceptionState extends ExceptionState {
  const factory InitExceptionState() = _$InitExceptionStateImpl;
  const InitExceptionState._() : super._();
}

/// @nodoc
abstract class _$$InvalidApiKeyExceptionStateImplCopyWith<$Res> {
  factory _$$InvalidApiKeyExceptionStateImplCopyWith(
          _$InvalidApiKeyExceptionStateImpl value,
          $Res Function(_$InvalidApiKeyExceptionStateImpl) then) =
      __$$InvalidApiKeyExceptionStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Key key, String message});
}

/// @nodoc
class __$$InvalidApiKeyExceptionStateImplCopyWithImpl<$Res>
    extends _$ExceptionStateCopyWithImpl<$Res,
        _$InvalidApiKeyExceptionStateImpl>
    implements _$$InvalidApiKeyExceptionStateImplCopyWith<$Res> {
  __$$InvalidApiKeyExceptionStateImplCopyWithImpl(
      _$InvalidApiKeyExceptionStateImpl _value,
      $Res Function(_$InvalidApiKeyExceptionStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? key = null,
    Object? message = null,
  }) {
    return _then(_$InvalidApiKeyExceptionStateImpl(
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as Key,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$InvalidApiKeyExceptionStateImpl extends InvalidApiKeyExceptionState
    with DiagnosticableTreeMixin {
  const _$InvalidApiKeyExceptionStateImpl(
      {required this.key, required this.message})
      : super._();

  @override
  final Key key;
  @override
  final String message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ExceptionState.invalidApiKey(key: $key, message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ExceptionState.invalidApiKey'))
      ..add(DiagnosticsProperty('key', key))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InvalidApiKeyExceptionStateImpl &&
            (identical(other.key, key) || other.key == key) &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, key, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InvalidApiKeyExceptionStateImplCopyWith<_$InvalidApiKeyExceptionStateImpl>
      get copyWith => __$$InvalidApiKeyExceptionStateImplCopyWithImpl<
          _$InvalidApiKeyExceptionStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(Key key, String message) invalidApiKey,
    required TResult Function(Key key, String message)
        resourceRequestedNotFound,
    required TResult Function(Key key, String message) noInternetException,
    required TResult Function(Key key, String message) unknownError,
  }) {
    return invalidApiKey(key, message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(Key key, String message)? invalidApiKey,
    TResult? Function(Key key, String message)? resourceRequestedNotFound,
    TResult? Function(Key key, String message)? noInternetException,
    TResult? Function(Key key, String message)? unknownError,
  }) {
    return invalidApiKey?.call(key, message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(Key key, String message)? invalidApiKey,
    TResult Function(Key key, String message)? resourceRequestedNotFound,
    TResult Function(Key key, String message)? noInternetException,
    TResult Function(Key key, String message)? unknownError,
    required TResult orElse(),
  }) {
    if (invalidApiKey != null) {
      return invalidApiKey(key, message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitExceptionState value) init,
    required TResult Function(InvalidApiKeyExceptionState value) invalidApiKey,
    required TResult Function(ResourceRequestedNotFoundExceptionState value)
        resourceRequestedNotFound,
    required TResult Function(noInternetExceptionExceptionState value)
        noInternetException,
    required TResult Function(UnknownErrorExceptionState value) unknownError,
  }) {
    return invalidApiKey(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitExceptionState value)? init,
    TResult? Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult? Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult? Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult? Function(UnknownErrorExceptionState value)? unknownError,
  }) {
    return invalidApiKey?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitExceptionState value)? init,
    TResult Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult Function(UnknownErrorExceptionState value)? unknownError,
    required TResult orElse(),
  }) {
    if (invalidApiKey != null) {
      return invalidApiKey(this);
    }
    return orElse();
  }
}

abstract class InvalidApiKeyExceptionState extends ExceptionState {
  const factory InvalidApiKeyExceptionState(
      {required final Key key,
      required final String message}) = _$InvalidApiKeyExceptionStateImpl;
  const InvalidApiKeyExceptionState._() : super._();

  Key get key;
  String get message;
  @JsonKey(ignore: true)
  _$$InvalidApiKeyExceptionStateImplCopyWith<_$InvalidApiKeyExceptionStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ResourceRequestedNotFoundExceptionStateImplCopyWith<$Res> {
  factory _$$ResourceRequestedNotFoundExceptionStateImplCopyWith(
          _$ResourceRequestedNotFoundExceptionStateImpl value,
          $Res Function(_$ResourceRequestedNotFoundExceptionStateImpl) then) =
      __$$ResourceRequestedNotFoundExceptionStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Key key, String message});
}

/// @nodoc
class __$$ResourceRequestedNotFoundExceptionStateImplCopyWithImpl<$Res>
    extends _$ExceptionStateCopyWithImpl<$Res,
        _$ResourceRequestedNotFoundExceptionStateImpl>
    implements _$$ResourceRequestedNotFoundExceptionStateImplCopyWith<$Res> {
  __$$ResourceRequestedNotFoundExceptionStateImplCopyWithImpl(
      _$ResourceRequestedNotFoundExceptionStateImpl _value,
      $Res Function(_$ResourceRequestedNotFoundExceptionStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? key = null,
    Object? message = null,
  }) {
    return _then(_$ResourceRequestedNotFoundExceptionStateImpl(
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as Key,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ResourceRequestedNotFoundExceptionStateImpl
    extends ResourceRequestedNotFoundExceptionState
    with DiagnosticableTreeMixin {
  const _$ResourceRequestedNotFoundExceptionStateImpl(
      {required this.key, required this.message})
      : super._();

  @override
  final Key key;
  @override
  final String message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ExceptionState.resourceRequestedNotFound(key: $key, message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty(
          'type', 'ExceptionState.resourceRequestedNotFound'))
      ..add(DiagnosticsProperty('key', key))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ResourceRequestedNotFoundExceptionStateImpl &&
            (identical(other.key, key) || other.key == key) &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, key, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ResourceRequestedNotFoundExceptionStateImplCopyWith<
          _$ResourceRequestedNotFoundExceptionStateImpl>
      get copyWith =>
          __$$ResourceRequestedNotFoundExceptionStateImplCopyWithImpl<
              _$ResourceRequestedNotFoundExceptionStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(Key key, String message) invalidApiKey,
    required TResult Function(Key key, String message)
        resourceRequestedNotFound,
    required TResult Function(Key key, String message) noInternetException,
    required TResult Function(Key key, String message) unknownError,
  }) {
    return resourceRequestedNotFound(key, message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(Key key, String message)? invalidApiKey,
    TResult? Function(Key key, String message)? resourceRequestedNotFound,
    TResult? Function(Key key, String message)? noInternetException,
    TResult? Function(Key key, String message)? unknownError,
  }) {
    return resourceRequestedNotFound?.call(key, message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(Key key, String message)? invalidApiKey,
    TResult Function(Key key, String message)? resourceRequestedNotFound,
    TResult Function(Key key, String message)? noInternetException,
    TResult Function(Key key, String message)? unknownError,
    required TResult orElse(),
  }) {
    if (resourceRequestedNotFound != null) {
      return resourceRequestedNotFound(key, message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitExceptionState value) init,
    required TResult Function(InvalidApiKeyExceptionState value) invalidApiKey,
    required TResult Function(ResourceRequestedNotFoundExceptionState value)
        resourceRequestedNotFound,
    required TResult Function(noInternetExceptionExceptionState value)
        noInternetException,
    required TResult Function(UnknownErrorExceptionState value) unknownError,
  }) {
    return resourceRequestedNotFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitExceptionState value)? init,
    TResult? Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult? Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult? Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult? Function(UnknownErrorExceptionState value)? unknownError,
  }) {
    return resourceRequestedNotFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitExceptionState value)? init,
    TResult Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult Function(UnknownErrorExceptionState value)? unknownError,
    required TResult orElse(),
  }) {
    if (resourceRequestedNotFound != null) {
      return resourceRequestedNotFound(this);
    }
    return orElse();
  }
}

abstract class ResourceRequestedNotFoundExceptionState extends ExceptionState {
  const factory ResourceRequestedNotFoundExceptionState(
          {required final Key key, required final String message}) =
      _$ResourceRequestedNotFoundExceptionStateImpl;
  const ResourceRequestedNotFoundExceptionState._() : super._();

  Key get key;
  String get message;
  @JsonKey(ignore: true)
  _$$ResourceRequestedNotFoundExceptionStateImplCopyWith<
          _$ResourceRequestedNotFoundExceptionStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$noInternetExceptionExceptionStateImplCopyWith<$Res> {
  factory _$$noInternetExceptionExceptionStateImplCopyWith(
          _$noInternetExceptionExceptionStateImpl value,
          $Res Function(_$noInternetExceptionExceptionStateImpl) then) =
      __$$noInternetExceptionExceptionStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Key key, String message});
}

/// @nodoc
class __$$noInternetExceptionExceptionStateImplCopyWithImpl<$Res>
    extends _$ExceptionStateCopyWithImpl<$Res,
        _$noInternetExceptionExceptionStateImpl>
    implements _$$noInternetExceptionExceptionStateImplCopyWith<$Res> {
  __$$noInternetExceptionExceptionStateImplCopyWithImpl(
      _$noInternetExceptionExceptionStateImpl _value,
      $Res Function(_$noInternetExceptionExceptionStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? key = null,
    Object? message = null,
  }) {
    return _then(_$noInternetExceptionExceptionStateImpl(
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as Key,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$noInternetExceptionExceptionStateImpl
    extends noInternetExceptionExceptionState with DiagnosticableTreeMixin {
  const _$noInternetExceptionExceptionStateImpl(
      {required this.key, required this.message})
      : super._();

  @override
  final Key key;
  @override
  final String message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ExceptionState.noInternetException(key: $key, message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ExceptionState.noInternetException'))
      ..add(DiagnosticsProperty('key', key))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$noInternetExceptionExceptionStateImpl &&
            (identical(other.key, key) || other.key == key) &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, key, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$noInternetExceptionExceptionStateImplCopyWith<
          _$noInternetExceptionExceptionStateImpl>
      get copyWith => __$$noInternetExceptionExceptionStateImplCopyWithImpl<
          _$noInternetExceptionExceptionStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(Key key, String message) invalidApiKey,
    required TResult Function(Key key, String message)
        resourceRequestedNotFound,
    required TResult Function(Key key, String message) noInternetException,
    required TResult Function(Key key, String message) unknownError,
  }) {
    return noInternetException(key, message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(Key key, String message)? invalidApiKey,
    TResult? Function(Key key, String message)? resourceRequestedNotFound,
    TResult? Function(Key key, String message)? noInternetException,
    TResult? Function(Key key, String message)? unknownError,
  }) {
    return noInternetException?.call(key, message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(Key key, String message)? invalidApiKey,
    TResult Function(Key key, String message)? resourceRequestedNotFound,
    TResult Function(Key key, String message)? noInternetException,
    TResult Function(Key key, String message)? unknownError,
    required TResult orElse(),
  }) {
    if (noInternetException != null) {
      return noInternetException(key, message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitExceptionState value) init,
    required TResult Function(InvalidApiKeyExceptionState value) invalidApiKey,
    required TResult Function(ResourceRequestedNotFoundExceptionState value)
        resourceRequestedNotFound,
    required TResult Function(noInternetExceptionExceptionState value)
        noInternetException,
    required TResult Function(UnknownErrorExceptionState value) unknownError,
  }) {
    return noInternetException(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitExceptionState value)? init,
    TResult? Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult? Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult? Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult? Function(UnknownErrorExceptionState value)? unknownError,
  }) {
    return noInternetException?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitExceptionState value)? init,
    TResult Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult Function(UnknownErrorExceptionState value)? unknownError,
    required TResult orElse(),
  }) {
    if (noInternetException != null) {
      return noInternetException(this);
    }
    return orElse();
  }
}

abstract class noInternetExceptionExceptionState extends ExceptionState {
  const factory noInternetExceptionExceptionState(
      {required final Key key,
      required final String message}) = _$noInternetExceptionExceptionStateImpl;
  const noInternetExceptionExceptionState._() : super._();

  Key get key;
  String get message;
  @JsonKey(ignore: true)
  _$$noInternetExceptionExceptionStateImplCopyWith<
          _$noInternetExceptionExceptionStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UnknownErrorExceptionStateImplCopyWith<$Res> {
  factory _$$UnknownErrorExceptionStateImplCopyWith(
          _$UnknownErrorExceptionStateImpl value,
          $Res Function(_$UnknownErrorExceptionStateImpl) then) =
      __$$UnknownErrorExceptionStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Key key, String message});
}

/// @nodoc
class __$$UnknownErrorExceptionStateImplCopyWithImpl<$Res>
    extends _$ExceptionStateCopyWithImpl<$Res, _$UnknownErrorExceptionStateImpl>
    implements _$$UnknownErrorExceptionStateImplCopyWith<$Res> {
  __$$UnknownErrorExceptionStateImplCopyWithImpl(
      _$UnknownErrorExceptionStateImpl _value,
      $Res Function(_$UnknownErrorExceptionStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? key = null,
    Object? message = null,
  }) {
    return _then(_$UnknownErrorExceptionStateImpl(
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as Key,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$UnknownErrorExceptionStateImpl extends UnknownErrorExceptionState
    with DiagnosticableTreeMixin {
  const _$UnknownErrorExceptionStateImpl(
      {required this.key, required this.message})
      : super._();

  @override
  final Key key;
  @override
  final String message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ExceptionState.unknownError(key: $key, message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ExceptionState.unknownError'))
      ..add(DiagnosticsProperty('key', key))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnknownErrorExceptionStateImpl &&
            (identical(other.key, key) || other.key == key) &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, key, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UnknownErrorExceptionStateImplCopyWith<_$UnknownErrorExceptionStateImpl>
      get copyWith => __$$UnknownErrorExceptionStateImplCopyWithImpl<
          _$UnknownErrorExceptionStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(Key key, String message) invalidApiKey,
    required TResult Function(Key key, String message)
        resourceRequestedNotFound,
    required TResult Function(Key key, String message) noInternetException,
    required TResult Function(Key key, String message) unknownError,
  }) {
    return unknownError(key, message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(Key key, String message)? invalidApiKey,
    TResult? Function(Key key, String message)? resourceRequestedNotFound,
    TResult? Function(Key key, String message)? noInternetException,
    TResult? Function(Key key, String message)? unknownError,
  }) {
    return unknownError?.call(key, message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(Key key, String message)? invalidApiKey,
    TResult Function(Key key, String message)? resourceRequestedNotFound,
    TResult Function(Key key, String message)? noInternetException,
    TResult Function(Key key, String message)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(key, message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitExceptionState value) init,
    required TResult Function(InvalidApiKeyExceptionState value) invalidApiKey,
    required TResult Function(ResourceRequestedNotFoundExceptionState value)
        resourceRequestedNotFound,
    required TResult Function(noInternetExceptionExceptionState value)
        noInternetException,
    required TResult Function(UnknownErrorExceptionState value) unknownError,
  }) {
    return unknownError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitExceptionState value)? init,
    TResult? Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult? Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult? Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult? Function(UnknownErrorExceptionState value)? unknownError,
  }) {
    return unknownError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitExceptionState value)? init,
    TResult Function(InvalidApiKeyExceptionState value)? invalidApiKey,
    TResult Function(ResourceRequestedNotFoundExceptionState value)?
        resourceRequestedNotFound,
    TResult Function(noInternetExceptionExceptionState value)?
        noInternetException,
    TResult Function(UnknownErrorExceptionState value)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(this);
    }
    return orElse();
  }
}

abstract class UnknownErrorExceptionState extends ExceptionState {
  const factory UnknownErrorExceptionState(
      {required final Key key,
      required final String message}) = _$UnknownErrorExceptionStateImpl;
  const UnknownErrorExceptionState._() : super._();

  Key get key;
  String get message;
  @JsonKey(ignore: true)
  _$$UnknownErrorExceptionStateImplCopyWith<_$UnknownErrorExceptionStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
