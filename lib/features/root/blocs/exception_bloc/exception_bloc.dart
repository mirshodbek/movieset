import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/provider/exceptions.dart';

part 'exception_bloc.freezed.dart';

class ExceptionBloc extends Bloc<ExceptionEvent, ExceptionState> {
  ExceptionBloc() : super(const ExceptionState.init()) {
    on<_Read>((event, emit) {
      if (event.exception is InvalidApiKeyException) {
        final message = (event.exception as InvalidApiKeyException).message;
        emit(ExceptionState.invalidApiKey(
          key: UniqueKey(),
          message: message,
        ));
      } else if (event.exception is ResourceRequestedNotFoundException) {
        final message = (event.exception as ResourceRequestedNotFoundException).message;
        emit(ExceptionState.resourceRequestedNotFound(
          key: UniqueKey(),
          message: message,
        ));
      } else if (event.exception is NoInternetException) {
        final message = (event.exception as NoInternetException).message;
        emit(ExceptionState.noInternetException(
          key: UniqueKey(),
          message: message,
        ));
      } else if (event.exception is UnknownErrorException) {
        final message = (event.exception as UnknownErrorException).message;
        emit(ExceptionState.unknownError(
          key: UniqueKey(),
          message: message,
        ));
      }
    });
  }
}

@freezed
class ExceptionEvent with _$ExceptionEvent {
  const factory ExceptionEvent.read({required Exception exception}) = _Read;
}

@freezed
class ExceptionState with _$ExceptionState {
  const ExceptionState._();

  const factory ExceptionState.init() = InitExceptionState;

  const factory ExceptionState.invalidApiKey({
    required Key key,
    required String message,
  }) = InvalidApiKeyExceptionState;

  const factory ExceptionState.resourceRequestedNotFound({
    required Key key,
    required String message,
  }) = ResourceRequestedNotFoundExceptionState;

  const factory ExceptionState.noInternetException({
    required Key key,
    required String message,
  }) = noInternetExceptionExceptionState;

  const factory ExceptionState.unknownError({
    required Key key,
    required String message,
  }) = UnknownErrorExceptionState;


}
