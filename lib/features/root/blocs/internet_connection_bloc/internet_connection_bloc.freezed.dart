// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'internet_connection_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$InternetConnectionEvent {
  ConnectionStatus? get connection => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ConnectionStatus? connection) listen,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ConnectionStatus? connection)? listen,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ConnectionStatus? connection)? listen,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Listen value) listen,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Listen value)? listen,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Listen value)? listen,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $InternetConnectionEventCopyWith<InternetConnectionEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InternetConnectionEventCopyWith<$Res> {
  factory $InternetConnectionEventCopyWith(InternetConnectionEvent value,
          $Res Function(InternetConnectionEvent) then) =
      _$InternetConnectionEventCopyWithImpl<$Res, InternetConnectionEvent>;
  @useResult
  $Res call({ConnectionStatus? connection});
}

/// @nodoc
class _$InternetConnectionEventCopyWithImpl<$Res,
        $Val extends InternetConnectionEvent>
    implements $InternetConnectionEventCopyWith<$Res> {
  _$InternetConnectionEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? connection = freezed,
  }) {
    return _then(_value.copyWith(
      connection: freezed == connection
          ? _value.connection
          : connection // ignore: cast_nullable_to_non_nullable
              as ConnectionStatus?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ListenImplCopyWith<$Res>
    implements $InternetConnectionEventCopyWith<$Res> {
  factory _$$ListenImplCopyWith(
          _$ListenImpl value, $Res Function(_$ListenImpl) then) =
      __$$ListenImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({ConnectionStatus? connection});
}

/// @nodoc
class __$$ListenImplCopyWithImpl<$Res>
    extends _$InternetConnectionEventCopyWithImpl<$Res, _$ListenImpl>
    implements _$$ListenImplCopyWith<$Res> {
  __$$ListenImplCopyWithImpl(
      _$ListenImpl _value, $Res Function(_$ListenImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? connection = freezed,
  }) {
    return _then(_$ListenImpl(
      connection: freezed == connection
          ? _value.connection
          : connection // ignore: cast_nullable_to_non_nullable
              as ConnectionStatus?,
    ));
  }
}

/// @nodoc

class _$ListenImpl implements _Listen {
  const _$ListenImpl({required this.connection});

  @override
  final ConnectionStatus? connection;

  @override
  String toString() {
    return 'InternetConnectionEvent.listen(connection: $connection)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ListenImpl &&
            (identical(other.connection, connection) ||
                other.connection == connection));
  }

  @override
  int get hashCode => Object.hash(runtimeType, connection);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ListenImplCopyWith<_$ListenImpl> get copyWith =>
      __$$ListenImplCopyWithImpl<_$ListenImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ConnectionStatus? connection) listen,
  }) {
    return listen(connection);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ConnectionStatus? connection)? listen,
  }) {
    return listen?.call(connection);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ConnectionStatus? connection)? listen,
    required TResult orElse(),
  }) {
    if (listen != null) {
      return listen(connection);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Listen value) listen,
  }) {
    return listen(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Listen value)? listen,
  }) {
    return listen?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Listen value)? listen,
    required TResult orElse(),
  }) {
    if (listen != null) {
      return listen(this);
    }
    return orElse();
  }
}

abstract class _Listen implements InternetConnectionEvent {
  const factory _Listen({required final ConnectionStatus? connection}) =
      _$ListenImpl;

  @override
  ConnectionStatus? get connection;
  @override
  @JsonKey(ignore: true)
  _$$ListenImplCopyWith<_$ListenImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$InternetConnectionState {
  ConnectionStatus? get connection => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ConnectionStatus? connection) result,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ConnectionStatus? connection)? result,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ConnectionStatus? connection)? result,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResultInternetConnectionState value) result,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ResultInternetConnectionState value)? result,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResultInternetConnectionState value)? result,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $InternetConnectionStateCopyWith<InternetConnectionState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InternetConnectionStateCopyWith<$Res> {
  factory $InternetConnectionStateCopyWith(InternetConnectionState value,
          $Res Function(InternetConnectionState) then) =
      _$InternetConnectionStateCopyWithImpl<$Res, InternetConnectionState>;
  @useResult
  $Res call({ConnectionStatus? connection});
}

/// @nodoc
class _$InternetConnectionStateCopyWithImpl<$Res,
        $Val extends InternetConnectionState>
    implements $InternetConnectionStateCopyWith<$Res> {
  _$InternetConnectionStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? connection = freezed,
  }) {
    return _then(_value.copyWith(
      connection: freezed == connection
          ? _value.connection
          : connection // ignore: cast_nullable_to_non_nullable
              as ConnectionStatus?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ResultInternetConnectionStateImplCopyWith<$Res>
    implements $InternetConnectionStateCopyWith<$Res> {
  factory _$$ResultInternetConnectionStateImplCopyWith(
          _$ResultInternetConnectionStateImpl value,
          $Res Function(_$ResultInternetConnectionStateImpl) then) =
      __$$ResultInternetConnectionStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({ConnectionStatus? connection});
}

/// @nodoc
class __$$ResultInternetConnectionStateImplCopyWithImpl<$Res>
    extends _$InternetConnectionStateCopyWithImpl<$Res,
        _$ResultInternetConnectionStateImpl>
    implements _$$ResultInternetConnectionStateImplCopyWith<$Res> {
  __$$ResultInternetConnectionStateImplCopyWithImpl(
      _$ResultInternetConnectionStateImpl _value,
      $Res Function(_$ResultInternetConnectionStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? connection = freezed,
  }) {
    return _then(_$ResultInternetConnectionStateImpl(
      connection: freezed == connection
          ? _value.connection
          : connection // ignore: cast_nullable_to_non_nullable
              as ConnectionStatus?,
    ));
  }
}

/// @nodoc

class _$ResultInternetConnectionStateImpl
    implements ResultInternetConnectionState {
  const _$ResultInternetConnectionStateImpl({required this.connection});

  @override
  final ConnectionStatus? connection;

  @override
  String toString() {
    return 'InternetConnectionState.result(connection: $connection)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ResultInternetConnectionStateImpl &&
            (identical(other.connection, connection) ||
                other.connection == connection));
  }

  @override
  int get hashCode => Object.hash(runtimeType, connection);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ResultInternetConnectionStateImplCopyWith<
          _$ResultInternetConnectionStateImpl>
      get copyWith => __$$ResultInternetConnectionStateImplCopyWithImpl<
          _$ResultInternetConnectionStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ConnectionStatus? connection) result,
  }) {
    return result(connection);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ConnectionStatus? connection)? result,
  }) {
    return result?.call(connection);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ConnectionStatus? connection)? result,
    required TResult orElse(),
  }) {
    if (result != null) {
      return result(connection);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResultInternetConnectionState value) result,
  }) {
    return result(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ResultInternetConnectionState value)? result,
  }) {
    return result?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResultInternetConnectionState value)? result,
    required TResult orElse(),
  }) {
    if (result != null) {
      return result(this);
    }
    return orElse();
  }
}

abstract class ResultInternetConnectionState
    implements InternetConnectionState {
  const factory ResultInternetConnectionState(
          {required final ConnectionStatus? connection}) =
      _$ResultInternetConnectionStateImpl;

  @override
  ConnectionStatus? get connection;
  @override
  @JsonKey(ignore: true)
  _$$ResultInternetConnectionStateImplCopyWith<
          _$ResultInternetConnectionStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
