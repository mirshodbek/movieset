import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../repo/internet_connection.dart';

part 'internet_connection_bloc.freezed.dart';

class InternetConnectionBloc extends Bloc<InternetConnectionEvent, InternetConnectionState> {
  InternetConnectionBloc({
    required this.repo,
  }) : super(const InternetConnectionState.result(
          connection: ConnectionStatus.wifi,
        )) {
    on<_Listen>((event, emit) => emit(InternetConnectionState.result(connection: event.connection)));
    _connectionSubscription = repo
        .internetStatus()
        .listen((result) => add(InternetConnectionEvent.listen(connection: result)));
  }

  late StreamSubscription _connectionSubscription;
  final InternetConnectionRepo repo;

  @override
  Future<void> close() {
    _connectionSubscription.cancel();
    return super.close();
  }
}

@freezed
class InternetConnectionEvent with _$InternetConnectionEvent {
  const factory InternetConnectionEvent.listen({
    required ConnectionStatus? connection,
  }) = _Listen;

}

@freezed
class InternetConnectionState with _$InternetConnectionState {
  const factory InternetConnectionState.result({
    required ConnectionStatus? connection,
  }) = ResultInternetConnectionState;
}
