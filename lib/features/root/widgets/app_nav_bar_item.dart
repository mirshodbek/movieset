import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:movie_set/constants/constants.dart';

class AppNavBarItem extends Equatable {
  static const home = AppNavBarItem._(
    0,
    Constants.home,
    Icons.home_outlined,
    Icons.home,
  );
  static const search = AppNavBarItem._(
    1,
    Constants.search,
    Icons.search,
    Icons.search,
  );
  static const bookmark = AppNavBarItem._(
    2,
    Constants.bookmark,
    Icons.bookmark_border,
    Icons.bookmark,
  );
  static const settings = AppNavBarItem._(
    3,
    Constants.settings,
    Icons.settings_outlined,
    Icons.settings,
  );

  static List<AppNavBarItem> get values => [home, search, bookmark, settings];

  final int index;
  final String label;
  final IconData icon;
  final IconData activeIcon;

  const AppNavBarItem._(
    this.index,
    this.label,
    this.icon,
    this.activeIcon,
  );

  @override
  List<Object?> get props => [index, label, icon, activeIcon];
}
