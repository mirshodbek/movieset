import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/features/root/cubits/app_nav_bar_cubit.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar_item.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/router.dart';

class AppNavBar extends StatelessWidget {
  final AppNavBarItem? currentItem;
  final bool root;

  const AppNavBar({
    Key? key,
    this.root = true,
    this.currentItem,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<AppNavBarCubit, AppNavBarState>(
          listener: (context, state) {
            switch (state.item.index) {
              case 0:
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.replace(
                    HomePage(),
                    allPages: true,
                  ),
                );
                break;
              case 1:
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.replace(
                    SearchPage(),
                    allPages: true,
                  ),
                );
                break;
              case 2:
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.replace(
                    BookmarkPage(),
                    allPages: true,
                  ),
                );
                break;
              case 3:
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.replace(
                    SettingsPage(),
                    allPages: true,
                  ),
                );
                break;
            }
          },
        ),
      ],
      child: BottomNavigationBar(
        currentIndex: currentItem?.index ?? 0,
        onTap: (index) {
          final item = AppNavBarItem.values.firstWhere((it) => it.index == index);
          context.read<AppNavBarCubit>().onChanged(item);
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(AppNavBarItem.home.icon),
            label: AppNavBarItem.home.label,
            activeIcon: Icon(
              root ? AppNavBarItem.home.activeIcon : AppNavBarItem.home.icon,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(AppNavBarItem.search.icon),
            label: AppNavBarItem.search.label,
            activeIcon: Icon(
              root ? AppNavBarItem.search.activeIcon : AppNavBarItem.search.icon,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(AppNavBarItem.bookmark.icon),
            label: AppNavBarItem.bookmark.label,
            activeIcon: Icon(
              root ? AppNavBarItem.bookmark.activeIcon : AppNavBarItem.bookmark.icon,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(AppNavBarItem.settings.icon),
            label: AppNavBarItem.settings.label,
            activeIcon: Icon(
              root ? AppNavBarItem.settings.activeIcon : AppNavBarItem.settings.icon,
            ),
          ),
        ],
      ),
    );
  }
}
