import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../constants/constants.dart';
import '../../../../models/media/media.dart';
import '../../../../provider/api/media/base.dart';

part 'panel_media_bloc.freezed.dart';


class PanelMediaBloc extends Bloc<PanelMediaEvent, PanelMediaState> {
  PanelMediaBloc({
    this.id,
    required this.media,
    required this.provider,
    required this.type,
    required this.handleException,
  }) : super(PanelMediaState.data(media: media)) {
    on<_Load>((event, emit) async {
      try {
        switch (type) {
          case Constants.typeMovies:
            final result = await provider.getMovies(page: event.page);
            emit(
              state.copyWith(
                media: [
                  if (event.page != 1) ...state.media,
                  ...result.where((it) => it.genreIds?.contains(16) == false).toList(),
                ],
              ),
            );

            return;
          case Constants.typeTvs:
            final result = await provider.getTvs(page: event.page);
            emit(
              state.copyWith(
                media: [
                  if (event.page != 1) ...state.media,
                  ...result,
                ],
              ),
            );
            return;
          case Constants.typeTvAnimations:
            final result = await provider.getTvAnimations(page: event.page);
            emit(
              state.copyWith(
                media: [
                  if (event.page != 1) ...state.media,
                  ...result,
                ],
              ),
            );
            return;
          case Constants.typeMovieAnimations:
            final result = await provider.getMovieAnimations(page: event.page);
            emit(
              state.copyWith(
                media: [
                  if (event.page != 1) ...state.media,
                  ...result,
                ],
              ),
            );
            return;
          case Constants.typeMovieRecommendations:
            final result = await provider.getRecommendationsMovie(id: id, page: event.page);
            emit(
              state.copyWith(
                media: [
                  if (event.page != 1) ...state.media,
                  ...result.skip(1),
                ],
              ),
            );
            return;
          case Constants.typeTvRecommendations:
            final result = await provider.getRecommendationsTv(id: id, page: event.page);
            emit(
              state.copyWith(
                media: [
                  if (event.page != 1) ...state.media,
                  ...result.skip(1),
                ],
              ),
            );
            return;
          case Constants.typeTrending:
            final result = await provider.getTrendingMedia(page: event.page);
            emit(
              state.copyWith(
                media: [
                  if (event.page != 1) ...state.media,
                  ...result.skip(1),
                ],
              ),
            );
            return;
        }
      } on Exception catch (error) {
        handleException(error);
      }
    });
  }

  final int? id;
  final String type;
  final List<Media> media;
  final BaseProviderMedia provider;
  final Function(Exception) handleException;
}

@freezed
class PanelMediaEvent with _$PanelMediaEvent {
  const factory PanelMediaEvent.load({
    required int page,
  }) = _Load;
}

@freezed
class PanelMediaState with _$PanelMediaState {
  const factory PanelMediaState.data({
    required List<Media> media,
  }) = PanelMediaStateData;
}
