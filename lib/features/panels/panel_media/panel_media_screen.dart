import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/router.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/widgets/app_list_tile/app_list_tile.dart';
import 'package:movie_set/widgets/app_list_tile/app_list_tile_item.dart';
import 'package:movie_set/widgets/main_app_bar.dart';
import 'package:movie_set/widgets/pull_to_refresh/pull_to_refresh.dart';

import '../../../models/media/media.dart';
import '../../../provider/api/media/provider.dart';
import 'bloc/panel_media_bloc.dart';

class PanelMediaScreen extends StatelessWidget {
  const PanelMediaScreen({
    Key? key,
    this.id,
    required this.items,
    required this.type,
    required this.totalPages,
    required this.primaryTitle,
  }) : super(key: key);
  final int? id;
  final String type;
  final int? totalPages;
  final List<Media> items;
  final String primaryTitle;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PanelMediaBloc>(
      create: (context) => PanelMediaBloc(
        media: items,
        provider: RepositoryProvider.of<ProviderMedia>(context),
        type: type,
        id: id,
        handleException: context.readException,
      ),
      child: Builder(
        builder: (context) {
          return Scaffold(
            appBar: MainAppBar(primaryTitle: primaryTitle),
            body: PullToRefresh(
              widget: [
                BlocBuilder<PanelMediaBloc, PanelMediaState>(
                  buildWhen: (prev, curr) => prev.media != curr.media,
                  builder: (context, state) {
                    return SliverToBoxAdapter(
                      child: Wrap(
                        children: List.generate(
                          state.media.length,
                          (index) {
                            final item = state.media[index];
                            return Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 8),
                              child: AppListTile(
                                height: 180,
                                width: context.widthGridViewListTile(3),
                                onTap: () {
                                  AppRouter.navigate(
                                    context,
                                    (configuration) => configuration.add(
                                      MediaDetailsPage({
                                        Constants.dataOne: item,
                                        Constants.dataTwo: type,
                                      }),
                                    ),
                                  );
                                },
                                item: AppListTileItem(
                                  imagePath: item.posterPath ?? item.backdropPath,
                                  voteAverage: item.voteAverage,
                                  smallTextStyleTitle: true,
                                  adult: item.adult,
                                  title: item.title,
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    );
                  },
                ),
              ],
              totalPages: totalPages,
              loadNextPage: (page) {
                context.read<PanelMediaBloc>().add(PanelMediaEvent.load(page: page++));
              },
              loadFirstPage: () {
                context.read<PanelMediaBloc>().add(const PanelMediaEvent.load(page: 1));
              },
            ),
            bottomNavigationBar: const AppNavBar(root: false),
          );
        },
      ),
    );
  }
}
