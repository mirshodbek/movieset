import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/models/person/person.dart';
import 'package:movie_set/provider/api/people/base.dart';

part 'panel_people_bloc.freezed.dart';

class PanelPeopleBloc extends Bloc<PanelPeopleEvent, PanelPeopleState> {
  PanelPeopleBloc({
    required this.people,
    required this.provider,
    required this.handleException,
  }) : super(PanelPeopleState.data(people: people)) {
    on<_Load>((event, emit) async {
      try {
        final result = await provider.getPeople(page: event.page);
        emit(
          state.copyWith(
            people: [
              if (event.page != 1) ...state.people,
              ...result,
            ],
          ),
        );
      } on Exception catch (exception) {
        handleException(exception);
      }
    });
  }

  final List<Person> people;
  final BaseProviderPeople provider;
  final Function(Exception) handleException;
}

@freezed
class PanelPeopleEvent with _$PanelPeopleEvent {
  const factory PanelPeopleEvent.load({
    required int page,
  }) = _Load;
}

@freezed
class PanelPeopleState with _$PanelPeopleState {
  const factory PanelPeopleState.data({
    required List<Person> people,
  }) = PanelPeopleStateData;
}
