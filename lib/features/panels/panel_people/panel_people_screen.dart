import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar.dart';
import 'package:movie_set/models/person/person.dart';
import 'package:movie_set/provider/api/people/provider.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/router.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/utils/extensions/int_extension.dart';
import 'package:movie_set/widgets/app_list_tile/app_list_tile.dart';
import 'package:movie_set/widgets/app_list_tile/app_list_tile_item.dart';
import 'package:movie_set/widgets/main_app_bar.dart';
import 'package:movie_set/widgets/pull_to_refresh/pull_to_refresh.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import 'bloc/panel_people_bloc.dart';

class PanelPeopleScreen extends StatelessWidget {
  const PanelPeopleScreen({
    Key? key,
    required this.people,
    required this.totalPages,
  }) : super(key: key);
  final List<Person> people;
  final int? totalPages;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PanelPeopleBloc>(
      create: (context) => PanelPeopleBloc(
        people: people,
        provider: RepositoryProvider.of<ProviderPeople>(context),
        handleException: context.readException,
      ),
      child: Builder(
        builder: (context) {
          return Scaffold(
            appBar: MainAppBar(primaryTitle: S.of(context).people),
            body: PullToRefresh(
              widget: [
                BlocBuilder<PanelPeopleBloc, PanelPeopleState>(
                  buildWhen: (prev, curr) => prev.people != curr.people,
                  builder: (context, state) {
                    return SliverToBoxAdapter(
                      child: Wrap(
                        children: List.generate(
                          state.people.length,
                          (index) {
                            final item = state.people[index];
                            return Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                              child: AppListTile(
                                height: 120,
                                width: context.widthGridViewListTile(4),
                                onTap: () {
                                  AppRouter.navigate(
                                    context,
                                    (configuration) => configuration.add(
                                      PersonDetailsPage(item),
                                    ),
                                  );
                                },
                                item: AppListTileItem(
                                  imagePath: item.profilePath,
                                  subTitle: item.gender?.genderToString,
                                  adult: item.adult,
                                  title: item.name,
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    );
                  },
                ),
              ],
              totalPages: totalPages,
              loadNextPage: (page) {
                context.read<PanelPeopleBloc>().add(PanelPeopleEvent.load(page: page));
              },
              loadFirstPage: () {
                context.read<PanelPeopleBloc>().add(const PanelPeopleEvent.load(page: 1));
              },
            ),
            bottomNavigationBar: const AppNavBar(root: false),
          );
        },
      ),
    );
  }
}
