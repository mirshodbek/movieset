import 'package:flutter/material.dart';
import 'package:movie_set/app_design/app_gradients.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/router.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

class InitScreen extends StatefulWidget {
  const InitScreen({
    Key? key,
    this.runTest = false,
  }) : super(key: key);
  final bool runTest;

  @override
  State<InitScreen> createState() => _InitScreenState();
}

class _InitScreenState extends State<InitScreen> {
  @override
  void initState() {
    super.initState();
    if (!widget.runTest) navigate();
  }

  Future<void> navigate() async {
    await Future.delayed(const Duration(seconds: 1)).whenComplete(() {
      AppRouter.navigate(
        context,
        (configuration) => configuration.add(HomePage()),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: context.brightness == Brightness.light
            ? AppGradients.splashLightGradient
            : AppGradients.splashDarkGradient,
      ),
      child: Stack(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                child: RotatedBox(
                  quarterTurns: -1,
                  child: Text(
                    '${Constants.logoMoviePartOne}.   ',
                    style: context.primaryTextTheme.displayLarge?.copyWith(
                      fontSize: context.logoFontSize,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: RotatedBox(
                  quarterTurns: -1,
                  child: Text(
                    Constants.logoMoviePartTwo,
                    style: context.primaryTextTheme.displayLarge?.copyWith(
                      fontSize: context.logoFontSize,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 52,
            left: 40,
            right: 0,
            child: Text(
              '${Constants.logoMoviePartOne}.${Constants.logoMoviePartTwo}',
              style: context.primaryTextTheme.displayMedium,
            ),
          ),
        ],
      ),
    );
  }
}
