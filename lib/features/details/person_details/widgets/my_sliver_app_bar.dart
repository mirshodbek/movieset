import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/details/person_details/blocs/details/person_details_bloc.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/utils/extensions/string_extension.dart';
import 'package:movie_set/widgets/app_list_tile/parts/image_list_tile.dart';

import '../../../../models/base_person.dart';

class MySliverAppBar extends SliverPersistentHeaderDelegate {
  final BasePerson person;
  final double expandedMaxHeight;
  final double expandedMinHeight;

  MySliverAppBar({
    required this.person,
    required this.expandedMaxHeight,
    required this.expandedMinHeight,
  });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    double shrinkPercentage = min(1, shrinkOffset / (maxExtent - minExtent));
    return BlocBuilder<PersonDetailsBloc, PersonDetailsState>(
      builder: (context, state) {
        return ColoredBox(
          color: Theme.of(context).scaffoldBackgroundColor,
          child: Stack(
            children: [
              Positioned(
                left: 16,
                bottom: 0,
                child: Opacity(
                  opacity: 1 - shrinkPercentage,
                  child: ImageListTile(
                    height: maxExtent - minExtent,
                    width: 100,
                    imagePath: person.profilePath ?? state.details?.profilePath ?? Constants.empty,
                  ),
                ),
              ),
              Positioned(
                left: 0,
                top: minExtent / 3,
                child: const BackButton(),
              ),
              Positioned(
                left: 124 / (shrinkPercentage + 1),
                top: minExtent / (shrinkPercentage + 1),
                child: SizedBox(
                  width: context.width - 124 / (shrinkPercentage + 1),
                  height: maxExtent - minExtent,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        person.name ?? state.details?.name ?? Constants.empty,
                        style: context.primaryTextTheme.bodyMedium,
                      ),
                      const Spacer(),
                      Opacity(
                        opacity: 1 - shrinkPercentage,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              person.knownForDepartment?.jobType(person.gender) ??
                                  state.details?.knownForDepartment.jobType(person.gender) ??
                                  Constants.empty,
                              style: context.textTheme.bodySmall,
                            ),
                            Text(
                              state.details?.birthday?.formatDate ?? Constants.empty,
                              style: context.textTheme.bodySmall,
                            ),
                            Text(
                              state.details?.birthday?.age ?? Constants.empty,
                              style: context.textTheme.bodySmall,
                            ),
                            Text(
                              state.details?.placeOfBirth ?? Constants.empty,
                              style: context.textTheme.bodySmall,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  double get maxExtent => expandedMaxHeight;

  @override
  double get minExtent => kToolbarHeight + 10;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
