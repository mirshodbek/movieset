import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/features/details/person_details/blocs/credits/person_credits_bloc.dart';
import 'package:movie_set/features/details/person_details/blocs/details/person_details_bloc.dart';
import 'package:movie_set/provider/api/people/provider.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

class InitBlocsPersonDetails extends StatelessWidget {
  const InitBlocsPersonDetails({
    Key? key,
    required this.id,
    required this.child,
  }) : super(key: key);
  final Widget child;
  final int? id;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<PersonDetailsBloc>(
          create: (context) => PersonDetailsBloc(
            id: id,
            internetBloc: context.internetBloc,
            provider: RepositoryProvider.of<ProviderPeople>(context),
          )..add(const PersonDetailsEvent.init()),
        ),
        BlocProvider(
          create: (context) => PersonCreditsBloc(
            id: id,
            internetBloc: context.internetBloc,
            provider: RepositoryProvider.of<ProviderPeople>(context),
            handleException: context.readException,
          )..add(const PersonCreditsEvent.load()),
        ),
      ],
      child: child,
    );
  }
}
