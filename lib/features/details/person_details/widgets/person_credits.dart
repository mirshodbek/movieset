import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/router.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/utils/extensions/string_extension.dart';

import '../../../../widgets/animations/shimmer/shimmer_list_tile.dart';
import '../../../../widgets/app_list_tile/app_list_tile.dart';
import '../../../../widgets/app_list_tile/app_list_tile_item.dart';
import '../../../../widgets/empty_box.dart';
import '../blocs/credits/person_credits_bloc.dart';

class PersonCredits extends StatelessWidget {
  const PersonCredits({
    Key? key,
    required this.type,
  }) : super(key: key);
  final String type;

  @override
  Widget build(BuildContext context) {
    final isCast = type == Constants.cast;
    return Padding(
      padding: EdgeInsets.only(top: context.viewPaddingTop + kToolbarHeight),
      child: BlocBuilder<PersonCreditsBloc, PersonCreditsState>(
        builder: (context, state) {
          return state.map(
            init: (_) => Wrap(
              children: List.generate(
                10,
                (index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4),
                    child: ShimmerListTile(
                      height: 180,
                      width: context.widthGridViewListTile(3),
                    ),
                  );
                },
              ),
            ),
            data: (credits) {
              if ((isCast && credits.cast.isEmpty) || (!isCast && credits.crew.isEmpty)) {
                return Container(
                  alignment: Alignment.center,
                  child: const EmptyBox(
                    height: 200,
                    width: 200,
                  ),
                );
              }
              return SingleChildScrollView(
                child: Wrap(
                  children: List.generate(
                    isCast ? credits.cast.length : credits.crew.length,
                    (index) {
                      final item = isCast ? credits.cast[index] : credits.crew[index];
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: AppListTile(
                          height: 180,
                          width: context.widthGridViewListTile(3),
                          onTap: () {
                            AppRouter.navigate(
                              context,
                              (configuration) => configuration.add(
                                MediaDetailsPage({
                                  Constants.dataOne: item,
                                  Constants.dataTwo: item.mediaType,
                                }),
                              ),
                            );
                          },
                          item: AppListTileItem(
                            imagePath: item.backdropPath ?? item.posterPath,
                            releaseDate: item.releaseDate?.yearOfDate,
                            smallTextStyleTitle: true,
                            adult: item.adult,
                            title: item.title,
                          ),
                        ),
                      );
                    },
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
