import 'package:flutter/material.dart';
import 'package:movie_set/utils/extensions/int_extension.dart';
import 'package:movie_set/utils/extensions/string_extension.dart';

import '../../../constants/constants.dart';
import '../../../models/base_person.dart';
import '../../root/widgets/app_nav_bar.dart';
import 'widgets/init_blocs.dart';
import 'widgets/my_sliver_app_bar.dart';
import 'widgets/person_credits.dart';

class PersonDetailsScreen extends StatelessWidget {
  const PersonDetailsScreen({
    Key? key,
    required this.person,
  }) : super(key: key);
  final BasePerson person;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: InitBlocsPersonDetails(
        id: person.id,
        child: Scaffold(
          body: NestedScrollView(
            headerSliverBuilder: (context, value) {
              return [
                SliverOverlapAbsorber(
                  handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                  sliver: SliverPersistentHeader(
                    delegate: MySliverAppBar(
                      expandedMaxHeight: 200,
                      expandedMinHeight: 80,
                      person: person,
                    ),
                    pinned: true,
                  ),
                ),
                SliverAppBar(
                  automaticallyImplyLeading: false,
                  pinned: true,
                  title: TabBar(
                    tabs: [
                      Tab(
                        text: person.gender?.genderToString,
                      ),
                      Tab(
                        text: person.knownForDepartment?.crew,
                      )
                    ],
                  ),
                ),
              ];
            },
            body: const TabBarView(
              physics: BouncingScrollPhysics(),
              children: [
                PersonCredits(type: Constants.cast),
                PersonCredits(type: Constants.crew),
              ],
            ),
          ),
          bottomNavigationBar: const AppNavBar(root: false),
        ),
      ),
    );
  }
}
