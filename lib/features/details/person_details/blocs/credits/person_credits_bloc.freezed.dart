// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'person_credits_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$PersonCreditsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Load value) load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Load value)? load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Load value)? load,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PersonCreditsEventCopyWith<$Res> {
  factory $PersonCreditsEventCopyWith(
          PersonCreditsEvent value, $Res Function(PersonCreditsEvent) then) =
      _$PersonCreditsEventCopyWithImpl<$Res, PersonCreditsEvent>;
}

/// @nodoc
class _$PersonCreditsEventCopyWithImpl<$Res, $Val extends PersonCreditsEvent>
    implements $PersonCreditsEventCopyWith<$Res> {
  _$PersonCreditsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadImplCopyWith<$Res> {
  factory _$$LoadImplCopyWith(
          _$LoadImpl value, $Res Function(_$LoadImpl) then) =
      __$$LoadImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadImplCopyWithImpl<$Res>
    extends _$PersonCreditsEventCopyWithImpl<$Res, _$LoadImpl>
    implements _$$LoadImplCopyWith<$Res> {
  __$$LoadImplCopyWithImpl(_$LoadImpl _value, $Res Function(_$LoadImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadImpl implements _Load {
  const _$LoadImpl();

  @override
  String toString() {
    return 'PersonCreditsEvent.load()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
  }) {
    return load();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
  }) {
    return load?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    required TResult orElse(),
  }) {
    if (load != null) {
      return load();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Load value) load,
  }) {
    return load(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Load value)? load,
  }) {
    return load?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Load value)? load,
    required TResult orElse(),
  }) {
    if (load != null) {
      return load(this);
    }
    return orElse();
  }
}

abstract class _Load implements PersonCreditsEvent {
  const factory _Load() = _$LoadImpl;
}

/// @nodoc
mixin _$PersonCreditsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(
            List<PersonCredits> cast, List<PersonCredits> crew)
        data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<PersonCredits> cast, List<PersonCredits> crew)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<PersonCredits> cast, List<PersonCredits> crew)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitPersonCreditsState value) init,
    required TResult Function(DataPersonCreditsState value) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitPersonCreditsState value)? init,
    TResult? Function(DataPersonCreditsState value)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitPersonCreditsState value)? init,
    TResult Function(DataPersonCreditsState value)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PersonCreditsStateCopyWith<$Res> {
  factory $PersonCreditsStateCopyWith(
          PersonCreditsState value, $Res Function(PersonCreditsState) then) =
      _$PersonCreditsStateCopyWithImpl<$Res, PersonCreditsState>;
}

/// @nodoc
class _$PersonCreditsStateCopyWithImpl<$Res, $Val extends PersonCreditsState>
    implements $PersonCreditsStateCopyWith<$Res> {
  _$PersonCreditsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitPersonCreditsStateImplCopyWith<$Res> {
  factory _$$InitPersonCreditsStateImplCopyWith(
          _$InitPersonCreditsStateImpl value,
          $Res Function(_$InitPersonCreditsStateImpl) then) =
      __$$InitPersonCreditsStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitPersonCreditsStateImplCopyWithImpl<$Res>
    extends _$PersonCreditsStateCopyWithImpl<$Res, _$InitPersonCreditsStateImpl>
    implements _$$InitPersonCreditsStateImplCopyWith<$Res> {
  __$$InitPersonCreditsStateImplCopyWithImpl(
      _$InitPersonCreditsStateImpl _value,
      $Res Function(_$InitPersonCreditsStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitPersonCreditsStateImpl implements InitPersonCreditsState {
  const _$InitPersonCreditsStateImpl();

  @override
  String toString() {
    return 'PersonCreditsState.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitPersonCreditsStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(
            List<PersonCredits> cast, List<PersonCredits> crew)
        data,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<PersonCredits> cast, List<PersonCredits> crew)? data,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<PersonCredits> cast, List<PersonCredits> crew)? data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitPersonCreditsState value) init,
    required TResult Function(DataPersonCreditsState value) data,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitPersonCreditsState value)? init,
    TResult? Function(DataPersonCreditsState value)? data,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitPersonCreditsState value)? init,
    TResult Function(DataPersonCreditsState value)? data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitPersonCreditsState implements PersonCreditsState {
  const factory InitPersonCreditsState() = _$InitPersonCreditsStateImpl;
}

/// @nodoc
abstract class _$$DataPersonCreditsStateImplCopyWith<$Res> {
  factory _$$DataPersonCreditsStateImplCopyWith(
          _$DataPersonCreditsStateImpl value,
          $Res Function(_$DataPersonCreditsStateImpl) then) =
      __$$DataPersonCreditsStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<PersonCredits> cast, List<PersonCredits> crew});
}

/// @nodoc
class __$$DataPersonCreditsStateImplCopyWithImpl<$Res>
    extends _$PersonCreditsStateCopyWithImpl<$Res, _$DataPersonCreditsStateImpl>
    implements _$$DataPersonCreditsStateImplCopyWith<$Res> {
  __$$DataPersonCreditsStateImplCopyWithImpl(
      _$DataPersonCreditsStateImpl _value,
      $Res Function(_$DataPersonCreditsStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? cast = null,
    Object? crew = null,
  }) {
    return _then(_$DataPersonCreditsStateImpl(
      cast: null == cast
          ? _value._cast
          : cast // ignore: cast_nullable_to_non_nullable
              as List<PersonCredits>,
      crew: null == crew
          ? _value._crew
          : crew // ignore: cast_nullable_to_non_nullable
              as List<PersonCredits>,
    ));
  }
}

/// @nodoc

class _$DataPersonCreditsStateImpl implements DataPersonCreditsState {
  const _$DataPersonCreditsStateImpl(
      {required final List<PersonCredits> cast,
      required final List<PersonCredits> crew})
      : _cast = cast,
        _crew = crew;

  final List<PersonCredits> _cast;
  @override
  List<PersonCredits> get cast {
    if (_cast is EqualUnmodifiableListView) return _cast;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_cast);
  }

  final List<PersonCredits> _crew;
  @override
  List<PersonCredits> get crew {
    if (_crew is EqualUnmodifiableListView) return _crew;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_crew);
  }

  @override
  String toString() {
    return 'PersonCreditsState.data(cast: $cast, crew: $crew)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataPersonCreditsStateImpl &&
            const DeepCollectionEquality().equals(other._cast, _cast) &&
            const DeepCollectionEquality().equals(other._crew, _crew));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_cast),
      const DeepCollectionEquality().hash(_crew));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataPersonCreditsStateImplCopyWith<_$DataPersonCreditsStateImpl>
      get copyWith => __$$DataPersonCreditsStateImplCopyWithImpl<
          _$DataPersonCreditsStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(
            List<PersonCredits> cast, List<PersonCredits> crew)
        data,
  }) {
    return data(cast, crew);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<PersonCredits> cast, List<PersonCredits> crew)? data,
  }) {
    return data?.call(cast, crew);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<PersonCredits> cast, List<PersonCredits> crew)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(cast, crew);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitPersonCreditsState value) init,
    required TResult Function(DataPersonCreditsState value) data,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitPersonCreditsState value)? init,
    TResult? Function(DataPersonCreditsState value)? data,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitPersonCreditsState value)? init,
    TResult Function(DataPersonCreditsState value)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class DataPersonCreditsState implements PersonCreditsState {
  const factory DataPersonCreditsState(
      {required final List<PersonCredits> cast,
      required final List<PersonCredits> crew}) = _$DataPersonCreditsStateImpl;

  List<PersonCredits> get cast;
  List<PersonCredits> get crew;
  @JsonKey(ignore: true)
  _$$DataPersonCreditsStateImplCopyWith<_$DataPersonCreditsStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
