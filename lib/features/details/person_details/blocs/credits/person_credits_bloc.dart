import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/models/details/person/credits/person_credits.dart';
import 'package:movie_set/provider/api/people/base.dart';

import '../../../../../repo/internet_connection.dart';
import '../../../../root/blocs/internet_connection_bloc/internet_connection_bloc.dart';

part 'person_credits_bloc.freezed.dart';

class PersonCreditsBloc extends Bloc<PersonCreditsEvent, PersonCreditsState> {
  PersonCreditsBloc({
    required this.id,
    required this.internetBloc,
    required this.provider,
    required this.handleException,
  }) : super(const PersonCreditsState.init()) {
    _listenInternetConnection();
    on<_Load>((event, emit) async {
      try {
        final result = await provider.getPersonCreditsCast(id: id);
        emit(PersonCreditsState.data(
          cast: result.first,
          crew: result.last,
        ));
      } on Exception catch (exception) {
        handleException(exception);
      }
    });
  }

  final int? id;
  final BaseProviderPeople provider;
  final InternetConnectionBloc internetBloc;
  final Function(Exception) handleException;
  late final StreamSubscription _subInternet;

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) {
      if (event.connection != ConnectionStatus.offline) {
        add(const PersonCreditsEvent.load());
      }
    });
  }

  @override
  Future<void> close() {
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class PersonCreditsEvent with _$PersonCreditsEvent {
  const factory PersonCreditsEvent.load() = _Load;
}

@freezed
class PersonCreditsState with _$PersonCreditsState {
  const factory PersonCreditsState.init() = InitPersonCreditsState;

  const factory PersonCreditsState.data({
    required List<PersonCredits> cast,
    required List<PersonCredits> crew,
  }) = DataPersonCreditsState;
}
