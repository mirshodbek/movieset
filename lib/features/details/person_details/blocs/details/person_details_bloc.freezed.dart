// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'person_details_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$PersonDetailsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PersonDetailsEventCopyWith<$Res> {
  factory $PersonDetailsEventCopyWith(
          PersonDetailsEvent value, $Res Function(PersonDetailsEvent) then) =
      _$PersonDetailsEventCopyWithImpl<$Res, PersonDetailsEvent>;
}

/// @nodoc
class _$PersonDetailsEventCopyWithImpl<$Res, $Val extends PersonDetailsEvent>
    implements $PersonDetailsEventCopyWith<$Res> {
  _$PersonDetailsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitImplCopyWith<$Res> {
  factory _$$InitImplCopyWith(
          _$InitImpl value, $Res Function(_$InitImpl) then) =
      __$$InitImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitImplCopyWithImpl<$Res>
    extends _$PersonDetailsEventCopyWithImpl<$Res, _$InitImpl>
    implements _$$InitImplCopyWith<$Res> {
  __$$InitImplCopyWithImpl(_$InitImpl _value, $Res Function(_$InitImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitImpl implements _Init {
  const _$InitImpl();

  @override
  String toString() {
    return 'PersonDetailsEvent.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements PersonDetailsEvent {
  const factory _Init() = _$InitImpl;
}

/// @nodoc
mixin _$PersonDetailsState {
  PersonDetails? get details => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(PersonDetails? details) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(PersonDetails? details)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(PersonDetails? details)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DataPersonDetailsState value) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(DataPersonDetailsState value)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DataPersonDetailsState value)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PersonDetailsStateCopyWith<PersonDetailsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PersonDetailsStateCopyWith<$Res> {
  factory $PersonDetailsStateCopyWith(
          PersonDetailsState value, $Res Function(PersonDetailsState) then) =
      _$PersonDetailsStateCopyWithImpl<$Res, PersonDetailsState>;
  @useResult
  $Res call({PersonDetails? details});
}

/// @nodoc
class _$PersonDetailsStateCopyWithImpl<$Res, $Val extends PersonDetailsState>
    implements $PersonDetailsStateCopyWith<$Res> {
  _$PersonDetailsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? details = freezed,
  }) {
    return _then(_value.copyWith(
      details: freezed == details
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as PersonDetails?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$DataPersonDetailsStateImplCopyWith<$Res>
    implements $PersonDetailsStateCopyWith<$Res> {
  factory _$$DataPersonDetailsStateImplCopyWith(
          _$DataPersonDetailsStateImpl value,
          $Res Function(_$DataPersonDetailsStateImpl) then) =
      __$$DataPersonDetailsStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({PersonDetails? details});
}

/// @nodoc
class __$$DataPersonDetailsStateImplCopyWithImpl<$Res>
    extends _$PersonDetailsStateCopyWithImpl<$Res, _$DataPersonDetailsStateImpl>
    implements _$$DataPersonDetailsStateImplCopyWith<$Res> {
  __$$DataPersonDetailsStateImplCopyWithImpl(
      _$DataPersonDetailsStateImpl _value,
      $Res Function(_$DataPersonDetailsStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? details = freezed,
  }) {
    return _then(_$DataPersonDetailsStateImpl(
      details: freezed == details
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as PersonDetails?,
    ));
  }
}

/// @nodoc

class _$DataPersonDetailsStateImpl implements DataPersonDetailsState {
  const _$DataPersonDetailsStateImpl({this.details});

  @override
  final PersonDetails? details;

  @override
  String toString() {
    return 'PersonDetailsState.data(details: $details)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataPersonDetailsStateImpl &&
            (identical(other.details, details) || other.details == details));
  }

  @override
  int get hashCode => Object.hash(runtimeType, details);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataPersonDetailsStateImplCopyWith<_$DataPersonDetailsStateImpl>
      get copyWith => __$$DataPersonDetailsStateImplCopyWithImpl<
          _$DataPersonDetailsStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(PersonDetails? details) data,
  }) {
    return data(details);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(PersonDetails? details)? data,
  }) {
    return data?.call(details);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(PersonDetails? details)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(details);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DataPersonDetailsState value) data,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(DataPersonDetailsState value)? data,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DataPersonDetailsState value)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class DataPersonDetailsState implements PersonDetailsState {
  const factory DataPersonDetailsState({final PersonDetails? details}) =
      _$DataPersonDetailsStateImpl;

  @override
  PersonDetails? get details;
  @override
  @JsonKey(ignore: true)
  _$$DataPersonDetailsStateImplCopyWith<_$DataPersonDetailsStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
