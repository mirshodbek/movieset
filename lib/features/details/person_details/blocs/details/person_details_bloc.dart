import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/models/details/person/details/person_details.dart';
import 'package:movie_set/provider/api/people/base.dart';

import '../../../../../repo/internet_connection.dart';
import '../../../../root/blocs/internet_connection_bloc/internet_connection_bloc.dart';

part 'person_details_bloc.freezed.dart';

class PersonDetailsBloc extends Bloc<PersonDetailsEvent, PersonDetailsState> {
  PersonDetailsBloc({
    required this.id,
    required this.internetBloc,
    required this.provider,
  }) : super(const PersonDetailsState.data()) {
    _listenInternetConnection();
    on<_Init>((event, emit) async {
      final result = await provider.getPersonDetails(id: id);
      emit(PersonDetailsState.data(details: result));
    });
  }


  final int? id;
  final BaseProviderPeople provider;
  final InternetConnectionBloc internetBloc;
  late final StreamSubscription _subInternet;

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) {
      if (event.connection != ConnectionStatus.offline) {
        add(const PersonDetailsEvent.init());
      }
    });
  }

  @override
  Future<void> close() {
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class PersonDetailsEvent with _$PersonDetailsEvent {
  const factory PersonDetailsEvent.init() = _Init;
}

@freezed
class PersonDetailsState with _$PersonDetailsState {
  const factory PersonDetailsState.data({PersonDetails? details}) = DataPersonDetailsState;
}
