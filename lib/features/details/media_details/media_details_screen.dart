import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pod_player/pod_player.dart';
import 'package:movie_set/models/base_media.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../root/widgets/app_nav_bar.dart';
import 'utils/utils.dart';
import 'widgets/actions/media_actions.dart';
import 'widgets/details/media_details.dart';
import 'widgets/init_blocs.dart';
import 'widgets/credits/media_credits_list.dart';
import 'widgets/recommendations_list.dart';
import 'widgets/video/video_data.dart';

class MediaDetailsScreen extends StatelessWidget {
  MediaDetailsScreen({
    Key? key,
    required this.type,
    required this.media,
  }) : super(key: key);
  final BaseMedia media;
  final String type;
  final ValueNotifier<PodPlayerController?> _controller = ValueNotifier(null);

  @override
  Widget build(BuildContext context) {
    final enableToVerticalFullScreen =
        UtilsMovieDetails.settingsVideoEnableToVertical(context);
    final verticalFullScreen = UtilsMovieDetails.settingsVideoVertical(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: InitBlocsMovieDetails(
        id: media.id,
        type: type,
        child: PopScope(
          onPopInvoked: (value) {
            if (enableToVerticalFullScreen) {
              context.settingsCubit.switchToVertical();
            }
          },
          child: Scaffold(
            body: CustomScrollView(
              physics: enableToVerticalFullScreen
                  ? const NeverScrollableScrollPhysics()
                  : const AlwaysScrollableScrollPhysics(),
              slivers: [
                SliverToBoxAdapter(
                  child: VideoData(
                    takenController: _controller.value,
                    verticalFullScreen: verticalFullScreen,
                    imagePath: media.posterPath ?? media.backdropPath,
                    enableToVerticalFullScreen: enableToVerticalFullScreen,
                    setController: (controller) =>
                        _controller.value = controller,
                  ),
                ),
                SliverToBoxAdapter(
                  child: MediaActions(type: type),
                ),
                SliverToBoxAdapter(
                  child: MediaDetails(type: type),
                ),
                ValueListenableBuilder<PodPlayerController?>(
                  valueListenable: _controller,
                  builder: (context, value, child) {
                    return SliverToBoxAdapter(
                      child: MediaCreditsList(controller: value),
                    );
                  },
                ),
                ValueListenableBuilder<PodPlayerController?>(
                  valueListenable: _controller,
                  builder: (context, value, child) {
                    return SliverToBoxAdapter(
                      child: Recommendations(
                        type: type,
                        id: media.id,
                        controller: value,
                      ),
                    );
                  },
                ),
              ],
            ),
            bottomNavigationBar: enableToVerticalFullScreen
                ? null
                : const AppNavBar(root: false),
          ),
        ),
      ),
    );
  }
}
