import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/features/root/blocs/internet_connection_bloc/internet_connection_bloc.dart';
import 'package:movie_set/models/details/movie_details/movie_details.dart';
import 'package:movie_set/models/details/tv_details/tv_details.dart';
import 'package:movie_set/repo/internet_connection.dart';

import '../../../../../constants/constants.dart';
import '../../../../../provider/api/media/base.dart';

part 'media_details_bloc.freezed.dart';

class MediaDetailsBloc extends Bloc<MediaDetailsEvent, MediaDetailsState> {
  MediaDetailsBloc({
    required this.id,
    required this.internetBloc,
    required this.type,
    required this.provider,
  }) : super(const MediaDetailsState.init()) {
    _listenInternetConnection();
    on<_Init>((event, emit) async {
      if (Constants.typeListMovies.contains(type)) {
        final result = await provider.getDetailsMovie(id: id);
        emit(MediaDetailsState.data(movie: result));
        return;
      } else {
        final result = await provider.getDetailsTv(id: id);
        emit(MediaDetailsState.data(tv: result));
        return;
      }
    });
  }

  final int? id;
  final String type;
  final BaseProviderMedia provider;
  final InternetConnectionBloc internetBloc;
  late final StreamSubscription _subInternet;

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) {
      if (event.connection != ConnectionStatus.offline) {
        add(const MediaDetailsEvent.init());
      }
    });
  }

  @override
  Future<void> close() {
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class MediaDetailsEvent with _$MediaDetailsEvent {
  const factory MediaDetailsEvent.init() = _Init;
}

@freezed
class MediaDetailsState with _$MediaDetailsState {
  const factory MediaDetailsState.init() = InitMediaDetailsState;

  const factory MediaDetailsState.data({
    MovieDetails? movie,
    TvDetails? tv,
  }) = DataMediaDetailsState;
}
