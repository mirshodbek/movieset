// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'media_details_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$MediaDetailsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MediaDetailsEventCopyWith<$Res> {
  factory $MediaDetailsEventCopyWith(
          MediaDetailsEvent value, $Res Function(MediaDetailsEvent) then) =
      _$MediaDetailsEventCopyWithImpl<$Res, MediaDetailsEvent>;
}

/// @nodoc
class _$MediaDetailsEventCopyWithImpl<$Res, $Val extends MediaDetailsEvent>
    implements $MediaDetailsEventCopyWith<$Res> {
  _$MediaDetailsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitImplCopyWith<$Res> {
  factory _$$InitImplCopyWith(
          _$InitImpl value, $Res Function(_$InitImpl) then) =
      __$$InitImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitImplCopyWithImpl<$Res>
    extends _$MediaDetailsEventCopyWithImpl<$Res, _$InitImpl>
    implements _$$InitImplCopyWith<$Res> {
  __$$InitImplCopyWithImpl(_$InitImpl _value, $Res Function(_$InitImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitImpl implements _Init {
  const _$InitImpl();

  @override
  String toString() {
    return 'MediaDetailsEvent.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements MediaDetailsEvent {
  const factory _Init() = _$InitImpl;
}

/// @nodoc
mixin _$MediaDetailsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(MovieDetails? movie, TvDetails? tv) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(MovieDetails? movie, TvDetails? tv)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(MovieDetails? movie, TvDetails? tv)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitMediaDetailsState value) init,
    required TResult Function(DataMediaDetailsState value) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitMediaDetailsState value)? init,
    TResult? Function(DataMediaDetailsState value)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitMediaDetailsState value)? init,
    TResult Function(DataMediaDetailsState value)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MediaDetailsStateCopyWith<$Res> {
  factory $MediaDetailsStateCopyWith(
          MediaDetailsState value, $Res Function(MediaDetailsState) then) =
      _$MediaDetailsStateCopyWithImpl<$Res, MediaDetailsState>;
}

/// @nodoc
class _$MediaDetailsStateCopyWithImpl<$Res, $Val extends MediaDetailsState>
    implements $MediaDetailsStateCopyWith<$Res> {
  _$MediaDetailsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitMediaDetailsStateImplCopyWith<$Res> {
  factory _$$InitMediaDetailsStateImplCopyWith(
          _$InitMediaDetailsStateImpl value,
          $Res Function(_$InitMediaDetailsStateImpl) then) =
      __$$InitMediaDetailsStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitMediaDetailsStateImplCopyWithImpl<$Res>
    extends _$MediaDetailsStateCopyWithImpl<$Res, _$InitMediaDetailsStateImpl>
    implements _$$InitMediaDetailsStateImplCopyWith<$Res> {
  __$$InitMediaDetailsStateImplCopyWithImpl(_$InitMediaDetailsStateImpl _value,
      $Res Function(_$InitMediaDetailsStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitMediaDetailsStateImpl implements InitMediaDetailsState {
  const _$InitMediaDetailsStateImpl();

  @override
  String toString() {
    return 'MediaDetailsState.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitMediaDetailsStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(MovieDetails? movie, TvDetails? tv) data,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(MovieDetails? movie, TvDetails? tv)? data,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(MovieDetails? movie, TvDetails? tv)? data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitMediaDetailsState value) init,
    required TResult Function(DataMediaDetailsState value) data,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitMediaDetailsState value)? init,
    TResult? Function(DataMediaDetailsState value)? data,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitMediaDetailsState value)? init,
    TResult Function(DataMediaDetailsState value)? data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitMediaDetailsState implements MediaDetailsState {
  const factory InitMediaDetailsState() = _$InitMediaDetailsStateImpl;
}

/// @nodoc
abstract class _$$DataMediaDetailsStateImplCopyWith<$Res> {
  factory _$$DataMediaDetailsStateImplCopyWith(
          _$DataMediaDetailsStateImpl value,
          $Res Function(_$DataMediaDetailsStateImpl) then) =
      __$$DataMediaDetailsStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({MovieDetails? movie, TvDetails? tv});
}

/// @nodoc
class __$$DataMediaDetailsStateImplCopyWithImpl<$Res>
    extends _$MediaDetailsStateCopyWithImpl<$Res, _$DataMediaDetailsStateImpl>
    implements _$$DataMediaDetailsStateImplCopyWith<$Res> {
  __$$DataMediaDetailsStateImplCopyWithImpl(_$DataMediaDetailsStateImpl _value,
      $Res Function(_$DataMediaDetailsStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? movie = freezed,
    Object? tv = freezed,
  }) {
    return _then(_$DataMediaDetailsStateImpl(
      movie: freezed == movie
          ? _value.movie
          : movie // ignore: cast_nullable_to_non_nullable
              as MovieDetails?,
      tv: freezed == tv
          ? _value.tv
          : tv // ignore: cast_nullable_to_non_nullable
              as TvDetails?,
    ));
  }
}

/// @nodoc

class _$DataMediaDetailsStateImpl implements DataMediaDetailsState {
  const _$DataMediaDetailsStateImpl({this.movie, this.tv});

  @override
  final MovieDetails? movie;
  @override
  final TvDetails? tv;

  @override
  String toString() {
    return 'MediaDetailsState.data(movie: $movie, tv: $tv)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataMediaDetailsStateImpl &&
            (identical(other.movie, movie) || other.movie == movie) &&
            (identical(other.tv, tv) || other.tv == tv));
  }

  @override
  int get hashCode => Object.hash(runtimeType, movie, tv);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataMediaDetailsStateImplCopyWith<_$DataMediaDetailsStateImpl>
      get copyWith => __$$DataMediaDetailsStateImplCopyWithImpl<
          _$DataMediaDetailsStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(MovieDetails? movie, TvDetails? tv) data,
  }) {
    return data(movie, tv);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(MovieDetails? movie, TvDetails? tv)? data,
  }) {
    return data?.call(movie, tv);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(MovieDetails? movie, TvDetails? tv)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(movie, tv);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitMediaDetailsState value) init,
    required TResult Function(DataMediaDetailsState value) data,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitMediaDetailsState value)? init,
    TResult? Function(DataMediaDetailsState value)? data,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitMediaDetailsState value)? init,
    TResult Function(DataMediaDetailsState value)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class DataMediaDetailsState implements MediaDetailsState {
  const factory DataMediaDetailsState(
      {final MovieDetails? movie,
      final TvDetails? tv}) = _$DataMediaDetailsStateImpl;

  MovieDetails? get movie;
  TvDetails? get tv;
  @JsonKey(ignore: true)
  _$$DataMediaDetailsStateImplCopyWith<_$DataMediaDetailsStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
