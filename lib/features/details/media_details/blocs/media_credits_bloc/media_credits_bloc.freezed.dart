// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'media_credits_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$MediaCreditsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MediaCreditsEventCopyWith<$Res> {
  factory $MediaCreditsEventCopyWith(
          MediaCreditsEvent value, $Res Function(MediaCreditsEvent) then) =
      _$MediaCreditsEventCopyWithImpl<$Res, MediaCreditsEvent>;
}

/// @nodoc
class _$MediaCreditsEventCopyWithImpl<$Res, $Val extends MediaCreditsEvent>
    implements $MediaCreditsEventCopyWith<$Res> {
  _$MediaCreditsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitImplCopyWith<$Res> {
  factory _$$InitImplCopyWith(
          _$InitImpl value, $Res Function(_$InitImpl) then) =
      __$$InitImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitImplCopyWithImpl<$Res>
    extends _$MediaCreditsEventCopyWithImpl<$Res, _$InitImpl>
    implements _$$InitImplCopyWith<$Res> {
  __$$InitImplCopyWithImpl(_$InitImpl _value, $Res Function(_$InitImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitImpl implements _Init {
  const _$InitImpl();

  @override
  String toString() {
    return 'MediaCreditsEvent.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements MediaCreditsEvent {
  const factory _Init() = _$InitImpl;
}

/// @nodoc
mixin _$MediaCreditsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(CrewCastList credits) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(CrewCastList credits)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(CrewCastList credits)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitMediaCreditsState value) init,
    required TResult Function(DataMediaCreditsState value) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitMediaCreditsState value)? init,
    TResult? Function(DataMediaCreditsState value)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitMediaCreditsState value)? init,
    TResult Function(DataMediaCreditsState value)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MediaCreditsStateCopyWith<$Res> {
  factory $MediaCreditsStateCopyWith(
          MediaCreditsState value, $Res Function(MediaCreditsState) then) =
      _$MediaCreditsStateCopyWithImpl<$Res, MediaCreditsState>;
}

/// @nodoc
class _$MediaCreditsStateCopyWithImpl<$Res, $Val extends MediaCreditsState>
    implements $MediaCreditsStateCopyWith<$Res> {
  _$MediaCreditsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitMediaCreditsStateImplCopyWith<$Res> {
  factory _$$InitMediaCreditsStateImplCopyWith(
          _$InitMediaCreditsStateImpl value,
          $Res Function(_$InitMediaCreditsStateImpl) then) =
      __$$InitMediaCreditsStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitMediaCreditsStateImplCopyWithImpl<$Res>
    extends _$MediaCreditsStateCopyWithImpl<$Res, _$InitMediaCreditsStateImpl>
    implements _$$InitMediaCreditsStateImplCopyWith<$Res> {
  __$$InitMediaCreditsStateImplCopyWithImpl(_$InitMediaCreditsStateImpl _value,
      $Res Function(_$InitMediaCreditsStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitMediaCreditsStateImpl implements InitMediaCreditsState {
  const _$InitMediaCreditsStateImpl();

  @override
  String toString() {
    return 'MediaCreditsState.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitMediaCreditsStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(CrewCastList credits) data,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(CrewCastList credits)? data,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(CrewCastList credits)? data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitMediaCreditsState value) init,
    required TResult Function(DataMediaCreditsState value) data,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitMediaCreditsState value)? init,
    TResult? Function(DataMediaCreditsState value)? data,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitMediaCreditsState value)? init,
    TResult Function(DataMediaCreditsState value)? data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitMediaCreditsState implements MediaCreditsState {
  const factory InitMediaCreditsState() = _$InitMediaCreditsStateImpl;
}

/// @nodoc
abstract class _$$DataMediaCreditsStateImplCopyWith<$Res> {
  factory _$$DataMediaCreditsStateImplCopyWith(
          _$DataMediaCreditsStateImpl value,
          $Res Function(_$DataMediaCreditsStateImpl) then) =
      __$$DataMediaCreditsStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({CrewCastList credits});
}

/// @nodoc
class __$$DataMediaCreditsStateImplCopyWithImpl<$Res>
    extends _$MediaCreditsStateCopyWithImpl<$Res, _$DataMediaCreditsStateImpl>
    implements _$$DataMediaCreditsStateImplCopyWith<$Res> {
  __$$DataMediaCreditsStateImplCopyWithImpl(_$DataMediaCreditsStateImpl _value,
      $Res Function(_$DataMediaCreditsStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? credits = null,
  }) {
    return _then(_$DataMediaCreditsStateImpl(
      credits: null == credits
          ? _value.credits
          : credits // ignore: cast_nullable_to_non_nullable
              as CrewCastList,
    ));
  }
}

/// @nodoc

class _$DataMediaCreditsStateImpl implements DataMediaCreditsState {
  const _$DataMediaCreditsStateImpl({required this.credits});

  @override
  final CrewCastList credits;

  @override
  String toString() {
    return 'MediaCreditsState.data(credits: $credits)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataMediaCreditsStateImpl &&
            (identical(other.credits, credits) || other.credits == credits));
  }

  @override
  int get hashCode => Object.hash(runtimeType, credits);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataMediaCreditsStateImplCopyWith<_$DataMediaCreditsStateImpl>
      get copyWith => __$$DataMediaCreditsStateImplCopyWithImpl<
          _$DataMediaCreditsStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(CrewCastList credits) data,
  }) {
    return data(credits);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(CrewCastList credits)? data,
  }) {
    return data?.call(credits);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(CrewCastList credits)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(credits);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitMediaCreditsState value) init,
    required TResult Function(DataMediaCreditsState value) data,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitMediaCreditsState value)? init,
    TResult? Function(DataMediaCreditsState value)? data,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitMediaCreditsState value)? init,
    TResult Function(DataMediaCreditsState value)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class DataMediaCreditsState implements MediaCreditsState {
  const factory DataMediaCreditsState({required final CrewCastList credits}) =
      _$DataMediaCreditsStateImpl;

  CrewCastList get credits;
  @JsonKey(ignore: true)
  _$$DataMediaCreditsStateImplCopyWith<_$DataMediaCreditsStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
