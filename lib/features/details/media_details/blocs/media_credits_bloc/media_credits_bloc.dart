import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../constants/constants.dart';
import '../../../../../models/details/credits/crew_cast_list.dart';
import '../../../../../provider/api/media/base.dart';
import '../../../../../repo/internet_connection.dart';
import '../../../../root/blocs/internet_connection_bloc/internet_connection_bloc.dart';

part 'media_credits_bloc.freezed.dart';

class MediaCreditsBloc extends Bloc<MediaCreditsEvent, MediaCreditsState> {
  MediaCreditsBloc({
    required this.id,
    required this.internetBloc,
    required this.type,
    required this.provider,
  }) : super(const MediaCreditsState.init()) {
    _listenInternetConnection();
    on<_Init>((event, emit) async {
      switch (type) {
        case Constants.movie:
        case Constants.typeMovies:
        case Constants.typeMovieAnimations:
          final result = await provider.getCreditsMovie(id: id);
          emit(MediaCreditsState.data(credits: result));
          return;
        case Constants.tv:
        case Constants.typeTvs:
        case Constants.typeTvAnimations:
          final result = await provider.getCreditsTv(id: id);
          emit(MediaCreditsState.data(credits: result));
          return;
      }
    });
  }

  final int? id;
  final String type;
  final BaseProviderMedia provider;
  final InternetConnectionBloc internetBloc;
  late final StreamSubscription _subInternet;

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) {
      if (event.connection != ConnectionStatus.offline) {
        add(const MediaCreditsEvent.init());
      }
    });
  }

  @override
  Future<void> close() {
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class MediaCreditsEvent with _$MediaCreditsEvent {
  const factory MediaCreditsEvent.init() = _Init;
}

@freezed
class MediaCreditsState with _$MediaCreditsState {
  const factory MediaCreditsState.init() = InitMediaCreditsState;

  const factory MediaCreditsState.data({required CrewCastList credits}) = DataMediaCreditsState;
}
