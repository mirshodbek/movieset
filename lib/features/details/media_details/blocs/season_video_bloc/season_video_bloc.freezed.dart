// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'season_video_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SeasonVideoEvent {
  List<List<Video>> get video => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<List<Video>> video) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<List<Video>> video)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<List<Video>> video)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SeasonVideoEventCopyWith<SeasonVideoEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SeasonVideoEventCopyWith<$Res> {
  factory $SeasonVideoEventCopyWith(
          SeasonVideoEvent value, $Res Function(SeasonVideoEvent) then) =
      _$SeasonVideoEventCopyWithImpl<$Res, SeasonVideoEvent>;
  @useResult
  $Res call({List<List<Video>> video});
}

/// @nodoc
class _$SeasonVideoEventCopyWithImpl<$Res, $Val extends SeasonVideoEvent>
    implements $SeasonVideoEventCopyWith<$Res> {
  _$SeasonVideoEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? video = null,
  }) {
    return _then(_value.copyWith(
      video: null == video
          ? _value.video
          : video // ignore: cast_nullable_to_non_nullable
              as List<List<Video>>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$InitImplCopyWith<$Res>
    implements $SeasonVideoEventCopyWith<$Res> {
  factory _$$InitImplCopyWith(
          _$InitImpl value, $Res Function(_$InitImpl) then) =
      __$$InitImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<List<Video>> video});
}

/// @nodoc
class __$$InitImplCopyWithImpl<$Res>
    extends _$SeasonVideoEventCopyWithImpl<$Res, _$InitImpl>
    implements _$$InitImplCopyWith<$Res> {
  __$$InitImplCopyWithImpl(_$InitImpl _value, $Res Function(_$InitImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? video = null,
  }) {
    return _then(_$InitImpl(
      video: null == video
          ? _value._video
          : video // ignore: cast_nullable_to_non_nullable
              as List<List<Video>>,
    ));
  }
}

/// @nodoc

class _$InitImpl implements _Init {
  const _$InitImpl({required final List<List<Video>> video}) : _video = video;

  final List<List<Video>> _video;
  @override
  List<List<Video>> get video {
    if (_video is EqualUnmodifiableListView) return _video;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_video);
  }

  @override
  String toString() {
    return 'SeasonVideoEvent.init(video: $video)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitImpl &&
            const DeepCollectionEquality().equals(other._video, _video));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_video));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InitImplCopyWith<_$InitImpl> get copyWith =>
      __$$InitImplCopyWithImpl<_$InitImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<List<Video>> video) init,
  }) {
    return init(video);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<List<Video>> video)? init,
  }) {
    return init?.call(video);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<List<Video>> video)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(video);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements SeasonVideoEvent {
  const factory _Init({required final List<List<Video>> video}) = _$InitImpl;

  @override
  List<List<Video>> get video;
  @override
  @JsonKey(ignore: true)
  _$$InitImplCopyWith<_$InitImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SeasonVideoState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<List<Video>> video) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<List<Video>> video)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<List<Video>> video)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitSeasonVideoState value) init,
    required TResult Function(DataSeasonVideoState value) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitSeasonVideoState value)? init,
    TResult? Function(DataSeasonVideoState value)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitSeasonVideoState value)? init,
    TResult Function(DataSeasonVideoState value)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SeasonVideoStateCopyWith<$Res> {
  factory $SeasonVideoStateCopyWith(
          SeasonVideoState value, $Res Function(SeasonVideoState) then) =
      _$SeasonVideoStateCopyWithImpl<$Res, SeasonVideoState>;
}

/// @nodoc
class _$SeasonVideoStateCopyWithImpl<$Res, $Val extends SeasonVideoState>
    implements $SeasonVideoStateCopyWith<$Res> {
  _$SeasonVideoStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitSeasonVideoStateImplCopyWith<$Res> {
  factory _$$InitSeasonVideoStateImplCopyWith(_$InitSeasonVideoStateImpl value,
          $Res Function(_$InitSeasonVideoStateImpl) then) =
      __$$InitSeasonVideoStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitSeasonVideoStateImplCopyWithImpl<$Res>
    extends _$SeasonVideoStateCopyWithImpl<$Res, _$InitSeasonVideoStateImpl>
    implements _$$InitSeasonVideoStateImplCopyWith<$Res> {
  __$$InitSeasonVideoStateImplCopyWithImpl(_$InitSeasonVideoStateImpl _value,
      $Res Function(_$InitSeasonVideoStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitSeasonVideoStateImpl implements InitSeasonVideoState {
  const _$InitSeasonVideoStateImpl();

  @override
  String toString() {
    return 'SeasonVideoState.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitSeasonVideoStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<List<Video>> video) data,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<List<Video>> video)? data,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<List<Video>> video)? data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitSeasonVideoState value) init,
    required TResult Function(DataSeasonVideoState value) data,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitSeasonVideoState value)? init,
    TResult? Function(DataSeasonVideoState value)? data,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitSeasonVideoState value)? init,
    TResult Function(DataSeasonVideoState value)? data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitSeasonVideoState implements SeasonVideoState {
  const factory InitSeasonVideoState() = _$InitSeasonVideoStateImpl;
}

/// @nodoc
abstract class _$$DataSeasonVideoStateImplCopyWith<$Res> {
  factory _$$DataSeasonVideoStateImplCopyWith(_$DataSeasonVideoStateImpl value,
          $Res Function(_$DataSeasonVideoStateImpl) then) =
      __$$DataSeasonVideoStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<List<Video>> video});
}

/// @nodoc
class __$$DataSeasonVideoStateImplCopyWithImpl<$Res>
    extends _$SeasonVideoStateCopyWithImpl<$Res, _$DataSeasonVideoStateImpl>
    implements _$$DataSeasonVideoStateImplCopyWith<$Res> {
  __$$DataSeasonVideoStateImplCopyWithImpl(_$DataSeasonVideoStateImpl _value,
      $Res Function(_$DataSeasonVideoStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? video = null,
  }) {
    return _then(_$DataSeasonVideoStateImpl(
      video: null == video
          ? _value._video
          : video // ignore: cast_nullable_to_non_nullable
              as List<List<Video>>,
    ));
  }
}

/// @nodoc

class _$DataSeasonVideoStateImpl implements DataSeasonVideoState {
  const _$DataSeasonVideoStateImpl({required final List<List<Video>> video})
      : _video = video;

  final List<List<Video>> _video;
  @override
  List<List<Video>> get video {
    if (_video is EqualUnmodifiableListView) return _video;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_video);
  }

  @override
  String toString() {
    return 'SeasonVideoState.data(video: $video)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataSeasonVideoStateImpl &&
            const DeepCollectionEquality().equals(other._video, _video));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_video));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataSeasonVideoStateImplCopyWith<_$DataSeasonVideoStateImpl>
      get copyWith =>
          __$$DataSeasonVideoStateImplCopyWithImpl<_$DataSeasonVideoStateImpl>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<List<Video>> video) data,
  }) {
    return data(video);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<List<Video>> video)? data,
  }) {
    return data?.call(video);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<List<Video>> video)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(video);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitSeasonVideoState value) init,
    required TResult Function(DataSeasonVideoState value) data,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitSeasonVideoState value)? init,
    TResult? Function(DataSeasonVideoState value)? data,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitSeasonVideoState value)? init,
    TResult Function(DataSeasonVideoState value)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class DataSeasonVideoState implements SeasonVideoState {
  const factory DataSeasonVideoState({required final List<List<Video>> video}) =
      _$DataSeasonVideoStateImpl;

  List<List<Video>> get video;
  @JsonKey(ignore: true)
  _$$DataSeasonVideoStateImplCopyWith<_$DataSeasonVideoStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
