import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/repo/api/media/base.dart';

import '../../../../../models/details/video/video.dart';
import '../../../../../repo/internet_connection.dart';
import '../../../../root/blocs/internet_connection_bloc/internet_connection_bloc.dart';

part 'season_video_bloc.freezed.dart';

class SeasonVideoBloc extends Bloc<SeasonVideoEvent, SeasonVideoState> {
  SeasonVideoBloc({
    required this.internetBloc,
    required this.tvId,
    required this.repo,
    required this.numberOfSeasons,
    required this.firstSeasonNumber,
  }) : super(const SeasonVideoState.init()) {
    _init();
    _listenInternetConnection();
    on<_Init>((event, emit) {
      emit(SeasonVideoState.data(video: event.video));
    });
  }

  final int? tvId;
  final BaseRepoMedia repo;
  final int? numberOfSeasons;
  final int? firstSeasonNumber;
  final InternetConnectionBloc internetBloc;
  late final StreamSubscription _subVideo;
  late final StreamSubscription _subInternet;

  Future<void> _init() async {
    _subVideo = repo
        .getVideosSeason(
      tvId: tvId,
      countSeason: numberOfSeasons,
      firstSeasonNumber: firstSeasonNumber,
    )
        .listen(
      (it) {
        if (it.isNotEmpty) {
          add(SeasonVideoEvent.init(video: it));
        }
      },
    );
  }

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) async {
      if (event.connection != ConnectionStatus.offline) {
        await _init();
      }
    });
  }

  @override
  Future<void> close() {
    _subVideo.cancel();
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class SeasonVideoEvent with _$SeasonVideoEvent {
  const factory SeasonVideoEvent.init({
    required List<List<Video>> video,
  }) = _Init;
}

@freezed
class SeasonVideoState with _$SeasonVideoState {
  const factory SeasonVideoState.init() = InitSeasonVideoState;

  const factory SeasonVideoState.data({
    required List<List<Video>> video,
  }) = DataSeasonVideoState;
}
