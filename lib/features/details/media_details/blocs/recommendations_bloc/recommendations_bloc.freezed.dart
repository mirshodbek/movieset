// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'recommendations_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$RecommendationsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecommendationsEventCopyWith<$Res> {
  factory $RecommendationsEventCopyWith(RecommendationsEvent value,
          $Res Function(RecommendationsEvent) then) =
      _$RecommendationsEventCopyWithImpl<$Res, RecommendationsEvent>;
}

/// @nodoc
class _$RecommendationsEventCopyWithImpl<$Res,
        $Val extends RecommendationsEvent>
    implements $RecommendationsEventCopyWith<$Res> {
  _$RecommendationsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitImplCopyWith<$Res> {
  factory _$$InitImplCopyWith(
          _$InitImpl value, $Res Function(_$InitImpl) then) =
      __$$InitImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitImplCopyWithImpl<$Res>
    extends _$RecommendationsEventCopyWithImpl<$Res, _$InitImpl>
    implements _$$InitImplCopyWith<$Res> {
  __$$InitImplCopyWithImpl(_$InitImpl _value, $Res Function(_$InitImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitImpl implements _Init {
  const _$InitImpl();

  @override
  String toString() {
    return 'RecommendationsEvent.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements RecommendationsEvent {
  const factory _Init() = _$InitImpl;
}

/// @nodoc
mixin _$RecommendationsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(
            List<Media> recommendations, String typeRecommendations)
        data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> recommendations, String typeRecommendations)?
        data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> recommendations, String typeRecommendations)?
        data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitRecommendationsState value) init,
    required TResult Function(DataRecommendationsState value) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitRecommendationsState value)? init,
    TResult? Function(DataRecommendationsState value)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitRecommendationsState value)? init,
    TResult Function(DataRecommendationsState value)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecommendationsStateCopyWith<$Res> {
  factory $RecommendationsStateCopyWith(RecommendationsState value,
          $Res Function(RecommendationsState) then) =
      _$RecommendationsStateCopyWithImpl<$Res, RecommendationsState>;
}

/// @nodoc
class _$RecommendationsStateCopyWithImpl<$Res,
        $Val extends RecommendationsState>
    implements $RecommendationsStateCopyWith<$Res> {
  _$RecommendationsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitRecommendationsStateImplCopyWith<$Res> {
  factory _$$InitRecommendationsStateImplCopyWith(
          _$InitRecommendationsStateImpl value,
          $Res Function(_$InitRecommendationsStateImpl) then) =
      __$$InitRecommendationsStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitRecommendationsStateImplCopyWithImpl<$Res>
    extends _$RecommendationsStateCopyWithImpl<$Res,
        _$InitRecommendationsStateImpl>
    implements _$$InitRecommendationsStateImplCopyWith<$Res> {
  __$$InitRecommendationsStateImplCopyWithImpl(
      _$InitRecommendationsStateImpl _value,
      $Res Function(_$InitRecommendationsStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitRecommendationsStateImpl implements InitRecommendationsState {
  const _$InitRecommendationsStateImpl();

  @override
  String toString() {
    return 'RecommendationsState.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitRecommendationsStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(
            List<Media> recommendations, String typeRecommendations)
        data,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> recommendations, String typeRecommendations)?
        data,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> recommendations, String typeRecommendations)?
        data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitRecommendationsState value) init,
    required TResult Function(DataRecommendationsState value) data,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitRecommendationsState value)? init,
    TResult? Function(DataRecommendationsState value)? data,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitRecommendationsState value)? init,
    TResult Function(DataRecommendationsState value)? data,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitRecommendationsState implements RecommendationsState {
  const factory InitRecommendationsState() = _$InitRecommendationsStateImpl;
}

/// @nodoc
abstract class _$$DataRecommendationsStateImplCopyWith<$Res> {
  factory _$$DataRecommendationsStateImplCopyWith(
          _$DataRecommendationsStateImpl value,
          $Res Function(_$DataRecommendationsStateImpl) then) =
      __$$DataRecommendationsStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Media> recommendations, String typeRecommendations});
}

/// @nodoc
class __$$DataRecommendationsStateImplCopyWithImpl<$Res>
    extends _$RecommendationsStateCopyWithImpl<$Res,
        _$DataRecommendationsStateImpl>
    implements _$$DataRecommendationsStateImplCopyWith<$Res> {
  __$$DataRecommendationsStateImplCopyWithImpl(
      _$DataRecommendationsStateImpl _value,
      $Res Function(_$DataRecommendationsStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? recommendations = null,
    Object? typeRecommendations = null,
  }) {
    return _then(_$DataRecommendationsStateImpl(
      recommendations: null == recommendations
          ? _value._recommendations
          : recommendations // ignore: cast_nullable_to_non_nullable
              as List<Media>,
      typeRecommendations: null == typeRecommendations
          ? _value.typeRecommendations
          : typeRecommendations // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$DataRecommendationsStateImpl implements DataRecommendationsState {
  const _$DataRecommendationsStateImpl(
      {required final List<Media> recommendations,
      required this.typeRecommendations})
      : _recommendations = recommendations;

  final List<Media> _recommendations;
  @override
  List<Media> get recommendations {
    if (_recommendations is EqualUnmodifiableListView) return _recommendations;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_recommendations);
  }

  @override
  final String typeRecommendations;

  @override
  String toString() {
    return 'RecommendationsState.data(recommendations: $recommendations, typeRecommendations: $typeRecommendations)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataRecommendationsStateImpl &&
            const DeepCollectionEquality()
                .equals(other._recommendations, _recommendations) &&
            (identical(other.typeRecommendations, typeRecommendations) ||
                other.typeRecommendations == typeRecommendations));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_recommendations),
      typeRecommendations);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataRecommendationsStateImplCopyWith<_$DataRecommendationsStateImpl>
      get copyWith => __$$DataRecommendationsStateImplCopyWithImpl<
          _$DataRecommendationsStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(
            List<Media> recommendations, String typeRecommendations)
        data,
  }) {
    return data(recommendations, typeRecommendations);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> recommendations, String typeRecommendations)?
        data,
  }) {
    return data?.call(recommendations, typeRecommendations);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> recommendations, String typeRecommendations)?
        data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(recommendations, typeRecommendations);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitRecommendationsState value) init,
    required TResult Function(DataRecommendationsState value) data,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InitRecommendationsState value)? init,
    TResult? Function(DataRecommendationsState value)? data,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitRecommendationsState value)? init,
    TResult Function(DataRecommendationsState value)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class DataRecommendationsState implements RecommendationsState {
  const factory DataRecommendationsState(
          {required final List<Media> recommendations,
          required final String typeRecommendations}) =
      _$DataRecommendationsStateImpl;

  List<Media> get recommendations;
  String get typeRecommendations;
  @JsonKey(ignore: true)
  _$$DataRecommendationsStateImplCopyWith<_$DataRecommendationsStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
