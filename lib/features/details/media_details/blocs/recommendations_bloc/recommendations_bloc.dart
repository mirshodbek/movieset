import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../constants/constants.dart';
import '../../../../../models/media/media.dart';
import '../../../../../provider/api/media/base.dart';
import '../../../../../repo/internet_connection.dart';
import '../../../../root/blocs/internet_connection_bloc/internet_connection_bloc.dart';

part 'recommendations_bloc.freezed.dart';

class RecommendationsBloc extends Bloc<RecommendationsEvent, RecommendationsState> {
  RecommendationsBloc({
    required this.id,
    required this.internetBloc,
    required this.type,
    required this.provider,
  }) : super(const RecommendationsState.init()) {
    _listenInternetConnection();
    on<_Init>((event, emit) async {
      if(Constants.typeListMovies.contains(type)){
        final result = await provider.getRecommendationsMovie(id: id);
        emit(RecommendationsState.data(
          recommendations: result,
          typeRecommendations: Constants.typeMovieRecommendations,
        ));
        return;
      }else{
        final result = await provider.getRecommendationsTv(id: id);
        emit(RecommendationsState.data(
          recommendations: result,
          typeRecommendations: Constants.typeTvRecommendations,
        ));
        return;
      }
    });
  }

  final int? id;
  final String type;
  final BaseProviderMedia provider;
  final InternetConnectionBloc internetBloc;
  late final StreamSubscription _subInternet;

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) {
      if (event.connection != ConnectionStatus.offline) {
        add(const RecommendationsEvent.init());
      }
    });
  }

  @override
  Future<void> close() {
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class RecommendationsEvent with _$RecommendationsEvent {
  const factory RecommendationsEvent.init() = _Init;
}

@freezed
class RecommendationsState with _$RecommendationsState {
  const factory RecommendationsState.init() = InitRecommendationsState;

  const factory RecommendationsState.data({
    required List<Media> recommendations,
    required String typeRecommendations,
  }) = DataRecommendationsState;
}
