part of '../video_bloc.dart';

extension PlayVideo on VideoBloc {
  Future<void> _playVideo(
    _PlayVideo event,
    Emitter<VideoState> emit,
  ) async {
    emit(const VideoState.init());
    await Future.delayed(const Duration(seconds: 1)).whenComplete(
      () => emit(
        VideoState.data(
          video: [event.seasonVideo],
        ),
      ),
    );
  }
}
