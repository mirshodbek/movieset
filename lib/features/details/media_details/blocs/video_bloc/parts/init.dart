part of '../video_bloc.dart';

extension InitVideo on VideoBloc {
  Future<void> _init(
    _Init event,
    Emitter<VideoState> emit,
  ) async {
    try {
      if (Constants.typeListMovies.contains(type)) {
        final result = await provider.getVideosMovie(id: id);
        emit(VideoState.data(video: result));
        return;
      } else {
        final result = await provider.getVideosTv(id: id);
        emit(VideoState.data(video: result));
        return;
      }
    } on Exception catch (exception) {
      handleException(exception);
    }
  }
}
