import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/models/details/video/video.dart';

import '../../../../../provider/api/media/base.dart';
import '../../../../../repo/internet_connection.dart';
import '../../../../root/blocs/internet_connection_bloc/internet_connection_bloc.dart';

part 'video_bloc.freezed.dart';

part 'parts/init.dart';

part 'parts/play_video.dart';

class VideoBloc extends Bloc<VideoEvent, VideoState> {
  VideoBloc({
    required this.id,
    required this.internetBloc,
    required this.type,
    required this.provider,
    required this.handleException,
  }) : super(const VideoState.init()) {
    _listenInternetConnection();
    on<_Init>(_init);
    on<_PlayVideo>(_playVideo);
  }

  final int? id;
  final String type;
  final BaseProviderMedia provider;
  final Function(Exception) handleException;
  final InternetConnectionBloc internetBloc;
  late final StreamSubscription _subInternet;

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) {
      if (event.connection != ConnectionStatus.offline) {
        add(const VideoEvent.init());
      }
    });
  }

  @override
  Future<void> close() {
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class VideoEvent with _$VideoEvent {
  const factory VideoEvent.init() = _Init;

  const factory VideoEvent.playVideo({
    required Video? seasonVideo,
  }) = _PlayVideo;
}

@freezed
class VideoState with _$VideoState {
  const factory VideoState.init() = InitVideoState;

  const factory VideoState.data({
    required List<Video?> video,
  }) = DataVideoState;
}
