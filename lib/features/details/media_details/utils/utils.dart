import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pod_player/pod_player.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/settings/cubits/settings_cubit.dart';
import 'package:movie_set/models/details/movie_details/movie_details.dart';
import 'package:movie_set/models/details/tv_details/tv_details.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/utils/extensions/list_extension.dart';
import 'package:movie_set/utils/extensions/string_extension.dart';
import 'package:movie_set/utils/extensions/int_extension.dart';

import '../../../../repo/internet_connection.dart';

class UtilsMovieDetails {
  static const aspectRatio = 1.4;

  static double widthImage(BuildContext context) => context.width;

  static double heightImage(BuildContext context) => context.height;

  static String durationToString(int current, int position) {
    Duration duration = Duration(milliseconds: current - position);
    return (duration.inMilliseconds / 60000)
        .toStringAsFixed(2)
        .replaceFirst('.', ':')
        .padLeft(5, '0');
  }

  static String subTitle(
    MovieDetails? movie,
    TvDetails? tv,
  ) {
    final details = movie ?? tv;
    final genres = details?.genres.setInList.convertListToString ?? Constants.empty;
    final season = tv?.numberOfSeasons.seasons ?? Constants.empty;
    final runtime = movie?.runTime.runTimeToHourString ?? Constants.empty;
    final series = tv?.numberOfEpisodes.series ?? Constants.empty;
    final year = details?.releaseDate.yearOfDate ?? Constants.empty;
    final productCountries =
        details?.productionCountries.setInList.convertListToString ?? Constants.empty;
    final adult = details?.adult ?? false ? ' • ${Constants.plusSixteen}' : Constants.empty;
    return '$genres • $season$series$runtime$year • $productCountries$adult';
  }

  static bool settingsWifi(BuildContext context) {
    final settingsCubitState = context.settingsState.wifi;
    final internetState = context.internetBlocState;

    if (settingsCubitState == true) {
      return internetState.connection == ConnectionStatus.wifi;
    } else {
      return internetState.connection == ConnectionStatus.wifi ||
          internetState.connection == ConnectionStatus.mobile;
    }
  }

  static bool settingsVideoEnableToVertical(BuildContext context) =>
      context.watch<SettingsCubit>().state.orientationVideo == OrientationVideo.enableVertical;

  static bool settingsVideoVertical(BuildContext context) =>
      context.watch<SettingsCubit>().state.orientationVideo == OrientationVideo.vertical;

  static void stopVideo(PodPlayerController? controller) {
    if (controller?.isInitialised ?? false) {
      controller?.pause();
    }
  }
}
