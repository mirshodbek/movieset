import 'package:flutter/material.dart';
import 'package:movie_set/router/router.dart';

import '../../../../../app_design/app_colors.dart';
import '../../../../../constants/constants.dart';
import '../../../../../widgets/animations/shimmer/shimmer_loading.dart';
import '../../../../../widgets/app_list_tile/parts/image_list_tile.dart';
import '../../utils/utils.dart';

class IconPlayArrow extends StatelessWidget {
  const IconPlayArrow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Icon(
      Icons.play_arrow,
      size: 50,
      color: AppColors.primaryExtraLight,
    );
  }
}

class BackButtonWidget extends StatelessWidget {
  const BackButtonWidget({
    Key? key,
    this.onPressed,
  }) : super(key: key);
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 0,
      top: 16,
      child: BackButton(
        color: AppColors.grayScaleWhite,
        style: TextButton.styleFrom(
          padding: const EdgeInsets.all(16),
        ),
        onPressed: onPressed ?? () => AppRouter.maybePop(context),
      ),
    );
  }
}

class NoVideo extends StatelessWidget {
  const NoVideo({
    Key? key,
    required this.imagePath,
  }) : super(key: key);
  final String? imagePath;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        AspectRatio(
          aspectRatio: UtilsMovieDetails.aspectRatio,
          child: ImageListTile(
            width: UtilsMovieDetails.widthImage(context),
            height: UtilsMovieDetails.heightImage(context),
            imagePath: imagePath ?? Constants.empty,
          ),
        ),
        const BackButtonWidget(),
        Icon(
          Icons.videocam_off_outlined,
          size: 100,
          color: AppColors.primarySuperExtraLight.withOpacity(.5),
        ),
      ],
    );
  }
}

class ShimmerLoadingVideo extends StatelessWidget {
  const ShimmerLoadingVideo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Shimmer(
          child: ShimmerLoading(
            child: AspectRatio(
              aspectRatio: UtilsMovieDetails.aspectRatio,
              child: Container(
                color: Colors.white,
              ),
            ),
          ),
        ),
        const BackButtonWidget(),
        const IconPlayArrow(),
      ],
    );
  }
}
