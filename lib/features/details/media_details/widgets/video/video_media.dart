import 'package:flutter/material.dart';
import 'package:pod_player/pod_player.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../../../app_design/app_colors.dart';
import '../../../../../constants/constants.dart';
import '../../utils/utils.dart';
import 'on_loading_video.dart';
import 'on_video_error.dart';
import 'overlay_builder_video.dart';
import 'widgets_movie_details.dart';

class VideoMedia extends StatefulWidget {
  const VideoMedia({
    Key? key,
    required this.site,
    required this.videoId,
    required this.imagePath,
    required this.settingsWifi,
    required this.setController,
    required this.takenController,
    required this.verticalFullScreen,
    required this.enableToVerticalFullScreen,
  }) : super(key: key);
  final String? site;
  final String? videoId;
  final String? imagePath;
  final bool settingsWifi;
  final bool verticalFullScreen;
  final bool enableToVerticalFullScreen;
  final PodPlayerController? takenController;
  final Function(PodPlayerController) setController;

  @override
  State<VideoMedia> createState() => _VideoMediaState();
}

class _VideoMediaState extends State<VideoMedia> {
  late PodPlayerController _controller;

  @override
  void initState() {
    _controller = PodPlayerController(
      playVideoFrom: widget.site == Constants.youtube
          ? PlayVideoFrom.youtube(
              '${Constants.youtubeUrl}${widget.videoId}',
            )
          : PlayVideoFrom.vimeo(
              '${Constants.vimeoUrl}${widget.videoId}',
            ),
      podPlayerConfig: PodPlayerConfig(
        autoPlay: widget.settingsWifi,
        forcedVideoFocus: true,
      ),
    )..initialise();

    _controller.addListener(() {
      if (widget.takenController != _controller) {
        widget.setController.call(_controller);
      }
    });

    super.initState();
  }

  @override
  void deactivate() {
    if (widget.enableToVerticalFullScreen) context.settingsCubit.switchToVertical();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: widget.enableToVerticalFullScreen
          ? context.width / context.height
          : UtilsMovieDetails.aspectRatio,
      child: ColoredBox(
        color: AppColors.othersBlack,
        child: Stack(
          children: [
            PodVideoPlayer(
              controller: _controller,
              videoAspectRatio: UtilsMovieDetails.aspectRatio,
              frameAspectRatio: UtilsMovieDetails.aspectRatio,
              podProgressBarConfig: const PodProgressBarConfig(
                playingBarColor: AppColors.secondaryDark,
                backgroundColor: AppColors.primaryLight,
                height: 2,
                circleHandlerColor: AppColors.secondaryDark,
                circleHandlerRadius: 6,
              ),
              onLoading: (context) => OnLoadingVideo(imagePath: widget.imagePath),
              onVideoError: () => OnVideoError(
                imagePath: widget.imagePath,
                onPressed: () => _controller.play(),
              ),
              overlayBuilder: (options) => OverlayBuilderVideo(
                horizontalFullScreen: widget.verticalFullScreen,
                enableToHorizontalFullScreen: widget.enableToVerticalFullScreen,
                controller: _controller,
                options: options,
              ),
            ),
            if (widget.enableToVerticalFullScreen)
              BackButtonWidget(onPressed: context.settingsCubit.switchToVertical),
          ],
        ),
      ),
    );
  }
}
