import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pod_player/pod_player.dart';

import '../../blocs/video_bloc/video_bloc.dart';
import '../../utils/utils.dart';
import 'video_media.dart';
import 'widgets_movie_details.dart';

class VideoData extends StatelessWidget {
  const VideoData({
    Key? key,
    required this.imagePath,
    required this.setController,
    required this.takenController,
    required this.verticalFullScreen,
    required this.enableToVerticalFullScreen,
  }) : super(key: key);
  final String? imagePath;
  final bool verticalFullScreen;
  final bool enableToVerticalFullScreen;
  final PodPlayerController? takenController;
  final Function(PodPlayerController) setController;

  @override
  Widget build(BuildContext context) {
    final onlyWifi = UtilsMovieDetails.settingsWifi(context);
    return BlocBuilder<VideoBloc, VideoState>(
      builder: (context, state) {
        return state.map(
          init: (_) => const ShimmerLoadingVideo(),
          data: (map) {
            if (map.video.isEmpty) {
              return NoVideo(imagePath: imagePath);
            }

            return VideoMedia(
              imagePath: imagePath,
              settingsWifi: onlyWifi,
              setController: setController,
              takenController: takenController,
              site: map.video.firstOrNull?.site,
              videoId: map.video.firstOrNull?.key,
              verticalFullScreen: verticalFullScreen,
              enableToVerticalFullScreen: enableToVerticalFullScreen,
            );
          },
        );
      },
    );
  }
}
