import 'package:flutter/material.dart';

import '../../../../../app_design/app_colors.dart';
import '../../../../../constants/constants.dart';
import '../../../../../widgets/app_list_tile/parts/image_list_tile.dart';
import '../../utils/utils.dart';
import 'widgets_movie_details.dart';

class OnLoadingVideo extends StatelessWidget {
  const OnLoadingVideo({
    Key? key,
    required this.imagePath,
  }) : super(key: key);
  final String? imagePath;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        ImageListTile(
          width: UtilsMovieDetails.widthImage(context),
          height: UtilsMovieDetails.heightImage(context),
          imagePath: imagePath ?? Constants.empty,
          borderRadius: 0,
        ),
        const CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(AppColors.primaryExtraLight),
        ),
        const BackButtonWidget(),
      ],
    );
  }
}
