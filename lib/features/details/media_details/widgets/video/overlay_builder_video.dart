import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pod_player/pod_player.dart';

import '../../../../../app_design/app_colors.dart';
import '../../../../../app_design/app_text_style.dart';
import '../../../../settings/cubits/settings_cubit.dart';
import '../../utils/utils.dart';
import 'widgets_movie_details.dart';

class OverlayBuilderVideo extends StatelessWidget {
  const OverlayBuilderVideo({
    Key? key,
    required this.horizontalFullScreen,
    required this.enableToHorizontalFullScreen,
    required this.options,
    required this.controller,
  }) : super(key: key);
  final PodPlayerController controller;
  final OverLayOptions options;
  final bool horizontalFullScreen;
  final bool enableToHorizontalFullScreen;

  void enableFullScreen(BuildContext context) =>
      options.isFullScreen ? controller.disableFullScreen(context) : controller.enableFullScreen();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: IconButton(
            icon: options.podVideoState == PodVideoState.paused
                ? const IconPlayArrow()
                : const Visibility(
                    visible: false,
                    child: IconPlayArrow(),
                  ),
            onPressed: () => controller.togglePlayPause(),
          ),
        ),
        if (!enableToHorizontalFullScreen || options.isFullScreen) const BackButtonWidget(),
        Positioned(
          bottom: 5,
          left: 16,
          child: Text(
            UtilsMovieDetails.durationToString(
              options.videoDuration.inMilliseconds,
              options.videoPosition.inMilliseconds,
            ),
            style: AppTextStyle.s14w400.copyWith(
              color: AppColors.primaryExtraLight,
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 32,
          child: IconButton(
            icon: Icon(
              options.isMute ? Icons.volume_off : Icons.volume_up,
              color: AppColors.primaryExtraLight,
            ),
            onPressed: () => controller.toggleVolume(),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          child: IconButton(
            onPressed: () {
              if (horizontalFullScreen) {
                if (enableToHorizontalFullScreen) {
                  enableFullScreen(context);
                } else {
                  context.read<SettingsCubit>().switchToEnableVertical();
                }
              } else {
                enableFullScreen(context);
              }
            },
            icon: Icon(
              horizontalFullScreen && !enableToHorizontalFullScreen
                  ? CupertinoIcons.fullscreen
                  : options.isFullScreen
                      ? Icons.fullscreen_exit
                      : Icons.fullscreen,
              color: AppColors.primaryExtraLight,
            ),
          ),
        ),
      ],
    );
  }
}
