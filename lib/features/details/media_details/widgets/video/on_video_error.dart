import 'package:flutter/material.dart';

import '../../../../../constants/constants.dart';
import '../../../../../widgets/app_list_tile/parts/image_list_tile.dart';
import '../../utils/utils.dart';
import 'widgets_movie_details.dart';

class OnVideoError extends StatelessWidget {
  const OnVideoError({
    Key? key,
    required this.imagePath,
    required this.onPressed,
  }) : super(key: key);
  final String? imagePath;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: UtilsMovieDetails.aspectRatio,
      child: Stack(
        alignment: Alignment.center,
        children: [
          ImageListTile(
            width: UtilsMovieDetails.widthImage(context),
            height: UtilsMovieDetails.heightImage(context),
            imagePath: imagePath ?? Constants.empty,
            borderRadius: 0,
          ),
          IconButton(
            onPressed: onPressed,
            icon:  const IconPlayArrow(),
          ),
          const BackButtonWidget(),
        ],
      ),
    );
  }
}
