import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pod_player/pod_player.dart';
import 'package:movie_set/features/details/media_details/utils/utils.dart';
import 'package:movie_set/utils/extensions/int_extension.dart';
import 'package:movie_set/widgets/app_title_category.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/router/pages.dart';

import '../../../../constants/constants.dart';
import '../../../../router/router.dart';
import '../../../../widgets/app_list_tile/app_list_tile.dart';
import '../../../../widgets/app_list_tile/app_list_tile_item.dart';
import '../blocs/recommendations_bloc/recommendations_bloc.dart';

class Recommendations extends StatelessWidget {
  const Recommendations({
    Key? key,
    required this.id,
    required this.type,
    required this.controller,
  }) : super(key: key);
  final String type;
  final int? id;
  final PodPlayerController? controller;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecommendationsBloc, RecommendationsState>(
      builder: (context, state) {
        return state.map(
          init: (_) => const SizedBox.shrink(),
          data: (map) {
            if (map.recommendations.isEmpty) return const SizedBox.shrink();

            final moreData = map.recommendations.firstOrNull?.totalPages?.moreData ?? false;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTitleCategory(
                  title: S.of(context).similar,
                  onPressed: moreData
                      ? () {
                          UtilsMovieDetails.stopVideo(controller);
                          AppRouter.navigate(
                            context,
                            (configuration) => configuration.add(
                              PanelMediaPage({
                                Constants.dataOne: map.recommendations,
                                Constants.dataTwo: S.of(context).similar,
                                Constants.dataThree: map.typeRecommendations,
                                Constants.dataFour: map.recommendations.firstOrNull?.totalPages,
                                Constants.dataFive: id,
                              }),
                            ),
                          );
                        }
                      : null,
                ),
                if (!moreData) const SizedBox(height: 16),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: List.generate(
                      map.recommendations.length,
                      (index) {
                        final item = map.recommendations[index];
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: AppListTile(
                            onTap: () {
                              UtilsMovieDetails.stopVideo(controller);
                              AppRouter.navigate(
                                context,
                                (configuration) => configuration.add(
                                  MediaDetailsPage({
                                    Constants.dataOne: item,
                                    Constants.dataTwo: type,
                                  }),
                                ),
                              );
                            },
                            item: AppListTileItem(
                              imagePath: item.posterPath ?? item.backdropPath,
                              voteAverage: item.voteAverage,
                              adult: item.adult,
                              title: item.title,
                              smallTextStyleTitle: true,
                            ),
                            width: 100,
                            height: 180,
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }
}
