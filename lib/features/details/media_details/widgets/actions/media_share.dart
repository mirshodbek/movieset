import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:share_plus/share_plus.dart';

import '../../../../../constants/constants.dart';
import '../../blocs/video_bloc/video_bloc.dart';

class MediaShare extends StatelessWidget {
  const MediaShare({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<VideoBloc, VideoState>(
      builder: (context, state) {
        return state.map(
          init: (_) => const IconButton(
            onPressed: null,
            icon: Icon(Icons.share),
          ),
          data: (map) {
            return IconButton(
              onPressed: map.video.isNotEmpty
                  ? () async {
                      if (map.video.firstOrNull?.site == Constants.youtube) {
                        await Share.share(
                          '${Constants.youtubeUrl}${map.video.firstOrNull?.key}',
                        );
                      } else {
                        await Share.share(
                          '${Constants.vimeoUrl}${map.video.firstOrNull?.key}',
                        );
                      }
                    }
                  : null,
              icon: const Icon(Icons.share),
            );
          },
        );
      },
    );
  }
}
