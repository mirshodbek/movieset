import 'package:flutter/material.dart';

import 'media_bookmark.dart';
import 'media_share.dart';
import 'media_vote_average.dart';

class MediaActions extends StatelessWidget {
  const MediaActions({
    Key? key,
    required this.type,
  }) : super(key: key);
  final String type;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        children:  [
          const MediaVoteAverage(),
          const Spacer(),
          MediaBookmark(type: type),
          const MediaShare(),
        ],
      ),
    );
  }
}
