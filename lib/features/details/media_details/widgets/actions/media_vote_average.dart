import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../../../app_design/app_colors.dart';
import '../../blocs/media_details_bloc/media_details_bloc.dart';

class MediaVoteAverage extends StatelessWidget {
  const MediaVoteAverage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MediaDetailsBloc, MediaDetailsState>(
      builder: (context, state) {
        return state.map(
          init: (_) => const Icon(
            Icons.star,
            color: AppColors.primaryExtraLight,
          ),
          data: (map) {
            return Text.rich(
              TextSpan(
                children: [
                  WidgetSpan(
                    alignment: PlaceholderAlignment.middle,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 4),
                      child: Icon(
                        Icons.star,
                        color:
                            context.light ? AppColors.primaryExtraLight : AppColors.secondaryMain,
                      ),
                    ),
                  ),
                  TextSpan(
                    text: (map.movie ?? map.tv)?.voteAverage?.toStringAsFixed(1),
                  ),
                ],
              ),
              style: context.textTheme.titleSmall,
              textAlign: TextAlign.center,
            );
          },
        );
      },
    );
  }
}
