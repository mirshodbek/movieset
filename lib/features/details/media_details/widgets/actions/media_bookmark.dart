import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/models/bookmark/bookmark.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../../../constants/constants.dart';
import '../../../../bookmark/bloc/bookmark_bloc.dart';
import '../../blocs/media_details_bloc/media_details_bloc.dart';

class MediaBookmark extends StatelessWidget {
  const MediaBookmark({
    Key? key,
    required this.type,
  }) : super(key: key);
  final String type;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MediaDetailsBloc, MediaDetailsState>(
      builder: (context, state) {
        return state.map(
          init: (_) {
            return const IconButton(
              onPressed: null,
              icon: Icon(Icons.bookmark_border),
            );
          },
          data: (map) {
            final details = map.movie ?? map.tv;
            final marked = context.bookmarks?.any((it) => it.id == (details)?.id) ?? false;
            final formedType =
                Constants.typeListMovies.contains(type) ? Constants.movie : Constants.tv;
            return IconButton(
              onPressed: () => context.bookmarkBloc.add(
                BookmarkEvent.bookmark(
                  add: !marked,
                  bookmark: Bookmark(
                    id: details?.id,
                    title: details?.title,
                    backdropPath: details?.backdropPath,
                    posterPath: details?.posterPath,
                    genreNames: details?.genres?.map((e) => e.name).toList(),
                    originCountry: details?.productionCountries?.map((e) => e.name).toList(),
                    releaseDate: details?.releaseDate,
                    type: formedType,
                    adult: details?.adult,
                    runtime: map.movie?.runTime,
                    numberOfEpisodes: map.tv?.numberOfEpisodes,
                    numberOfSeasons: map.tv?.numberOfSeasons,
                  ),
                ),
              ),
              icon: Icon(marked ? Icons.bookmark : Icons.bookmark_border),
            );
          },
        );
      },
    );
  }
}
