import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pod_player/pod_player.dart';
import 'package:movie_set/app_design/app_colors.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import '../../blocs/media_credits_bloc/media_credits_bloc.dart';
import 'credits_list.dart';

import 'media_credits_shimmer_list.dart';

class MediaCreditsList extends StatelessWidget {
  const MediaCreditsList({
    Key? key,
    required this.controller,
  }) : super(key: key);
  final PodPlayerController? controller;

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: context.light ? AppColors.primaryExtraLight.withOpacity(.5) : AppColors.primaryDark,
      child: BlocBuilder<MediaCreditsBloc, MediaCreditsState>(
        builder: (context, state) {
          return state.map(
            init: (_) => Column(
              children: [
                MediaCreditsShimmerList(title: S.of(context).castMembers),
                const SizedBox(height: 34),
                MediaCreditsShimmerList(title: S.of(context).crewMovie),
                const SizedBox(height: 16),
              ],
            ),
            data: (map) => CreditsList(
              credits: map.credits,
              controller: controller,
            ),
          );
        },
      ),
    );
  }
}
