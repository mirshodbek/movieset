import 'package:flutter/material.dart';

import '../../../../../app_design/app_colors.dart';
import '../../../../../widgets/animations/shimmer/shimmer_list.dart';
import '../../../../../widgets/animations/shimmer/shimmer_loading.dart';

class MediaCreditsShimmerList extends StatelessWidget {
  const MediaCreditsShimmerList({
    Key? key,
    required this.title,
  }) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return ShimmerList(
      title: title,
      shimmerTile: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Shimmer(
              child: ShimmerLoading(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Container(
                    color: AppColors.grayScaleWhite,
                    width: 50,
                    height: 50,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 4),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Shimmer(
              child: ShimmerLoading(
                child: Container(
                  color: AppColors.grayScaleWhite,
                  height: 6,
                  width: 50,
                ),
              ),
            ),
          ),
          const SizedBox(height: 2),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Shimmer(
              child: ShimmerLoading(
                child: Container(
                  color: AppColors.grayScaleWhite,
                  height: 6,
                  width: 25,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
