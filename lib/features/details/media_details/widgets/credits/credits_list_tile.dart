import 'package:flutter/material.dart';
import 'package:pod_player/pod_player.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/details/media_details/utils/utils.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import '../../../../../models/details/credits/movie_credits.dart';
import '../../../../../router/pages.dart';
import '../../../../../router/router.dart';
import '../../../../../widgets/app_list_tile/parts/image_list_tile.dart';

class CreditsListTile extends StatelessWidget {
  const CreditsListTile({
    Key? key,
    required this.credit,
    required this.controller,
  }) : super(key: key);
  final MovieCredits credit;
  final PodPlayerController? controller;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        UtilsMovieDetails.stopVideo(controller);
        AppRouter.navigate(
          context,
          (configuration) => configuration.add(
            PersonDetailsPage(credit),
          ),
        );
      },
      borderRadius: BorderRadius.circular(8),
      child: SizedBox(
        width: 60,
        height: 104,
        child: Column(
          children: [
            SizedBox(
              height: 50,
              child: ImageListTile(
                imagePath: credit.profilePath ?? Constants.empty,
                width: 50,
                height: 50,
                borderRadius: 50,
              ),
            ),
            Flexible(
              child: Text(
                credit.name ?? Constants.empty,
                style: context.textTheme.labelMedium,
                textAlign: TextAlign.center,
                maxLines: 2,
              ),
            ),
            Flexible(
              child: Text(
                credit.job ?? Constants.empty,
                style: context.textTheme.labelSmall,
                textAlign: TextAlign.center,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
