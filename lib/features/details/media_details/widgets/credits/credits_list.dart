import 'package:flutter/material.dart';
import 'package:pod_player/pod_player.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/widgets/empty_box.dart';

import '../../../../../models/details/credits/crew_cast_list.dart';
import '../../../../../widgets/app_title_category.dart';
import 'credits_list_tile.dart';

class CreditsList extends StatelessWidget {
  const CreditsList({
    Key? key,
    required this.credits,
    required this.controller,
  }) : super(key: key);
  final CrewCastList credits;
  final PodPlayerController? controller;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        AppTitleCategory(title: S.of(context).castMembers),
        const SizedBox(height: 16),
        credits.casts.isEmpty
            ? const EmptyBox(
                width: 100,
                height: 100,
              )
            : SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: List.generate(
                    credits.casts.length,
                    (index) {
                      final item = credits.casts[index];
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: CreditsListTile(credit: item, controller: controller),
                      );
                    },
                  ),
                ),
              ),
        AppTitleCategory(title: S.of(context).crewMovie),
        const SizedBox(height: 16),
        credits.crews.isEmpty
            ? const EmptyBox(
                width: 100,
                height: 100,
              )
            : SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: List.generate(
                    credits.crews.length,
                    (index) {
                      final item = credits.crews[index];
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: CreditsListTile(credit: item, controller: controller),
                      );
                    },
                  ),
                ),
              ),
        const SizedBox(height: 16),
      ],
    );
  }
}
