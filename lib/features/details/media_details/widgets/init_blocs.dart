import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../../provider/api/media/provider.dart';
import '../blocs/media_credits_bloc/media_credits_bloc.dart';
import '../blocs/media_details_bloc/media_details_bloc.dart';
import '../blocs/recommendations_bloc/recommendations_bloc.dart';
import '../blocs/video_bloc/video_bloc.dart';

class InitBlocsMovieDetails extends StatelessWidget {
  const InitBlocsMovieDetails({
    Key? key,
    required this.id,
    required this.type,
    required this.child,
  }) : super(key: key);
  final Widget child;
  final String type;
  final int? id;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<MediaDetailsBloc>(
          create: (context) => MediaDetailsBloc(
            provider: RepositoryProvider.of<ProviderMedia>(context),
            internetBloc: context.internetBloc,
            type: type,
            id: id,
          )..add(const MediaDetailsEvent.init()),
        ),
        BlocProvider<VideoBloc>(
          create: (context) => VideoBloc(
            provider: RepositoryProvider.of<ProviderMedia>(context),
            internetBloc: context.internetBloc,
            type: type,
            id: id,
            handleException: context.readException,
          )..add(const VideoEvent.init()),
        ),
        BlocProvider<MediaCreditsBloc>(
          create: (context) => MediaCreditsBloc(
            provider: RepositoryProvider.of<ProviderMedia>(context),
            internetBloc: context.internetBloc,
            type: type,
            id: id,
          )..add(const MediaCreditsEvent.init()),
        ),
        BlocProvider<RecommendationsBloc>(
          create: (context) => RecommendationsBloc(
            provider: RepositoryProvider.of<ProviderMedia>(context),
            internetBloc: context.internetBloc,
            type: type,
            id: id,
          )..add(const RecommendationsEvent.init()),
        ),
      ],
      child: child,
    );
  }
}
