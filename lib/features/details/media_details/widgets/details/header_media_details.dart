import 'package:flutter/material.dart';
import 'package:movie_set/app_design/app_colors.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/details/media_details/utils/utils.dart';
import 'package:movie_set/models/details/movie_details/movie_details.dart';
import 'package:movie_set/models/details/tv_details/tv_details.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import 'seasons/seasons_list.dart';

class HeaderMediaDetails extends StatelessWidget {
  HeaderMediaDetails({
    Key? key,
    required this.movie,
    required this.tv,
  }) : super(key: key);
  final MovieDetails? movie;
  final TvDetails? tv;
  final ValueNotifier<bool> _readMore = ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    final details = movie ?? tv;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            details?.title ?? Constants.empty,
            style: context.primaryTextTheme.bodyMedium,
          ),
          const SizedBox(height: 8),
          Text(
            UtilsMovieDetails.subTitle(movie, tv),
            style: context.textTheme.bodySmall?.copyWith(
              color: context.light ? AppColors.primaryLight : AppColors.primaryExtraLight,
            ),
          ),
          if (tv != null) ...[
            const SizedBox(height: 16),
            SeasonsList(tv: tv!),
          ],
          const SizedBox(height: 16),
          ValueListenableBuilder(
            valueListenable: _readMore,
            builder: (context, value, child) {
              return GestureDetector(
                onTap: () {
                  _readMore.value = !_readMore.value;
                },
                child: Text(
                  details?.overview ?? Constants.empty,
                  maxLines: value ? null : 4,
                  overflow: value ? null : TextOverflow.ellipsis,
                  style: context.textTheme.titleSmall?.copyWith(
                    color: context.dark ? AppColors.grayScaleBackgroundGrey : null,
                  ),
                ),
              );
            },
          ),
          const SizedBox(height: 16),
        ],
      ),
    );
  }
}
