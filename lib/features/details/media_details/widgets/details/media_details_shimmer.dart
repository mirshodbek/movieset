import 'package:flutter/material.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../../../app_design/app_colors.dart';
import '../../../../../constants/constants.dart';
import '../../../../../widgets/animations/shimmer/shimmer_loading.dart';

class MediaDetailsShimmer extends StatelessWidget {
  const MediaDetailsShimmer({
    Key? key,
    required this.type,
  }) : super(key: key);
  final String type;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 16),
        ShimmerLoadingContainer(
          width: context.width / 3,
          height: 20,
        ),
        const SizedBox(height: 16),
        ShimmerLoadingContainer(
          width: context.width,
          height: 10,
        ),
        const SizedBox(height: 4),
        ShimmerLoadingContainer(
          width: context.width / 2,
          height: 10,
        ),
        if (Constants.typeListTvs.contains(type)) ...[
          const SizedBox(height: 16),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(
                3,
                (index) => ShimmerLoadingContainer(
                  width: context.width / 4.5,
                  height: 10,
                ),
              ),
            ),
          ),
          const SizedBox(height: 16),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(
                3,
                (index) => ShimmerLoadingContainer(
                  width: context.width / 3,
                  height: 50,
                  radius: 8,
                ),
              ),
            ),
          ),
          const SizedBox(height: 16),
        ],
        const SizedBox(height: 16),
        for (int i = 0; i < 5; i++) ...[
          ShimmerLoadingContainer(
            width: context.width,
            height: 10,
          ),
          const SizedBox(height: 4),
        ],
        const SizedBox(height: 12),
      ],
    );
  }
}

class ShimmerLoadingContainer extends StatelessWidget {
  const ShimmerLoadingContainer({
    Key? key,
    this.radius,
    this.padding,
    required this.width,
    required this.height,
  }) : super(key: key);
  final double height;
  final double width;
  final double? radius;
  final EdgeInsets? padding;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? const EdgeInsets.symmetric(horizontal: 16),
      child: Shimmer(
        child: ShimmerLoading(
          child: Container(
            width: width,
            height: height,
            decoration: BoxDecoration(
              color: AppColors.grayScaleWhite,
              borderRadius: BorderRadius.circular(radius ?? 0),
            ),
          ),
        ),
      ),
    );
  }
}
