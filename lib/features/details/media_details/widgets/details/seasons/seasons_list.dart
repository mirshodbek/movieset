import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/repo/api/media/repo.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../../../../models/details/tv_details/season/season.dart';
import '../../../../../../models/details/tv_details/tv_details.dart';
import '../../../blocs/season_video_bloc/season_video_bloc.dart';
import 'seasons_title.dart';
import 'seasons_video.dart';

class SeasonsList extends StatelessWidget {
  SeasonsList({
    Key? key,
    required this.tv,
  }) : super(key: key);
  final TvDetails tv;
  late final ValueNotifier<SeasonItem> _season = ValueNotifier(
    SeasonItem(
      index: 0,
      season: tv.seasons?.first,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SeasonVideoBloc(
        internetBloc: context.internetBloc,
        tvId: tv.id,
        repo: RepositoryProvider.of<RepoMedia>(context),
        numberOfSeasons: tv.numberOfSeasons,
        firstSeasonNumber: tv.seasons?.firstOrNull?.seasonNumber
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SeasonTitles(
            seasons: tv.seasons,
            onTap: (season, index) {
              _season.value = SeasonItem(index: index, season: season);
            },
          ),
          ValueListenableBuilder<SeasonItem>(
            valueListenable: _season,
            builder: (context, value, child) {
              return SeasonsVideo(
                seasonIndex: value.index,
                imagePath: value.season?.posterPath,
              );
            },
          ),
        ],
      ),
    );
  }
}

class SeasonItem extends Equatable {
  final int index;
  final Season? season;

  const SeasonItem({
    required this.index,
    required this.season,
  });

  @override
  List<Object?> get props => [season, index];
}
