import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/app_design/app_colors.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/widgets/app_list_tile/parts/image_list_tile.dart';

import '../../../../../../app_design/app_text_style.dart';
import '../../../blocs/season_video_bloc/season_video_bloc.dart';
import '../../../blocs/video_bloc/video_bloc.dart';
import '../media_details_shimmer.dart';

class SeasonsVideo extends StatelessWidget {
  SeasonsVideo({
    Key? key,
    required this.seasonIndex,
    required this.imagePath,
  }) : super(key: key);
  final int seasonIndex;
  final String? imagePath;
  final ValueNotifier<int> _selected = ValueNotifier(-1);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SeasonVideoBloc, SeasonVideoState>(
      builder: (context, state) {
        return state.map(
          init: (_) => SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(
                3,
                (index) => Column(
                  children: [
                    ShimmerLoadingContainer(
                      width: context.width / 3,
                      padding: const EdgeInsets.only(right: 16),
                      height: 50,
                      radius: 8,
                    ),
                    const SizedBox(height: 4),
                    ShimmerLoadingContainer(
                      padding: const EdgeInsets.only(right: 16),
                      width: context.width / 4,
                      height: 5,
                    )
                  ],
                ),
              ),
            ),
          ),
          data: (map) {
            if (map.video[seasonIndex].isEmpty) {
              return const Align(
                alignment: Alignment.center,
                child: Icon(
                  Icons.videocam_off_outlined,
                  size: 80,
                  color: AppColors.primaryExtraLight,
                ),
              );
            }
            return ValueListenableBuilder<int>(
              valueListenable: _selected,
              builder: (context, value, child) {
                return SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: List.generate(
                      map.video[seasonIndex].length,
                      (index) {
                        final video = map.video[seasonIndex][index];
                        return Padding(
                          padding: const EdgeInsets.only(right: 16),
                          child: InkWell(
                            onTap: () {
                              if(value !=index) {
                                _selected.value = index;
                                context.read<VideoBloc>().add(
                                      VideoEvent.playVideo(
                                        seasonVideo: video,
                                      ),
                                    );
                              }
                            },
                            borderRadius: BorderRadius.circular(8),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 50,
                                  child: ImageListTile(
                                    width: 100,
                                    height: 50,
                                    imagePath: imagePath ?? Constants.empty,
                                  ),
                                ),
                                SizedBox(
                                  width: 100,
                                  height: 30,
                                  child: Text(
                                    video.name ?? Constants.empty,
                                    style: value == index
                                        ? context.textTheme.labelMedium?.copyWith(
                                            color: context.dark ? AppColors.grayScaleWhite : null,
                                          )
                                        : AppTextStyle.s10w500.copyWith(
                                            color: AppColors.primaryExtraLight,
                                          ),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                );
              },
            );
          },
        );
      },
    );
  }
}
