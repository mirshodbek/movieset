import 'package:flutter/material.dart';
import 'package:movie_set/app_design/app_text_style.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../../../../app_design/app_colors.dart';
import '../../../../../../constants/constants.dart';
import '../../../../../../models/details/tv_details/season/season.dart';

class SeasonTitles extends StatelessWidget {
  SeasonTitles({
    Key? key,
    required this.onTap,
    required this.seasons,
  }) : super(key: key);
  final List<Season>? seasons;
  final Function(Season?, int) onTap;
  final ValueNotifier<int> _active = ValueNotifier(0);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<int>(
      valueListenable: _active,
      builder: (context, value, child) {
        return SizedBox(
          height: 50,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(
                seasons?.length ?? 0,
                (index) {
                  final season = seasons?[index];
                  return Padding(
                    padding: const EdgeInsets.only(right: 32),
                    child: InkWell(
                      onTap: () {
                        onTap(season, index);
                        _active.value = index;
                      },
                      child: Container(
                        padding: const EdgeInsets.all(4),
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: value == index ? AppColors.secondaryDark : Colors.transparent,
                              width: 2,
                            ),
                          ),
                        ),
                        child: Text(
                          season?.name ?? Constants.empty,
                          style: value == index
                              ? context.textTheme.displayMedium
                              : AppTextStyle.s16w500.copyWith(
                                  color: AppColors.primaryExtraLight,
                                ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }
}
