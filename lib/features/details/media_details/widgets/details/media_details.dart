import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/media_details_bloc/media_details_bloc.dart';
import 'header_media_details.dart';
import 'media_details_shimmer.dart';

class MediaDetails extends StatelessWidget {
  const MediaDetails({
    Key? key,
    required this.type,
  }) : super(key: key);
  final String type;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MediaDetailsBloc, MediaDetailsState>(
      builder: (context, state) {
        return state.map(
          init: (_) => MediaDetailsShimmer(type: type),
          data: (bloc) => HeaderMediaDetails(movie: bloc.movie, tv: bloc.tv),
        );
      },
    );
  }
}
