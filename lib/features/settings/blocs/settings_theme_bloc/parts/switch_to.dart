part of '../settings_theme_bloc.dart';

extension SwitchTo on SettingsThemeBloc {
  Future<void> _switchTo(
    _SwitchTo event,
    Emitter<SettingsThemeState> emit,
  ) async {
    switch (event.themeType) {
      case Constants.light:
        await repo.updateTheme(
          theme: Constants.light,
          themeType: Constants.platformTheme,
        );
        emit(state.copyWith(theme: ThemeMode.light, themeType: Constants.light));
        return;

      case Constants.dark:
        await repo.updateTheme(
          theme: Constants.dark,
          themeType: Constants.dark,
        );
        emit(state.copyWith(theme: ThemeMode.dark, themeType: Constants.dark));
        return;

      default:
        switch (brightness) {
          case Brightness.dark:
            await repo.updateTheme(
              theme: Constants.dark,
              themeType: Constants.platformTheme,
            );
            emit(state.copyWith(theme: ThemeMode.dark, themeType: Constants.platformTheme));
            return;
          default:
            await repo.updateTheme(
              theme: Constants.light,
              themeType: Constants.platformTheme,
            );
            emit(state.copyWith(theme: ThemeMode.light, themeType: Constants.platformTheme));
            return;
        }
    }
  }
}
