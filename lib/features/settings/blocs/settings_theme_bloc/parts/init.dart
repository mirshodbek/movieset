part of '../settings_theme_bloc.dart';

extension Init on SettingsThemeBloc{
  Future<void> _init(
      _Init event,
      Emitter<SettingsThemeState> emit,
      ) async {
    final theme = repo.getTheme();
    final themeType = repo.getThemeType();
    if (theme == null || themeType == null || themeType == Constants.platformTheme) {
      switch (brightness) {
        case Brightness.dark:
          await repo.updateTheme(
            theme: Constants.dark,
            themeType: Constants.platformTheme,
          );
          emit(state.copyWith(theme: ThemeMode.dark, themeType: Constants.platformTheme));
          return;
        default:

          await repo.updateTheme(
            theme: Constants.light,
            themeType: Constants.platformTheme,
          );
          emit(state.copyWith(theme: ThemeMode.light, themeType: Constants.platformTheme));
          return;
      }
    } else {
      switch (themeType) {
        case Constants.dark:
          emit(state.copyWith(theme: ThemeMode.dark, themeType: Constants.dark));
          return;
        default:
          emit(state.copyWith(theme: ThemeMode.light, themeType: Constants.light));
          return;
      }
    }
  }
}