// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'settings_theme_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SettingsThemeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(String themeType) switchTo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(String themeType)? switchTo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(String themeType)? switchTo,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SwitchTo value) switchTo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
    TResult? Function(_SwitchTo value)? switchTo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SwitchTo value)? switchTo,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsThemeEventCopyWith<$Res> {
  factory $SettingsThemeEventCopyWith(
          SettingsThemeEvent value, $Res Function(SettingsThemeEvent) then) =
      _$SettingsThemeEventCopyWithImpl<$Res, SettingsThemeEvent>;
}

/// @nodoc
class _$SettingsThemeEventCopyWithImpl<$Res, $Val extends SettingsThemeEvent>
    implements $SettingsThemeEventCopyWith<$Res> {
  _$SettingsThemeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitImplCopyWith<$Res> {
  factory _$$InitImplCopyWith(
          _$InitImpl value, $Res Function(_$InitImpl) then) =
      __$$InitImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitImplCopyWithImpl<$Res>
    extends _$SettingsThemeEventCopyWithImpl<$Res, _$InitImpl>
    implements _$$InitImplCopyWith<$Res> {
  __$$InitImplCopyWithImpl(_$InitImpl _value, $Res Function(_$InitImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitImpl implements _Init {
  const _$InitImpl();

  @override
  String toString() {
    return 'SettingsThemeEvent.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(String themeType) switchTo,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(String themeType)? switchTo,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(String themeType)? switchTo,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SwitchTo value) switchTo,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
    TResult? Function(_SwitchTo value)? switchTo,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SwitchTo value)? switchTo,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements SettingsThemeEvent {
  const factory _Init() = _$InitImpl;
}

/// @nodoc
abstract class _$$SwitchToImplCopyWith<$Res> {
  factory _$$SwitchToImplCopyWith(
          _$SwitchToImpl value, $Res Function(_$SwitchToImpl) then) =
      __$$SwitchToImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String themeType});
}

/// @nodoc
class __$$SwitchToImplCopyWithImpl<$Res>
    extends _$SettingsThemeEventCopyWithImpl<$Res, _$SwitchToImpl>
    implements _$$SwitchToImplCopyWith<$Res> {
  __$$SwitchToImplCopyWithImpl(
      _$SwitchToImpl _value, $Res Function(_$SwitchToImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? themeType = null,
  }) {
    return _then(_$SwitchToImpl(
      themeType: null == themeType
          ? _value.themeType
          : themeType // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SwitchToImpl implements _SwitchTo {
  const _$SwitchToImpl({required this.themeType});

  @override
  final String themeType;

  @override
  String toString() {
    return 'SettingsThemeEvent.switchTo(themeType: $themeType)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SwitchToImpl &&
            (identical(other.themeType, themeType) ||
                other.themeType == themeType));
  }

  @override
  int get hashCode => Object.hash(runtimeType, themeType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SwitchToImplCopyWith<_$SwitchToImpl> get copyWith =>
      __$$SwitchToImplCopyWithImpl<_$SwitchToImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(String themeType) switchTo,
  }) {
    return switchTo(themeType);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(String themeType)? switchTo,
  }) {
    return switchTo?.call(themeType);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(String themeType)? switchTo,
    required TResult orElse(),
  }) {
    if (switchTo != null) {
      return switchTo(themeType);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SwitchTo value) switchTo,
  }) {
    return switchTo(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
    TResult? Function(_SwitchTo value)? switchTo,
  }) {
    return switchTo?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SwitchTo value)? switchTo,
    required TResult orElse(),
  }) {
    if (switchTo != null) {
      return switchTo(this);
    }
    return orElse();
  }
}

abstract class _SwitchTo implements SettingsThemeEvent {
  const factory _SwitchTo({required final String themeType}) = _$SwitchToImpl;

  String get themeType;
  @JsonKey(ignore: true)
  _$$SwitchToImplCopyWith<_$SwitchToImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SettingsThemeState {
  ThemeMode get theme => throw _privateConstructorUsedError;
  String? get themeType => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ThemeMode theme, String? themeType) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ThemeMode theme, String? themeType)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ThemeMode theme, String? themeType)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DataSettingsThemeState value) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(DataSettingsThemeState value)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DataSettingsThemeState value)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SettingsThemeStateCopyWith<SettingsThemeState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsThemeStateCopyWith<$Res> {
  factory $SettingsThemeStateCopyWith(
          SettingsThemeState value, $Res Function(SettingsThemeState) then) =
      _$SettingsThemeStateCopyWithImpl<$Res, SettingsThemeState>;
  @useResult
  $Res call({ThemeMode theme, String? themeType});
}

/// @nodoc
class _$SettingsThemeStateCopyWithImpl<$Res, $Val extends SettingsThemeState>
    implements $SettingsThemeStateCopyWith<$Res> {
  _$SettingsThemeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? theme = null,
    Object? themeType = freezed,
  }) {
    return _then(_value.copyWith(
      theme: null == theme
          ? _value.theme
          : theme // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
      themeType: freezed == themeType
          ? _value.themeType
          : themeType // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$DataSettingsThemeStateImplCopyWith<$Res>
    implements $SettingsThemeStateCopyWith<$Res> {
  factory _$$DataSettingsThemeStateImplCopyWith(
          _$DataSettingsThemeStateImpl value,
          $Res Function(_$DataSettingsThemeStateImpl) then) =
      __$$DataSettingsThemeStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({ThemeMode theme, String? themeType});
}

/// @nodoc
class __$$DataSettingsThemeStateImplCopyWithImpl<$Res>
    extends _$SettingsThemeStateCopyWithImpl<$Res, _$DataSettingsThemeStateImpl>
    implements _$$DataSettingsThemeStateImplCopyWith<$Res> {
  __$$DataSettingsThemeStateImplCopyWithImpl(
      _$DataSettingsThemeStateImpl _value,
      $Res Function(_$DataSettingsThemeStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? theme = null,
    Object? themeType = freezed,
  }) {
    return _then(_$DataSettingsThemeStateImpl(
      theme: null == theme
          ? _value.theme
          : theme // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
      themeType: freezed == themeType
          ? _value.themeType
          : themeType // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$DataSettingsThemeStateImpl implements DataSettingsThemeState {
  const _$DataSettingsThemeStateImpl({required this.theme, this.themeType});

  @override
  final ThemeMode theme;
  @override
  final String? themeType;

  @override
  String toString() {
    return 'SettingsThemeState.data(theme: $theme, themeType: $themeType)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataSettingsThemeStateImpl &&
            (identical(other.theme, theme) || other.theme == theme) &&
            (identical(other.themeType, themeType) ||
                other.themeType == themeType));
  }

  @override
  int get hashCode => Object.hash(runtimeType, theme, themeType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataSettingsThemeStateImplCopyWith<_$DataSettingsThemeStateImpl>
      get copyWith => __$$DataSettingsThemeStateImplCopyWithImpl<
          _$DataSettingsThemeStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ThemeMode theme, String? themeType) data,
  }) {
    return data(theme, themeType);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ThemeMode theme, String? themeType)? data,
  }) {
    return data?.call(theme, themeType);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ThemeMode theme, String? themeType)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(theme, themeType);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DataSettingsThemeState value) data,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(DataSettingsThemeState value)? data,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DataSettingsThemeState value)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class DataSettingsThemeState implements SettingsThemeState {
  const factory DataSettingsThemeState(
      {required final ThemeMode theme,
      final String? themeType}) = _$DataSettingsThemeStateImpl;

  @override
  ThemeMode get theme;
  @override
  String? get themeType;
  @override
  @JsonKey(ignore: true)
  _$$DataSettingsThemeStateImplCopyWith<_$DataSettingsThemeStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
