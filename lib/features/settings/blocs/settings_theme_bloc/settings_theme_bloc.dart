import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../constants/constants.dart';
import '../../../../repo/storage/base_repo_storage.dart';

part 'settings_theme_bloc.freezed.dart';

part 'parts/init.dart';

part 'parts/switch_to.dart';

class SettingsThemeBloc extends Bloc<SettingsThemeEvent, SettingsThemeState> {
  SettingsThemeBloc(this.repo) : super(const SettingsThemeState.data(theme: ThemeMode.light)) {
    on<_Init>(_init);
    on<_SwitchTo>(_switchTo);
  }

  final BaseRepoStorage repo;
  late final brightness = SchedulerBinding.instance.window.platformBrightness;

  ThemeMode get theme =>state.theme;
}

@freezed
class SettingsThemeEvent with _$SettingsThemeEvent {
  const factory SettingsThemeEvent.init() = _Init;

  const factory SettingsThemeEvent.switchTo({
    required String themeType,
  }) = _SwitchTo;
}

@freezed
class SettingsThemeState with _$SettingsThemeState {
  const factory SettingsThemeState.data({
    required ThemeMode theme,
    String? themeType,
  }) = DataSettingsThemeState;
}
