part of '../settings_localization_bloc.dart';

extension SwitchTo on SettingsLocalizationBloc {
  Future<void> _switchTo(
    _SwitchTo event,
    Emitter<SettingsLocalizationState> emit,
  ) async {
    await repo.updateLocale(event.locale);
    emit(state.copyWith(locale: event.locale));
  }
}
