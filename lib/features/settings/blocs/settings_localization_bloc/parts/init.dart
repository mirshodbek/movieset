part of '../settings_localization_bloc.dart';

extension Init on SettingsLocalizationBloc {
  Future<void> _init(
    _Init event,
    Emitter<SettingsLocalizationState> emit,
  ) async {
    final result = repo.readLocale();
    if (result == null) {
      if (event.platform.startsWith('ru')) {
        await repo.updateLocale(AppLocale.ru);
        emit(state.copyWith(locale: AppLocale.ru));
        return;
      } else if (event.platform.startsWith('en')) {
        await repo.updateLocale(AppLocale.en);
        emit(state.copyWith(locale: AppLocale.en));
        return;
      } else {
        await repo.updateLocale(AppLocale.en);
        emit(state.copyWith(locale: AppLocale.en));
        return;
      }
    } else {
      emit(state.copyWith(locale: result));
    }
  }
}
