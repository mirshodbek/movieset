import 'dart:ui';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../repo/storage/base_repo_storage.dart';

part 'settings_localization_bloc.freezed.dart';

part 'parts/init.dart';

part 'parts/switch_to.dart';

class SettingsLocalizationBloc extends Bloc<SettingsLocalizationEvent, SettingsLocalizationState> {
  SettingsLocalizationBloc(this.repo)
      : super(const SettingsLocalizationState.data(locale: AppLocale.en)) {
    on<_Init>(_init);
    on<_SwitchTo>(_switchTo);
  }

  Iterable<Locale> get supportedLocales {
    return AppLocale.values.map((e) => Locale(e.name));
  }

  final BaseRepoStorage repo;
}

@freezed
class SettingsLocalizationEvent with _$SettingsLocalizationEvent {
  const factory SettingsLocalizationEvent.init({required String platform}) = _Init;

  const factory SettingsLocalizationEvent.switchTo({required AppLocale locale}) = _SwitchTo;
}

@freezed
class SettingsLocalizationState with _$SettingsLocalizationState {
  const factory SettingsLocalizationState.data({
    required AppLocale locale,
  }) = DataSettingsLocalizationState;
}

enum AppLocale { ru, en }
