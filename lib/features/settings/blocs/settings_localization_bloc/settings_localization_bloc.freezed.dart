// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'settings_localization_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SettingsLocalizationEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String platform) init,
    required TResult Function(AppLocale locale) switchTo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String platform)? init,
    TResult? Function(AppLocale locale)? switchTo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String platform)? init,
    TResult Function(AppLocale locale)? switchTo,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SwitchTo value) switchTo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
    TResult? Function(_SwitchTo value)? switchTo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SwitchTo value)? switchTo,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsLocalizationEventCopyWith<$Res> {
  factory $SettingsLocalizationEventCopyWith(SettingsLocalizationEvent value,
          $Res Function(SettingsLocalizationEvent) then) =
      _$SettingsLocalizationEventCopyWithImpl<$Res, SettingsLocalizationEvent>;
}

/// @nodoc
class _$SettingsLocalizationEventCopyWithImpl<$Res,
        $Val extends SettingsLocalizationEvent>
    implements $SettingsLocalizationEventCopyWith<$Res> {
  _$SettingsLocalizationEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitImplCopyWith<$Res> {
  factory _$$InitImplCopyWith(
          _$InitImpl value, $Res Function(_$InitImpl) then) =
      __$$InitImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String platform});
}

/// @nodoc
class __$$InitImplCopyWithImpl<$Res>
    extends _$SettingsLocalizationEventCopyWithImpl<$Res, _$InitImpl>
    implements _$$InitImplCopyWith<$Res> {
  __$$InitImplCopyWithImpl(_$InitImpl _value, $Res Function(_$InitImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? platform = null,
  }) {
    return _then(_$InitImpl(
      platform: null == platform
          ? _value.platform
          : platform // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$InitImpl implements _Init {
  const _$InitImpl({required this.platform});

  @override
  final String platform;

  @override
  String toString() {
    return 'SettingsLocalizationEvent.init(platform: $platform)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitImpl &&
            (identical(other.platform, platform) ||
                other.platform == platform));
  }

  @override
  int get hashCode => Object.hash(runtimeType, platform);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InitImplCopyWith<_$InitImpl> get copyWith =>
      __$$InitImplCopyWithImpl<_$InitImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String platform) init,
    required TResult Function(AppLocale locale) switchTo,
  }) {
    return init(platform);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String platform)? init,
    TResult? Function(AppLocale locale)? switchTo,
  }) {
    return init?.call(platform);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String platform)? init,
    TResult Function(AppLocale locale)? switchTo,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(platform);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SwitchTo value) switchTo,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
    TResult? Function(_SwitchTo value)? switchTo,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SwitchTo value)? switchTo,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements SettingsLocalizationEvent {
  const factory _Init({required final String platform}) = _$InitImpl;

  String get platform;
  @JsonKey(ignore: true)
  _$$InitImplCopyWith<_$InitImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SwitchToImplCopyWith<$Res> {
  factory _$$SwitchToImplCopyWith(
          _$SwitchToImpl value, $Res Function(_$SwitchToImpl) then) =
      __$$SwitchToImplCopyWithImpl<$Res>;
  @useResult
  $Res call({AppLocale locale});
}

/// @nodoc
class __$$SwitchToImplCopyWithImpl<$Res>
    extends _$SettingsLocalizationEventCopyWithImpl<$Res, _$SwitchToImpl>
    implements _$$SwitchToImplCopyWith<$Res> {
  __$$SwitchToImplCopyWithImpl(
      _$SwitchToImpl _value, $Res Function(_$SwitchToImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? locale = null,
  }) {
    return _then(_$SwitchToImpl(
      locale: null == locale
          ? _value.locale
          : locale // ignore: cast_nullable_to_non_nullable
              as AppLocale,
    ));
  }
}

/// @nodoc

class _$SwitchToImpl implements _SwitchTo {
  const _$SwitchToImpl({required this.locale});

  @override
  final AppLocale locale;

  @override
  String toString() {
    return 'SettingsLocalizationEvent.switchTo(locale: $locale)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SwitchToImpl &&
            (identical(other.locale, locale) || other.locale == locale));
  }

  @override
  int get hashCode => Object.hash(runtimeType, locale);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SwitchToImplCopyWith<_$SwitchToImpl> get copyWith =>
      __$$SwitchToImplCopyWithImpl<_$SwitchToImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String platform) init,
    required TResult Function(AppLocale locale) switchTo,
  }) {
    return switchTo(locale);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String platform)? init,
    TResult? Function(AppLocale locale)? switchTo,
  }) {
    return switchTo?.call(locale);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String platform)? init,
    TResult Function(AppLocale locale)? switchTo,
    required TResult orElse(),
  }) {
    if (switchTo != null) {
      return switchTo(locale);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SwitchTo value) switchTo,
  }) {
    return switchTo(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
    TResult? Function(_SwitchTo value)? switchTo,
  }) {
    return switchTo?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SwitchTo value)? switchTo,
    required TResult orElse(),
  }) {
    if (switchTo != null) {
      return switchTo(this);
    }
    return orElse();
  }
}

abstract class _SwitchTo implements SettingsLocalizationEvent {
  const factory _SwitchTo({required final AppLocale locale}) = _$SwitchToImpl;

  AppLocale get locale;
  @JsonKey(ignore: true)
  _$$SwitchToImplCopyWith<_$SwitchToImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SettingsLocalizationState {
  AppLocale get locale => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AppLocale locale) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(AppLocale locale)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AppLocale locale)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DataSettingsLocalizationState value) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(DataSettingsLocalizationState value)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DataSettingsLocalizationState value)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SettingsLocalizationStateCopyWith<SettingsLocalizationState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsLocalizationStateCopyWith<$Res> {
  factory $SettingsLocalizationStateCopyWith(SettingsLocalizationState value,
          $Res Function(SettingsLocalizationState) then) =
      _$SettingsLocalizationStateCopyWithImpl<$Res, SettingsLocalizationState>;
  @useResult
  $Res call({AppLocale locale});
}

/// @nodoc
class _$SettingsLocalizationStateCopyWithImpl<$Res,
        $Val extends SettingsLocalizationState>
    implements $SettingsLocalizationStateCopyWith<$Res> {
  _$SettingsLocalizationStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? locale = null,
  }) {
    return _then(_value.copyWith(
      locale: null == locale
          ? _value.locale
          : locale // ignore: cast_nullable_to_non_nullable
              as AppLocale,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$DataSettingsLocalizationStateImplCopyWith<$Res>
    implements $SettingsLocalizationStateCopyWith<$Res> {
  factory _$$DataSettingsLocalizationStateImplCopyWith(
          _$DataSettingsLocalizationStateImpl value,
          $Res Function(_$DataSettingsLocalizationStateImpl) then) =
      __$$DataSettingsLocalizationStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({AppLocale locale});
}

/// @nodoc
class __$$DataSettingsLocalizationStateImplCopyWithImpl<$Res>
    extends _$SettingsLocalizationStateCopyWithImpl<$Res,
        _$DataSettingsLocalizationStateImpl>
    implements _$$DataSettingsLocalizationStateImplCopyWith<$Res> {
  __$$DataSettingsLocalizationStateImplCopyWithImpl(
      _$DataSettingsLocalizationStateImpl _value,
      $Res Function(_$DataSettingsLocalizationStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? locale = null,
  }) {
    return _then(_$DataSettingsLocalizationStateImpl(
      locale: null == locale
          ? _value.locale
          : locale // ignore: cast_nullable_to_non_nullable
              as AppLocale,
    ));
  }
}

/// @nodoc

class _$DataSettingsLocalizationStateImpl
    implements DataSettingsLocalizationState {
  const _$DataSettingsLocalizationStateImpl({required this.locale});

  @override
  final AppLocale locale;

  @override
  String toString() {
    return 'SettingsLocalizationState.data(locale: $locale)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataSettingsLocalizationStateImpl &&
            (identical(other.locale, locale) || other.locale == locale));
  }

  @override
  int get hashCode => Object.hash(runtimeType, locale);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataSettingsLocalizationStateImplCopyWith<
          _$DataSettingsLocalizationStateImpl>
      get copyWith => __$$DataSettingsLocalizationStateImplCopyWithImpl<
          _$DataSettingsLocalizationStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AppLocale locale) data,
  }) {
    return data(locale);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(AppLocale locale)? data,
  }) {
    return data?.call(locale);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AppLocale locale)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(locale);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DataSettingsLocalizationState value) data,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(DataSettingsLocalizationState value)? data,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DataSettingsLocalizationState value)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class DataSettingsLocalizationState
    implements SettingsLocalizationState {
  const factory DataSettingsLocalizationState(
      {required final AppLocale locale}) = _$DataSettingsLocalizationStateImpl;

  @override
  AppLocale get locale;
  @override
  @JsonKey(ignore: true)
  _$$DataSettingsLocalizationStateImplCopyWith<
          _$DataSettingsLocalizationStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
