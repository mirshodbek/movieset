import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

import '../../../app_design/app_colors.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../cubits/settings_cubit.dart';

class SettingsWifi extends StatelessWidget {
  const SettingsWifi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        decoration: BoxDecoration(
          color: context.colorScheme.onTertiary,
          borderRadius: BorderRadius.circular(8),
        ),
        child: BlocBuilder<SettingsCubit, SettingsState>(
          buildWhen: (prev, curr) => prev.wifi != curr.wifi,
          builder: (context, state) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  S.of(context).downloadOnlyViaWifi,
                  style: context.textTheme.titleSmall?.copyWith(
                    color:
                        context.light ? AppColors.primaryLight : AppColors.primarySuperExtraLight,
                  ),
                ),
                CupertinoSwitch(
                  value: state.wifi,
                  activeColor: AppColors.secondaryMain,
                  trackColor: AppColors.primarySuperExtraLight,
                  onChanged: (value) {
                    context.read<SettingsCubit>().switchToWifi(value).then(
                      (exception) {
                        if (exception == null) return;
                        return context.readException(exception);
                      },
                    );
                  },
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
