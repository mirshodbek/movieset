import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/app_design/app_colors.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../../constants/constants.dart';
import '../blocs/settings_theme_bloc/settings_theme_bloc.dart';

class SettingsTheme extends StatelessWidget {
  const SettingsTheme({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            S.of(context).designTheme,
            style: context.textTheme.displaySmall,
          ),
          const SizedBox(height: 16),
          BlocBuilder<SettingsThemeBloc, SettingsThemeState>(
            builder: (context, state) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _IconContainer(
                    onTap: () {
                      context.read<SettingsThemeBloc>().add(
                            const SettingsThemeEvent.switchTo(
                              themeType: Constants.platformTheme,
                            ),
                          );
                    },
                    selected: state.themeType == Constants.platformTheme,
                    icon: Icons.settings,
                    dark: state.theme == ThemeMode.dark,
                    title: S.of(context).system,
                  ),
                  const SizedBox(width: 4),
                  _IconContainer(
                    onTap: () {
                      context.read<SettingsThemeBloc>().add(
                            const SettingsThemeEvent.switchTo(
                              themeType: Constants.dark,
                            ),
                          );
                    },
                    selected: state.themeType == Constants.dark,
                    icon: CupertinoIcons.moon_fill,
                    dark: true,
                    title: S.of(context).dark,
                  ),
                  const SizedBox(width: 4),
                  _IconContainer(
                    onTap: () {
                      context.read<SettingsThemeBloc>().add(
                            const SettingsThemeEvent.switchTo(
                              themeType: Constants.light,
                            ),
                          );
                    },
                    selected: state.themeType == Constants.light,
                    icon: Icons.sunny,
                    title: S.of(context).light,
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}

class _IconContainer extends StatelessWidget {
  const _IconContainer({
    Key? key,
    this.dark = false,
    required this.selected,
    required this.onTap,
    required this.icon,
    required this.title,
  }) : super(key: key);
  final IconData icon;
  final String title;
  final VoidCallback onTap;
  final bool selected;
  final bool dark;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
          decoration: BoxDecoration(
            color: selected
                ? dark
                    ? AppColors.primaryMain
                    : AppColors.secondaryMain
                : context.colorScheme.onTertiary,
            border: Border.all(
              color: selected ? AppColors.secondaryMain : context.colorScheme.onTertiary,
            ),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            children: [
              Icon(
                icon,
                color:
                    selected ? context.colorScheme.tertiaryContainer : context.colorScheme.primary,
              ),
              const SizedBox(height: 8),
              Text(
                title,
                style: context.textTheme.titleMedium?.copyWith(
                  color: selected ? AppColors.grayScaleWhite : null,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
