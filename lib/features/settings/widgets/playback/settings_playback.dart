import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import '../../../../app_design/app_colors.dart';
import '../../cubits/settings_cubit.dart';

class SettingsPlayback extends StatelessWidget {
  const SettingsPlayback({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            S.of(context).playback,
            style: context.textTheme.displaySmall,
          ),
          const SizedBox(height: 16),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            decoration: BoxDecoration(
              color: context.colorScheme.onTertiary,
              borderRadius: BorderRadius.circular(8),
            ),
            child:  Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      S.of(context).horizontalOnly,
                      style: context.textTheme.titleSmall?.copyWith(
                        color: context.light
                            ? AppColors.primaryLight
                            : AppColors.primarySuperExtraLight,
                      ),
                    ),
                    Text(
                      S.of(context).orientationPlayer,
                      style: context.textTheme.bodySmall?.copyWith(
                        color: context.light
                            ? AppColors.primaryExtraLight
                            : AppColors.grayScaleBorderLightGrey,
                      ),
                    ),
                  ],
                ),
                BlocBuilder<SettingsCubit, SettingsState>(
                  builder: (context, state) {
                    return CupertinoSwitch(
                      value: state.orientationVideo != OrientationVideo.vertical,
                      activeColor: AppColors.secondaryMain,
                      trackColor: AppColors.primarySuperExtraLight,
                      onChanged: (value) {
                        context.read<SettingsCubit>().switchToOrientationVideo(value).then(
                              (exception) {
                            if (exception == null) return;
                            return context.readException(exception);
                          },
                        );
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
