import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/utils/extensions/int_extension.dart';

import '../../../app_design/app_colors.dart';
import '../../../router/route_path.dart';
import '../../../router/router.dart';
import '../../../widgets/bottom_sheet/app_bottom_sheet.dart';
import '../../../widgets/bottom_sheet/bottom_sheet_item.dart';
import '../blocs/settings_localization_bloc/settings_localization_bloc.dart';

class SettingsLanguage extends StatelessWidget {
  const SettingsLanguage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: ListTile(
        onTap: () async {
          AppRouter.navigate(
            context,
            (configuration) => configuration.add(
              SettingsPage(newLocation: RoutePath.settingsBottomSheetLanguages),
            ),
          );
          await AppRouter.showBottomSheet(
            context: context,
            builder: (_) {
              return AppBottomSheet(
                onTap: (items) {
                  if (items?.isNotEmpty ?? false) {
                    context.read<SettingsLocalizationBloc>().add(
                          SettingsLocalizationEvent.switchTo(
                            locale:
                                AppLocale.values.firstWhere((it) => it.index == items!.first.id),
                          ),
                        );
                  }
                },
                items: const [
                  BottomSheetItem(id: 0, name: Constants.russian),
                  BottomSheetItem(id: 1, name: Constants.english),
                ],
                initial: [BottomSheetItem(id: context.appLocale.index)],
                type: Constants.typeOneCheck,
                clearButton: false,
                titleButton: S.of(context).apply,
                primaryTitle: S.of(context).changeLanguage,
              );
            },
          ).whenComplete(
            () {
              AppRouter.navigate(
                context,
                (configuration) => configuration.replace(
                  SettingsPage(),
                  allPages: true,
                ),
              );
            },
          );
        },
        title: Text(
          S.of(context).changeLanguage,
          style: context.textTheme.titleSmall?.copyWith(
            color: context.light ? AppColors.primaryLight : AppColors.primarySuperExtraLight,
          ),
        ),
        trailing: SizedBox(
          width: 84,
          child: Row(
            children: [
              Text(
                context.appLocale.index.toLocaleName,
                style: context.textTheme.bodySmall?.copyWith(
                  color: context.colorScheme.tertiary,
                ),
              ),
              const SizedBox(width: 8),
              const Icon(Icons.keyboard_arrow_right),
            ],
          ),
        ),
      ),
    );
  }
}
