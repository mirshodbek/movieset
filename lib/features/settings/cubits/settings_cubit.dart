import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/provider/exceptions.dart';
import 'package:movie_set/repo/storage/base_repo_storage.dart';

class SettingsCubit extends Cubit<SettingsState> {
  SettingsCubit({
    required this.repo,
  }) : super(const SettingsState()) {
    readState();
  }

  void readState() async {
    final wifi = repo.getSettingsWifi() ?? false;
    final orientationVideo = repo.getSettingsOrientationVideo() ?? OrientationVideo.horizontal.name;
    emit(
      state.copyWith(
        wifi: wifi,
        orientationVideo: OrientationVideo.values.firstWhere((it) => it.name == orientationVideo),
      ),
    );
  }

  Future<Exception?> switchToWifi(bool value) async {
    final result = await repo.updateSettingsWifi(value);
    if (result is UnknownErrorException) return result as UnknownErrorException;
    emit(state.copyWith(wifi: value));
    return null;
  }

  Future<Exception?> switchToOrientationVideo(bool value) async {
    final orientationVideo = value ? OrientationVideo.horizontal : OrientationVideo.vertical;
    final result = await repo.updateSettingsOrientationVideo(orientationVideo);
    if (result is UnknownErrorException) return result as UnknownErrorException;
    emit(
      state.copyWith(
        orientationVideo: OrientationVideo.values.firstWhere(
          (it) => it.name == orientationVideo.name,
        ),
      ),
    );
    return null;
  }

  void switchToEnableVertical() =>
      emit(state.copyWith(orientationVideo: OrientationVideo.enableVertical));

  void switchToVertical() => emit(state.copyWith(orientationVideo: OrientationVideo.vertical));

  final BaseRepoStorage repo;
}

class SettingsState extends Equatable {
  final bool wifi;
  final OrientationVideo orientationVideo;

  const SettingsState({
    this.wifi = false,
    this.orientationVideo = OrientationVideo.vertical,
  });

  SettingsState copyWith({
    bool? wifi,
    OrientationVideo? orientationVideo,
  }) {
    return SettingsState(
      wifi: wifi ?? this.wifi,
      orientationVideo: orientationVideo ?? this.orientationVideo,
    );
  }

  @override
  List<Object?> get props => [wifi, orientationVideo];
}

enum OrientationVideo { vertical, enableVertical, horizontal }
