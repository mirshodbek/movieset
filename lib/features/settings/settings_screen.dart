import 'package:flutter/material.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';
import 'package:movie_set/widgets/main_app_bar.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

import 'widgets/playback/settings_playback.dart';
import 'widgets/settings_language.dart';
import 'widgets/settings_theme.dart';
import 'widgets/settings_wifi.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppBar(
        primaryTitle: S.of(context).settings,
        backButton: false,
      ),
      body: const CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: SettingsWifi(),
          ),
          SliverToBoxAdapter(
            child: SettingsLanguage(),
          ),
          SliverToBoxAdapter(
            child: SettingsTheme(),
          ),
          SliverToBoxAdapter(
            child: SettingsPlayback(),
          ),
        ],
      ),
      bottomNavigationBar: AppNavBar(currentItem: context.appNavBarItem),
    );
  }
}
