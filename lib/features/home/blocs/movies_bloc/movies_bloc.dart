import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/features/root/blocs/internet_connection_bloc/internet_connection_bloc.dart';
import 'package:movie_set/features/settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';
import 'package:movie_set/repo/api/media/base.dart';

import '../../../../models/media/media.dart';
import '../../../../repo/internet_connection.dart';

part 'movies_bloc.freezed.dart';

class MoviesBloc extends Bloc<MoviesEvent, MoviesState> {
  MoviesBloc({
    this.runTest = false,
    required this.repo,
    required this.internetBloc,
    required this.localizationBloc,
  }) : super(const MoviesState.init()) {
    _init();
    _updateDataWithLocale();
    _listenInternetConnection();
    on<_Init>((event, emit) {
      emit(MoviesState.readyMovies(movies: event.movies));
    });
  }

  final bool runTest;
  final BaseRepoMedia repo;
  final InternetConnectionBloc internetBloc;
  final SettingsLocalizationBloc localizationBloc;
  late final StreamSubscription _subMovies;
  late final StreamSubscription _subLocale;
  late final StreamSubscription _subInternet;

  Future<void> _init() async {
    _subMovies = repo.getMovies().listen((it) {
      if (it.isNotEmpty) {
        add(MoviesEvent.init(movies: it));
      }
    });
  }

  Future<void> _updateDataWithLocale() async => _subLocale =
      localizationBloc.stream.listen((event) async => await _init());

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) async {
      if (event.connection != ConnectionStatus.offline && !runTest) {
        await _init();
      }
    });
  }

  @override
  Future<void> close() {
    _subMovies.cancel();
    _subLocale.cancel();
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class MoviesEvent with _$MoviesEvent {
  const factory MoviesEvent.init({
    required List<Media> movies,
  }) = _Init;
}

@freezed
class MoviesState with _$MoviesState {
  const factory MoviesState.init() = MoviesInitState;

  const factory MoviesState.readyMovies({
    required List<Media> movies,
  }) = MoviesReadyState;
}
