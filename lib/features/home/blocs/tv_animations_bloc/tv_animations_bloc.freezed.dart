// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'tv_animations_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$TvAnimationsEvent {
  List<Media> get tvAnimations => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Media> tvAnimations) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<Media> tvAnimations)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Media> tvAnimations)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TvAnimationsEventCopyWith<TvAnimationsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TvAnimationsEventCopyWith<$Res> {
  factory $TvAnimationsEventCopyWith(
          TvAnimationsEvent value, $Res Function(TvAnimationsEvent) then) =
      _$TvAnimationsEventCopyWithImpl<$Res, TvAnimationsEvent>;
  @useResult
  $Res call({List<Media> tvAnimations});
}

/// @nodoc
class _$TvAnimationsEventCopyWithImpl<$Res, $Val extends TvAnimationsEvent>
    implements $TvAnimationsEventCopyWith<$Res> {
  _$TvAnimationsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tvAnimations = null,
  }) {
    return _then(_value.copyWith(
      tvAnimations: null == tvAnimations
          ? _value.tvAnimations
          : tvAnimations // ignore: cast_nullable_to_non_nullable
              as List<Media>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$InitImplCopyWith<$Res>
    implements $TvAnimationsEventCopyWith<$Res> {
  factory _$$InitImplCopyWith(
          _$InitImpl value, $Res Function(_$InitImpl) then) =
      __$$InitImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<Media> tvAnimations});
}

/// @nodoc
class __$$InitImplCopyWithImpl<$Res>
    extends _$TvAnimationsEventCopyWithImpl<$Res, _$InitImpl>
    implements _$$InitImplCopyWith<$Res> {
  __$$InitImplCopyWithImpl(_$InitImpl _value, $Res Function(_$InitImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tvAnimations = null,
  }) {
    return _then(_$InitImpl(
      tvAnimations: null == tvAnimations
          ? _value._tvAnimations
          : tvAnimations // ignore: cast_nullable_to_non_nullable
              as List<Media>,
    ));
  }
}

/// @nodoc

class _$InitImpl implements _Init {
  const _$InitImpl({required final List<Media> tvAnimations})
      : _tvAnimations = tvAnimations;

  final List<Media> _tvAnimations;
  @override
  List<Media> get tvAnimations {
    if (_tvAnimations is EqualUnmodifiableListView) return _tvAnimations;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_tvAnimations);
  }

  @override
  String toString() {
    return 'TvAnimationsEvent.init(tvAnimations: $tvAnimations)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitImpl &&
            const DeepCollectionEquality()
                .equals(other._tvAnimations, _tvAnimations));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_tvAnimations));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InitImplCopyWith<_$InitImpl> get copyWith =>
      __$$InitImplCopyWithImpl<_$InitImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Media> tvAnimations) init,
  }) {
    return init(tvAnimations);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<Media> tvAnimations)? init,
  }) {
    return init?.call(tvAnimations);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Media> tvAnimations)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(tvAnimations);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements TvAnimationsEvent {
  const factory _Init({required final List<Media> tvAnimations}) = _$InitImpl;

  @override
  List<Media> get tvAnimations;
  @override
  @JsonKey(ignore: true)
  _$$InitImplCopyWith<_$InitImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$TvAnimationsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> tvAnimations) readyTvAnimations,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> tvAnimations)? readyTvAnimations,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> tvAnimations)? readyTvAnimations,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TvAnimationsInitState value) init,
    required TResult Function(TvAnimationsReadyState value) readyTvAnimations,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(TvAnimationsInitState value)? init,
    TResult? Function(TvAnimationsReadyState value)? readyTvAnimations,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TvAnimationsInitState value)? init,
    TResult Function(TvAnimationsReadyState value)? readyTvAnimations,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TvAnimationsStateCopyWith<$Res> {
  factory $TvAnimationsStateCopyWith(
          TvAnimationsState value, $Res Function(TvAnimationsState) then) =
      _$TvAnimationsStateCopyWithImpl<$Res, TvAnimationsState>;
}

/// @nodoc
class _$TvAnimationsStateCopyWithImpl<$Res, $Val extends TvAnimationsState>
    implements $TvAnimationsStateCopyWith<$Res> {
  _$TvAnimationsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$TvAnimationsInitStateImplCopyWith<$Res> {
  factory _$$TvAnimationsInitStateImplCopyWith(
          _$TvAnimationsInitStateImpl value,
          $Res Function(_$TvAnimationsInitStateImpl) then) =
      __$$TvAnimationsInitStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$TvAnimationsInitStateImplCopyWithImpl<$Res>
    extends _$TvAnimationsStateCopyWithImpl<$Res, _$TvAnimationsInitStateImpl>
    implements _$$TvAnimationsInitStateImplCopyWith<$Res> {
  __$$TvAnimationsInitStateImplCopyWithImpl(_$TvAnimationsInitStateImpl _value,
      $Res Function(_$TvAnimationsInitStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$TvAnimationsInitStateImpl implements TvAnimationsInitState {
  const _$TvAnimationsInitStateImpl();

  @override
  String toString() {
    return 'TvAnimationsState.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TvAnimationsInitStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> tvAnimations) readyTvAnimations,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> tvAnimations)? readyTvAnimations,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> tvAnimations)? readyTvAnimations,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TvAnimationsInitState value) init,
    required TResult Function(TvAnimationsReadyState value) readyTvAnimations,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(TvAnimationsInitState value)? init,
    TResult? Function(TvAnimationsReadyState value)? readyTvAnimations,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TvAnimationsInitState value)? init,
    TResult Function(TvAnimationsReadyState value)? readyTvAnimations,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class TvAnimationsInitState implements TvAnimationsState {
  const factory TvAnimationsInitState() = _$TvAnimationsInitStateImpl;
}

/// @nodoc
abstract class _$$TvAnimationsReadyStateImplCopyWith<$Res> {
  factory _$$TvAnimationsReadyStateImplCopyWith(
          _$TvAnimationsReadyStateImpl value,
          $Res Function(_$TvAnimationsReadyStateImpl) then) =
      __$$TvAnimationsReadyStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Media> tvAnimations});
}

/// @nodoc
class __$$TvAnimationsReadyStateImplCopyWithImpl<$Res>
    extends _$TvAnimationsStateCopyWithImpl<$Res, _$TvAnimationsReadyStateImpl>
    implements _$$TvAnimationsReadyStateImplCopyWith<$Res> {
  __$$TvAnimationsReadyStateImplCopyWithImpl(
      _$TvAnimationsReadyStateImpl _value,
      $Res Function(_$TvAnimationsReadyStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tvAnimations = null,
  }) {
    return _then(_$TvAnimationsReadyStateImpl(
      tvAnimations: null == tvAnimations
          ? _value._tvAnimations
          : tvAnimations // ignore: cast_nullable_to_non_nullable
              as List<Media>,
    ));
  }
}

/// @nodoc

class _$TvAnimationsReadyStateImpl implements TvAnimationsReadyState {
  const _$TvAnimationsReadyStateImpl({required final List<Media> tvAnimations})
      : _tvAnimations = tvAnimations;

  final List<Media> _tvAnimations;
  @override
  List<Media> get tvAnimations {
    if (_tvAnimations is EqualUnmodifiableListView) return _tvAnimations;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_tvAnimations);
  }

  @override
  String toString() {
    return 'TvAnimationsState.readyTvAnimations(tvAnimations: $tvAnimations)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TvAnimationsReadyStateImpl &&
            const DeepCollectionEquality()
                .equals(other._tvAnimations, _tvAnimations));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_tvAnimations));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TvAnimationsReadyStateImplCopyWith<_$TvAnimationsReadyStateImpl>
      get copyWith => __$$TvAnimationsReadyStateImplCopyWithImpl<
          _$TvAnimationsReadyStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> tvAnimations) readyTvAnimations,
  }) {
    return readyTvAnimations(tvAnimations);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> tvAnimations)? readyTvAnimations,
  }) {
    return readyTvAnimations?.call(tvAnimations);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> tvAnimations)? readyTvAnimations,
    required TResult orElse(),
  }) {
    if (readyTvAnimations != null) {
      return readyTvAnimations(tvAnimations);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TvAnimationsInitState value) init,
    required TResult Function(TvAnimationsReadyState value) readyTvAnimations,
  }) {
    return readyTvAnimations(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(TvAnimationsInitState value)? init,
    TResult? Function(TvAnimationsReadyState value)? readyTvAnimations,
  }) {
    return readyTvAnimations?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TvAnimationsInitState value)? init,
    TResult Function(TvAnimationsReadyState value)? readyTvAnimations,
    required TResult orElse(),
  }) {
    if (readyTvAnimations != null) {
      return readyTvAnimations(this);
    }
    return orElse();
  }
}

abstract class TvAnimationsReadyState implements TvAnimationsState {
  const factory TvAnimationsReadyState(
      {required final List<Media> tvAnimations}) = _$TvAnimationsReadyStateImpl;

  List<Media> get tvAnimations;
  @JsonKey(ignore: true)
  _$$TvAnimationsReadyStateImplCopyWith<_$TvAnimationsReadyStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
