import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/features/root/blocs/internet_connection_bloc/internet_connection_bloc.dart';
import 'package:movie_set/repo/api/media/base.dart';

import '../../../../models/media/media.dart';
import '../../../../repo/internet_connection.dart';
import '../../../settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';

part 'tv_animations_bloc.freezed.dart';

class TvAnimationsBloc extends Bloc<TvAnimationsEvent, TvAnimationsState> {
  TvAnimationsBloc({
    required this.repo,
    required this.internetBloc,
    required this.localizationBloc,
  }) : super(const TvAnimationsState.init()) {
    _init();
    _updateDataWithLocale();
    _listenInternetConnection();
    on<_Init>((event, emit) {
      emit(TvAnimationsState.readyTvAnimations(tvAnimations: event.tvAnimations));
    });
  }

  final BaseRepoMedia repo;
  final InternetConnectionBloc internetBloc;
  final SettingsLocalizationBloc localizationBloc;
  late final StreamSubscription _subTvs;
  late final StreamSubscription _subLocale;
  late final StreamSubscription _subInternet;

  Future<void> _init() async {
    _subTvs = repo.getTvAnimations().listen((it) {
      if (it.isNotEmpty) {
        add(TvAnimationsEvent.init(tvAnimations: it));
      }
    });
  }

  Future<void> _updateDataWithLocale() async =>
      _subLocale = localizationBloc.stream.listen((event) async => await _init());

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) async {
      if (event.connection != ConnectionStatus.offline) {
        await _init();
      }
    });
  }

  @override
  Future<void> close() {
    _subTvs.cancel();
    _subLocale.cancel();
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class TvAnimationsEvent with _$TvAnimationsEvent {
  const factory TvAnimationsEvent.init({
    required List<Media> tvAnimations,
  }) = _Init;
}

@freezed
class TvAnimationsState with _$TvAnimationsState {
  const factory TvAnimationsState.init() = TvAnimationsInitState;

  const factory TvAnimationsState.readyTvAnimations({
    required List<Media> tvAnimations,
  }) = TvAnimationsReadyState;
}
