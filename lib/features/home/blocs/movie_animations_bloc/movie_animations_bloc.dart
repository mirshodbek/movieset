import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/features/root/blocs/internet_connection_bloc/internet_connection_bloc.dart';
import 'package:movie_set/repo/api/media/base.dart';

import '../../../../models/media/media.dart';
import '../../../../repo/internet_connection.dart';
import '../../../settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';

part 'movie_animations_bloc.freezed.dart';

class MovieAnimationsBloc extends Bloc<MovieAnimationsEvent, MovieAnimationsState> {
  MovieAnimationsBloc({
    required this.repo,
    required this.internetBloc,
    required this.localizationBloc,
  }) : super(const MovieAnimationsState.init()) {
    _init();
    _updateDataWithLocale();
    _listenInternetConnection();
    on<_Init>((event, emit) {
      emit(MovieAnimationsState.readyMovieAnimations(animations: event.animations));
    });
  }

  final BaseRepoMedia repo;
  final InternetConnectionBloc internetBloc;
  final SettingsLocalizationBloc localizationBloc;
  late final StreamSubscription _subMovie;
  late final StreamSubscription _subLocale;
  late final StreamSubscription _subInternet;

  Future<void> _init() async {
    _subMovie = repo.getMovieAnimations().listen((it) {
      if (it.isNotEmpty) {
        add(MovieAnimationsEvent.init(animations: it));
      }
    });
  }

  Future<void> _updateDataWithLocale() async =>
      _subLocale = localizationBloc.stream.listen((event) async => await _init());

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) async {
      if (event.connection != ConnectionStatus.offline) {
        await _init();
      }
    });
  }

  @override
  Future<void> close() {
    _subMovie.cancel();
    _subLocale.cancel();
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class MovieAnimationsEvent with _$MovieAnimationsEvent {
  const factory MovieAnimationsEvent.init({
    required List<Media> animations,
  }) = _Init;
}

@freezed
class MovieAnimationsState with _$MovieAnimationsState {
  const factory MovieAnimationsState.init() = MovieAnimationsInitState;

  const factory MovieAnimationsState.readyMovieAnimations({
    required List<Media> animations,
  }) = MovieAnimationsReadyState;
}
