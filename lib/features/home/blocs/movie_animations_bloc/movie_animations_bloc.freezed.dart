// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'movie_animations_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$MovieAnimationsEvent {
  List<Media> get animations => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Media> animations) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<Media> animations)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Media> animations)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MovieAnimationsEventCopyWith<MovieAnimationsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieAnimationsEventCopyWith<$Res> {
  factory $MovieAnimationsEventCopyWith(MovieAnimationsEvent value,
          $Res Function(MovieAnimationsEvent) then) =
      _$MovieAnimationsEventCopyWithImpl<$Res, MovieAnimationsEvent>;
  @useResult
  $Res call({List<Media> animations});
}

/// @nodoc
class _$MovieAnimationsEventCopyWithImpl<$Res,
        $Val extends MovieAnimationsEvent>
    implements $MovieAnimationsEventCopyWith<$Res> {
  _$MovieAnimationsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? animations = null,
  }) {
    return _then(_value.copyWith(
      animations: null == animations
          ? _value.animations
          : animations // ignore: cast_nullable_to_non_nullable
              as List<Media>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$InitImplCopyWith<$Res>
    implements $MovieAnimationsEventCopyWith<$Res> {
  factory _$$InitImplCopyWith(
          _$InitImpl value, $Res Function(_$InitImpl) then) =
      __$$InitImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<Media> animations});
}

/// @nodoc
class __$$InitImplCopyWithImpl<$Res>
    extends _$MovieAnimationsEventCopyWithImpl<$Res, _$InitImpl>
    implements _$$InitImplCopyWith<$Res> {
  __$$InitImplCopyWithImpl(_$InitImpl _value, $Res Function(_$InitImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? animations = null,
  }) {
    return _then(_$InitImpl(
      animations: null == animations
          ? _value._animations
          : animations // ignore: cast_nullable_to_non_nullable
              as List<Media>,
    ));
  }
}

/// @nodoc

class _$InitImpl implements _Init {
  const _$InitImpl({required final List<Media> animations})
      : _animations = animations;

  final List<Media> _animations;
  @override
  List<Media> get animations {
    if (_animations is EqualUnmodifiableListView) return _animations;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_animations);
  }

  @override
  String toString() {
    return 'MovieAnimationsEvent.init(animations: $animations)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitImpl &&
            const DeepCollectionEquality()
                .equals(other._animations, _animations));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_animations));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InitImplCopyWith<_$InitImpl> get copyWith =>
      __$$InitImplCopyWithImpl<_$InitImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Media> animations) init,
  }) {
    return init(animations);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<Media> animations)? init,
  }) {
    return init?.call(animations);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Media> animations)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(animations);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements MovieAnimationsEvent {
  const factory _Init({required final List<Media> animations}) = _$InitImpl;

  @override
  List<Media> get animations;
  @override
  @JsonKey(ignore: true)
  _$$InitImplCopyWith<_$InitImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$MovieAnimationsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> animations) readyMovieAnimations,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> animations)? readyMovieAnimations,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> animations)? readyMovieAnimations,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MovieAnimationsInitState value) init,
    required TResult Function(MovieAnimationsReadyState value)
        readyMovieAnimations,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MovieAnimationsInitState value)? init,
    TResult? Function(MovieAnimationsReadyState value)? readyMovieAnimations,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MovieAnimationsInitState value)? init,
    TResult Function(MovieAnimationsReadyState value)? readyMovieAnimations,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieAnimationsStateCopyWith<$Res> {
  factory $MovieAnimationsStateCopyWith(MovieAnimationsState value,
          $Res Function(MovieAnimationsState) then) =
      _$MovieAnimationsStateCopyWithImpl<$Res, MovieAnimationsState>;
}

/// @nodoc
class _$MovieAnimationsStateCopyWithImpl<$Res,
        $Val extends MovieAnimationsState>
    implements $MovieAnimationsStateCopyWith<$Res> {
  _$MovieAnimationsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$MovieAnimationsInitStateImplCopyWith<$Res> {
  factory _$$MovieAnimationsInitStateImplCopyWith(
          _$MovieAnimationsInitStateImpl value,
          $Res Function(_$MovieAnimationsInitStateImpl) then) =
      __$$MovieAnimationsInitStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$MovieAnimationsInitStateImplCopyWithImpl<$Res>
    extends _$MovieAnimationsStateCopyWithImpl<$Res,
        _$MovieAnimationsInitStateImpl>
    implements _$$MovieAnimationsInitStateImplCopyWith<$Res> {
  __$$MovieAnimationsInitStateImplCopyWithImpl(
      _$MovieAnimationsInitStateImpl _value,
      $Res Function(_$MovieAnimationsInitStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$MovieAnimationsInitStateImpl implements MovieAnimationsInitState {
  const _$MovieAnimationsInitStateImpl();

  @override
  String toString() {
    return 'MovieAnimationsState.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MovieAnimationsInitStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> animations) readyMovieAnimations,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> animations)? readyMovieAnimations,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> animations)? readyMovieAnimations,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MovieAnimationsInitState value) init,
    required TResult Function(MovieAnimationsReadyState value)
        readyMovieAnimations,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MovieAnimationsInitState value)? init,
    TResult? Function(MovieAnimationsReadyState value)? readyMovieAnimations,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MovieAnimationsInitState value)? init,
    TResult Function(MovieAnimationsReadyState value)? readyMovieAnimations,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class MovieAnimationsInitState implements MovieAnimationsState {
  const factory MovieAnimationsInitState() = _$MovieAnimationsInitStateImpl;
}

/// @nodoc
abstract class _$$MovieAnimationsReadyStateImplCopyWith<$Res> {
  factory _$$MovieAnimationsReadyStateImplCopyWith(
          _$MovieAnimationsReadyStateImpl value,
          $Res Function(_$MovieAnimationsReadyStateImpl) then) =
      __$$MovieAnimationsReadyStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Media> animations});
}

/// @nodoc
class __$$MovieAnimationsReadyStateImplCopyWithImpl<$Res>
    extends _$MovieAnimationsStateCopyWithImpl<$Res,
        _$MovieAnimationsReadyStateImpl>
    implements _$$MovieAnimationsReadyStateImplCopyWith<$Res> {
  __$$MovieAnimationsReadyStateImplCopyWithImpl(
      _$MovieAnimationsReadyStateImpl _value,
      $Res Function(_$MovieAnimationsReadyStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? animations = null,
  }) {
    return _then(_$MovieAnimationsReadyStateImpl(
      animations: null == animations
          ? _value._animations
          : animations // ignore: cast_nullable_to_non_nullable
              as List<Media>,
    ));
  }
}

/// @nodoc

class _$MovieAnimationsReadyStateImpl implements MovieAnimationsReadyState {
  const _$MovieAnimationsReadyStateImpl({required final List<Media> animations})
      : _animations = animations;

  final List<Media> _animations;
  @override
  List<Media> get animations {
    if (_animations is EqualUnmodifiableListView) return _animations;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_animations);
  }

  @override
  String toString() {
    return 'MovieAnimationsState.readyMovieAnimations(animations: $animations)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MovieAnimationsReadyStateImpl &&
            const DeepCollectionEquality()
                .equals(other._animations, _animations));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_animations));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MovieAnimationsReadyStateImplCopyWith<_$MovieAnimationsReadyStateImpl>
      get copyWith => __$$MovieAnimationsReadyStateImplCopyWithImpl<
          _$MovieAnimationsReadyStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> animations) readyMovieAnimations,
  }) {
    return readyMovieAnimations(animations);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> animations)? readyMovieAnimations,
  }) {
    return readyMovieAnimations?.call(animations);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> animations)? readyMovieAnimations,
    required TResult orElse(),
  }) {
    if (readyMovieAnimations != null) {
      return readyMovieAnimations(animations);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MovieAnimationsInitState value) init,
    required TResult Function(MovieAnimationsReadyState value)
        readyMovieAnimations,
  }) {
    return readyMovieAnimations(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MovieAnimationsInitState value)? init,
    TResult? Function(MovieAnimationsReadyState value)? readyMovieAnimations,
  }) {
    return readyMovieAnimations?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MovieAnimationsInitState value)? init,
    TResult Function(MovieAnimationsReadyState value)? readyMovieAnimations,
    required TResult orElse(),
  }) {
    if (readyMovieAnimations != null) {
      return readyMovieAnimations(this);
    }
    return orElse();
  }
}

abstract class MovieAnimationsReadyState implements MovieAnimationsState {
  const factory MovieAnimationsReadyState(
          {required final List<Media> animations}) =
      _$MovieAnimationsReadyStateImpl;

  List<Media> get animations;
  @JsonKey(ignore: true)
  _$$MovieAnimationsReadyStateImplCopyWith<_$MovieAnimationsReadyStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
