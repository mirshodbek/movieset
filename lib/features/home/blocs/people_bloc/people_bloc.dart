import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/features/root/blocs/internet_connection_bloc/internet_connection_bloc.dart';
import 'package:movie_set/models/person/person.dart';
import 'package:movie_set/repo/api/people/base.dart';

import '../../../../repo/internet_connection.dart';

part 'people_bloc.freezed.dart';

class PeopleBloc extends Bloc<PeopleEvent, PeopleState> {
  PeopleBloc({
    required this.internetBloc,
    required this.repoPeople,
  }) : super(const PeopleState.init()) {
    _init();
    _listenInternetConnection();
    on<_Init>((event, emit) {
      emit(PeopleState.readyPeople(people: event.people));
    });
  }

  final BaseRepoPeople repoPeople;
  final InternetConnectionBloc internetBloc;
  late final StreamSubscription _subPeople;
  late final StreamSubscription _subInternet;

  Future<void> _init() async {
    _subPeople = repoPeople.getPeople().listen((it) {
      if (it.isNotEmpty) {
        add(PeopleEvent.init(people: it));
      }
    });
  }

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) async {
      if (event.connection != ConnectionStatus.offline) {
        await _init();
      }
    });
  }

  @override
  Future<void> close() {
    _subPeople.cancel();
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class PeopleEvent with _$PeopleEvent {
  const factory PeopleEvent.init({
    required List<Person> people,
  }) = _Init;
}

@freezed
class PeopleState with _$PeopleState {
  const factory PeopleState.init() = PeopleInitState;

  const factory PeopleState.readyPeople({
    required List<Person> people,
  }) = PeopleReadyState;
}
