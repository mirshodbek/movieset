// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'tvs_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$TvsEvent {
  List<Media> get tvs => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Media> tvs) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<Media> tvs)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Media> tvs)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TvsEventCopyWith<TvsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TvsEventCopyWith<$Res> {
  factory $TvsEventCopyWith(TvsEvent value, $Res Function(TvsEvent) then) =
      _$TvsEventCopyWithImpl<$Res, TvsEvent>;
  @useResult
  $Res call({List<Media> tvs});
}

/// @nodoc
class _$TvsEventCopyWithImpl<$Res, $Val extends TvsEvent>
    implements $TvsEventCopyWith<$Res> {
  _$TvsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tvs = null,
  }) {
    return _then(_value.copyWith(
      tvs: null == tvs
          ? _value.tvs
          : tvs // ignore: cast_nullable_to_non_nullable
              as List<Media>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$InitImplCopyWith<$Res> implements $TvsEventCopyWith<$Res> {
  factory _$$InitImplCopyWith(
          _$InitImpl value, $Res Function(_$InitImpl) then) =
      __$$InitImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<Media> tvs});
}

/// @nodoc
class __$$InitImplCopyWithImpl<$Res>
    extends _$TvsEventCopyWithImpl<$Res, _$InitImpl>
    implements _$$InitImplCopyWith<$Res> {
  __$$InitImplCopyWithImpl(_$InitImpl _value, $Res Function(_$InitImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tvs = null,
  }) {
    return _then(_$InitImpl(
      tvs: null == tvs
          ? _value._tvs
          : tvs // ignore: cast_nullable_to_non_nullable
              as List<Media>,
    ));
  }
}

/// @nodoc

class _$InitImpl implements _Init {
  const _$InitImpl({required final List<Media> tvs}) : _tvs = tvs;

  final List<Media> _tvs;
  @override
  List<Media> get tvs {
    if (_tvs is EqualUnmodifiableListView) return _tvs;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_tvs);
  }

  @override
  String toString() {
    return 'TvsEvent.init(tvs: $tvs)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitImpl &&
            const DeepCollectionEquality().equals(other._tvs, _tvs));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_tvs));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InitImplCopyWith<_$InitImpl> get copyWith =>
      __$$InitImplCopyWithImpl<_$InitImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Media> tvs) init,
  }) {
    return init(tvs);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<Media> tvs)? init,
  }) {
    return init?.call(tvs);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Media> tvs)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(tvs);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Init value)? init,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements TvsEvent {
  const factory _Init({required final List<Media> tvs}) = _$InitImpl;

  @override
  List<Media> get tvs;
  @override
  @JsonKey(ignore: true)
  _$$InitImplCopyWith<_$InitImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$TvsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> tvs) readyTvs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> tvs)? readyTvs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> tvs)? readyTvs,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TvsInitState value) init,
    required TResult Function(TvsReadyState value) readyTvs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(TvsInitState value)? init,
    TResult? Function(TvsReadyState value)? readyTvs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TvsInitState value)? init,
    TResult Function(TvsReadyState value)? readyTvs,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TvsStateCopyWith<$Res> {
  factory $TvsStateCopyWith(TvsState value, $Res Function(TvsState) then) =
      _$TvsStateCopyWithImpl<$Res, TvsState>;
}

/// @nodoc
class _$TvsStateCopyWithImpl<$Res, $Val extends TvsState>
    implements $TvsStateCopyWith<$Res> {
  _$TvsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$TvsInitStateImplCopyWith<$Res> {
  factory _$$TvsInitStateImplCopyWith(
          _$TvsInitStateImpl value, $Res Function(_$TvsInitStateImpl) then) =
      __$$TvsInitStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$TvsInitStateImplCopyWithImpl<$Res>
    extends _$TvsStateCopyWithImpl<$Res, _$TvsInitStateImpl>
    implements _$$TvsInitStateImplCopyWith<$Res> {
  __$$TvsInitStateImplCopyWithImpl(
      _$TvsInitStateImpl _value, $Res Function(_$TvsInitStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$TvsInitStateImpl implements TvsInitState {
  const _$TvsInitStateImpl();

  @override
  String toString() {
    return 'TvsState.init()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$TvsInitStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> tvs) readyTvs,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> tvs)? readyTvs,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> tvs)? readyTvs,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TvsInitState value) init,
    required TResult Function(TvsReadyState value) readyTvs,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(TvsInitState value)? init,
    TResult? Function(TvsReadyState value)? readyTvs,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TvsInitState value)? init,
    TResult Function(TvsReadyState value)? readyTvs,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class TvsInitState implements TvsState {
  const factory TvsInitState() = _$TvsInitStateImpl;
}

/// @nodoc
abstract class _$$TvsReadyStateImplCopyWith<$Res> {
  factory _$$TvsReadyStateImplCopyWith(
          _$TvsReadyStateImpl value, $Res Function(_$TvsReadyStateImpl) then) =
      __$$TvsReadyStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Media> tvs});
}

/// @nodoc
class __$$TvsReadyStateImplCopyWithImpl<$Res>
    extends _$TvsStateCopyWithImpl<$Res, _$TvsReadyStateImpl>
    implements _$$TvsReadyStateImplCopyWith<$Res> {
  __$$TvsReadyStateImplCopyWithImpl(
      _$TvsReadyStateImpl _value, $Res Function(_$TvsReadyStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tvs = null,
  }) {
    return _then(_$TvsReadyStateImpl(
      tvs: null == tvs
          ? _value._tvs
          : tvs // ignore: cast_nullable_to_non_nullable
              as List<Media>,
    ));
  }
}

/// @nodoc

class _$TvsReadyStateImpl implements TvsReadyState {
  const _$TvsReadyStateImpl({required final List<Media> tvs}) : _tvs = tvs;

  final List<Media> _tvs;
  @override
  List<Media> get tvs {
    if (_tvs is EqualUnmodifiableListView) return _tvs;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_tvs);
  }

  @override
  String toString() {
    return 'TvsState.readyTvs(tvs: $tvs)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TvsReadyStateImpl &&
            const DeepCollectionEquality().equals(other._tvs, _tvs));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_tvs));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TvsReadyStateImplCopyWith<_$TvsReadyStateImpl> get copyWith =>
      __$$TvsReadyStateImplCopyWithImpl<_$TvsReadyStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(List<Media> tvs) readyTvs,
  }) {
    return readyTvs(tvs);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(List<Media> tvs)? readyTvs,
  }) {
    return readyTvs?.call(tvs);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(List<Media> tvs)? readyTvs,
    required TResult orElse(),
  }) {
    if (readyTvs != null) {
      return readyTvs(tvs);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TvsInitState value) init,
    required TResult Function(TvsReadyState value) readyTvs,
  }) {
    return readyTvs(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(TvsInitState value)? init,
    TResult? Function(TvsReadyState value)? readyTvs,
  }) {
    return readyTvs?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TvsInitState value)? init,
    TResult Function(TvsReadyState value)? readyTvs,
    required TResult orElse(),
  }) {
    if (readyTvs != null) {
      return readyTvs(this);
    }
    return orElse();
  }
}

abstract class TvsReadyState implements TvsState {
  const factory TvsReadyState({required final List<Media> tvs}) =
      _$TvsReadyStateImpl;

  List<Media> get tvs;
  @JsonKey(ignore: true)
  _$$TvsReadyStateImplCopyWith<_$TvsReadyStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
