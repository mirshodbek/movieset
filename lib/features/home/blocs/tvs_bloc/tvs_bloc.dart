import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movie_set/features/root/blocs/internet_connection_bloc/internet_connection_bloc.dart';
import 'package:movie_set/repo/api/media/base.dart';

import '../../../../models/media/media.dart';
import '../../../../repo/internet_connection.dart';
import '../../../settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';

part 'tvs_bloc.freezed.dart';

class TvsBloc extends Bloc<TvsEvent, TvsState> {
  TvsBloc({
    required this.repo,
    required this.internetBloc,
    required this.localizationBloc,
  }) : super(const TvsState.init()) {
    _init();
    _updateDataWithLocale();
    _listenInternetConnection();
    on<_Init>((event, emit) {
      emit(TvsState.readyTvs(tvs: event.tvs));
    });
  }

  final BaseRepoMedia repo;
  final InternetConnectionBloc internetBloc;
  final SettingsLocalizationBloc localizationBloc;
  late final StreamSubscription _subTvs;
  late final StreamSubscription _subLocale;
  late final StreamSubscription _subInternet;

  Future<void> _init() async {
    _subTvs = repo.getTvs().listen((it) {
      if (it.isNotEmpty) {
        add(TvsEvent.init(tvs: it));
      }
    });
  }

  Future<void> _updateDataWithLocale() async =>
      _subLocale = localizationBloc.stream.listen((event) async => await _init());

  Future<void> _listenInternetConnection() async {
    _subInternet = internetBloc.stream.listen((event) async {
      if (event.connection != ConnectionStatus.offline) {
        await _init();
      }
    });
  }

  @override
  Future<void> close() {
    _subTvs.cancel();
    _subLocale.cancel();
    _subInternet.cancel();
    return super.close();
  }
}

@freezed
class TvsEvent with _$TvsEvent {
  const factory TvsEvent.init({
    required List<Media> tvs,
  }) = _Init;
}

@freezed
class TvsState with _$TvsState {
  const factory TvsState.init() = TvsInitState;

  const factory TvsState.readyTvs({
    required List<Media> tvs,
  }) = TvsReadyState;
}
