import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/home/blocs/people_bloc/people_bloc.dart';
import 'package:movie_set/widgets/animations/shimmer/shimmer_list.dart';
import 'package:movie_set/widgets/app_title_category.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/router.dart';
import 'package:movie_set/utils/extensions/string_extension.dart';
import 'package:movie_set/widgets/app_list_tile/app_list_tile.dart';
import 'package:movie_set/widgets/app_list_tile/app_list_tile_item.dart';
import 'package:movie_set/l10n/generated/l10n.dart';

class PeopleList extends StatelessWidget {
  const PeopleList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 184,
      child: BlocBuilder<PeopleBloc, PeopleState>(
        builder: (context, state) => state.map(
          init: (_) => ShimmerList(
            height: 120,
            width: 80,
            title: S.of(context).popularAnime,
          ),
          readyPeople: (bloc) {
            return Column(
              children: [
                AppTitleCategory(
                  title: S.of(context).favoriteActors,
                  onPressed: () {
                    AppRouter.navigate(
                      context,
                      (configuration) => configuration.add(
                        PanelPeoplePage({
                          Constants.dataOne: bloc.people,
                          Constants.dataTwo: bloc.people.firstOrNull?.totalPages,
                        }),
                      ),
                    );
                  },
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: List.generate(
                      bloc.people.length,
                      (index) {
                        final item = bloc.people[index];
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: AppListTile(
                            height: 120,
                            width: 80,
                            onTap: () {
                              AppRouter.navigate(
                                context,
                                (configuration) => configuration.add(
                                  PersonDetailsPage(item),
                                ),
                              );
                            },
                            item: AppListTileItem(
                              imagePath: item.profilePath,
                              subTitle: item.knownForDepartment?.jobType(item.gender),
                              adult: item.adult,
                              title: item.name,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
