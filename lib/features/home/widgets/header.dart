import 'package:flutter/material.dart';
import 'package:movie_set/app_design/app_colors.dart';
import 'package:movie_set/app_design/app_text_style.dart';
import 'package:movie_set/constants/constants.dart';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Text.rich(
        TextSpan(
          text: Constants.logoMoviePartOne,
          children: [
            TextSpan(
              text: '.',
              style: AppTextStyle.s32w900.copyWith(
                color: AppColors.secondaryDark,
              ),
            ),
            const TextSpan(text: Constants.logoMoviePartTwo),
          ],
        ),
        style: Theme.of(context).primaryTextTheme.displaySmall,
        textAlign: TextAlign.center,
      ),
    );
  }
}
