import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/home/blocs/tv_animations_bloc/tv_animations_bloc.dart';
import 'package:movie_set/features/home/widgets/list_widget.dart';
import 'package:movie_set/widgets/animations/shimmer/shimmer_list.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/router.dart';

class TvAnimationsList extends StatelessWidget {
  const TvAnimationsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 404,
      child: BlocBuilder<TvAnimationsBloc, TvAnimationsState>(
        builder: (context, state) => state.map(
          init: (_) => ShimmerList(
            height: 340,
            width: 200,
            title: S.of(context).popularAnime,
          ),
          readyTvAnimations: (bloc) {
            return ListWidget(
              title: S.of(context).popularAnime,
              onPressedTitle: () {
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.add(
                    PanelMediaPage({
                      Constants.dataOne: bloc.tvAnimations,
                      Constants.dataTwo: S.of(context).popularAnime,
                      Constants.dataThree: Constants.typeTvAnimations,
                      Constants.dataFour: bloc.tvAnimations.firstOrNull?.totalPages,
                    }),
                  ),
                );
              },
              onTapListTile: (movie) {
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.add(
                    MediaDetailsPage({
                      Constants.dataOne: movie,
                      Constants.dataTwo: Constants.typeTvAnimations,
                    }),
                  ),
                );
              },
              height: 340,
              width: 200,
              items: bloc.tvAnimations,
            );
          },
        ),
      ),
    );
  }
}
