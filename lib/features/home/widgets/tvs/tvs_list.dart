import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/home/blocs/tvs_bloc/tvs_bloc.dart';
import 'package:movie_set/features/home/widgets/list_widget.dart';
import 'package:movie_set/widgets/animations/shimmer/shimmer_list.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/router.dart';

class TvsList extends StatelessWidget {
  const TvsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 224,
      child: BlocBuilder<TvsBloc, TvsState>(
        builder: (context, state) => state.map(
          init: (_) => ShimmerList(
            width: 200,
            height: 160,
            title: S.of(context).bestSeries,
          ),
          readyTvs: (bloc) {
            return ListWidget(
              isTv: true,
              title: S.of(context).bestSeries,
              onPressedTitle: () {
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.add(
                    PanelMediaPage({
                      Constants.dataOne: bloc.tvs,
                      Constants.dataTwo: S.of(context).bestSeries,
                      Constants.dataThree: Constants.typeTvs,
                      Constants.dataFour: bloc.tvs.firstOrNull?.totalPages,
                    }),
                  ),
                );
              },
              onTapListTile: (movie) {
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.add(
                    MediaDetailsPage({
                      Constants.dataOne: movie,
                      Constants.dataTwo: Constants.typeTvs,
                    }),
                  ),
                );
              },
              width: 200,
              height: 160,
              items: bloc.tvs,
            );
          },
        ),
      ),
    );
  }
}
