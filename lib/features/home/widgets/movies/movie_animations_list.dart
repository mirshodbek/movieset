import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/home/blocs/movie_animations_bloc/movie_animations_bloc.dart';
import 'package:movie_set/features/home/widgets/list_widget.dart';
import 'package:movie_set/widgets/animations/shimmer/shimmer_list.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/router.dart';

class MovieAnimationsList extends StatelessWidget {
  const MovieAnimationsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 404,
      child: BlocBuilder<MovieAnimationsBloc, MovieAnimationsState>(
        builder: (context, state) {
          return state.map(
            init: (_) => ShimmerList(
              height: 340,
              width: 200,
              title: S.of(context).newAnimations,
            ),
            readyMovieAnimations: (bloc) => ListWidget(
              title: S.of(context).newAnimations,
              onPressedTitle: () {
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.add(
                    PanelMediaPage({
                      Constants.dataOne: bloc.animations,
                      Constants.dataTwo: S.of(context).newAnimations,
                      Constants.dataThree: Constants.typeMovieAnimations,
                      Constants.dataFour: bloc.animations.firstOrNull?.totalPages,
                    }),
                  ),
                );
              },
              onTapListTile: (movie) {
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.add(
                    MediaDetailsPage({
                      Constants.dataOne: movie,
                      Constants.dataTwo: Constants.typeMovieAnimations,
                    }),
                  ),
                );
              },
              items: bloc.animations,
              height: 340,
              width: 200,
            ),
          );
        },
      ),
    );
  }
}
