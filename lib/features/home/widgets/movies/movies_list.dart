import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/home/blocs/movies_bloc/movies_bloc.dart';
import 'package:movie_set/features/home/widgets/list_widget.dart';
import 'package:movie_set/widgets/animations/shimmer/shimmer_list.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/router/pages.dart';
import 'package:movie_set/router/router.dart';

class MoviesList extends StatelessWidget {
  const MoviesList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 404,
      child: BlocBuilder<MoviesBloc, MoviesState>(
        builder: (context, state) {
          return state.map(
            init: (_) => ShimmerList(
              height: 340,
              width: 200,
              title: S.of(context).trending,
            ),
            readyMovies: (bloc) => ListWidget(
              title: S.of(context).trending,
              onPressedTitle: () {
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.add(
                    PanelMediaPage({
                      Constants.dataOne: bloc.movies,
                      Constants.dataTwo: S.of(context).trending,
                      Constants.dataThree: Constants.typeMovies,
                      Constants.dataFour: bloc.movies.firstOrNull?.totalPages,
                    }),
                  ),
                );
              },
              onTapListTile: (movie) {
                AppRouter.navigate(
                  context,
                  (configuration) => configuration.add(
                    MediaDetailsPage({
                      Constants.dataOne: movie,
                      Constants.dataTwo: Constants.typeMovies,
                    }),
                  ),
                );
              },
              items: bloc.movies,
              height: 340,
              width: 200,
            ),
          );
        },
      ),
    );
  }
}
