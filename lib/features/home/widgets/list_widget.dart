import 'package:flutter/material.dart';
import 'package:movie_set/utils/extensions/string_extension.dart';
import 'package:movie_set/widgets/app_title_category.dart';
import 'package:movie_set/widgets/app_list_tile/app_list_tile.dart';
import 'package:movie_set/widgets/app_list_tile/app_list_tile_item.dart';

import '../../../models/media/media.dart';

class ListWidget extends StatelessWidget {
  const ListWidget({
    Key? key,
    this.isTv = false,
    required this.width,
    required this.height,
    required this.items,
    required this.title,
    required this.onTapListTile,
    required this.onPressedTitle,
  }) : super(key: key);
  final VoidCallback onPressedTitle;
  final Function(Media) onTapListTile;
  final String title;
  final List<Media> items;
  final double height;
  final double width;
  final bool isTv;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AppTitleCategory(
          title: title,
          onPressed: onPressedTitle,
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: List.generate(
              items.length,
              (index) {
                final item = items[index];
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: AppListTile(
                    height: height,
                    width: width,
                    onTap: () => onTapListTile.call(item),
                    item: AppListTileItem(
                      imagePath: isTv
                          ? (item.backdropPath ?? item.posterPath)
                          : (item.posterPath ?? item.backdropPath),
                      releaseDate: item.releaseDate?.yearOfDate,
                      voteAverage: item.voteAverage,
                      adult: item.adult,
                      title: item.title,
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
