import 'package:flutter/material.dart';
import 'package:movie_set/features/home/widgets/movies/movie_animations_list.dart';
import 'package:movie_set/features/home/widgets/movies/movies_list.dart';
import 'package:movie_set/features/home/widgets/people_list.dart';
import 'package:movie_set/features/home/widgets/tvs/tv_animations_list.dart';
import 'package:movie_set/features/home/widgets/tvs/tvs_list.dart';
import 'package:movie_set/features/home/widgets/header.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar.dart';
import 'package:movie_set/utils/extensions/build_context_extension.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(toolbarHeight: 0),
      body: CustomScrollView(
        slivers: [
          SliverPadding(
            padding: EdgeInsets.only(top: context.viewPaddingTop),
            sliver: const Header(),
          ),
          const SliverToBoxAdapter(
            child: MoviesList(),
          ),
          const SliverToBoxAdapter(
            child: TvsList(),
          ),
          const SliverToBoxAdapter(
            child: TvAnimationsList(),
          ),
          const SliverToBoxAdapter(
            child: MovieAnimationsList(),
          ),
          const SliverToBoxAdapter(
            child: PeopleList(),
          ),
          const SliverToBoxAdapter(
            child: SizedBox(height: 16),
          ),
        ],
      ),
      bottomNavigationBar: AppNavBar(currentItem: context.appNavBarItem),
    );
  }
}
