part of '../repo.dart';

extension GetMovies on RepoMedia {
  Stream<List<Media>> _getMovies() async* {
    yield await dbProvider.getMediaByType(Constants.typeMovies);
    final movies = await provider.getMovies();
    if (movies.isNotEmpty) {
      yield movies;
      await dbProvider.deleteMedia(Constants.typeMovies);
      await Future.forEach(
        movies,
        (movie) async {
          await dbProvider.insertMedia(
            media: const Media().copyWith(movie, Constants.typeMovies),
          );
        },
      );
    }
    yield await dbProvider.getMediaByType(Constants.typeMovies);
  }
}
