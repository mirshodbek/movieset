part of '../repo.dart';

extension GetTvAnimations on RepoMedia {
  Stream<List<Media>> _getTvAnimations() async* {
    yield await dbProvider.getMediaByType(Constants.typeTvAnimations);
    final seasonAnimations = await provider.getTvAnimations();
    if(seasonAnimations.isNotEmpty){
      yield seasonAnimations;
      await dbProvider.deleteMedia(Constants.typeTvAnimations);
      await Future.forEach(
        seasonAnimations,
        (movie) async {
          await dbProvider.insertMedia(
            media: const Media().copyWith(movie, Constants.typeTvAnimations),
          );
        },
      );
    }
    yield await dbProvider.getMediaByType(Constants.typeTvAnimations);
  }
}
