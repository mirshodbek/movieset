part of '../repo.dart';

extension GetBookmarks on RepoMedia {
  Stream<List<Bookmark>> _getBookmarks() async* {
    yield await dbProvider.getBookmarks();
    await Future.forEach(
      await dbProvider.getBookmarks(),
      (it) async {
        if (it.type == Constants.movie) {
          final movie = await provider.getDetailsMovie(id: it.id);
          if (movie.id != null && it.id == movie.id) {
            await dbProvider.deleteBookmark(movie.id);
          }
          return await dbProvider.insertBookmark(
            Bookmark(
              id: movie.id,
              title: movie.title,
              backdropPath: movie.backdropPath,
              posterPath: movie.posterPath,
              genreNames: movie.genres?.map((e) => e.name).toList(),
              originCountry: movie.productionCountries?.map((e) => e.name).toList(),
              releaseDate: movie.releaseDate,
              runtime: movie.runTime,
              type: Constants.movie,
              adult: movie.adult,
            ),
          );
        } else {
          final tv = await provider.getDetailsTv(id: it.id);
          if (tv.id != null && it.id == tv.id) {
            await dbProvider.deleteBookmark(tv.id);
          }
          return await dbProvider.insertBookmark(
            Bookmark(
              id: tv.id,
              title: tv.title,
              backdropPath: tv.backdropPath,
              posterPath: tv.posterPath,
              genreNames: tv.genres?.map((e) => e.name).toList(),
              originCountry: tv.productionCountries?.map((e) => e.name).toList(),
              releaseDate: tv.releaseDate,
              type: Constants.tv,
              adult: tv.adult,
              numberOfEpisodes: tv.numberOfEpisodes,
              numberOfSeasons: tv.numberOfSeasons,
            ),
          );
        }
      },
    );
    yield await dbProvider.getBookmarks();
  }
}
