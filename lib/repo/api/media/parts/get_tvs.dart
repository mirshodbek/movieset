part of '../repo.dart';

extension GetTvs on RepoMedia {
  Stream<List<Media>> _getTvs() async* {
    yield await dbProvider.getMediaByType(Constants.typeTvs);
    final seasons = await provider.getTvs();
    if (seasons.isNotEmpty) {
      yield seasons;
      await dbProvider.deleteMedia(Constants.typeTvs);
      await Future.forEach(
        seasons,
        (movie) async {
          await dbProvider.insertMedia(
            media: const Media().copyWith(movie, Constants.typeTvs),
          );
        },
      );
    }
    yield await dbProvider.getMediaByType(Constants.typeTvs);
  }
}
