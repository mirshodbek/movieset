part of '../repo.dart';

extension GetMovieAnimations on RepoMedia {
  Stream<List<Media>> _getMovieAnimations() async* {
    yield await dbProvider.getMediaByType(Constants.typeMovieAnimations);
    final movieAnimations = await provider.getMovieAnimations();
    if (movieAnimations.isNotEmpty) {
      yield movieAnimations;
      await dbProvider.deleteMedia(Constants.typeMovieAnimations);
      await Future.forEach(
        movieAnimations,
        (movie) async {
          await dbProvider.insertMedia(
            media: const Media().copyWith(movie, Constants.typeMovieAnimations),
          );
        },
      );
    }
    yield await dbProvider.getMediaByType(Constants.typeMovieAnimations);
  }
}
