part of '../repo.dart';

extension GetVideosSeason on RepoMedia {
  Stream<List<List<Video>>> _getVideosSeason({
    required int? tvId,
    required int? countSeason,
    required int? firstSeasonNumber,
  }) async* {
    List<List<Video>> videos = [];
    final int fSN = firstSeasonNumber ?? 1;
    final seasons = await provider.getVideosSeason(
      tvId: tvId,
      seasonNumber: fSN,
    );
    videos.add(seasons);
    yield videos;
    if ((countSeason ?? 0) > fSN) {
      int length = 1 + fSN;
      while (length <= countSeason!) {
        final result = await provider.getVideosSeason(
          tvId: tvId,
          seasonNumber: length,
        );
        length++;
        videos.add(result);
      }
    }
    yield videos;
  }
}
