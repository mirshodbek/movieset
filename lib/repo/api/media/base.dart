import 'package:movie_set/models/bookmark/bookmark.dart';
import 'package:movie_set/models/details/video/video.dart';

import '../../../models/media/media.dart';

abstract class BaseRepoMedia {
  Stream<List<Media>> getMovies();

  Stream<List<Media>> getMovieAnimations();

  Stream<List<Media>> getTvs();

  Stream<List<Media>> getTvAnimations();

  Stream<List<List<Video>>> getVideosSeason({
    required int? tvId,
    required int? countSeason,
    required int? firstSeasonNumber,
  });

  Stream<List<Bookmark>> getBookmarks();
}
