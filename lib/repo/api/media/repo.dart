import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/models/details/video/video.dart';
import 'package:movie_set/provider/db_provider/database.dart';
import '../../../models/bookmark/bookmark.dart';
import '../../../models/media/media.dart';
import '../../../provider/api/media/base.dart';
import 'base.dart';

part 'parts/get_movies.dart';

part 'parts/get_animations.dart';

part 'parts/get_tv_animations.dart';

part 'parts/get_tvs.dart';

part 'parts/get_videos_season.dart';

part 'parts/get_bookmarks.dart';

class RepoMedia implements BaseRepoMedia {
  final BaseProviderMedia provider;

  final DbProvider dbProvider;

  RepoMedia({
    required this.provider,
    required this.dbProvider,
  });

  @override
  Stream<List<Media>> getMovies() => _getMovies();

  @override
  Stream<List<Media>> getMovieAnimations() => _getMovieAnimations();

  @override
  Stream<List<Media>> getTvs() => _getTvs();

  @override
  Stream<List<Media>> getTvAnimations() => _getTvAnimations();

  @override
  Stream<List<List<Video>>> getVideosSeason({
    required int? tvId,
    required int? countSeason,
    required int? firstSeasonNumber,
  }) {
    return _getVideosSeason(
      tvId: tvId,
      countSeason: countSeason,
      firstSeasonNumber: firstSeasonNumber,
    );
  }

  @override
  Stream<List<Bookmark>> getBookmarks() {
    return _getBookmarks();
  }
}
