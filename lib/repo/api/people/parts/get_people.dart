part of '../repo.dart';

extension GetPeople on RepoPeople {
  Stream<List<Person>> _getPeople() async* {
    yield await dbProvider.getPeople();
    final people =
        (await provider.getPeople()).where((it) => it.knownForMovie?.isNotEmpty ?? false).toList();
    if (people.isNotEmpty) {
      yield people;
      await dbProvider.deletePeople();
      await Future.forEach(
        people,
        (person) async {
          await dbProvider.insertPerson(person: person);
        },
      );
    }
    yield await dbProvider.getPeople();
  }
}
