
import 'package:movie_set/models/person/person.dart';
import 'package:movie_set/provider/api/people/base.dart';
import 'package:movie_set/provider/db_provider/database.dart';
import 'package:movie_set/repo/api/people/base.dart';

part 'parts/get_people.dart';

class RepoPeople extends BaseRepoPeople {
  final BaseProviderPeople provider;

  final DbProvider dbProvider;

  RepoPeople({
    required this.provider,
    required this.dbProvider,
  });

  @override
  Stream<List<Person>> getPeople() => _getPeople();
}
