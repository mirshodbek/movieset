import 'package:movie_set/models/person/person.dart';

abstract class BaseRepoPeople {
  Stream<List<Person>> getPeople();
}
