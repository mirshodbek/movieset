import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:movie_set/constants/constants.dart';

class UtilityRepo {
  static Future<String> stringBytes(String? url) async {
    if (url != null && url.isNotEmpty) {
      return base64Encode((await NetworkAssetBundle(Uri.parse(
        '${Constants.w200SizeUrlImage}$url',
      )).load('${Constants.w200SizeUrlImage}$url'))
          .buffer
          .asUint8List());
    }
    return Constants.empty;
  }
}
