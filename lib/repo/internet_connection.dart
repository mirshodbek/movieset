import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:rxdart/rxdart.dart';

enum ConnectionStatus { wifi, mobile, offline }

class InternetConnectionRepo {
  final Connectivity _connectivity = Connectivity();

  final _controller = BehaviorSubject.seeded(ConnectionStatus.wifi);
  StreamSubscription? _connectionSubscription;

  InternetConnectionRepo() {
    _checkInternetConnection();
  }

  Stream<ConnectionStatus> internetStatus() {
    _connectionSubscription ??= _connectivity.onConnectivityChanged
        .listen((event) => _checkInternetConnection(event: event));
    return _controller.stream;
  }

  Future<void> _checkInternetConnection({ConnectivityResult? event}) async {
    try {
      await Future.delayed(const Duration(seconds: 1));
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        if (event != null && event == ConnectivityResult.mobile) {
          _controller.sink.add(ConnectionStatus.mobile);
        } else {
          _controller.sink.add(ConnectionStatus.wifi);
        }
      } else {
        _controller.sink.add(ConnectionStatus.offline);
      }
    } on SocketException catch (_) {
      _controller.sink.add(ConnectionStatus.offline);
    }
  }

  Future<void> close() async {
    await _connectionSubscription?.cancel();
    await _controller.close();
  }
}
