import '../../features/settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';
import '../../features/settings/cubits/settings_cubit.dart';

abstract class BaseRepoStorage {
  Future<AppLocale?> updateLocale(AppLocale locale);

  AppLocale? readLocale();

  Future<bool> deleteLocale();

  Future<bool> updateTheme({
    required String theme,
    required String themeType,
  });

  String? getTheme();

  String? getThemeType();

  Future<bool> updateSettingsWifi(bool value);

  bool? getSettingsWifi();

  Future<bool> updateSettingsOrientationVideo(OrientationVideo value);

  String? getSettingsOrientationVideo();

}
