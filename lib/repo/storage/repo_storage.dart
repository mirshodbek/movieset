import 'package:movie_set/constants/logger.dart';
import 'package:movie_set/features/settings/cubits/settings_cubit.dart';
import 'package:movie_set/provider/exceptions.dart';
import 'package:movie_set/provider/provider_shared_prefs.dart';
import 'package:movie_set/utils/extensions/string_extension.dart';

import '../../features/settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';
import 'base_repo_storage.dart';

class RepoStorage implements BaseRepoStorage {
  RepoStorage({
    required this.sharedPrefs,
  });

  final ProviderSharedPrefs sharedPrefs;

  @override
  Future<AppLocale?> updateLocale(AppLocale locale) async {
    try {
      await sharedPrefs.storage.setString(
        _Fields.locale,
        locale.name,
      );
      return locale;
    } catch (error) {
      appLogger.e("Error in $runtimeType  | $error}");
      throw UnknownErrorException();
    }
  }

  @override
  AppLocale? readLocale() {
    try {
      return sharedPrefs.storage.getString(_Fields.locale).toAppLocale;
    } catch (error) {
      appLogger.e("Error in $runtimeType  | $error}");
      throw UnknownErrorException();
    }
  }

  @override
  Future<bool> deleteLocale() async {
    try {
      await sharedPrefs.storage.remove(_Fields.locale);
      return true;
    } catch (error) {
      appLogger.e("Error in $runtimeType  | $error}");
      throw UnknownErrorException();
    }
  }

  @override
  String? getTheme() {
    return sharedPrefs.storage.getString(_Fields.theme);
  }

  @override
  String? getThemeType() {
    return sharedPrefs.storage.getString(_Fields.themeType);
  }

  @override
  Future<bool> updateTheme({
    required String theme,
    required String themeType,
  }) async {
    try {
      await sharedPrefs.storage.setString(_Fields.theme, theme);
      await sharedPrefs.storage.setString(_Fields.themeType, themeType);
      return true;
    } catch (error) {
      appLogger.e("Error in $runtimeType  | $error}");
      throw UnknownErrorException();
    }
  }

  @override
  bool? getSettingsWifi() {
    return sharedPrefs.storage.getBool(_Fields.settingsWifi);
  }

  @override
  Future<bool> updateSettingsWifi(bool value) async {
    try {
      await sharedPrefs.storage.setBool(_Fields.settingsWifi, value);
      return true;
    } catch (error) {
      appLogger.e("Error in $runtimeType  | $error}");
      throw UnknownErrorException();
    }
  }

  @override
  String? getSettingsOrientationVideo() {
    return sharedPrefs.storage.getString(_Fields.settingsVideoScreenState);
  }

  @override
  Future<bool> updateSettingsOrientationVideo(OrientationVideo value) async {
    try {
      await sharedPrefs.storage.setString(_Fields.settingsVideoScreenState, value.name);
      return true;
    } catch (error) {
      appLogger.e("Error in $runtimeType  | $error}");
      throw UnknownErrorException();
    }
  }
}

abstract class _Fields {
  static const String locale = 'locale';
  static const String theme = 'theme';
  static const String themeType = 'theme_type';
  static const String settingsWifi = 'settings_wifi';
  static const String settingsVideoScreenState = 'settings_orientation_video';
}
