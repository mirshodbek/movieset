import 'package:movie_set/l10n/generated/l10n.dart';

class InvalidApiKeyException implements Exception {
  final String message = S.current.invalidApiKey;
}

class ResourceRequestedNotFoundException implements Exception {
  final String message = S.current.resourceRequestedNotFound;
}

class NoInternetException implements Exception {
  final String message = S.current.noInternet;
}

class UnknownErrorException implements Exception {
  final String message = S.current.unknownError;
}

class UtilsException {
  static Never statusCode(int? code) {
    switch (code) {
      case 401:
        throw InvalidApiKeyException();
      case 404:
        throw ResourceRequestedNotFoundException();
      default:
        throw NoInternetException();
    }
  }
}
