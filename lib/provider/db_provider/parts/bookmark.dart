part of '../database.dart';

extension BookmarkDbProvider on DbProvider {
  Future<List<Bookmark>> _getBookmarks() async {
    final result = await _database.query('bookmark');
    return result.map((e) => const Bookmark().fromDB(e)).toList();
  }

  Future<int> _deleteBookmark(int? id) async {
    return await _database.delete('bookmark', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> _insertBookmark(Bookmark bookmark) async {
    return await _database.insert(
      'bookmark',
      bookmark.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }
}
