part of '../database.dart';

extension MediaDbProvider on DbProvider {
  Future<List<Media>> _getMediaByType(String type) async {
    final result = await _database.query(
      'media',
      where: 'type = ?',
      whereArgs: [type],
    );
    return result.map((it) => const Media().fromMapDB(it)).toList();
  }

  Future<int> _insertMedia({required Media media}) async {
    return await _database.insert(
      'media',
      media.toMapDB(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<int> _deleteMedia(String type) async {
    return await _database.delete('media', where: 'type = ?', whereArgs: [type]);
  }

}
