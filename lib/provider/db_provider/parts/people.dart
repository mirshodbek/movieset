part of '../database.dart';

extension PeopleDbProvider on DbProvider {
  Future<int> _insertPerson({required Person person}) async {
    return await _database.insert(
      'people',
      person.toMapDB(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<int> _deletePeople() async {
    return await _database.delete('people');
  }

  Future<List<Person>> _getPeople() async {
    const query = 'SELECT * FROM people';
    final result = await _database.rawQuery(query);
    return result.map((it) => const Person().fromMapDB(it)).toList();
  }
}
