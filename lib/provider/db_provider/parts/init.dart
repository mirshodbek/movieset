part of '../database.dart';

extension DbProviderInit on DbProvider {
  Future<DbProvider> _init() async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, 'movie.db');
    _database = await openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async {
        // media DB
        await db.execute('''
        CREATE TABLE IF NOT EXISTS media
          (
            key INTEGER PRIMARY KEY AUTOINCREMENT,
            id INTEGER UNIQUE,
            adult INTEGER,
            backdrop_path TEXT,
            genre_ids TEXT,
            original_language TEXT,
            original_title TEXT,
            overview TEXT,
            popularity DOUBLE,
            poster_path TEXT,
            release_date TEXT,
            title TEXT,
            video INTEGER,
            vote_average DOUBLE,
            vote_count INTEGER,
            type TEXT,
            origin_country TEXT,
            total_pages INTEGER
          )
        ''');

        // people DB
        await db.execute('''
         CREATE TABLE IF NOT EXISTS people
          (
            key INTEGER PRIMARY KEY AUTOINCREMENT,
            id INTEGER UNIQUE,
            name TEXT,
            adult INTEGER,
            gender INTEGER,
            popularity DOUBLE,
            profile_path TEXT,
            known_for_department TEXT,
            total_pages INTEGER
          )
        ''');

        // filter DB
        await db.execute('''
         CREATE TABLE IF NOT EXISTS filter
          (
            key INTEGER PRIMARY KEY AUTOINCREMENT,
            type_media TEXT,
            genre_ids TEXT,
            years INTEGER,
            ratings INTEGER,
            country INTEGER
          )
        ''');

        // bookmark DB
        await db.execute('''
         CREATE TABLE IF NOT EXISTS bookmark
          (
            key INTEGER PRIMARY KEY AUTOINCREMENT,
            id INTEGER UNIQUE,
            backdrop_path TEXT,
            poster_path TEXT,
            genre_names TEXT,
            origin_country TEXT,
            type TEXT,
            runtime INTEGER,
            release_date TEXT,
            title TEXT,
            adult INTEGER,
            number_of_seasons INTEGER,
            number_of_episodes INTEGER
          )
        ''');
      },
    );
    return this;
  }
}
