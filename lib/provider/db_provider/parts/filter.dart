part of '../database.dart';

extension FilterDbProvider on DbProvider {
  Future<Filter> _getFilter() async {
    final result = await _database.query('filter');
    return const Filter().fromDB(result.first);
  }

  Future<int> _deleteFilter() async {
    return await _database.delete('filter');
  }

  Future<Filter> _insertFilter(Filter filter) async {
    await _database.delete('filter');
    await _database.insert(
      'filter',
      filter.toMapDB(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return await _getFilter();
  }
}
