import 'package:movie_set/models/bookmark/bookmark.dart';
import 'package:movie_set/models/person/person.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../../models/filter/filter.dart';
import '../../models/media/media.dart';

part 'parts/init.dart';

part 'parts/media.dart';

part 'parts/people.dart';

part 'parts/bookmark.dart';

part 'parts/filter.dart';

class DbProvider {
  late Database _database;

  Future<DbProvider> init() => _init();

  Future<int> insertMedia({required Media media}) async => _insertMedia(media: media);

  Future<int> deleteMedia(String type) async => _deleteMedia(type);

  Future<List<Media>> getMediaByType(String type) => _getMediaByType(type);

  Future<void> insertPerson({required Person person}) => _insertPerson(person: person);

  Future<int> deletePeople() => _deletePeople();

  Future<List<Person>> getPeople() => _getPeople();

  Future<Filter> getFilter() => _getFilter();

  Future<Filter> insertFilter(Filter filter) => _insertFilter(filter);

  Future<int> deleteFilter() => _deleteFilter();

  Future<List<Bookmark>> getBookmarks() => _getBookmarks();

  Future<int> insertBookmark(Bookmark bookmark) => _insertBookmark(bookmark);

  Future<int> deleteBookmark(int? id) => _deleteBookmark(id);
}
