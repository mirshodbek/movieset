import 'dart:math' as math;

import 'package:dio/dio.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/models/details/base_details/genre/genre.dart';
import 'package:movie_set/models/details/credits/movie_credits.dart';
import 'package:movie_set/models/details/movie_details/movie_details.dart';
import 'package:movie_set/models/details/tv_details/tv_details.dart';
import 'package:movie_set/models/details/video/video.dart';
import 'package:movie_set/provider/api.dart';
import 'package:movie_set/provider/exceptions.dart';
import 'package:movie_set/utils/extensions/int_extension.dart';
import 'package:movie_set/utils/extensions/list_extension.dart';

import '../../../models/details/credits/crew_cast_list.dart';
import '../../../models/filter/filter.dart';
import '../../../models/media/media.dart';
import 'base.dart';

part 'parts/media/get_movies.dart';

part 'parts/media/get_movie_animations.dart';

part 'parts/media/get_tvs.dart';

part 'parts/media/get_tv_animations.dart';

part 'parts/details/get_recommendations_movie.dart';

part 'parts/details/get_recommendations_tv.dart';

part 'parts/details/get_credits_tv.dart';

part 'parts/details/get_credits_movie.dart';

part 'parts/details/get_videos_movie.dart';

part 'parts/details/get_videos_tv.dart';

part 'parts/details/get_details_movie.dart';

part 'parts/details/get_details_tv.dart';

part 'parts/details/get_videos_season.dart';

part 'parts/search/get_search_media.dart';

part 'parts/search/get_trending_media.dart';

part 'parts/filter/get_genres.dart';

part 'parts/filter/get_discover_movies.dart';

part 'parts/filter/get_discover_tvs.dart';

class ProviderMedia implements BaseProviderMedia {
  final MovieSetApi api;

  ProviderMedia(this.api);

  @override
  Future<List<Media>> getMovies({
    bool runTest = false,
    int page = 1,
  }) {
    return _getMovies(runTest, page);
  }

  @override
  Future<List<Media>> getMovieAnimations({
    bool runTest = false,
    int page = 1,
  }) {
    return _getMovieAnimations(runTest, page);
  }

  @override
  Future<List<Media>> getTvs({
    bool runTest = false,
    int page = 1,
  }) {
    return _getTvs(runTest, page);
  }

  @override
  Future<List<Media>> getTvAnimations({
    bool runTest = false,
    int page = 1,
  }) {
    return _getTvAnimations(runTest, page);
  }

  @override
  Future<MovieDetails> getDetailsMovie({
    bool runTest = false,
    required int? id,
  }) {
    return _getDetailsMovie(runTest, id);
  }

  @override
  Future<TvDetails> getDetailsTv({
    bool runTest = false,
    required int? id,
  }) {
    return _getDetailsTv(runTest, id);
  }

  @override
  Future<List<Media>> getRecommendationsMovie({
    bool runTest = false,
    int page = 1,
    required int? id,
  }) {
    return _getRecommendationsMovie(runTest, page, id);
  }

  @override
  Future<List<Media>> getRecommendationsTv({
    bool runTest = false,
    int page = 1,
    required int? id,
  }) {
    return _getRecommendationsTv(runTest, page, id);
  }

  @override
  Future<CrewCastList> getCreditsTv({
    bool runTest = false,
    required int? id,
  }) {
    return _getCreditsTv(runTest, id);
  }

  @override
  Future<CrewCastList> getCreditsMovie({
    bool runTest = false,
    required int? id,
  }) {
    return _getCreditsMovie(runTest, id);
  }

  @override
  Future<List<Video>> getVideosMovie({
    bool runTest = false,
    required int? id,
  }) {
    return _getVideosMovie(runTest, id);
  }

  @override
  Future<List<Video>> getVideosTv({
    bool runTest = false,
    required int? id,
  }) {
    return _getVideosTv(runTest, id);
  }

  @override
  Future<List<Video>> getVideosSeason({
    bool runTest = false,
    required int? tvId,
    required int? seasonNumber,
  }) {
    return _getVideosSeason(runTest, tvId, seasonNumber);
  }

  @override
  Future<List<Media>> getSearchMedia({
    bool runTest = false,
    int page = 1,
    required String? query,
  }) {
    return _getSearchMedia(runTest, page, query);
  }

  @override
  Future<List<Media>> getTrendingMedia({
    bool runTest = false,
    int page = 1,
  }) {
    return _getTrendingMedia(runTest, page);
  }

  @override
  Future<List<Genre>> getGenres({bool runTest = false}) {
    return _getGenres(runTest);
  }

  @override
  Future<List<Media>> getDiscoverMedia({
    bool runTest = false,
    int page = 1,
    Filter? filter,
  }) async {
    if (Constants.typeListTvs.contains(filter?.typeMedia)) {
      return _getDiscoverTvs(runTest, page, filter);
    } else if (Constants.typeListMovies.contains(filter?.typeMedia)) {
      return _getDiscoverMovies(runTest, page, filter);
    } else {
      final movies = await _getDiscoverMovies(runTest, page, filter);
      final tvs = await _getDiscoverTvs(runTest, page, filter);
      return Iterable.generate(math.max(movies.length, tvs.length)).expand((i) sync* {
        if (i < movies.length) yield movies[i];
        if (i < tvs.length) yield tvs[i];
      }).toList();
    }
  }
}

class _Fields {
  static const results = 'results';
  static const cast = 'cast';
  static const crew = 'crew';
  static const page = 'page';
  static const withGenres = 'with_genres';
  static const sixTeen = '16';
  static const totalPages = 'total_pages';
  static const includeAdult = 'include_adult';
  static const query = 'query';
  static const mediaType = 'media_type';
  static const tv = 'tv';
  static const movie = 'movie';
  static const genres = 'genres';
  static const startDateMovies = 'primary_release_date.gte';
  static const endDateMovies = 'primary_release_date.lte';
  static const startDateTvs = 'first_air_date.gte';
  static const endDateTvs = 'first_air_date.lte';
  static const withOriginalLanguage = 'with_original_language';
  static const voteAverage = 'vote_average.gte';
  static const sortBy = 'sort_by';
  // static const sortByTimeMovies = 'primary_release_date.desc';
  static const sortByPopularity = 'popularity.desc';
  // static const sortByTimeTvs = 'first_air_date.desc';

  // filter
  static String filterWithCartoon(Filter? filter) => '16,${filterWithGenres(filter)}';

  static String? filterWithGenres(Filter? filter) => filter?.genreIds?.convertListToString;

  static Map<String, dynamic> formatStartDate(String type, Filter? filter) =>
      filter?.years != null ? {type: '${filter?.years}-01-01'} : {};

  static Map<String, dynamic> formatEndDate(String type, Filter? filter) =>
      filter?.years != null ? {type: '${filter!.years! + 9}-12-31'} : {};

  static Map<String, dynamic> region(Filter? filter) =>
      filter?.country != null ? {withOriginalLanguage: filter?.country?.toCountryLanguage} : {};

  static Map<String, dynamic> ratings(Filter? filter) =>
      filter?.ratings != null ? {voteAverage: filter?.ratings} : {};
}
