import 'package:movie_set/models/details/base_details/genre/genre.dart';
import 'package:movie_set/models/details/movie_details/movie_details.dart';
import 'package:movie_set/models/details/tv_details/tv_details.dart';
import 'package:movie_set/models/details/video/video.dart';
import 'package:movie_set/models/filter/filter.dart';

import '../../../models/details/credits/crew_cast_list.dart';
import '../../../models/media/media.dart';

abstract class BaseProviderMedia {
  Future<List<Media>> getMovies({
    bool runTest = false,
    int page = 1,
  });

  Future<List<Media>> getMovieAnimations({
    bool runTest = false,
    int page = 1,
  });

  Future<List<Media>> getTvs({
    bool runTest = false,
    int page = 1,
  });

  Future<List<Media>> getTvAnimations({
    bool runTest = false,
    int page = 1,
  });

  Future<MovieDetails> getDetailsMovie({
    bool runTest = false,
    required int? id,
  });

  Future<TvDetails> getDetailsTv({
    bool runTest = false,
    required int? id,
  });

  Future<List<Media>> getRecommendationsMovie({
    bool runTest = false,
    int page = 1,
    required int? id,
  });

  Future<List<Media>> getRecommendationsTv({
    bool runTest = false,
    int page = 1,
    required int? id,
  });

  Future<CrewCastList> getCreditsMovie({
    bool runTest = false,
    required int? id,
  });

  Future<CrewCastList> getCreditsTv({
    bool runTest = false,
    required int? id,
  });

  Future<List<Video>> getVideosMovie({
    bool runTest = false,
    required int? id,
  });

  Future<List<Video>> getVideosTv({
    bool runTest = false,
    required int? id,
  });

  Future<List<Video>> getVideosSeason({
    bool runTest = false,
    required int? tvId,
    required int? seasonNumber,
  });

  Future<List<Media>> getSearchMedia({
    bool runTest = false,
    int page = 1,
    required String? query,
  });

  Future<List<Media>> getTrendingMedia({
    bool runTest = false,
    int page = 1,
  });

  Future<List<Media>> getDiscoverMedia({
    bool runTest = false,
    int page = 1,
    Filter? filter,
  });

  Future<List<Genre>> getGenres({bool runTest = false});
}
