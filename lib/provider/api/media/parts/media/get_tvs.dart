part of '../../provider.dart';

extension GetTvs on ProviderMedia {
  Future<List<Media>> _getTvs(bool runTest, int page) async {
    try {
      final response = await api.get(
        'trending/tv/day',
        queryParameters: {
          _Fields.page: page,
        },
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.results] as List;
      final data = <Media>[];
      for (final item in decode) {
        data.add(const Media().fromJsonTv(item, totalPages: response.data[_Fields.totalPages]));
      }
      return data.where((it) => it.genreIds?.contains(16) == false).toList();
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
