part of '../../provider.dart';

extension GetTvAnimations on ProviderMedia {
  Future<List<Media>> _getTvAnimations(bool runTest, int page) async {
    try {
      final response = await api.get(
        'discover/tv',
        queryParameters: {
          _Fields.page: page,
          _Fields.withGenres: _Fields.sixTeen,
          _Fields.includeAdult: false,
        },
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.results] as List;
      final data = <Media>[];
      for (final item in decode) {
        data.add(const Media().fromJsonTv(item, totalPages: response.data[_Fields.totalPages]));
      }
      return data;
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
