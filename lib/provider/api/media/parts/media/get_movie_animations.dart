part of '../../provider.dart';

extension GetMovieAnimations on ProviderMedia {
  Future<List<Media>> _getMovieAnimations(bool runTest, int page) async {
    try {
      final response = await api.get(
        'discover/movie',
        queryParameters: {
          _Fields.page: page,
          _Fields.withGenres: _Fields.sixTeen,
          _Fields.includeAdult: false,
        },
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.results] as List;
      final data = <Media>[];
      for (final item in decode) {
        data.add(const Media().fromJsonMovie(item, totalPages: response.data[_Fields.totalPages]));
      }
      return data;
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
