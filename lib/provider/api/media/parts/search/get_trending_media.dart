part of '../../provider.dart';

extension GetTrendingMedia on ProviderMedia {
  Future<List<Media>> _getTrendingMedia(bool runTest, int page) async {
    try {
      final response = await api.get(
        'trending/all/day',
        queryParameters: {
          _Fields.page: page,
        },
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.results] as List;
      final data = <Media>[];
      for (final item in decode) {
        if (item[_Fields.mediaType] == _Fields.tv) {
          data.add(
            const Media().fromJsonTv(
              item,
              totalPages: response.data[_Fields.totalPages],
              type: _Fields.tv,
            ),
          );
        } else if (item[_Fields.mediaType] == _Fields.movie) {
          data.add(
            const Media().fromJsonMovie(
              item,
              totalPages: response.data[_Fields.totalPages],
              type: _Fields.movie,
            ),
          );
        }
      }
      return data;
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
