part of '../../provider.dart';

extension GetDetailsMovie on ProviderMedia {
  Future<MovieDetails> _getDetailsMovie(bool runTest, int? id) async {
    try {
      final response = await api.get(
        'movie/$id',
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      return const MovieDetails().fromJson(response.data);
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
