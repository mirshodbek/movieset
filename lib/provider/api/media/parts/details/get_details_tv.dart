part of '../../provider.dart';

extension GetDetailsTv on ProviderMedia {
  Future<TvDetails> _getDetailsTv(bool runTest, int? id) async {
    try {
      final response = await api.get(
        'tv/$id',
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }

      return const TvDetails().fromJson(response.data);
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
