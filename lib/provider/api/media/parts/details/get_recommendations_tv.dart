part of '../../provider.dart';

extension GetRecommendationsTv on ProviderMedia {
  Future<List<Media>> _getRecommendationsTv(
    bool runTest,
    int page,
    int? id,
  ) async {
    try {
      final response = await api.get(
        'tv/$id/recommendations',
        queryParameters: {
          _Fields.page: page,
        },
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.results] as List;
      final data = <Media>[];
      for (final item in decode) {
        data.add(const Media().fromJsonTv(item, totalPages: response.data[_Fields.totalPages]));
      }
      return data;
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
