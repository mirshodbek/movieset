part of '../../provider.dart';

extension GetVideosTv on ProviderMedia {
  Future<List<Video>> _getVideosTv(bool runTest, int? id) async {
    try {
      final response = await api.get(
        'tv/$id/videos',
        locale: Constants.en,
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.results] as List;
      final data = <Video>[];
      for (final item in decode) {
        data.add(const Video().fromJson(item));
      }
      return data;
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
