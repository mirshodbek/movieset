part of '../../provider.dart';

extension GetRecommendationsMovie on ProviderMedia {
  Future<List<Media>> _getRecommendationsMovie(
    bool runTest,
    int page,
    int? id,
  ) async {
    try {
      final response = await api.get(
        'movie/$id/recommendations',
        queryParameters: {
          _Fields.page: page,
        },
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.results] as List;
      final data = <Media>[];
      for (final item in decode) {
        data.add(const Media().fromJsonMovie(item, totalPages: response.data[_Fields.totalPages]));
      }
      return data;
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
