part of '../../provider.dart';

extension GetCreditsMovie on ProviderMedia {
  Future<CrewCastList> _getCreditsMovie(
    bool runTest,
    int? id,
  ) async {
    try {
      final response = await api.get(
        'movie/$id/credits',
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decodeCasts = response.data[_Fields.cast] as List;
      final casts = <MovieCredits>[];
      for (final item in decodeCasts) {
        casts.add(const MovieCredits().fromJsonCast(item));
      }
      final decodeCrews = response.data[_Fields.crew] as List;
      final crews = <MovieCredits>[];
      for (final item in decodeCrews) {
        crews.add(const MovieCredits().fromJsonCrew(item));
      }
      casts.sortListCredits;
      crews.sortListCredits;
      return CrewCastList(
        casts: casts,
        crews: crews,
      );
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
