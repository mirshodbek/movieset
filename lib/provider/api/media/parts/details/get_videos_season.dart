part of '../../provider.dart';

extension GetVideosSeason on ProviderMedia {
  Future<List<Video>> _getVideosSeason(
    bool runTest,
    int? tvId,
    int? seasonNumber,
  ) async {
    try {
      final response = await api.get(
        'tv/$tvId/season/$seasonNumber/videos',
        locale: Constants.en,
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.results] as List;
      final data = <Video>[];
      for (final item in decode) {
        data.add(const Video().fromJson(item));
      }
      return data.reversed.toList();
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
