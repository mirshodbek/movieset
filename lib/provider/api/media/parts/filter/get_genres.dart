part of '../../provider.dart';

extension GetGenres on ProviderMedia {
  Future<List<Genre>> _getGenres(bool runTest) async {
    try {
      final response = await api.get(
        'genre/movie/list',
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.genres] as List;
      final data = <Genre>[];
      for (final item in decode) {
        data.add(const Genre().fromJson(item));
      }
      return data;
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
