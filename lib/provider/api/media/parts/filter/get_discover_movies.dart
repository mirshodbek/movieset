part of '../../provider.dart';

extension GetDiscoverMovies on ProviderMedia {
  Future<List<Media>> _getDiscoverMovies(
    bool runTest,
    int page,
    Filter? filter,
  ) async {
    try {
      final response = await api.get(
        'discover/movie',
        queryParameters: {
          _Fields.page: page,
          _Fields.includeAdult: false,
          _Fields.sortBy: _Fields.sortByPopularity,
          _Fields.withGenres: filter?.typeMedia == Constants.typeMovieAnimations
              ? _Fields.filterWithCartoon(filter)
              : _Fields.filterWithGenres(filter),
          ..._Fields.formatStartDate(_Fields.startDateMovies, filter),
          ..._Fields.formatEndDate(_Fields.endDateMovies, filter),
          ..._Fields.region(filter),
          ..._Fields.ratings(filter),
        },
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.results] as List;
      final data = <Media>[];
      for (final item in decode) {
        data.add(
          const Media().fromJsonMovie(
            item,
            totalPages: response.data[_Fields.totalPages],
            type: _Fields.movie,
          ),
        );
      }
      return data;
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
