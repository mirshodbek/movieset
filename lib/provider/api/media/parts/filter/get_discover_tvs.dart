part of '../../provider.dart';

extension GetDiscoverTvs on ProviderMedia {
  Future<List<Media>> _getDiscoverTvs(
    bool runTest,
    int page,
    Filter? filter,
  ) async {
    try {
      final response = await api.get(
        'discover/tv',
        queryParameters: {
          _Fields.page: page,
          _Fields.includeAdult: false,
          _Fields.sortBy:_Fields.sortByPopularity,
          _Fields.withGenres: filter?.typeMedia == Constants.typeTvAnimations
              ? _Fields.filterWithCartoon(filter)
              : _Fields.filterWithGenres(filter),
          ..._Fields.formatStartDate(_Fields.startDateTvs, filter),
          ..._Fields.formatEndDate(_Fields.endDateTvs, filter),
          ..._Fields.region(filter),
          ..._Fields.ratings(filter),
        },
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.results] as List;
      final data = <Media>[];
      for (final item in decode) {
        data.add(
          const Media().fromJsonTv(
            item,
            totalPages: response.data[_Fields.totalPages],
            type: _Fields.tv,
          ),
        );
      }
      return data;
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
