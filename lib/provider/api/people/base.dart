import 'package:movie_set/models/details/person/credits/person_credits.dart';
import 'package:movie_set/models/details/person/details/person_details.dart';
import 'package:movie_set/models/person/person.dart';

abstract class BaseProviderPeople {
  Future<List<Person>> getPeople({
    bool runTest = false,
    int page = 1,
  });

  Future<PersonDetails> getPersonDetails({
    bool runTest = false,
    required int? id,
  });

  Future<List<List<PersonCredits>>> getPersonCreditsCast({
    bool runTest = false,
    required int? id,
  });
}
