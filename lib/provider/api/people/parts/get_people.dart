part of '../provider.dart';

extension GetPeople on ProviderPeople {
  Future<List<Person>> _getPeople(bool runTest, int page) async {
    try {
      final response = await api.get(
        'trending/person/week',
        queryParameters: {
          _Fields.page: page,
        },
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final decode = response.data[_Fields.results] as List;
      final data = <Person>[];
      for (final item in decode) {
        data.add(const Person().fromJson(item, totalPages: response.data[_Fields.totalPages]));
      }
      return data;
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
