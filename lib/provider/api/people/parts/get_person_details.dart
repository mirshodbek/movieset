part of '../provider.dart';

extension GetPersonDetails on ProviderPeople {
  Future<PersonDetails> _getPersonDetails(bool runTest, int? id) async {
    try {
      final response = await api.get(
        'person/$id',
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      return const PersonDetails().fromJson(response.data);
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
