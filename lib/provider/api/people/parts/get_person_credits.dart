part of '../provider.dart';

extension GetPersonCredits on ProviderPeople {
  Future<List<List<PersonCredits>>> _getPersonCreditsCast(bool runTest, int? id) async {
    try {
      final response = await api.get(
        'person/$id/combined_credits',
      );
      if (response.data == null && runTest) {
        UtilsException.statusCode(response.statusCode);
      }
      final cast = response.data[_Fields.cast] as List;
      final crew = response.data[_Fields.crew] as List;
      final castData = <PersonCredits>[];
      final crewData = <PersonCredits>[];
      for (final item in cast) {
        castData.add(const PersonCredits().fromJson(item));
      }
      for (final item in crew) {
        crewData.add(const PersonCredits().fromJson(item));
      }
      return [castData, crewData];
    } on DioException catch (error) {
      UtilsException.statusCode(error.response?.statusCode);
    }
  }
}
