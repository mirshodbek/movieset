import 'package:dio/dio.dart';
import 'package:movie_set/models/details/person/credits/person_credits.dart';
import 'package:movie_set/models/details/person/details/person_details.dart';
import 'package:movie_set/models/person/person.dart';
import 'package:movie_set/provider/api.dart';
import 'package:movie_set/provider/api/people/base.dart';
import 'package:movie_set/provider/exceptions.dart';

part 'parts/get_people.dart';

part 'parts/get_person_details.dart';

part 'parts/get_person_credits.dart';


class ProviderPeople extends BaseProviderPeople {
  final MovieSetApi api;

  ProviderPeople(this.api);

  @override
  Future<List<Person>> getPeople({bool runTest = false, int page = 1}) => _getPeople(runTest, page);

  @override
  Future<PersonDetails> getPersonDetails({
    bool runTest = false,
    required int? id,
  }) {
    return _getPersonDetails(runTest, id);
  }

  @override
  Future<List<List<PersonCredits>>> getPersonCreditsCast({
    bool runTest = false,
    required int? id,
  }) {
    return _getPersonCreditsCast(runTest, id);
  }

}

class _Fields {
  static const results = 'results';
  static const page = 'page';
  static const cast = 'cast';
  static const crew = 'crew';
  static const totalPages = 'total_pages';
}
