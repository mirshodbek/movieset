import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/models/request_response.dart';

abstract class DioClient {
  Future<RequestResponse<T>> get<T>(
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
    String? locale,
  });

  Future<T> post<T>(
    String path, {
    Map<String, dynamic>? data,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
  });
}

class BaseInterceptor implements Interceptor {
  final _logger = Logger(
    printer: PrettyPrinter(methodCount: 0, lineLength: 20),
  );

  @override
  void onError(DioException error, ErrorInterceptorHandler handler) {
    var output = error.message ?? Constants.empty;
    if (error.response?.statusMessage != null) {
      output += ' - ${error.response?.statusMessage}';
    }
    output += '\n${error.response?.data.toString()}';
    if (error.requestOptions.method == 'get') {
      output += ' \n${error.response?.realUri}';
    } else {
      output += '\n${error.requestOptions.path}\nParameters:\n${error.requestOptions.data}';
    }
    _logger.f(output);
    handler.next(error);
  }

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.contentType = 'application/json; charset=utf-8';
    options.headers = {
      'Accept': 'application/json',
    };

    handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    handler.next(response);
  }
}
