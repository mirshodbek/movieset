import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/models/request_response.dart';
import 'package:movie_set/provider/dio_helper.dart';

import '../features/settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';

class MovieSetApi implements DioClient {
  final Future<Response<dynamic>>? testResponse;
  final SettingsLocalizationBloc? blocLocale;

  MovieSetApi({
    this.testResponse,
    this.blocLocale,
  });

  late final _dio = Dio(_options)..interceptors.add(BaseInterceptor());

  final _options = BaseOptions(
    baseUrl: 'https://api.themoviedb.org/3/',
    connectTimeout: const Duration(milliseconds: 30000),
    receiveTimeout: const Duration(milliseconds: 30000),
    sendTimeout: const Duration(milliseconds: 30000),
  );

  @override
  Future<RequestResponse<T>> get<T>(
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
    String? locale,
  }) async {
    final response = await testResponse ??
        await _dio.get<T>(
          path,
          queryParameters: {
            Constants.apiKey: Environment.key,
            Constants.language: locale ?? blocLocale?.state.locale.name,
            ...queryParameters ?? {},
          },
          options: Options(
            headers: headers,
            sendTimeout: _options.sendTimeout,
            receiveTimeout: _options.receiveTimeout,
          ),
        );
    return RequestResponse(
      data: (await testResponse != null && response.data != null)
          ? jsonDecode(response.data)
          : response.data,
      statusCode: response.statusCode,
    );
  }

  @override
  Future<T> post<T>(
    String path, {
    Map<String, dynamic>? data,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
  }) {
    // TODO: implement post
    throw UnimplementedError();
  }
}
