// ignore_for_file: unnecessary_lambdas, avoid_web_libraries_in_flutter
import 'dart:html' as html;
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/constants/logger.dart';
import 'package:flutter_web_plugins/flutter_web_plugins.dart';
import 'package:movie_set/init_app/app.dart';
import 'package:movie_set/provider/db_provider/database.dart';
import 'package:movie_set/provider/provider_shared_prefs.dart';

/// Запуск для веба
void run() =>
    // Зона перехвата всех ошибок верхнего уровня
    runZonedGuarded<void>(
      () async {
        WidgetsFlutterBinding.ensureInitialized();
        SystemChrome.setSystemUIOverlayStyle(
          const SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarIconBrightness: Brightness.light,
            systemNavigationBarColor: Colors.black,
            systemNavigationBarIconBrightness: Brightness.light,
            systemNavigationBarDividerColor: null,
          ),
        );
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitDown,
          DeviceOrientation.portraitUp,
        ]);
        String platformLocale = Constants.empty;
        try {
          platformLocale = Platform.localeName;
        } catch (error) {
          platformLocale = Constants.empty;
          appLogger.e("👀 Can't get Platform.localeName | $error}");
        }

        // https://docs.flutter.dev/development/ui/navigation/url-strategies
        //setUrlStrategy(PathUrlStrategy());
        setUrlStrategy(const HashUrlStrategy());
        final dbProvider = await DbProvider().init();
        final repoSharedPrefs = ProviderSharedPrefs();
        await repoSharedPrefs.init();

        // Запустить приложение
        App.run(
          repoSharedPrefs: repoSharedPrefs,
          dbProvider: dbProvider,
          platform: platformLocale,
        );

        // Удалить прогресс индикатор после запуска приложения
        Future<void>.delayed(
          const Duration(seconds: 1),
          () {
            html.document
                .getElementsByClassName('loading')
                .toList(growable: false)
                .forEach((element) => element.remove());
          },
        );
      },
      (final error, final stackTrace) {
        // appLogger.e(
        //   'web_top_level_error: ${error.toString()}',
        //   stackTrace,
        // );
      },
    );
