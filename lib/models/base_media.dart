import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

@immutable
abstract class BaseMedia extends Equatable {
  final int? id;
  final bool? video;
  final bool? adult;
  final String? title;
  final int? voteCount;
  final String? overview;
  final double? popularity;
  final String? posterPath;
  final double? voteAverage;
  final String? releaseDate;
  final String? backdropPath;
  final String? originalTitle;
  final List<dynamic>? genreIds;
  final String? originalLanguage;
  final List<dynamic>? originCountry;

  const BaseMedia({
    this.genreIds,
    required this.video,
    required this.backdropPath,
    required this.id,
    required this.originalLanguage,
    required this.overview,
    required this.popularity,
    required this.posterPath,
    required this.voteAverage,
    required this.voteCount,
    required this.originalTitle,
    required this.releaseDate,
    required this.title,
    required this.adult,
    required this.originCountry,
  });

  @override
  List<Object?> get props => [
        adult,
        backdropPath,
        genreIds,
        id,
        originalLanguage,
        originalTitle,
        overview,
        popularity,
        posterPath,
        releaseDate,
        title,
        video,
        voteAverage,
        voteCount,
        adult,
        originCountry,
      ];
}
