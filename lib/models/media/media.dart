import 'package:flutter/foundation.dart';
import 'package:movie_set/models/base_media.dart';
import 'package:movie_set/utils/extensions/list_extension.dart';

part 'converter.dart';

@immutable
class Media extends BaseMedia {
  final String? type;
  final int? totalPages;

  const Media({
    this.type,
    this.totalPages,
    bool? video,
    String? backdropPath,
    List<dynamic>? genres,
    int? id,
    String? originalLanguage,
    String? overview,
    double? popularity,
    String? posterPath,
    double? voteAverage,
    int? voteCount,
    String? originalTitle,
    String? releaseDate,
    String? title,
    bool? adult,
    List<dynamic>? originCountry,
  }) : super(
          video: video,
          backdropPath: backdropPath,
          genreIds: genres,
          id: id,
          originalLanguage: originalLanguage,
          overview: overview,
          popularity: popularity,
          posterPath: posterPath,
          voteAverage: voteAverage,
          voteCount: voteCount,
          originalTitle: originalTitle,
          releaseDate: releaseDate,
          title: title,
          adult: adult,
          originCountry: originCountry,
        );

  @override
  List<Object?> get props => [...super.props, type, totalPages];
}
