part of 'media.dart';

extension ConverterMovie on Media {
  Media fromJsonMovie(
    Map<String, dynamic> json, {
    int? totalPages,
    String? type,
  }) {
    return Media(
      totalPages: totalPages,
      adult: json[_Fields.adult],
      backdropPath: json[_Fields.backdropPath],
      genres: json[_Fields.genres],
      id: json[_Fields.id],
      originalLanguage: json[_Fields.originalLanguage],
      originalTitle: json[_Fields.originalTitle],
      overview: json[_Fields.overview],
      popularity: json[_Fields.popularity],
      posterPath: json[_Fields.posterPath],
      releaseDate: json[_Fields.releaseDate],
      title: json[_Fields.title],
      video: json[_Fields.video],
      voteAverage: json[_Fields.voteAverage]?.toDouble(),
      voteCount: json[_Fields.voteCount],
      type: type,
    );
  }

  Media fromJsonTv(
    Map<String, dynamic> json, {
    int? totalPages,
    String? type,
  }) {
    return Media(
      totalPages: totalPages,
      originCountry: json[_Fields.originCountry],
      backdropPath: json[_Fields.backdropPath],
      genres: json[_Fields.genres],
      id: json[_Fields.id],
      originalLanguage: json[_Fields.originalLanguage],
      releaseDate: json[_Fields.firstAirDate],
      overview: json[_Fields.overview],
      popularity: json[_Fields.popularity]?.toDouble(),
      posterPath: json[_Fields.posterPath],
      originalTitle: json[_Fields.originalName],
      title: json[_Fields.name],
      voteAverage: json[_Fields.voteAverage]?.toDouble(),
      voteCount: json[_Fields.voteCount],
      type: type,
    );
  }

  Media fromMapDB(Map<String, dynamic> dbData) {
    return Media(
      adult: dbData[_Fields.adult] == 1,
      backdropPath: dbData[_Fields.backdropPath],
      genres: dbData[_Fields.genres]?.split(','),
      id: dbData[_Fields.id],
      originalLanguage: dbData[_Fields.originalLanguage],
      originalTitle: dbData[_Fields.originalTitle],
      overview: dbData[_Fields.overview],
      popularity: dbData[_Fields.popularity],
      posterPath: dbData[_Fields.posterPath],
      releaseDate: dbData[_Fields.releaseDate],
      title: dbData[_Fields.title],
      video: dbData[_Fields.video] == 1,
      voteAverage: dbData[_Fields.voteAverage],
      voteCount: dbData[_Fields.voteCount],
      type: dbData[_Fields.type],
      originCountry: dbData[_Fields.originCountry]?.split(','),
      totalPages: dbData[_Fields.totalPages],
    );
  }

  Media copyWith(
    Media movie,
    String type,
  ) {
    return Media(
      adult: movie.adult,
      backdropPath: movie.backdropPath,
      genres: movie.genreIds,
      id: movie.id,
      originalLanguage: movie.originalLanguage,
      originalTitle: movie.originalTitle,
      overview: movie.overview,
      popularity: movie.popularity,
      posterPath: movie.posterPath,
      releaseDate: movie.releaseDate,
      title: movie.title,
      video: movie.video,
      voteAverage: movie.voteAverage,
      voteCount: movie.voteCount,
      originCountry: movie.originCountry,
      totalPages: movie.totalPages,
      type: type,
    );
  }

  Map<String, dynamic> toMapDB() {
    return <String, dynamic>{
      _Fields.adult: (adult ?? false) ? 1 : 0,
      _Fields.backdropPath: backdropPath,
      _Fields.genres: genreIds?.convertListToString,
      _Fields.id: id,
      _Fields.originalLanguage: originalLanguage,
      _Fields.originalTitle: originalTitle,
      _Fields.overview: overview,
      _Fields.popularity: popularity,
      _Fields.posterPath: posterPath,
      _Fields.releaseDate: releaseDate,
      _Fields.title: title,
      _Fields.video: (video ?? false) ? 1 : 0,
      _Fields.voteAverage: voteAverage,
      _Fields.voteCount: voteCount,
      _Fields.type: type,
      _Fields.originCountry: originCountry?.convertListToString,
      _Fields.totalPages: totalPages,
    };
  }
}

class _Fields {
  static const adult = 'adult';
  static const backdropPath = 'backdrop_path';
  static const genres = 'genre_ids';
  static const id = 'id';
  static const originalLanguage = 'original_language';
  static const originalTitle = 'original_title';
  static const overview = 'overview';
  static const popularity = 'popularity';
  static const posterPath = 'poster_path';
  static const releaseDate = 'release_date';
  static const title = 'title';
  static const video = 'video';
  static const voteAverage = 'vote_average';
  static const voteCount = 'vote_count';
  static const type = 'type';
  static const originCountry = 'origin_country';
  static const firstAirDate = 'first_air_date';
  static const originalName = 'original_name';
  static const name = 'name';
  static const totalPages = 'total_pages';
}
