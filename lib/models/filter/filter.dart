import 'package:equatable/equatable.dart';
import 'package:movie_set/utils/extensions/list_extension.dart';

part 'converter.dart';

class Filter extends Equatable {
  final List<String?>? genreIds;
  final String? typeMedia;
  final int? ratings;
  final int? country;
  final int? years;

  const Filter({
    this.typeMedia,
    this.genreIds,
    this.years,
    this.ratings,
    this.country,
  });

  @override
  List<Object?> get props => [
        typeMedia,
        genreIds,
        years,
        ratings,
        country,
      ];
}
