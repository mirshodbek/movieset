part of 'filter.dart';

extension ConverterFilter on Filter {
  Filter fromDB(Map<String, dynamic> db) {
    return Filter(
      typeMedia: db[_Fields.typeMedia],
      genreIds: db[_Fields.genreIds]?.split(','),
      years: db[_Fields.years],
      ratings: db[_Fields.ratings],
      country: db[_Fields.country],
    );
  }

  Map<String, dynamic> toMapDB() {
    return <String, dynamic>{
      _Fields.typeMedia: typeMedia,
      _Fields.years: years,
      _Fields.ratings: ratings,
      _Fields.country: country,
      _Fields.genreIds: genreIds?.convertListToString,
    };
  }
}

class _Fields {
  static const typeMedia = 'type_media';
  static const genreIds = 'genre_ids';
  static const years = 'years';
  static const ratings = 'ratings';
  static const country = 'country';
}
