part of 'person.dart';

extension ConverterPerson on Person {
  Person fromJson(Map<String, dynamic> json, {int? totalPages}) {
    return Person(
      totalPages: totalPages,
      adult: json[_Fields.adult],
      knownForDepartment: json[_Fields.knownForDepartment],
      knownForMovie:
          (json[_Fields.knownFor] as List?)?.map((e) => const PersonCredits().fromJson(e)).toList(),
      gender: json[_Fields.gender],
      name: json[_Fields.name],
      popularity: json[_Fields.popularity],
      profilePath: json[_Fields.profilePath],
      id: json[_Fields.id],
    );
  }

  Person fromMapDB(Map<String, dynamic> dbData) {
    return Person(
      adult: dbData[_Fields.adult] == 1,
      knownForDepartment: dbData[_Fields.knownForDepartment],
      gender: dbData[_Fields.gender],
      name: dbData[_Fields.name],
      popularity: dbData[_Fields.popularity],
      profilePath: dbData[_Fields.profilePath],
      id: dbData[_Fields.id],
      totalPages: dbData[_Fields.totalPages],
    );
  }

  Map<String, dynamic> toMapDB() {
    return <String, dynamic>{
      _Fields.adult: (adult ?? false) ? 1 : 0,
      _Fields.gender: gender,
      _Fields.id: id,
      _Fields.knownForDepartment: knownForDepartment,
      _Fields.name: name,
      _Fields.popularity: popularity,
      _Fields.profilePath: profilePath,
      _Fields.totalPages: totalPages,
    };
  }
}

@immutable
class _Fields {
  static const knownForDepartment = 'known_for_department';
  static const knownFor = 'known_for';
  static const gender = 'gender';
  static const adult = 'adult';
  static const name = 'name';
  static const popularity = 'popularity';
  static const profilePath = 'profile_path';
  static const id = 'id';
  static const totalPages = 'total_pages';
}
