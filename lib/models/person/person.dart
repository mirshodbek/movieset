import 'package:flutter/foundation.dart';
import 'package:movie_set/models/base_person.dart';
import 'package:movie_set/models/details/person/credits/person_credits.dart';

part 'converter.dart';

@immutable
class Person extends BasePerson {
  final List<PersonCredits>? knownForMovie;
  final int? totalPages;

  const Person({
    bool? adult,
    int? gender,
    int? id,
    String? knownForDepartment,
    String? name,
    double? popularity,
    String? profilePath,
    this.knownForMovie,
    this.totalPages,
  }) : super(
          adult: adult,
          gender: gender,
          id: id,
          knownForDepartment: knownForDepartment,
          name: name,
          popularity: popularity,
          profilePath: profilePath,
        );

  @override
  List<Object?> get props => [
        ...super.props,
        knownForMovie,
        totalPages,
      ];
}
