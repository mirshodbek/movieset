part of 'movie_details.dart';

extension ConverterMovieDetails on MovieDetails {
  MovieDetails fromJson(Map<String, dynamic> json) {
    return MovieDetails(
      budget: json[_Fields.budget],
      imdbId: json[_Fields.imdbId],
      revenue: json[_Fields.revenue],
      runTime: json[_Fields.runTime],
      homepage: json[_Fields.homepage],
      genres: (json[_Fields.genres] as List?)?.map((e) => const Genre().fromJson(e)).toList(),
      status: json[_Fields.status],
      spokenLanguages: (json[_Fields.spokenLanguages] as List?)
          ?.map((e) => const SpokenLanguage().fromJson(e))
          .toList(),
      tagline: json[_Fields.tagline],
      adult: json[_Fields.adult],
      backdropPath: json[_Fields.backdropPath],
      id: json[_Fields.id],
      originalLanguage: json[_Fields.originalLanguage],
      originalTitle: json[_Fields.originalTitle],
      overview: json[_Fields.overview],
      popularity: json[_Fields.popularity],
      posterPath: json[_Fields.posterPath],
      releaseDate: json[_Fields.releaseDate],
      title: json[_Fields.title],
      video: json[_Fields.video],
      voteAverage: json[_Fields.voteAverage]?.toDouble(),
      voteCount: json[_Fields.voteCount],
      productionCompanies: (json[_Fields.productionCompanies] as List?)
          ?.map((e) => const ProductionCompany().fromJson(e))
          .toList(),
      productionCountries: (json[_Fields.productionCountries] as List?)
          ?.map((e) => const ProductionCountry().fromJson(e))
          .toList(),
    );
  }
}

class _Fields {
  static const adult = 'adult';
  static const backdropPath = 'backdrop_path';
  static const id = 'id';
  static const originalLanguage = 'original_language';
  static const originalTitle = 'original_title';
  static const overview = 'overview';
  static const popularity = 'popularity';
  static const posterPath = 'poster_path';
  static const releaseDate = 'release_date';
  static const title = 'title';
  static const video = 'video';
  static const voteAverage = 'vote_average';
  static const voteCount = 'vote_count';
  static const homepage = 'homepage';
  static const genres = 'genres';
  static const status = 'status';
  static const spokenLanguages = 'spoken_languages';
  static const tagline = 'tagline';
  static const budget = 'budget';
  static const imdbId = 'imdb_id';
  static const revenue = 'revenue';
  static const runTime = 'runtime';
  static const productionCountries = 'production_countries';
  static const productionCompanies = 'production_companies';
}
