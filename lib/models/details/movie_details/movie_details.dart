import 'package:movie_set/models/details/base_details/base_details.dart';
import 'package:movie_set/models/details/base_details/genre/genre.dart';
import 'package:movie_set/models/details/base_details/production_company/production_company.dart';
import 'package:movie_set/models/details/base_details/production_country/production_country.dart';
import 'package:movie_set/models/details/base_details/spoken_language/spoken_language.dart';

part 'converter.dart';

class MovieDetails extends BaseDetails {
  final int? budget;
  final String? imdbId;
  final int? revenue;
  final int? runTime;

  const MovieDetails({
    this.budget,
    this.imdbId,
    this.revenue,
    this.runTime,
    List<ProductionCountry>? productionCountries,
    List<ProductionCompany>? productionCompanies,
    List<Genre>? genres,
    String? homepage,
    List<SpokenLanguage>? spokenLanguages,
    String? status,
    String? tagline,
    bool? video,
    String? backdropPath,
    int? id,
    String? originalLanguage,
    String? overview,
    double? popularity,
    String? posterPath,
    double? voteAverage,
    int? voteCount,
    String? originalTitle,
    String? releaseDate,
    String? title,
    bool? adult,
    List<dynamic>? originCountry,
  }) : super(
          productionCountries: productionCountries,
          productionCompanies: productionCompanies,
          genres: genres,
          homepage: homepage,
          spokenLanguages: spokenLanguages,
          status: status,
          tagline: tagline,
          video: video,
          backdropPath: backdropPath,
          id: id,
          originalLanguage: originalLanguage,
          overview: overview,
          popularity: popularity,
          posterPath: posterPath,
          voteAverage: voteAverage,
          voteCount: voteCount,
          originalTitle: originalTitle,
          releaseDate: releaseDate,
          title: title,
          adult: adult,
          originCountry: originCountry,
        );

  @override
  List<Object?> get props => [
        ...super.props,
        budget,
        imdbId,
        productionCountries,
        revenue,
        runTime,
      ];
}
