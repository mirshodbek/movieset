import 'package:equatable/equatable.dart';

import 'movie_credits.dart';

class CrewCastList extends Equatable {
  final List<MovieCredits> casts;
  final List<MovieCredits> crews;

  const CrewCastList({
    required this.casts,
    required this.crews,
  });

  @override
  List<Object?> get props => [crews, casts];
}
