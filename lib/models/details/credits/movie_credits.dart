import 'package:movie_set/models/base_person.dart';

part 'converter.dart';

class MovieCredits extends BasePerson {
  final String? originalName;
  final String? character;
  final String? creditId;
  final int? order;
  final String? department;
  final String? job;

  const MovieCredits({
    this.originalName,
    this.character,
    this.creditId,
    this.order,
    this.department,
    this.job,
    bool? adult,
    int? gender,
    int? id,
    String? knownForDepartment,
    String? name,
    double? popularity,
    String? profilePath,
  }) : super(
          adult: adult,
          gender: gender,
          id: id,
          knownForDepartment: knownForDepartment,
          name: name,
          popularity: popularity,
          profilePath: profilePath,
        );

  @override
  List<Object?> get props => [
        ...super.props,
        originalName,
        character,
        creditId,
        order,
        department,
        job,
      ];
}
