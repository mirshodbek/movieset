part of 'movie_credits.dart';

extension ConverterMovieCredits on MovieCredits {
  MovieCredits fromJsonCast(Map<String, dynamic> json) {
    return MovieCredits(
      originalName: json[_Fields.originalName],
      character: json[_Fields.character],
      creditId: json[_Fields.creditId],
      order: json[_Fields.order],
      adult: json[_Fields.adult],
      knownForDepartment: json[_Fields.knownForDepartment],
      gender: json[_Fields.gender],
      name: json[_Fields.name],
      popularity: json[_Fields.popularity],
      profilePath: json[_Fields.profilePath],
      id: json[_Fields.id],
      job: null,
    );
  }

  MovieCredits fromJsonCrew(Map<String, dynamic> json) {
    return MovieCredits(
      originalName: json[_Fields.originalName],
      department: json[_Fields.department],
      creditId: json[_Fields.creditId],
      job: json[_Fields.job],
      adult: json[_Fields.adult],
      knownForDepartment: json[_Fields.knownForDepartment],
      gender: json[_Fields.gender],
      name: json[_Fields.name],
      popularity: json[_Fields.popularity],
      profilePath: json[_Fields.profilePath],
      id: json[_Fields.id],
    );
  }
}

class _Fields {
  static const knownForDepartment = 'known_for_department';
  static const gender = 'gender';
  static const adult = 'adult';
  static const name = 'name';
  static const popularity = 'popularity';
  static const profilePath = 'profile_path';
  static const id = 'id';
  static const order = 'order';
  static const creditId = 'credit_id';
  static const character = 'character';
  static const originalName = 'original_name';
  static const job = 'job';
  static const department = 'department';
}
