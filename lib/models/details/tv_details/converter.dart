part of 'tv_details.dart';

extension ConverterTvDetails on TvDetails {
  TvDetails fromJson(Map<String, dynamic> json) {
    return TvDetails(
      createdBy:
          (json[_Fields.createdBy] as List?)?.map((e) => const CreatedBy().fomJson(e)).toList(),
      languages: json[_Fields.languages],
      lastAirDate: json[_Fields.lastAirDate],
      lastEpisodeToAir: json[_Fields.lastEpisodeToAir] != null
          ? const Episode().fromJson(json[_Fields.lastEpisodeToAir])
          : null,
      nextEpisodeToAir: json[_Fields.nextEpisodeToAir] != null
          ? const Episode().fromJson(json[_Fields.nextEpisodeToAir])
          : null,
      numberOfEpisodes: json[_Fields.numberOfEpisodes],
      numberOfSeasons: json[_Fields.numberOfSeasons],
      networks: (json[_Fields.networks] as List?)
          ?.map((e) => const ProductionCompany().fromJson(e))
          .toList(),
      seasons: (json[_Fields.seasons] as List?)?.map((e) => const Season().fromJson(e)).toList(),
      type: json[_Fields.type],
      episodeRunTime: json[_Fields.episodeRunTime],
      inProduction: json[_Fields.inProduction],
      homepage: json[_Fields.homepage],
      genres: (json[_Fields.genres] as List?)?.map((e) => const Genre().fromJson(e)).toList(),
      status: json[_Fields.status],
      spokenLanguages: (json[_Fields.spokenLanguages] as List?)
          ?.map((e) => const SpokenLanguage().fromJson(e))
          .toList(),
      tagline: json[_Fields.tagline],
      adult: json[_Fields.adult],
      backdropPath: json[_Fields.backdropPath],
      id: json[_Fields.id],
      originalLanguage: json[_Fields.originalLanguage],
      originalTitle: json[_Fields.originalName],
      overview: json[_Fields.overview],
      popularity: json[_Fields.popularity],
      posterPath: json[_Fields.posterPath],
      releaseDate: json[_Fields.firstAirDate],
      title: json[_Fields.name],
      voteAverage: json[_Fields.voteAverage]?.toDouble(),
      voteCount: json[_Fields.voteCount],
      productionCompanies: (json[_Fields.productionCompanies] as List?)
          ?.map((e) => const ProductionCompany().fromJson(e))
          .toList(),
      productionCountries: (json[_Fields.productionCountries] as List?)
          ?.map((e) => const ProductionCountry().fromJson(e))
          .toList(),
    );
  }
}

class _Fields {
  static const adult = 'adult';
  static const backdropPath = 'backdrop_path';
  static const id = 'id';
  static const originalLanguage = 'original_language';
  static const originalName = 'original_name';
  static const overview = 'overview';
  static const popularity = 'popularity';
  static const posterPath = 'poster_path';
  static const firstAirDate = 'first_air_date';
  static const name = 'name';
  static const voteAverage = 'vote_average';
  static const voteCount = 'vote_count';
  static const homepage = 'homepage';
  static const genres = 'genres';
  static const status = 'status';
  static const spokenLanguages = 'spoken_languages';
  static const tagline = 'tagline';
  static const createdBy = 'created_by';
  static const inProduction = 'in_production';
  static const languages = 'languages';
  static const lastAirDate = 'last_air_date';
  static const lastEpisodeToAir = 'last_episode_to_air';
  static const nextEpisodeToAir = 'next_episode_to_air';
  static const numberOfEpisodes = 'number_of_episodes';
  static const numberOfSeasons = 'number_of_seasons';
  static const networks = 'networks';
  static const seasons = 'seasons';
  static const type = 'type';
  static const episodeRunTime = 'episode_run_time';
  static const productionCountries = 'production_countries';
  static const productionCompanies = 'production_companies';
}
