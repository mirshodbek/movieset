import 'package:equatable/equatable.dart';
part 'converter.dart';
class Episode extends Equatable {
  final int? id;
  final int? showId;
  final String? name;
  final int? runTime;
  final int? voteCount;
  final String? airDate;
  final String? overview;
  final int? seasonNumber;
  final String? stillPath;
  final int? episodeNumber;
  final double? voteAverage;
  final String? productionCode;

  const Episode({
    this.airDate,
    this.episodeNumber,
    this.id,
    this.name,
    this.overview,
    this.productionCode,
    this.runTime,
    this.seasonNumber,
    this.showId,
    this.stillPath,
    this.voteAverage,
    this.voteCount,
  });

  @override
  List<Object?> get props => [
        airDate,
        episodeNumber,
        id,
        name,
        overview,
        productionCode,
        runTime,
        seasonNumber,
        showId,
        stillPath,
        voteAverage,
        voteCount,
      ];
}
