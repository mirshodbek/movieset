part of 'episode.dart';

extension ConverterEpisode on Episode {
  Episode fromJson(Map<String, dynamic> json) {
    return Episode(
      id: json[_Fields.id],
      airDate: json[_Fields.airDate],
      seasonNumber: json[_Fields.seasonNumber],
      showId: json[_Fields.showId],
      name: json[_Fields.name],
      productionCode: json[_Fields.productionCode],
      runTime: json[_Fields.runTime],
      stillPath: json[_Fields.stillPath],
      voteCount: json[_Fields.voteCount],
      voteAverage: json[_Fields.voteAverage],
      episodeNumber: json[_Fields.episodeNumber],
      overview: json[_Fields.overview],
    );
  }
}

class _Fields {
  static const id = 'id';
  static const name = 'name';
  static const airDate = 'air_date';
  static const seasonNumber = 'season_number';
  static const showId = 'show_id';
  static const productionCode = 'production_code';
  static const runTime = 'runtime';
  static const stillPath = 'still_path';
  static const voteCount = 'vote_count';
  static const voteAverage = 'vote_average';
  static const episodeNumber = 'episode_number';
  static const overview = 'overview';
}
