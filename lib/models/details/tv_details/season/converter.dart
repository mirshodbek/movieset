part of 'season.dart';

extension ConverterSeason on Season {
  Season fromJson(Map<String, dynamic> json) {
    return Season(
      id: json[_Fields.id],
      airDate: json[_Fields.airDate],
      seasonNumber: json[_Fields.seasonNumber],
      name: json[_Fields.name],
      posterPath: json[_Fields.posterPath],
      episodeCount: json[_Fields.episodeCount],
    );
  }
}

class _Fields {
  static const id = 'id';
  static const name = 'name';
  static const airDate = 'air_date';
  static const seasonNumber = 'season_number';
  static const posterPath = 'poster_path';
  static const episodeCount = 'episode_count';
}
