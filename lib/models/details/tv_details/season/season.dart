import 'package:equatable/equatable.dart';
part 'converter.dart';
class Season extends Equatable {
  final int? id;
  final String? name;
  final String? airDate;
  final int? episodeCount;
  final String? overview;
  final int? seasonNumber;
  final String? posterPath;

  const Season({
    this.airDate,
    this.episodeCount,
    this.id,
    this.name,
    this.overview,
    this.seasonNumber,
    this.posterPath,
  });

  @override
  List<Object?> get props => [
        airDate,
        episodeCount,
        id,
        name,
        overview,
        seasonNumber,
        posterPath,
      ];
}
