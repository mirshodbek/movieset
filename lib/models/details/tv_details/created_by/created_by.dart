import 'package:equatable/equatable.dart';
part 'converter.dart';
class CreatedBy extends Equatable {
  final int? id;
  final int? gender;
  final String? name;
  final String? creditId;
  final String? profilePath;

  const CreatedBy({
    this.id,
    this.creditId,
    this.name,
    this.gender,
    this.profilePath,
  });

  @override
  List<Object?> get props => [id, creditId, name, gender, profilePath];
}
