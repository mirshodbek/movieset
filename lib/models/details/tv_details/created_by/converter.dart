part of 'created_by.dart';

extension ConverterCreatedBy on CreatedBy {
  CreatedBy fomJson(Map<String, dynamic> json) {
    return CreatedBy(
      id: json[_Fields.id],
      name: json[_Fields.name],
      creditId: json[_Fields.creditId],
      gender: json[_Fields.gender],
      profilePath: json[_Fields.profilePath],
    );
  }
}

class _Fields {
  static const id = 'id';
  static const name = 'name';
  static const creditId = 'credit_id';
  static const gender = 'gender';
  static const profilePath = 'profile_path';
}
