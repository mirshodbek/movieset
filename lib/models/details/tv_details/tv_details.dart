import 'package:movie_set/models/details/base_details/base_details.dart';
import 'package:movie_set/models/details/base_details/genre/genre.dart';
import 'package:movie_set/models/details/base_details/production_company/production_company.dart';
import 'package:movie_set/models/details/base_details/production_country/production_country.dart';
import 'package:movie_set/models/details/base_details/spoken_language/spoken_language.dart';
import 'package:movie_set/models/details/tv_details/created_by/created_by.dart';
import 'package:movie_set/models/details/tv_details/episode/episode.dart';
import 'package:movie_set/models/details/tv_details/season/season.dart';

part 'converter.dart';

class TvDetails extends BaseDetails {
  final List<CreatedBy>? createdBy;
  final List<dynamic>? episodeRunTime;
  final bool? inProduction;
  final List<dynamic>? languages;
  final String? lastAirDate;
  final Episode? lastEpisodeToAir;
  final Episode? nextEpisodeToAir;
  final int? numberOfEpisodes;
  final int? numberOfSeasons;
  final List<ProductionCompany>? networks;
  final List<Season>? seasons;
  final String? type;

  const TvDetails({
    this.languages,
    this.lastAirDate,
    this.lastEpisodeToAir,
    this.nextEpisodeToAir,
    this.numberOfEpisodes,
    this.numberOfSeasons,
    this.networks,
    this.seasons,
    this.type,
    this.createdBy,
    this.episodeRunTime,
    this.inProduction,
    List<ProductionCountry>? productionCountries,
    List<ProductionCompany>? productionCompanies,
    List<Genre>? genres,
    String? homepage,
    List<SpokenLanguage>? spokenLanguages,
    String? status,
    String? tagline,
    bool? video,
    String? backdropPath,
    int? id,
    String? originalLanguage,
    String? overview,
    double? popularity,
    String? posterPath,
    double? voteAverage,
    int? voteCount,
    String? originalTitle,
    String? releaseDate,
    String? title,
    bool? adult,
    List<dynamic>? originCountry,
  }) : super(
          productionCountries: productionCountries,
          productionCompanies: productionCompanies,
          genres: genres,
          homepage: homepage,
          spokenLanguages: spokenLanguages,
          status: status,
          tagline: tagline,
          video: video,
          backdropPath: backdropPath,
          id: id,
          originalLanguage: originalLanguage,
          overview: overview,
          popularity: popularity,
          posterPath: posterPath,
          voteAverage: voteAverage,
          voteCount: voteCount,
          originalTitle: originalTitle,
          releaseDate: releaseDate,
          title: title,
          adult: adult,
          originCountry: originCountry,
        );

  @override
  List<Object?> get props => [
        ...super.props,
        languages,
        lastAirDate,
        lastEpisodeToAir,
        nextEpisodeToAir,
        numberOfEpisodes,
        numberOfSeasons,
        networks,
        seasons,
        type,
        createdBy,
        episodeRunTime,
        inProduction,
      ];

  @override
  String toString() {
    return 'TvDetails{createdBy: $createdBy, episodeRunTime: $episodeRunTime, inProduction: $inProduction, languages: $languages, lastAirDate: $lastAirDate, lastEpisodeToAir: $lastEpisodeToAir, nextEpisodeToAir: $nextEpisodeToAir, numberOfEpisodes: $numberOfEpisodes, numberOfSeasons: $numberOfSeasons, networks: $networks, seasons: $seasons, type: $type}';
  }
}
