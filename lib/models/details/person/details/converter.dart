part of 'person_details.dart';

extension ConverterPersonDetails on PersonDetails {
  PersonDetails fromJson(Map<String, dynamic> json) {
    return PersonDetails(
      adult: json[_Fields.adult],
      knownForDepartment: json[_Fields.knownForDepartment],
      gender: json[_Fields.gender],
      name: json[_Fields.name],
      popularity: json[_Fields.popularity],
      profilePath: json[_Fields.profilePath],
      id: json[_Fields.id],
      alsoKnownAs: json[_Fields.alsoKnownAs],
      biography: json[_Fields.biography],
      birthday: json[_Fields.birthday],
      deathDay: json[_Fields.deathDay],
      homePage: json[_Fields.homepage],
      imdbId: json[_Fields.imdbId],
      placeOfBirth: json[_Fields.placeOfBirth],
    );
  }
}

class _Fields {
  static const knownForDepartment = 'known_for_department';
  static const gender = 'gender';
  static const adult = 'adult';
  static const name = 'name';
  static const popularity = 'popularity';
  static const profilePath = 'profile_path';
  static const id = 'id';
  static const alsoKnownAs = 'also_known_as';
  static const biography = 'biography';
  static const birthday = 'birthday';
  static const deathDay = 'deathday';
  static const homepage = 'homepage';
  static const imdbId = 'imdb_id';
  static const placeOfBirth = 'place_of_birth';
}
