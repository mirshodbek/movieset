import 'package:flutter/foundation.dart';
import 'package:movie_set/models/base_person.dart';

part 'converter.dart';

@immutable
class PersonDetails extends BasePerson {
  final List<dynamic>? alsoKnownAs;
  final String? placeOfBirth;
  final String? biography;
  final String? birthday;
  final String? deathDay;
  final String? homePage;
  final String? imdbId;

  const PersonDetails({
    this.alsoKnownAs,
    this.biography,
    this.birthday,
    this.deathDay,
    this.homePage,
    this.imdbId,
    this.placeOfBirth,
    bool? adult,
    int? gender,
    int? id,
    String? knownForDepartment,
    String? name,
    double? popularity,
    String? profilePath,
  }) : super(
          adult: adult,
          gender: gender,
          id: id,
          knownForDepartment: knownForDepartment,
          name: name,
          popularity: popularity,
          profilePath: profilePath,
        );

  @override
  List<Object?> get props => [
        ...super.props,
        alsoKnownAs,
        biography,
        birthday,
        deathDay,
        homePage,
        imdbId,
        placeOfBirth,
      ];
}
