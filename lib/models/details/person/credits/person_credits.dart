import 'package:flutter/foundation.dart';
import 'package:movie_set/models/base_media.dart';

part 'converter.dart';

@immutable
class PersonCredits extends BaseMedia {
  final String? department;
  final int? episodeCount;
  final String? mediaType;
  final String? character;
  final String? creditId;
  final String? job;
  final int? order;

  const PersonCredits({
    this.mediaType,
    this.character,
    this.creditId,
    this.order,
    this.episodeCount,
    this.job,
    this.department,
    bool? video,
    String? backdropPath,
    List<dynamic>? genreIds,
    int? id,
    String? originalLanguage,
    String? overview,
    double? popularity,
    String? posterPath,
    double? voteAverage,
    int? voteCount,
    String? originalTitle,
    String? releaseDate,
    String? title,
    bool? adult,
    List<dynamic>? originCountry,
  }) : super(
          video: video,
          backdropPath: backdropPath,
          genreIds: genreIds,
          id: id,
          originalLanguage: originalLanguage,
          overview: overview,
          popularity: popularity,
          posterPath: posterPath,
          voteAverage: voteAverage,
          voteCount: voteCount,
          originalTitle: originalTitle,
          releaseDate: releaseDate,
          title: title,
          adult: adult,
          originCountry: originCountry,
        );

  @override
  List<Object?> get props => [
        ...super.props,
        mediaType,
        character,
        creditId,
        episodeCount,
        order,
        job,
        department,
      ];
}
