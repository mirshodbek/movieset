part of 'person_credits.dart';

extension ConverterPersonCredits on PersonCredits {
  PersonCredits fromJson(Map<String, dynamic> json) {
    if (json[_Fields.mediaType] == _Fields.mediaTypeMovie) {
      return const PersonCredits().fromJsonMovie(json);
    }
    return const PersonCredits().fromJsonSeason(json);
  }

  PersonCredits fromJsonMovie(Map<String, dynamic> json) {
    return PersonCredits(
      adult: json[_Fields.adult],
      backdropPath: json[_Fields.backdropPath],
      genreIds: json[_Fields.genreIds],
      id: json[_Fields.id],
      originalLanguage: json[_Fields.originalLanguage],
      originalTitle: json[_Fields.originalTitle],
      overview: json[_Fields.overview],
      popularity: json[_Fields.popularity],
      posterPath: json[_Fields.posterPath],
      releaseDate: json[_Fields.releaseDate],
      title: json[_Fields.title],
      video: json[_Fields.video],
      voteAverage: json[_Fields.voteAverage]?.toDouble(),
      voteCount: json[_Fields.voteCount],
      mediaType: json[_Fields.mediaType],
      character: json[_Fields.character],
      creditId: json[_Fields.creditId],
      order: json[_Fields.order],
      job: json[_Fields.job],
      department: json[_Fields.department],
    );
  }

  PersonCredits fromJsonSeason(Map<String, dynamic> json) {
    return PersonCredits(
      originCountry: json[_Fields.originCountry],
      backdropPath: json[_Fields.backdropPath],
      genreIds: json[_Fields.genreIds],
      id: json[_Fields.id],
      originalLanguage: json[_Fields.originalLanguage],
      releaseDate: json[_Fields.firstAirDate],
      overview: json[_Fields.overview],
      popularity: json[_Fields.popularity]?.toDouble(),
      posterPath: json[_Fields.posterPath],
      originalTitle: json[_Fields.originalName],
      title: json[_Fields.name],
      voteAverage: json[_Fields.voteAverage]?.toDouble(),
      voteCount: json[_Fields.voteCount],
      mediaType: json[_Fields.mediaType],
      character: json[_Fields.character],
      creditId: json[_Fields.creditId],
      episodeCount: json[_Fields.episodeCount],
      job: json[_Fields.job],
      department: json[_Fields.department],
    );
  }
}

class _Fields {
  static const adult = 'adult';
  static const backdropPath = 'backdrop_path';
  static const genreIds = 'genre_ids';
  static const id = 'id';
  static const originalLanguage = 'original_language';
  static const originalTitle = 'original_title';
  static const overview = 'overview';
  static const popularity = 'popularity';
  static const posterPath = 'poster_path';
  static const releaseDate = 'release_date';
  static const title = 'title';
  static const video = 'video';
  static const voteAverage = 'vote_average';
  static const voteCount = 'vote_count';
  static const originCountry = 'origin_country';
  static const firstAirDate = 'first_air_date';
  static const originalName = 'original_name';
  static const name = 'name';
  static const mediaType = 'media_type';
  static const order = 'order';
  static const episodeCount = 'episode_count';
  static const creditId = 'credit_id';
  static const character = 'character';
  static const mediaTypeMovie = 'movie';
  static const job = 'job';
  static const department = 'department';
}
