part of 'spoken_language.dart';

extension ConverterSpokenLanguage on SpokenLanguage {
  SpokenLanguage fromJson(Map<String, dynamic> json) {
    return SpokenLanguage(
      name: json[_Fields.name],
      englishName: json[_Fields.englishName],
      iso6391: json[_Fields.iso6391],
    );
  }
}

class _Fields {
  static const name = 'name';
  static const englishName = 'english_name';
  static const iso6391 = 'iso_639_1';
}
