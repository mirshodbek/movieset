import 'package:equatable/equatable.dart';

part 'converter.dart';

class ProductionCountry extends Equatable {
  final String? name;
  final String? iso31661;

  const ProductionCountry({
    this.name,
    this.iso31661,
  });

  @override
  List<Object?> get props => [name, iso31661];
}
