part of 'production_country.dart';

extension ConverterProductionCountry on ProductionCountry {
  ProductionCountry fromJson(Map<String, dynamic> json) {
    return ProductionCountry(
      name: json[_Fields.name],
      iso31661: json[_Fields.iso31661],
    );
  }
}

class _Fields {
  static const name = 'name';
  static const iso31661 = 'iso_3166_1';
}
