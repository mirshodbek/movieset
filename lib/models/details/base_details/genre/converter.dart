part of 'genre.dart';

extension ConverterGenre on Genre{
  Genre fromJson(Map<String,dynamic> json){
    return Genre(
      id: json[_Fields.id],
      name: json[_Fields.name],
    );
  }
}

class _Fields{
  static const id  = 'id';
  static const name  = 'name';
}