import 'package:movie_set/models/base_media.dart';
import 'package:movie_set/models/details/base_details/genre/genre.dart';
import 'package:movie_set/models/details/base_details/production_company/production_company.dart';
import 'package:movie_set/models/details/base_details/production_country/production_country.dart';
import 'package:movie_set/models/details/base_details/spoken_language/spoken_language.dart';

abstract class BaseDetails extends BaseMedia {
  final String? status;
  final String? tagline;
  final String? homepage;
  final List<Genre>? genres;
  final List<SpokenLanguage>? spokenLanguages;
  final List<ProductionCompany>? productionCompanies;
  final List<ProductionCountry>? productionCountries;

  const BaseDetails({
    required this.homepage,
    required this.genres,
    required this.status,
    required this.spokenLanguages,
    required this.tagline,
    required this.productionCompanies,
    required this.productionCountries,
    required bool? video,
    required String? backdropPath,
    required int? id,
    required String? originalLanguage,
    required String? overview,
    required double? popularity,
    required String? posterPath,
    required double? voteAverage,
    required int? voteCount,
    required String? originalTitle,
    required String? releaseDate,
    required String? title,
    required bool? adult,
    required List<dynamic>? originCountry,
  }) : super(
          video: video,
          backdropPath: backdropPath,
          id: id,
          originalLanguage: originalLanguage,
          overview: overview,
          popularity: popularity,
          posterPath: posterPath,
          voteAverage: voteAverage,
          voteCount: voteCount,
          originalTitle: originalTitle,
          releaseDate: releaseDate,
          title: title,
          adult: adult,
          originCountry: originCountry,
        );

  @override
  List<Object?> get props => [
        ...super.props,
        homepage,
        genres,
        spokenLanguages,
        tagline,
        status,
        productionCompanies,
        productionCountries,
      ];
}
