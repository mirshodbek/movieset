part of 'production_company.dart';

extension ConverterProductionCompany on ProductionCompany {
  ProductionCompany fromJson(Map<String, dynamic> json) {
    return ProductionCompany(
      name: json[_Fields.name],
      logoPath: json[_Fields.logoPath],
      id: json[_Fields.id],
      originCountry: json[_Fields.originCountry],
    );
  }
}

class _Fields {
  static const name = 'name';
  static const logoPath = 'logo_path';
  static const id = 'id';
  static const originCountry = 'origin_country';
}
