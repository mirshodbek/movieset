part of 'video.dart';

extension ConverterVideo on Video {
  Video fromJson(Map<String, dynamic> json) {
    return Video(
      iso6391: json[_Fields.iso_639_1],
      iso31661: json[_Fields.iso_3166_1],
      name: json[_Fields.name],
      key: json[_Fields.key],
      site: json[_Fields.site],
      size: json[_Fields.size],
      type: json[_Fields.type],
      official: json[_Fields.official],
      publishedAt: json[_Fields.publishedAt],
      id: json[_Fields.id],
    );
  }
}

class _Fields {
  static const iso_639_1 = "iso_639_1";
  static const iso_3166_1 = "iso_3166_1";
  static const name = "name";
  static const key = "key";
  static const site = "site";
  static const size = "size";
  static const type = "type";
  static const official = "official";
  static const publishedAt = "published_at";
  static const id = "id";
}
