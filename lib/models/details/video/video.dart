import 'package:equatable/equatable.dart';
part 'converter.dart';
class Video extends Equatable {
  final String? publishedAt;
  final String? iso6391;
  final String? iso31661;
  final bool? official;
  final String? name;
  final String? site;
  final String? type;
  final String? key;
  final String? id;
  final int? size;

  const Video({
    this.iso6391,
    this.iso31661,
    this.name,
    this.key,
    this.site,
    this.size,
    this.type,
    this.official,
    this.publishedAt,
    this.id,
  });

  @override
  List<Object?> get props => [
    iso6391,
    iso31661,
    name,
    key,
    site,
    size,
    type,
    official,
    publishedAt,
    id,
  ];
}
