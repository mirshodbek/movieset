part of 'bookmark.dart';

extension ConverterBookmark on Bookmark {
  Bookmark fromDB(Map<String, dynamic> db) {
    return Bookmark(
      id: db[_Fields.id],
      backdropPath: db[_Fields.backdropPath],
      posterPath: db[_Fields.posterPath],
      genreNames: db[_Fields.genreNames]?.split(','),
      originCountry: db[_Fields.originCountry]?.split(','),
      type: db[_Fields.type],
      runtime: db[_Fields.runtime],
      title: db[_Fields.title],
      releaseDate: db[_Fields.releaseDate],
      adult: db[_Fields.adult] == 1,
      numberOfSeasons: db[_Fields.numberOfSeasons],
      numberOfEpisodes: db[_Fields.numberOfEpisodes],
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      _Fields.id: id,
      _Fields.backdropPath: backdropPath,
      _Fields.posterPath: posterPath,
      _Fields.genreNames: genreNames?.convertListToString,
      _Fields.originCountry: originCountry?.convertListToString,
      _Fields.type: type,
      _Fields.runtime: runtime,
      _Fields.adult: (adult ?? false) ? 1 : 0,
      _Fields.title: title,
      _Fields.releaseDate: releaseDate,
      _Fields.numberOfEpisodes: numberOfEpisodes,
      _Fields.numberOfSeasons: numberOfSeasons,
    };
  }
}

class _Fields {
  static const id = 'id';
  static const backdropPath = 'backdrop_path';
  static const posterPath = 'poster_path';
  static const genreNames = 'genre_names';
  static const originCountry = 'origin_country';
  static const type = 'type';
  static const runtime = 'runtime';
  static const adult = 'adult';
  static const title = 'title';
  static const releaseDate = 'release_date';
  static const numberOfSeasons = 'number_of_seasons';
  static const numberOfEpisodes = 'number_of_episodes';
}
