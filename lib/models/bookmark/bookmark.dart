import 'package:equatable/equatable.dart';
import 'package:movie_set/utils/extensions/list_extension.dart';

part 'converter.dart';

class Bookmark extends Equatable {
  final int? id;
  final bool? adult;
  final String? type;
  final int? runtime;
  final String? title;
  final String? posterPath;
  final String? backdropPath;
  final String? releaseDate;
  final int? numberOfSeasons;
  final int? numberOfEpisodes;
  final List<dynamic>? genreNames;
  final List<dynamic>? originCountry;

  const Bookmark({
    this.id,
    this.backdropPath,
    this.posterPath,
    this.genreNames,
    this.originCountry,
    this.type,
    this.runtime,
    this.adult,
    this.title,
    this.releaseDate,
    this.numberOfSeasons,
    this.numberOfEpisodes,
  });

  @override
  List<Object?> get props => [
        id,
        backdropPath,
        posterPath,
        genreNames,
        originCountry,
        type,
        runtime,
        adult,
        title,
        releaseDate,
        numberOfSeasons,
        numberOfEpisodes,
      ];
}
