import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

@immutable
abstract class BasePerson extends Equatable {
  final int? id;
  final bool? adult;
  final int? gender;
  final String? name;
  final double? popularity;
  final String? profilePath;
  final String? knownForDepartment;

  const BasePerson({
    required this.adult,
    required this.gender,
    required this.id,
    required this.knownForDepartment,
    required this.name,
    required this.popularity,
    required this.profilePath,
  });

  @override
  List<Object?> get props => [
        adult,
        gender,
        id,
        knownForDepartment,
        name,
        popularity,
        profilePath,
      ];
}
