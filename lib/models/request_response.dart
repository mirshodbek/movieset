
class RequestResponse<T> {
  final T? data;
  final int? statusCode;

  RequestResponse({
    this.data,
    this.statusCode,
  });

  @override
  String toString() {
    return 'RequestResponse(data: $data, statusCode: $statusCode)';
  }
}
