import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:movie_set/features/settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';
import 'package:movie_set/repo/storage/base_repo_storage.dart';
import 'package:movie_set/repo/storage/repo_storage.dart';
import 'package:movie_set/provider/provider_shared_prefs.dart';
import 'package:shared_preferences_platform_interface/shared_preferences_platform_interface.dart';
import 'package:shared_preferences_platform_interface/types.dart';

void main() {
  late FakeSharedPreferencesStore storage;
  late BaseRepoStorage preferences;
  late ProviderSharedPrefs sharedPrefs;
  const Map<String, Object> data = <String, Object>{
    'flutter.String': 'value',
  };
  setUp(() async {
    storage = FakeSharedPreferencesStore(data);
    SharedPreferencesStorePlatform.instance = storage;
    sharedPrefs = ProviderSharedPrefs();
    await sharedPrefs.init();
    preferences = RepoStorage(sharedPrefs: sharedPrefs);
  });

  test('Test SharedPreferences', () async {
    // Testing write app locale value
    expect(await preferences.updateLocale(AppLocale.ru), AppLocale.ru);

    // Testing read app locale value
    expect(preferences.readLocale(), AppLocale.ru);

    // Testing delete app locale value
    expect(await preferences.deleteLocale(), true);

    // Testing write app theme value
    expect(await preferences.updateTheme(theme: 'light', themeType: ''), true);

    // Testing read app locale value
    expect(preferences.getTheme(), 'light');
  });
}

class FakeSharedPreferencesStore implements SharedPreferencesStorePlatform {
  FakeSharedPreferencesStore(Map<String, Object> data)
      : backend = InMemorySharedPreferencesStore.withData(data);

  final InMemorySharedPreferencesStore backend;
  final List<MethodCall> log = <MethodCall>[];

  @override
  bool get isMock => true;

  @override
  Future<bool> clear() {
    log.add(const MethodCall('clear'));
    return backend.clear();
  }

  @override
  Future<Map<String, Object>> getAll() {
    log.add(const MethodCall('getAll'));
    return backend.getAll();
  }

  @override
  Future<bool> remove(String key) {
    log.add(MethodCall('remove', key));
    return backend.remove(key);
  }

  @override
  Future<bool> setValue(String valueType, String key, Object value) {
    log.add(MethodCall('setValue', <dynamic>[valueType, key, value]));
    return backend.setValue(valueType, key, value);
  }

  @override
  Future<bool> clearWithParameters(ClearParameters parameters) {
    // TODO: implement clearWithParameters
    throw UnimplementedError();
  }

  @override
  Future<bool> clearWithPrefix(String prefix) {
    // TODO: implement clearWithPrefix
    throw UnimplementedError();
  }

  @override
  Future<Map<String, Object>> getAllWithParameters(GetAllParameters parameters) {
    // TODO: implement getAllWithParameters
    throw UnimplementedError();
  }

  @override
  Future<Map<String, Object>> getAllWithPrefix(String prefix) {
    // TODO: implement getAllWithPrefix
    throw UnimplementedError();
  }
}
