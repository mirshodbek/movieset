import 'dart:io';

import 'package:dio/dio.dart';

class MovieSetApiTest {
  static Future<Response> responseSuccess(String file) async {
    return Response(
      data: await File(file).readAsString(),
      requestOptions: RequestOptions(
        headers: {Headers.contentTypeHeader: 'application/json; charset=utf-8'},
      ),
      statusCode: 200,
    );
  }

  static Future<Response> get responseInvalidKey async {
    return Response(
      requestOptions: RequestOptions(),
      statusCode: 401,
    );
  }

  static Future<Response> get responseInvalidRequest async {
    return Response(
      requestOptions: RequestOptions(),
      statusCode: 404,
    );
  }
}
