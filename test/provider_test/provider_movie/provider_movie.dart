import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:movie_set/l10n/generated/l10n.dart';
import 'package:movie_set/provider/api.dart';
import 'package:movie_set/provider/api/media/base.dart';
import 'package:movie_set/provider/api/media/provider.dart';
import 'package:movie_set/provider/exceptions.dart';
import '../api.dart';

void main() async {
  // mimic localization delegate init
  await S.load(const Locale.fromSubtags(languageCode: 'en'));

  test(
    'Success fetching movies list which are playing at the moment',
    () async {
      BaseProviderMedia repoMovieWithFakeData = MovieHelper.repo(
        MovieSetApiTest.responseSuccess(
            'test/provider_test/provider_movie/movies.json'),
      );
      final result = await repoMovieWithFakeData.getMovies(runTest: true);
      expect(result.length, 2);
      expect(result.first.title, "Black Panther: Wakanda Forever");
      expect(result.first.posterPath, "/sv1xJUazXeYqALzczSZ3O6nkH75.jpg");
      expect(result.first.voteAverage, 7.5);
      expect(result.first.voteCount, 3036);
    },
  );

  test(
    'Invalid API key is thrown correctly',
    () async {
      BaseProviderMedia repoMovieWithInvalidKey = MovieHelper.repo(
        MovieSetApiTest.responseInvalidKey,
      );
      expect(
        repoMovieWithInvalidKey.getMovies(runTest: true),
        throwsA(predicate((exception) => exception is InvalidApiKeyException)),
      );
    },
  );

  test(
    'Invalid Resource Requested is thrown correctly',
    () async {
      BaseProviderMedia repoMovieWithInvalidRequest = MovieHelper.repo(
        MovieSetApiTest.responseInvalidRequest,
      );
      expect(
        repoMovieWithInvalidRequest.getMovies(runTest: true),
        throwsA(predicate(
            (exception) => exception is ResourceRequestedNotFoundException)),
      );
    },
  );

  // For passing the test, you need turn off internet.
  bool result = await InternetConnectionChecker().hasConnection;
  if (!result) {
    test('No Internet Connection is thrown correctly', () async {
      BaseProviderMedia repoMovieWithInvalidRequest = MovieHelper.repo(null);
      expect(
        repoMovieWithInvalidRequest.getMovies(runTest: true),
        throwsA(predicate((exception) => exception is NoInternetException)),
      );
    });
  }
}

class MovieHelper {
  static MovieSetApi _api(dynamic testResponse) =>
      MovieSetApi(testResponse: testResponse);

  static BaseProviderMedia repo(dynamic apiTest) =>
      ProviderMedia(_api(apiTest));
}
