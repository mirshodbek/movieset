import 'package:flutter_test/flutter_test.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar_item.dart';

void main() {
  test(
    'App Nav Bar Item Test',
    () {
      expect(AppNavBarItem.home == AppNavBarItem.values.first, true);
      expect(AppNavBarItem.values.length, 4);
      expect(AppNavBarItem.settings.index, 3);
      expect(AppNavBarItem.home.hashCode != AppNavBarItem.settings.hashCode, true);
    },
  );
}
