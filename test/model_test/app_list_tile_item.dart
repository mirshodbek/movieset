import 'package:flutter_test/flutter_test.dart';
import 'package:movie_set/widgets/app_list_tile/app_list_tile_item.dart';
import 'package:equatable/src/equatable_utils.dart';

const firstListTileItem = AppListTileItem(
  adult: false,
  title: 'Black Panther: Wakanda Forever',
  subTitle: 'Wakanda',
  imagePath: '/xDMIl84Qo5Tsu62c9DGWhmPI67A.jpg',
  releaseDate: '2021',
  voteAverage: 7.5,
);

const secondListTileItem = AppListTileItem(
  adult: true,
  title: 'Horror Movie',
  imagePath: '/xDMIl84Qo5Tsu62c9DGWhmPI67A.jpg',
  releaseDate: '2023',
  voteAverage: 9.5,
);

void main() async {
  group('AppListTileItem Model Equatable Test', () {
    test('should correct toString', () {
      expect(firstListTileItem.toString(),
          'AppListTileItem(false, 7.5, 2021, /xDMIl84Qo5Tsu62c9DGWhmPI67A.jpg, Wakanda, Black Panther: Wakanda Forever, false)');
    });

    test('should return true when item is the same', () {
      expect(firstListTileItem == firstListTileItem, true);
    });

    test('should return correct hashCode', () {
      expect(
        firstListTileItem.hashCode,
        firstListTileItem.runtimeType.hashCode ^ mapPropsToHashCode(firstListTileItem.props),
      );
    });

    test('should return false when values are different', () {
      expect(firstListTileItem == secondListTileItem, false);
      expect(firstListTileItem.hashCode == secondListTileItem.hashCode, false);
    });
  });
}
