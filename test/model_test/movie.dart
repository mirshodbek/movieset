import 'dart:convert';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:equatable/src/equatable_utils.dart';
import 'package:movie_set/models/media/media.dart';

const firstMovie = Media(
  adult: false,
  backdropPath: "/xDMIl84Qo5Tsu62c9DGWhmPI67A.jpg",
  genres: [28, 12, 878],
  id: 505642,
  originalLanguage: "en",
  originalTitle: "Black Panther: Wakanda Forever",
  overview:
      'Queen Ramonda, Shuri, M’Baku, Okoye and the Dora Milaje fight to protect their nation from intervening world powers in the wake of King T’Challa’s death.  As the Wakandans strive to embrace their next chapter, the heroes must band together with the help of War Dog Nakia and Everett Ross and forge a new path for the kingdom of Wakanda.',
  popularity: 6117.07,
  posterPath: "/sv1xJUazXeYqALzczSZ3O6nkH75.jpg",
  releaseDate: "2022-11-09",
  title: "Black Panther: Wakanda Forever",
  video: false,
  voteAverage: 7.5,
  voteCount: 3036,
  type: Constants.typeMovies,
);

const secondMovie = Media(
  adult: false,
  backdropPath: "/9Rq14Eyrf7Tu1xk0Pl7VcNbNh1n.jpg",
  genres: [28, 12, 53],
  id: 646389,
  originalLanguage: "en",
  originalTitle: "Plane",
  overview:
      "After a heroic job of successfully landing his storm-damaged aircraft in a war zone, a fearless pilot finds himself between the agendas of multiple militias planning to take the plane and its passengers hostage.",
  popularity: 4867.593,
  posterPath: "/2g9ZBjUfF1X53EinykJqiBieUaO.jpg",
  releaseDate: "2023-01-13",
  title: "Plane",
  video: false,
  voteAverage: 6.8,
  voteCount: 359,
  type: Constants.typeMovies,
);

void main() async {
  group('Movie Model Equatable Test', () {
    test('should correct toString', () {
      expect(
        firstMovie.toString(),
        'Media(false, /xDMIl84Qo5Tsu62c9DGWhmPI67A.jpg, [28, 12, 878], 505642, en, Black Panther: Wakanda Forever, Queen Ramonda, Shuri, M’Baku, Okoye and the Dora Milaje fight to protect their nation from intervening world powers in the wake of King T’Challa’s death.  As the Wakandans strive to embrace their next chapter, the heroes must band together with the help of War Dog Nakia and Everett Ross and forge a new path for the kingdom of Wakanda., 6117.07, /sv1xJUazXeYqALzczSZ3O6nkH75.jpg, 2022-11-09, Black Panther: Wakanda Forever, false, 7.5, 3036, false, null, type_movies, null)',
      );
    });

    test('should return true when instance is the same', () {
      expect(firstMovie == firstMovie, true);
    });

    test('should return correct hashCode', () {
      expect(
        firstMovie.hashCode,
        firstMovie.runtimeType.hashCode ^ mapPropsToHashCode(firstMovie.props),
      );
    });

    test('should return false when values are different', () {
      expect(firstMovie == secondMovie, false);
      expect(firstMovie.hashCode == secondMovie.hashCode, false);
    });
  });

  group('Movie Model Json Equatable Test', () {
    late String file;
    late Media instanceJsonA;
    late Media instanceJsonB;

    setUp(() async {
      file = await File('test/provider_test/provider_movie/movies.json')
          .readAsString();
      instanceJsonA = const Media().fromJsonMovie(
          json.decode(file)['results'][0] as Map<String, dynamic>);
      instanceJsonB = const Media().fromJsonMovie(
          json.decode(file)['results'][1] as Map<String, dynamic>);
    });

    test('should correct toString', () async {
      expect(instanceJsonA.toString(),
          'Media(false, /xDMIl84Qo5Tsu62c9DGWhmPI67A.jpg, [28, 12, 878], 505642, en, Black Panther: Wakanda Forever, Queen Ramonda, Shuri, M’Baku, Okoye and the Dora Milaje fight to protect their nation from intervening world powers in the wake of King T’Challa’s death.  As the Wakandans strive to embrace their next chapter, the heroes must band together with the help of War Dog Nakia and Everett Ross and forge a new path for the kingdom of Wakanda., 6117.07, /sv1xJUazXeYqALzczSZ3O6nkH75.jpg, 2022-11-09, Black Panther: Wakanda Forever, false, 7.5, 3036, false, null, null, null)');
    });

    test('should return true when instance is the same', () {
      expect(instanceJsonA == instanceJsonA, true);
    });

    test('should return correct hashCode', () {
      expect(
        instanceJsonA.hashCode,
        instanceJsonA.runtimeType.hashCode ^
            mapPropsToHashCode(instanceJsonA.props),
      );
    });

    test('should return false when values are different', () {
      expect(instanceJsonA == instanceJsonB, false);
      expect(instanceJsonA.hashCode == instanceJsonB.hashCode, false);
    });
  });
}
