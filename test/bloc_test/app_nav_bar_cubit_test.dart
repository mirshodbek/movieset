import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:movie_set/features/root/cubits/app_nav_bar_cubit.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar_item.dart';

void main() {
  blocTest<AppNavBarCubit, AppNavBarState>(
    'App Navigation Bar Cubit Test',
    build: () => AppNavBarCubit(),
    verify: (bloc){
      expect(bloc.state.item, AppNavBarItem.home);
    },
    act: (bloc) {
      bloc.onChanged(AppNavBarItem.home);
    },
  );
}
