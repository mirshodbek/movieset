import 'dart:ui';

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:movie_set/features/settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';
import 'package:movie_set/features/settings/cubits/settings_cubit.dart';
import 'package:movie_set/repo/storage/base_repo_storage.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  late FakeRepoStorage repo;

  setUp(() async {
    repo = FakeRepoStorage();
  });

  group(
    'Testing SettingsLocalizationBloc',
    () {
      blocTest<SettingsLocalizationBloc, SettingsLocalizationState>(
        'Testing SettingsLocalizationBloc init',
        build: () => SettingsLocalizationBloc(
          repo,
        ),
        verify: (bloc) {
          // Correctly initialize app locale
          expect(bloc.state.locale, AppLocale.en);

          // Correctly set locales for supportedLocales
          expect(bloc.supportedLocales.toList(),
              [const Locale('ru'), const Locale('en')]);

          expect(bloc.state is DataSettingsLocalizationState, true);
        },
      );

      blocTest<SettingsLocalizationBloc, SettingsLocalizationState>(
        'Testing SettingsLocalizationBloc update',
        build: () => SettingsLocalizationBloc(repo),
        verify: (bloc) => expect(bloc.state.locale, AppLocale.ru),
        act: (bloc) => bloc.add(
            const SettingsLocalizationEvent.switchTo(locale: AppLocale.ru)),
      );
    },
  );
}

class FakeRepoStorage implements BaseRepoStorage {
  @override
  Future<bool> deleteLocale() async {
    return true;
  }

  @override
  AppLocale? readLocale() {
    return AppLocale.en;
  }

  @override
  Future<AppLocale?> updateLocale(AppLocale locale) async {
    return locale;
  }

  @override
  String? getTheme() {
    return 'dark';
  }

  @override
  Future<bool> updateTheme({
    required String theme,
    required String themeType,
  }) async {
    return true;
  }

  @override
  bool? getSettingsWifi() {
    // TODO: implement getStateLoadWifi
    throw UnimplementedError();
  }

  @override
  Future<bool> updateSettingsWifi(bool value) {
    // TODO: implement updateLoadWifi
    throw UnimplementedError();
  }

  @override
  String? getThemeType() {
    // TODO: implement getThemeType
    throw UnimplementedError();
  }

  @override
  String? getSettingsOrientationVideo() {
    // TODO: implement getSettingsVideoScreenState
    throw UnimplementedError();
  }

  @override
  Future<bool> updateSettingsOrientationVideo(OrientationVideo value) {
    // TODO: implement updateSettingsVideoScreenState
    throw UnimplementedError();
  }
}
