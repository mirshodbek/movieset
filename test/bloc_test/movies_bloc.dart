import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:movie_set/features/home/blocs/movies_bloc/movies_bloc.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:movie_set/features/root/blocs/internet_connection_bloc/internet_connection_bloc.dart';
import 'package:movie_set/features/settings/blocs/settings_localization_bloc/settings_localization_bloc.dart';
import 'package:movie_set/models/bookmark/bookmark.dart';
import 'package:movie_set/models/details/video/video.dart';
import 'package:movie_set/models/media/media.dart';
import 'package:movie_set/provider/provider_shared_prefs.dart';
import 'package:movie_set/repo/api/media/base.dart';
import 'package:movie_set/repo/internet_connection.dart';
import 'package:movie_set/repo/storage/repo_storage.dart';
import '../model_test/movie.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  late FakeRepoMovie repoMovieTest;

  setUp(() {
    repoMovieTest = FakeRepoMovie();
  });

  blocTest<MoviesBloc, MoviesState>(
    'Movies bloc initialized correctly',
    build: () => MoviesBloc(
      runTest: true,
      repo: repoMovieTest,
      localizationBloc: SettingsLocalizationBloc(
        RepoStorage(
          sharedPrefs: ProviderSharedPrefs()..init(),
        ),
      ),
      internetBloc: InternetConnectionBloc(repo: InternetConnectionRepo()),
    ),
    verify: (bloc) {
      expect(bloc.state is MoviesReadyState, true);
      final state = bloc.state as MoviesReadyState;
      expect(state.movies.length, 2);
      expect(state.movies[0].title, 'Black Panther: Wakanda Forever');
      expect(state.movies[0].id, 505642);
      expect(state.movies[1].genreIds, [28, 12, 53]);
    },
  );
}

class FakeRepoMovie extends BaseRepoMedia {
  late List<Media> _movies;

  FakeRepoMovie() {
    _movies = [firstMovie, secondMovie];
  }

  @override
  Stream<List<Media>> getMovies() async* {
    yield _movies;
  }

  @override
  Stream<List<Media>> getMovieAnimations() {
    // TODO: implement getAnimations
    throw UnimplementedError();
  }

  @override
  Stream<List<Media>> getTvAnimations() {
    // TODO: implement getSeasonAnimations
    throw UnimplementedError();
  }

  @override
  Stream<List<Media>> getTvs() {
    // TODO: implement getSeasons
    throw UnimplementedError();
  }

  @override
  Stream<List<List<Video>>> getVideosSeason({
    required int? tvId,
    required int? countSeason,
    required int? firstSeasonNumber,
  }) {
    // TODO: implement getVideosSeason
    throw UnimplementedError();
  }

  @override
  Stream<List<Bookmark>> getBookmarks() {
    // TODO: implement getBookmarks
    throw UnimplementedError();
  }
}
