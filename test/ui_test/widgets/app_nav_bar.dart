import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:movie_set/features/root/cubits/app_nav_bar_cubit.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar.dart';
import 'package:movie_set/features/root/widgets/app_nav_bar_item.dart';

import '../main.dart';

void main() {
  late MainInit mainUi;
  late MockAppNavBarCubit cubit;

  setUp(() {
    cubit = MockAppNavBarCubit();
    mainUi = MainInit();
  });

  testWidgets(
    'Testing App Nav Bar Widget',
    (WidgetTester tester) async {
      whenListen(
        cubit,
        Stream<AppNavBarState>.fromIterable([const AppNavBarState(item:AppNavBarItem.home)]),
        initialState: const AppNavBarState(item: AppNavBarItem.search),
      );

      // Assert that the initial state is correct.
      expect(cubit.state.item, equals(AppNavBarItem.search));

      await tester.pumpWidget(
        mainUi.initScreen(
          child: BlocProvider(
            create: (context) => AppNavBarCubit(),
            child: const AppNavBar(
              currentItem: AppNavBarItem.bookmark,
            ),
          ),
        ),
      );

      final bottomNavigationBar = find.byType(BottomNavigationBar);
      expect(bottomNavigationBar, findsOneWidget);

      final bottomNavigationBarWidget = tester.allWidgets.whereType<BottomNavigationBar>().first;
      expect(bottomNavigationBarWidget.currentIndex, 2);
      expect(bottomNavigationBarWidget.items.length, 4);
    },
  );
}

class MockAppNavBarCubit extends MockCubit<AppNavBarState> implements AppNavBarCubit {}
