import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:movie_set/app_design/app_gradients.dart';
import 'package:movie_set/widgets/animations/shimmer/shimmer_loading.dart';

import '../main.dart';

void main() {
  late MainInit mainUi;
  late BuildContext context;

  setUp(() {
    mainUi = MainInit();
  });

  testWidgets(
    'Testing Shimmer Loading',
    (tester) async {
      await tester.pumpWidget(
        mainUi.initWidget(
          child: Shimmer(
              child: Builder(
                builder: (ctx) {
                  context = ctx;
                  return ShimmerLoading(
                    child: Container(
                      width: 300,
                      height: 300,
                      color: Colors.white,
                    ),
                  );
                },

            ),
          ),
        ),
      );

      AnimationController shimmerController =
          AnimationController.unbounded(vsync: const TestVSync())
            ..repeat(
              min: -0.5,
              max: 1.5,
              period: const Duration(milliseconds: 1000),
            );

      final shimmerState = Shimmer.of(context);

      expect(shimmerState?.isSized, true);

      await tester.pump();
      expect(shimmerState?.size.width, 300);
      expect(shimmerState?.size.height, 300);

      expect(shimmerState?.gradient is LinearGradient, true);
      expect(shimmerState?.gradient.stops?.length, 3);
      expect(shimmerState?.gradient.colors.length, 3);
      expect(shimmerState?.gradient.begin, const Alignment(-1.0, -0.3));
      expect(shimmerState?.gradient.end, const Alignment(1.0, 0.3));
      expect(shimmerState?.gradient.colors, AppGradients.shimmerGradient(shimmerController).colors);

      expect(kAlwaysCompleteAnimation, hasOneLineDescription);
      expect(kAlwaysDismissedAnimation, hasOneLineDescription);

      expect(shimmerController.status, AnimationStatus.forward);
      expect(shimmerController, hasOneLineDescription);

      final shimmer = tester.allWidgets.whereType<ShimmerLoading>().first;
      expect(shimmer.child is Container, true);
      expect((shimmer.child as Container).color, Colors.white);

      shimmerController.stop();
    },
  );
}
