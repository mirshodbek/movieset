import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:network_image_mock/network_image_mock.dart';
import 'package:movie_set/app_design/app_colors.dart';
import 'package:movie_set/app_design/app_text_style.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/widgets/app_list_tile/parts/assigned_adult.dart';
import 'package:movie_set/widgets/app_list_tile/parts/image_list_tile.dart';
import 'package:movie_set/widgets/app_list_tile/parts/title_list_tile.dart';

import '../../model_test/app_list_tile_item.dart';
import '../main.dart';
import '../type_widgets/type_widgets.dart';

void main() {
  late MainInit mainUi;
  const double width = 200;
  const double height = 300;

  setUp(() {
    mainUi = MainInit();
  });

  testWidgets(
    'Testing Image Network',
    (tester) async {
      mockNetworkImagesFor(
        () async => await tester.pumpWidget(
          mainUi.initWidget(
            child: Image.network(
              '${Constants.originalSizeUrlImage}${firstListTileItem.imagePath}',
            ),
          ),
        ),
      );
    },
  );

  testWidgets(
    'Testing App List Tile Widget',
    (tester) async {
      await tester.pumpWidget(
        mainUi.initWidget(
          child: ImageListTile(
            imagePath: firstListTileItem.imagePath!,
            width: width,
            height: height,
          ),
        ),
      );

      final allWidgets = tester.allWidgets.whereType();
      final booleanClipRRect = TypeClipRRect.isClipRRect(
        widget: allWidgets.firstWhereOrNull((it) => it is ClipRRect),
        radius: BorderRadius.circular(8),
      );
      expect(booleanClipRRect, true);

      final booleanImageNetwork = TypeImage.isImageNetwork(
        widget: allWidgets.firstWhereOrNull((it) => it is Image),
        width: width,
        height: height,
        fit: BoxFit.cover,
      );

      expect(booleanImageNetwork, true);
    },
  );

  testWidgets(
    'Testing Assigned Adult Widget',
    (tester) async {
      await tester.pumpWidget(
        mainUi.initWidget(
          child: const Stack(
            children: [
              AssignedAdult(),
            ],
          ),
        ),
      );

      final allWidgets = tester.allWidgets.whereType();
      final booleanPositioned = TypePositioned.isPositioned(
        widget: allWidgets.firstWhereOrNull((it) => it is Positioned),
        top: 0.0,
        right: 0.0,
      );
      expect(booleanPositioned, true);

      final booleanContainer = TypeContainer.isContainer(
        widget: allWidgets.firstWhereOrNull((it) => it is Container),
        color: AppColors.othersBlack.withOpacity(.4),
        radius: const BorderRadius.only(
          topRight: Radius.circular(8),
          bottomLeft: Radius.circular(8),
        ),
      );
      expect(booleanContainer, true);

      final booleanText = TypeText.isText(
        widget: allWidgets.firstWhereOrNull((it) => it is Text),
        data: Constants.plusSixteen,
        style: AppTextStyle.s10w700.copyWith(
          color: AppColors.grayScaleWhite.withOpacity(.6),
        ),
        textAlign: TextAlign.center,
      );

      expect(booleanText, true);
    },
  );

  testWidgets(
    'Testing Title List Tile Widget',
    (tester) async {
      await tester.pumpWidget(
        mainUi.initWidget(
          child: const TitleListTile(item: firstListTileItem),
        ),
      );

      final allWidgets = tester.allWidgets.whereType();

      final booleanSizedBox = TypeSizedBox.isSizedBox(
        widget: allWidgets.firstWhereOrNull((it) => it is SizedBox),
        height: 57,
      );
      expect(booleanSizedBox, true);

      final booleanColumn = TypeColumn.isColumn(
        widget: allWidgets.firstWhereOrNull((it) => it is Column),
        crossAxisAlignment: CrossAxisAlignment.start,
        lengthChildren: 2,
      );
      expect(booleanColumn, true);

      final booleanRow = TypeRow.isRow(
        widget: allWidgets.firstWhereOrNull((it) => it is Row),
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        lengthChildren: 2,
      );
      expect(booleanRow, true);

      final booleanTextRich = TypeTextRich.isTextRich(
        widget: allWidgets.firstWhereOrNull((it) => it is RichText),
        maxLines: 2,
        style: AppTextStyle.s14w500.copyWith(
          color: AppColors.primaryLight,
        ),
        firstTextSpanText: firstListTileItem.title,
      );
      expect(booleanTextRich, true);

      final booleanPadding = TypePadding.isPadding(
        widget: allWidgets.firstWhereOrNull((it) => it is Padding),
        vertical: 0.0,
        horizontal: 4.0,
      );
      expect(booleanPadding, true);
    },
  );
}
