import 'package:flutter/material.dart';
import 'package:movie_set/app_design/app_theme.dart';

class MainInit {
  late BuildContext context;

  Widget initScreen({
    required Widget child,
  }) =>
      MaterialApp(
        theme: AppMainTheme().lightTheme,
        darkTheme: AppMainTheme().darkTheme,
        home: Builder(
          builder: (ctx) {
            context = ctx;
            return child;
          },
        ),
      );

  Widget initWidget({
    required Widget child,
  }) =>
      MaterialApp(
        theme: AppMainTheme().lightTheme,
        darkTheme: AppMainTheme().darkTheme,
        home: Scaffold(
          body: child,
        ),
      );
}


