import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
part 'parts/positioned.dart';
part 'parts/container.dart';
part 'parts/text.dart';
part 'parts/clip_r_rect.dart';
part 'parts/image.dart';
part 'parts/sized_box.dart';
part 'parts/column.dart';
part 'parts/row.dart';
part 'parts/text_rich.dart';
part 'parts/padding.dart';
class TypeWidgets {





  static bool _textStyle(TextStyle? firstStyle,
      TextStyle? secondStyle,) =>
      secondStyle != null
          ? (firstStyle?.color == secondStyle.color &&
          firstStyle?.fontFamily == secondStyle.fontFamily &&
          firstStyle?.fontSize == secondStyle.fontSize &&
          firstStyle?.fontWeight == secondStyle.fontWeight)
          : true;

}
