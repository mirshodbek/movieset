part of '../type_widgets.dart';

extension TypeColumn on TypeWidgets{

  static bool isColumn({
    required Widget? widget,
    CrossAxisAlignment? crossAxisAlignment,
    MainAxisAlignment? mainAxisAlignment,
    int? lengthChildren,
  }) {
    final column = widget is Column ? widget : null;
    final booleanCrossAxisAlignment =
    crossAxisAlignment != null ? column?.crossAxisAlignment == crossAxisAlignment : true;
    final booleanMainAxisAlignment =
    mainAxisAlignment != null ? column?.mainAxisAlignment == mainAxisAlignment : true;
    final booleanChildren =
    lengthChildren != null ? column?.children.length == lengthChildren : true;
    return booleanCrossAxisAlignment && booleanMainAxisAlignment && booleanChildren;
  }
}