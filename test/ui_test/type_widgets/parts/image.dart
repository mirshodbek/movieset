part of '../type_widgets.dart';

extension TypeImage on TypeWidgets{

  static bool isImageNetwork({
    required Widget? widget,
    double? width,
    double? height,
    BoxFit? fit,
  }) {
    final image = widget is Image ? widget : null;
    final booleanWidth = width != null ? image?.width == width : true;
    final booleanHeight = height != null ? image?.height == height : true;
    final booleanBoxFit = fit != null ? image?.fit == fit : true;
    return booleanWidth && booleanHeight && booleanBoxFit;
  }

}