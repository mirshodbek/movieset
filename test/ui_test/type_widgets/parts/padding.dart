part of '../type_widgets.dart';

extension TypePadding on TypeWidgets{

  static bool isPadding({
    required Widget? widget,
    required double vertical,
    required double horizontal,
  }) {
    final padding = widget is Padding ? widget : null;
    final booleanVertical = vertical == padding?.padding.vertical;
    final booleanHorizontal = horizontal == padding?.padding.horizontal;
    return booleanVertical && booleanHorizontal;
  }
}