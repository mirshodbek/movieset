part of '../type_widgets.dart';

extension TypeClipRRect on TypeWidgets{
  static bool isClipRRect({
    required Widget? widget,
    BorderRadius? radius,
  }) {
    final clipRRect = widget is ClipRRect ? widget : null;
    final booleanRadius = radius != null ? clipRRect?.borderRadius == radius : true;

    return booleanRadius;
  }
}