part of '../type_widgets.dart';

extension TypeSizedBox on TypeWidgets{

  static bool isSizedBox({
    required Widget? widget,
    double? width,
    double? height,
  }) {
    final sizedBox = widget is SizedBox ? widget : null;
    final booleanWidth = width != null ? sizedBox?.width == width : true;
    final booleanHeight = height != null ? sizedBox?.height == height : true;
    return booleanWidth && booleanHeight;
  }
}