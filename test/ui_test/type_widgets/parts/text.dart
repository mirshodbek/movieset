part of '../type_widgets.dart';

extension TypeText on TypeWidgets{

  static bool isText({
    required Widget? widget,
    String? data,
    TextStyle? style,
    TextAlign? textAlign,
  }) {
    final text = widget is Text ? widget : null;
    final booleanTextData = data != null ? text?.data == data : true;
    final booleanTextStyle = TypeWidgets._textStyle(text?.style, style);
    final booleanTextAlign = textAlign != null ? text?.textAlign == textAlign : true;
    return booleanTextData && booleanTextStyle && booleanTextAlign;
  }

}