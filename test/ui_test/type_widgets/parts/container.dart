part of '../type_widgets.dart';

extension TypeContainer on TypeWidgets{

  static bool isContainer({
    required Widget? widget,
    Color? color,
    BorderRadius? radius,
  }) {
    Container? container = widget is Container ? widget : null;
    BoxDecoration? asBoxDecoration =
    (container?.decoration is BoxDecoration) ? container?.decoration as BoxDecoration : null;

    BorderRadius? asBorderRadius = (asBoxDecoration?.borderRadius is BorderRadius)
        ? asBoxDecoration?.borderRadius as BorderRadius
        : null;

    final booleanColor = color != null ? asBoxDecoration?.color == color : true;
    final booleanRadius = radius != null ? asBorderRadius == radius : true;

    return booleanColor && booleanRadius;
  }

}