part of '../type_widgets.dart';

extension TypeRow on TypeWidgets{


  static bool isRow({
    required Widget? widget,
    CrossAxisAlignment? crossAxisAlignment,
    MainAxisAlignment? mainAxisAlignment,
    int? lengthChildren,
  }) {
    final row = widget is Row ? widget : null;
    final booleanCrossAxisAlignment =
    crossAxisAlignment != null ? row?.crossAxisAlignment == crossAxisAlignment : true;
    final booleanMainAxisAlignment =
    mainAxisAlignment != null ? row?.mainAxisAlignment == mainAxisAlignment : true;
    final booleanChildren = lengthChildren != null ? row?.children.length == lengthChildren : true;
    return booleanCrossAxisAlignment && booleanMainAxisAlignment && booleanChildren;
  }
}