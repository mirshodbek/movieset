part of '../type_widgets.dart';

extension TypeTextRich on TypeWidgets{

  static bool isTextRich({
    required Widget? widget,
    int? maxLines,
    TextStyle? style,
    String? firstTextSpanText,
    TextStyle? firstTextSpanStyle,
    List<String?>? otherTexts,
  }) {
    final textRich = widget is RichText ? widget : null;
    final booleanMaxLines = maxLines != null ? textRich?.maxLines == maxLines : true;
    final booleanTextStyle = TypeWidgets._textStyle(textRich?.text.style, style);
    final textSpan = textRich?.text is TextSpan ? textRich?.text as TextSpan : null;
    final firstTextSpan = textSpan?.children
        ?.whereType<TextSpan>()
        .firstOrNull;

    final booleanFirstTextSpanText =
    firstTextSpanText != null ? firstTextSpan?.text == firstTextSpanText : true;
    final booleanFirstTextSpanTextStyle =
    firstTextSpanStyle != null ? firstTextSpan?.style == firstTextSpanStyle : true;
    List<String?>? textsTextSpan = [
      for (TextSpan item in firstTextSpan?.children?.whereType<TextSpan>().toList() ?? []) item.text
    ];
    final booleanOtherTextSpanTexts =
    (otherTexts?.isNotEmpty ?? false) ? textsTextSpan.equals(otherTexts!) : true;


    return booleanMaxLines &&
        booleanTextStyle &&
        booleanFirstTextSpanText &&
        booleanOtherTextSpanTexts &&
        booleanFirstTextSpanTextStyle;
  }

}