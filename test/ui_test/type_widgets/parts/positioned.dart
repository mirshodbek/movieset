part of '../type_widgets.dart';

extension TypePositioned on TypeWidgets{
  static bool isPositioned({
    required Widget? widget,
    double? top,
    double? bottom,
    double? left,
    double? right,
  }) {
    Positioned? positioned = widget is Positioned ? widget : null;
    final booleanTop = top != null ? positioned?.top == top : true;
    final booleanBottom = bottom != null ? positioned?.bottom == bottom : true;
    final booleanLeft = left != null ? positioned?.left == left : true;
    final booleanRight = right != null ? positioned?.right == right : true;
    return booleanTop && booleanBottom && booleanLeft && booleanRight;
  }
}