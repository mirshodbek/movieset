import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:movie_set/app_design/app_gradients.dart';
import 'package:movie_set/constants/constants.dart';
import 'package:movie_set/features/init/init_screen.dart';

import '../main.dart';

void main() {
  late MainInit mainUi;

  setUp(() {
    mainUi = MainInit();
  });

  testWidgets(
    'Init Screen Widgets Test',
    (WidgetTester tester) async {
      await tester.pumpWidget(
        mainUi.initScreen(
          child: const InitScreen(runTest: true),
        ),
      );

      await tester.pumpAndSettle();

      // Testing container
      final containerWidget = find.byWidgetPredicate(
        (widget) =>
            widget is Container &&
            widget.child is Stack &&
            widget.decoration is BoxDecoration &&
            (widget.decoration as BoxDecoration).gradient is LinearGradient &&
            (widget.decoration as BoxDecoration).gradient ==
                AppGradients.splashLightGradient,
      );

      expect(containerWidget, findsOneWidget);

      // Testing all of texts
      final text = tester.allWidgets.whereType<Text>().toList();
      expect(text.length, 3);
      expect(text.first.data, '${Constants.logoMoviePartOne}.   ');
      final textTheme = Theme.of(mainUi.context).primaryTextTheme;
      final fontSize = MediaQuery.of(mainUi.context).size.height * .25;
      expect(
        text.first.style,
        textTheme.displayLarge?.copyWith(fontSize: fontSize),
      );
      expect(text[1].data, Constants.logoMoviePartTwo);
      expect(
        text[1].style,
        textTheme.displayLarge?.copyWith(fontSize: fontSize),
      );
      expect(text.last.data,
          '${Constants.logoMoviePartOne}.${Constants.logoMoviePartTwo}');
      expect(text.last.style, textTheme.displayMedium);
      expect(Theme.of(mainUi.context).brightness == Brightness.light, true);
    },
  );
}
